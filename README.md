An Embedded Hardware Description Language using Dependent Types.

This repository contains the Agda source code of the Π-Ware project,
developed as doctoral research by [João Paulo Pizani Flor at Utrecht University](http://www.cs.uu.nl/staff/pizani.html).

Here you can only find source code. To consult more documentation and context about the project, go to its [root repository](https://gitlab.com/joaopizani/piware).

To use the Π-Ware library you must follow the usual
[Agda Library Managament conventions](http://agda.readthedocs.org/en/latest/tools/package-system.html).
Namely, you must append a line containing the absolute path of the `piware.agda-lib` file into your `$HOME/.agda/libraries`.
Then, you can either pass the `-l piware` flag to the `agda` command any time you want to perform typechecking,
or you can append a line saying `piware` into your `$HOME/.agda/defaults`.

There is a module under `src` called `WholeLibrary.lagda`, which imports all other modules in the project.
You can typecheck this module as a sort of quick sanity check for the whole library.

Lastly, the tip of this branch of Π-Ware has been checked to typecheck
with the following versions of Agda and its standard library:

* Agda. Either:
  + Commit from the Git repo:
    [98ec44a0fda0b68e0e66e412ab4f06c8164f6246](https://github.com/agda/agda/tree/98ec44a0fda0b68e0e66e412ab4f06c8164f6246)
  + Stable version:  2.5.1.1

* Agda standard library. Either:
  + Commit from the Git repo:
    [659857fbaebf2b91fe363adf6fbe68893b859baa](https://github.com/agda/agda-stdlib/tree/659857fbaebf2b91fe363adf6fbe68893b859baa)
  + Stable version:  0.12
