\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Category.Functor.Extra where

open import Function.Base using (id; _∘_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)

open import Effect.Functor using (RawFunctor; Morphism)
open import Effect.Applicative using (RawApplicative) renaming (Morphism to AppMorphism)
open AppMorphism using (functorMorphism)
open RawApplicative using (rawFunctor)

open import Notation.JP.Base using (ℓ)
\end{code}


-- the identity functor
%<*Id>
\AgdaTarget{Id}
\begin{code}
Id : RawFunctor {ℓ} id
Id = record { _<$>_ = id }
\end{code}
%</Id>


%<*NaturalT-composition-vertical>
\AgdaTarget{\_∘⇓\_}
\begin{code}
_∘⇓_ :  {F′ G′ H′ : Set ℓ → Set ℓ} {F : RawFunctor F′} {G : RawFunctor G′} {H : RawFunctor H′}
        → Morphism G H → Morphism F G → Morphism F H
_∘⇓_ {F} {H} ε η = record { op = op ε ∘ op η;  op-<$> = ∘-<$> }
  where  open RawFunctor F using () renaming (_<$>_ to _<$ᶠ>_)
         open RawFunctor H using () renaming (_<$>_ to _<$ʰ>_)
         open Morphism using (op; op-<$>)

         -- naturality condition of the composition: the "rectangle made of two rectangles"
         ∘-<$> : ∀ {X Y} (f : X → Y) x → (op ε ∘ op η) (f <$ᶠ> x) ≡ (f <$ʰ> ((op ε ∘ op η) x))
         ∘-<$> f x rewrite op-<$> η f x = op-<$> ε f (op η x)
\end{code}
%</NaturalT-composition-vertical>


-- Transforms an applicative functor morphism into a regular functor morphism (nat. transformation)
%<*app-NaturalT>
\AgdaTarget{app-NaturalT}
\begin{code}
app-NaturalT  : {F′ G′ : Set ℓ → Set ℓ} {F : RawApplicative F′} {G : RawApplicative G′}
              → AppMorphism F G → Morphism (rawFunctor F) (rawFunctor G)
app-NaturalT = functorMorphism
\end{code}
%</app-NaturalT>
