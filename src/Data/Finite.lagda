\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.Finite where

open import Function.Base using (_∘_; _∘′_; id)
open import Data.Nat.Base using (_*_; suc; zero)
open import Data.Fin.Base using (Fin; remQuot; combine) renaming (zero to Fz; suc to Fs)
open import Data.Fin.Properties using (remQuot-combine; combine-remQuot)
open import Data.Product.Base using (proj₁; proj₂; _×_; _,_; uncurry) renaming (map to map×)
open import Data.Product.Relation.Binary.Pointwise.NonDependent using (≡×≡⇒≡)
open import Data.Vec.Base using (Vec; []; _∷_; tabulate; lookup)
open import Data.Vec.Properties using (tabulate-cong; tabulate∘lookup)
open import Data.Nat.Properties using (+-*-commutativeSemiring)
open import Algebra.Properties.CommutativeSemiring.Exp (+-*-commutativeSemiring) using (_^_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong₂; cong; module ≡-Reasoning; _≗_)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-∣; _∎)

open import Function.Bijection.Sets using (_InverseOf′_)

open import Data.Finite.Base public
open Finite ⦃ … ⦄ using (#α; from; to; left-inverse-of; right-inverse-of)

open import Notation.JP.Base using (m; n; α; β)
\end{code}


-- TODO: this will come from future stdlib release
\begin{code}
quotient : ∀ n → Fin (m * n) → Fin m
quotient n = proj₁ ∘ remQuot n

remainder : ∀ n → Fin (m * n) → Fin n
remainder {m} n = proj₂ ∘ remQuot {m} n

finToFun : Fin (m ^ n) → (Fin n → Fin m)
finToFun {m} {suc n} i Fz      = quotient (m ^ n) i
finToFun {m} {suc n} i (Fs j)  = finToFun (remainder {m} (m ^ n) i) j

funToFin : (Fin m → Fin n) → Fin (n ^ m)
funToFin {m = zero}   f = Fz
funToFin {m = suc _}  f = combine (f Fz) (funToFin (f ∘ Fs))
\end{code}

\begin{code}
finToFun-funToFin : (f : Fin m → Fin n) → finToFun (funToFin f) ≗ f
finToFun-funToFin {suc m′} {n} f Fz = begin
  quotient (n ^ m′) (combine (f Fz) (funToFin (f ∘ Fs)))  ≡⟨ cong proj₁ (remQuot-combine _ _) ⟩
  proj₁ (f Fz , funToFin (f ∘ Fs))                        ≡⟨⟩
  f Fz                                                    ∎
finToFun-funToFin {suc m′} {n} f (Fs i) = begin
  finToFun (remainder {n} (n ^ m′) (combine (f Fz) (funToFin (f ∘ Fs)))) i  ≡⟨ cong (λ rq → finToFun (proj₂ rq) i) (remQuot-combine {n} _ _) ⟩
  finToFun (proj₂ (f Fz , funToFin (f ∘ Fs))) i                             ≡⟨⟩
  finToFun (funToFin (f ∘ Fs)) i                                            ≡⟨ finToFun-funToFin (f ∘ Fs) i ⟩
  (f ∘ Fs) i                                                                ≡⟨⟩
  f (Fs i)                                                                  ∎
\end{code}

\begin{code}
funToFin-finToFun : funToFin {m} {n} ∘ finToFun ≗ id
funToFin-finToFun {m = zero}  {n} Fz  = refl
funToFin-finToFun {m = suc m′} {n} k   = begin
    combine (finToFun {n} {suc m′} k Fz) (funToFin (finToFun {n} {suc m′} k ∘ Fs))
  ≡⟨⟩
    combine (quotient {n} (n ^ m′) k) (funToFin (finToFun {n} {m′} (remainder {n} (n ^ m′) k)))
  ≡⟨ cong (combine (quotient {n} (n ^ m′) k)) (funToFin-finToFun {m′} (remainder {n} (n ^ m′) k)) ⟩
    combine (quotient {n} (n ^ m′) k) (remainder {n} (n ^ m′) k)
  ≡⟨⟩
    uncurry combine (remQuot {n} (n ^ m′) k)
  ≡⟨ combine-remQuot {n} (n ^ m′) k ⟩
    k
  ∎
\end{code}

\begin{code}
postulate funToFin-cong : ∀ {m n f g} → f ≗ g → funToFin {m} {n} f ≡ funToFin {m} {n} g
\end{code}



%<*from-product>
\AgdaTarget{from×}
\begin{code}
from× : ⦃ fα : Finite α ⦄ ⦃ fβ : Finite β ⦄ → Fin ((#α ⦃ fα ⦄) * (#α ⦃ fβ ⦄)) → α × β
from× ⦃ fα ⦄ ⦃ fβ ⦄ = map× (from ⦃ fα ⦄) (from ⦃ fβ ⦄) ∘′ uncombine
  where uncombine = remQuot (#α ⦃ fβ ⦄)
\end{code}
%</from-product>


%<*to-product>
\AgdaTarget{to×}
\begin{code}
to× : ⦃ fα : Finite α ⦄ ⦃ fβ : Finite β ⦄ → α × β → Fin ((#α ⦃ fα ⦄) * (#α ⦃ fβ ⦄))
to× (a , b) = combine (to a) (to b)
\end{code}
%</to-product>


%<*fromTo×L>
\AgdaTarget{fromTo×L}
\begin{code}
fromTo×L : ⦃ fα : Finite α ⦄ ⦃ fβ : Finite β ⦄ (xy : α × β) → from× (to× xy) ≡ xy
fromTo×L ⦃ fβ ⦄ (x , y) =
  let i      = remQuot (#α ⦃ fβ ⦄) (combine (to x) (to y))
      lemma  = remQuot-combine (to x) (to y)
  in begin
    (from (proj₁ i)              , from (proj₂ i))              ≡⟨ cong₂ _,_  (cong (from ∘′ proj₁) lemma)
                                                                              (cong (from ∘′ proj₂) lemma) ⟩
    (from (proj₁ (to x , to y))  , from (proj₂ (to x , to y)))  ≡⟨⟩
    (from (to x)                 , from (to y))                 ≡⟨ ≡×≡⇒≡ (left-inverse-of x , left-inverse-of y) ⟩
    (x                           , y )                          ∎
\end{code}
%</fromTo×L>

%<*fromTo×R>
\AgdaTarget{fromTo×R}
\begin{code}
fromTo×R : ⦃ fα : Finite α ⦄ ⦃ fβ : Finite β ⦄
           (ixy : Fin (#α ⦃ fα ⦄ * #α ⦃ fβ ⦄)) → to× ⦃ fα ⦄ ⦃ fβ ⦄ (from× ixy) ≡ ixy
fromTo×R ⦃ fα ⦄ ⦃ fβ ⦄ ixy =
  let unc = remQuot (#α ⦃ fβ ⦄) ixy
  in begin
    combine (to ⦃ fα ⦄ (from (proj₁ unc))) (to ⦃ fβ ⦄ (from (proj₂ unc)))  ≡⟨ cong₂ combine  (right-inverse-of ⦃ fα ⦄ (proj₁ unc))
                                                                                             (right-inverse-of ⦃ fβ ⦄ (proj₂ unc)) ⟩
    combine                  (proj₁ unc)                    (proj₂ unc)    ≡⟨⟩
    uncurry combine unc                                                    ≡⟨ combine-remQuot {#α ⦃ fα ⦄} (#α ⦃ fβ ⦄) ixy ⟩
    ixy ∎
\end{code}
%</fromTo×R>

%<*fromTo×>
\AgdaTarget{fromTo×}
\begin{code}
fromTo× : ⦃ fα : Finite α ⦄ ⦃ fβ : Finite β ⦄ → (from× ⦃ fα ⦄ ⦃ fβ ⦄) InverseOf′ (to× ⦃ fα ⦄ ⦃ fβ ⦄)
fromTo× ⦃ fα ⦄ ⦃ fβ ⦄ = record  { left-inverse-of   = fromTo×L
                               ; right-inverse-of  = fromTo×R ⦃ fα ⦄ ⦃ fβ ⦄ }
\end{code}
%</fromTo×>


%<*Finite-×>
\AgdaTarget{Finite-×}
\begin{code}
Finite-× : ⦃ fα : Finite α ⦄ ⦃ fβ : Finite β ⦄ → Finite (α × β)
Finite-× ⦃ fα ⦄ ⦃ fβ ⦄ = record  { #α       = (#α ⦃ fα ⦄) * (#α ⦃ fβ ⦄)
                                 ; mapping  = record  { to          = to×
                                                      ; from        = from×
                                                      ; inverse-of  = fromTo× } }
\end{code}
%</Finite-×>



%<*fromVec>
\AgdaTarget{fromVec}
\begin{code}
fromVec : ∀ ⦃ fα : Finite α ⦄ {n} → Fin (#α ⦃ fα ⦄ ^ n) → Vec α n
fromVec ⦃ fα ⦄ ixs = tabulate (from ⦃ fα ⦄ ∘ finToFun ixs)
\end{code}
%</fromVec>


%<*toVec>
\AgdaTarget{toVec}
\begin{code}
toVec : ∀ ⦃ fα : Finite α ⦄ {n} → Vec α n → Fin (#α ⦃ fα ⦄ ^ n)
toVec ⦃ fα ⦄ xs = funToFin (to ⦃ fα ⦄ ∘ lookup xs)
\end{code}
%</toVec>


%<*fromToVecL>
\AgdaTarget{fromToVecL}
\begin{code}
fromToVecL : ∀ ⦃ fα : Finite α ⦄ {n} (xs : Vec α n) → fromVec (toVec xs) ≡ xs
fromToVecL ⦃ fα ⦄ xs =
  begin
    tabulate (from ∘ finToFun (funToFin  (  to ∘ lookup xs)))  ≡⟨ tabulate-cong (λ i → cong from (finToFun-funToFin (to ∘ lookup xs) i)) ⟩
    tabulate (from ∘                        to ∘ lookup xs)    ≡⟨ tabulate-cong (λ i → left-inverse-of (lookup xs i)) ⟩
    tabulate (                                   lookup xs)    ≡⟨ tabulate∘lookup xs ⟩
    xs ∎
\end{code}
%</fromToVecL>

%<*fromToVecR>
\AgdaTarget{fromToVecR}
\begin{code}
fromToVecR : ∀ ⦃ fα : Finite α ⦄ {n} (ixs : Fin (#α ⦃ fα ⦄ ^ n)) → toVec ⦃ fα ⦄ {n} (fromVec ixs) ≡ ixs
fromToVecR ⦃ fα ⦄ ixs = ⊥ where postulate ⊥ : _
\end{code}
%</fromToVecR>

%<*fromToVec>
\AgdaTarget{fromToVec}
\begin{code}
fromToVec : ∀ ⦃ fα : Finite α ⦄ {n} → (fromVec ⦃ fα ⦄ {n}) InverseOf′ (toVec ⦃ fα ⦄ {n})
fromToVec ⦃ fα ⦄ {n} = record  { left-inverse-of   = fromToVecL ⦃ fα ⦄ {n}
                               ; right-inverse-of  = fromToVecR ⦃ fα ⦄ {n} }
\end{code}
%</fromToVec>


%<*Finite-Vec>
\AgdaTarget{Finite-Vec}
\begin{code}
Finite-Vec : ∀ ⦃ fα : Finite α ⦄ {n} → Finite (Vec α n)
Finite-Vec ⦃ fα ⦄ {n} = record  { #α       = #α ⦃ fα ⦄ ^ n
                                ; mapping  = record  { to          = toVec
                                                     ; from        = fromVec
                                                     ; inverse-of  = fromToVec } }
\end{code}
%</Finite-Vec>
