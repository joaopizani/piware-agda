\begin{code}
module Data.FiniteInhabited.Base where

open import Data.Finite.Base using (Finite; module Finite)

open import Notation.JP.Base using (ℓ)
\end{code}


%<*FiniteInhabited>
\begin{AgdaMultiCode}
\AgdaTarget{FiniteInhabited, finite, default}
\begin{code}
record FiniteInhabited (α : Set ℓ) : Set ℓ where
  field  finite   : Finite α
         default  : α
\end{code}
\begin{code}[hide]
  open Finite finite public
\end{code}
\end{AgdaMultiCode}
%</FiniteInhabited>
