\begin{code}
module Data.FiniteInhabited.Bool where

open import Data.Bool.Base using (Bool; false)

open import Data.FiniteInhabited.Base using (FiniteInhabited)
open import Data.Finite.Bool using (Finite-Bool)
\end{code}


\begin{code}
instance
\end{code}
%<*FiniteInhabited-Bool>
\AgdaTarget{FiniteInhabited-Bool}
\begin{code}
 FiniteInhabited-Bool : FiniteInhabited Bool
 FiniteInhabited-Bool = record  { finite   = Finite-Bool
                                ; default  = false }
\end{code}
%</FiniteInhabited-Bool>
