\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.Serializable where

open import Function.Base using (_∘′_; const)
open import Data.Unit.Base using (⊤; tt)
open import Data.Bool.Base using (Bool; if_then_else_)
open import Data.Nat.Base using (ℕ; suc; _+_; _*_; _⊔_)
open import Data.Product.Base using (_×_; _,_; uncurry′) renaming (map to map×)
open import Data.Sum.Base using (_⊎_; inj₁; inj₂; [_,_]) renaming (map to map⊎)
open import Data.Vec.Base using (Vec; []; _∷_; _++_; splitAt; concat; map)
open import Relation.Binary.PropositionalEquality.Core using (refl)
open import Relation.Nullary.Decidable.Core using (⌊_⌋)

open import Data.Vec.Extra using (group′; splitAt′)
open import Data.Vec.Padding using (padTo₁_use_; unpadFrom₁; padTo₂_use_; unpadFrom₂)

open import Notation.JP.Base using (α; β)
\end{code}

\begin{code}
private
 variable
  A : Set  -- the type of atoms
  i j : ℕ  -- sizes of vectors for serialized versions of data
\end{code}


\begin{code}
infix 0 _⇕_
\end{code}

%<*Serializable>
\AgdaTarget{\_⇕\_, \_⇓⇑\_, ⇓, ⇑}
\begin{code}
record _⇕_ (α : Set) (A : Set) {i : ℕ} : Set where
  constructor _⇓⇑_
  field  ⇓ : α → Vec A i
         ⇑ : Vec A i → α
\end{code}
%</Serializable>

\begin{code}
open _⇕_ ⦃ ... ⦄
\end{code}


\begin{code}
instance
\end{code}
%<*Serializable-Unit>
\AgdaTarget{⇕⊤}
\begin{code}
 ⇕⊤ : ⊤ ⇕ A
 ⇕⊤ = const [] ⇓⇑ const tt
\end{code}
%</Serializable-Unit>


%<*⇓×>
\AgdaTarget{⇓×}
\begin{code}
⇓× : ∀ i ⦃ α̂ : (α ⇕ A) {i} ⦄ ⦃ β̂ : (β ⇕ A) {j} ⦄ → (α × β) → Vec A (i + j)
⇓× _i = uncurry′ _++_ ∘′ map× ⇓ ⇓
\end{code}
%</⇓×>

%<*⇑×>
\AgdaTarget{⇑×}
\begin{code}
⇑× : ∀ i ⦃ α̂ : (α ⇕ A) {i} ⦄ ⦃ β̂ : (β ⇕ A) {j} ⦄ → Vec A (i + j) → (α × β)
⇑× i = map× ⇑ ⇑ ∘′ splitAt′ i
\end{code}
%</⇑×>

\begin{code}
instance
\end{code}
%<*Serializable-Product>
\AgdaTarget{⇕×}
\begin{code}
 ⇕× : ⦃ α̂ : (α ⇕ A) {i} ⦄ ⦃ β̂ : (β ⇕ A) {j} ⦄ → (α × β ⇕ A)
 ⇕× {i} = (⇓× i) ⇓⇑ (⇑× i)
\end{code}
%</Serializable-Product>


%<*⇓Vec>
\AgdaTarget{⇓Vec}
\begin{code}
⇓Vec : ∀ n ⦃ α̂ : (α ⇕ A) {i} ⦄ → Vec α n → Vec A (n * i)
⇓Vec _n = concat ∘′ map ⇓
\end{code}
%</⇓Vec>

%<*⇑Vec>
\AgdaTarget{⇑Vec}
\begin{code}
⇑Vec : ∀ n ⦃ α̂ : (α ⇕ A) {i} ⦄ → Vec A (n * i) → Vec α n
⇑Vec {i} n = map ⇑ ∘′ group′ n i
\end{code}
%</⇑Vec>

\begin{code}
instance
\end{code}
%<*Serializable-Vec>
\AgdaTarget{⇕Vec}
\begin{code}
 ⇕Vec : ∀ ⦃ α̂ : (α ⇕ A) {i} ⦄ {n} → (Vec α n) ⇕ A
 ⇕Vec {n} = (⇓Vec n) ⇓⇑ (⇑Vec n)
\end{code}
%</Serializable-Vec>


%<*untagUnpad>
\AgdaTarget{untagUnpad}
\begin{code}
untagUnpad : ∀ (l : A) (eq : A → A → Bool) → Vec A (suc (i ⊔ j)) → (Vec A i) ⊎ (Vec A j)
untagUnpad {i} {j} l eq (t ∷ ab) = (  if eq t l
                                      then inj₁ ∘′ unpadFrom₁ j
                                      else inj₂ ∘′ unpadFrom₂ i ) ab
\end{code}
%</untagUnpad>

%<*⇓⊎>
\AgdaTarget{⇓⊎}
\begin{code}
⇓⊎ : ∀ (l r p : A) ⦃ α̂ : (α ⇕ A) {i} ⦄ ⦃ β̂ : (β ⇕ A) {j} ⦄ → α ⊎ β → Vec A (suc (i ⊔ j))
⇓⊎ {i} {j} l r p =  [ (l ∷_) ∘′ padTo₁ j use p ∘′ ⇓
                    , (r ∷_) ∘′ padTo₂ i use p ∘′ ⇓ ]
\end{code}
%</⇓⊎>

%<*⇑⊎>
\AgdaTarget{⇑⊎}
\begin{code}
⇑⊎ : ∀ (l : A) (eq : A → A → Bool) ⦃ α̂ : (α ⇕ A) {i} ⦄ ⦃ β̂ : (β ⇕ A) {j} ⦄ → Vec A (suc (i ⊔ j)) → α ⊎ β
⇑⊎ l eq = map⊎ ⇑ ⇑ ∘′ (untagUnpad l eq)
\end{code}
%</⇑⊎>

\begin{code}
\end{code}
instance
%<*Serializable-Sum>
\AgdaTarget{⇕⊎}
\begin{code}
⇕⊎ : (l r p : A) (eq : A → A → Bool) ⦃ α̂ : (α ⇕ A) {i} ⦄ ⦃ β̂ : (β ⇕ A) {j} ⦄ → (α ⊎ β) ⇕ A
⇕⊎ l r p eq = (⇓⊎ l r p) ⇓⇑ (⇑⊎ l eq)
\end{code}
%</Serializable-Sum>
