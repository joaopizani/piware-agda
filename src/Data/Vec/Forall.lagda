\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.Vec.Forall where

open import Function.Base using (_∘_)
open import Data.Vec.Base using (Vec; tabulate)

open import Data.HVec using (vec↑)
open import Data.Finite using (Finite; module Finite; Finite-Vec)
open import Data.Finite.Forall using (∀-Finite)

open import Notation.JP.Base using (α; n)
\end{code}

\begin{code}
module FiniteVec n {ℓ} {α : Set ℓ} ⦃ f ⦄ = Finite (Finite-Vec {ℓ} {α} ⦃ f ⦄ {n})
\end{code}

\begin{code}
open FiniteVec using () renaming (from to fromᵥ)
\end{code}


%<*∀-Vec-Finite>
\AgdaTarget{∀-Vec-Finite}
\begin{code}
∀-Vec-Finite : {P : Vec α n → Set} ⦃ fα : Finite α ⦄ {ps : vec↑ (tabulate (P ∘ fromᵥ n))} → (∀ v → P v)
∀-Vec-Finite {P} ⦃ fα ⦄ {ps} = ∀-Finite {P = P} ⦃ Finite-Vec ⦄ {ps}
\end{code}
%</∀-Vec-Finite>
