\begin{code}
module Data.Vec.Extra where

open import Level using () renaming (suc to lsuc)
open import Function.Base using (id; _∘_; _∘′_)
open import Data.Nat.Base using (ℕ; suc; _+_; _*_)
open import Data.Product.Base using (∃₂; _×_; proj₁; proj₂) renaming (map to map×)
open import Data.Vec.Base using (Vec; splitAt; _++_; group; initLast)
open import Data.Vec.Instances using (vecFunctor)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)
open import Effect.Functor using (RawFunctor; Morphism)
open import Effect.Applicative.Indexed using (module RawIApplicative)
open RawIApplicative using (rawFunctor)

open import Notation.JP.Base using (ℓ; α; m; n)
\end{code}


%<*proj-synonyms>
\AgdaTarget{₁,₂}
\begin{code}
₁ = proj₁
₂ = proj₂
\end{code}
%</proj-synonyms>

%<*proj₂-ignore-eq>
\AgdaTarget{proj₂′}
\begin{code}
proj₂′ : {xs : Vec α (n + m)} → (∃₂ λ (ys : Vec α n) (zs : Vec α m) → xs ≡ ys ++ zs) → Vec α m
proj₂′ = proj₁ ∘ proj₂
\end{code}
%</proj₂-ignore-eq>

%<*p₂-ignore-eq>
\AgdaTarget{₂′}
\begin{code}
₂′ : {xs : Vec α (n + m)} → (∃₂ λ (ys : Vec α n) (zs : Vec α m) → xs ≡ ys ++ zs) → Vec α m
₂′ = proj₂′
\end{code}
%</p₂-ignore-eq>


%<*splitAt-noproof>
\AgdaTarget{splitAt′}
\begin{code}
splitAt′ : ∀ m {n} → Vec α (m + n) → Vec α m × Vec α n
splitAt′ m v = map× id proj₁ (splitAt m v)
\end{code}
%</splitAt-noproof>


%<*₁∘split1′>
\AgdaTarget{₁∘split1′}
\begin{code}
₁∘split1′ : Vec α (1 + n) → Vec α 1
₁∘split1′ = proj₁ ∘′ splitAt′ 1
\end{code}
%</₁∘split1′>

%<*₂∘split1′>
\AgdaTarget{₂∘split1′}
\begin{code}
₂∘split1′ : Vec α (1 + n) → Vec α n
₂∘split1′ = proj₂ ∘′ splitAt′ 1
\end{code}
%</₂∘split1′>



%<*group-ignore-eq>
\AgdaTarget{group′}
\begin{code}
group′ : ∀ n k → Vec α (n * k) → Vec (Vec α k) n
group′ n k = proj₁ ∘ group n k
\end{code}
%</group-ignore-eq>


%<*initLast-noproof>
\AgdaTarget{initLast′}
\begin{code}
initLast′ : (xs : Vec α (suc n)) → Vec α n × α
initLast′ = map× id proj₁ ∘ initLast
\end{code}
%</initLast-noproof>


%<*VecF>
\AgdaTarget{VecF}
\begin{code}
VecF : ∀ n → RawFunctor (λ (α : Set ℓ) → Vec α n)
VecF _ = vecFunctor
\end{code}
%</VecF>


%<*VecMorphism>
\AgdaTarget{VecMorphism}
\begin{code}
VecMorphism : ∀ {ℓ} → ℕ → ℕ → Set (lsuc ℓ)
VecMorphism {ℓ} m n = Morphism (VecF {ℓ} m) (VecF {ℓ} n)
\end{code}
%</VecMorphism>
