\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.Vec.Properties.Extra where

open import Function.Base using (_∘_; _$_; id)
open import Data.Nat.Base using (zero; suc; _+_; ℕ)
open import Data.Nat.Properties using (+-identityʳ)
open import Data.Fin.Base using (Fin) renaming (zero to Fz; suc to Fs)
open import Data.Product.Base using (proj₁; proj₂; _×_; _,_) renaming (map to map×)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; cong₂; cong; _≗_)
open import Data.Vec.Base using (head; Vec; splitAt; _++_; [_]; _∷_; []; tabulate; initLast; _∷ʳ_; map; replicate; zipWith)
open import Data.Vec.Properties using (∷-injective)
open import Data.Vec.Relation.Binary.Equality.Propositional
  using (_≋_; ≋-refl; ≋-sym; ≋-trans; ++-identityʳ; ≡⇒≋; ≋⇒≡) renaming ([] to []-cong; _∷_ to _∷-cong_)

open import Data.Vec.Extra using (proj₂′; ₁; ₂′; initLast′; splitAt′)

open import Notation.JP.Base using (α; β; γ; m; n; k)
\end{code}

\begin{code}
private variable o : ℕ
\end{code}


%<singleton-head>
\AgdaTarget{singleton∘head}
\begin{code}
singleton∘head : (v : Vec α 1) → ([_] ∘ head) v ≡ v
singleton∘head (_ ∷ []) = refl
\end{code}
%</singleton-head>


%<*splitAt++>
\AgdaTarget{splitAt++}
\begin{code}
splitAt++ : (v : Vec α m) (w : Vec α n) → splitAt m (v ++ w) ≡ (v , w , refl)
splitAt++ []        w = refl
splitAt++ (x ∷ xs)  w rewrite splitAt++ xs w = refl
\end{code}
%</splitAt++>

%<*proj₁∘splitAt++>
\AgdaTarget{proj₁∘splitAt++}
\begin{code}
proj₁∘splitAt++ : (v : Vec α m) (w : Vec α n) → proj₁ (splitAt m (v ++ w)) ≡ v
proj₁∘splitAt++ v w = cong proj₁ (splitAt++ v w)
\end{code}
%</proj₁∘splitAt++>

%<*proj₂′∘splitAt++>
\AgdaTarget{proj₂′∘splitAt++}
\begin{code}
proj₂′∘splitAt++ : (v : Vec α m) (w : Vec α n) → proj₂′ (splitAt m (v ++ w)) ≡ w
proj₂′∘splitAt++ v w = cong (proj₁ ∘ proj₂) (splitAt++ v w)
\end{code}
%</proj₂′∘splitAt++>


%<*proj₁∘splitAt-last>
\AgdaTarget{proj₁∘splitAt-last}
\begin{code}
proj₁∘splitAt-last : (v : Vec α (n + 0)) → proj₁ (splitAt n v) ≋ v
proj₁∘splitAt-last {n}  v           with splitAt n v
proj₁∘splitAt-last      .(v ++ [])  | v , [] , refl = ≋-sym (++-identityʳ v)
\end{code}
%</proj₁∘splitAt-last>


%<*proj₁∘splitAt-last≋>
\AgdaTarget{proj₁∘splitAt-last≋}
\begin{code}
proj₁∘splitAt-last≋ : {v : Vec α (n + 0)} {w : Vec α n} → v ≋ w → proj₁ (splitAt n v) ≋ w
proj₁∘splitAt-last≋ {v} v≋w = ≋-trans (proj₁∘splitAt-last v) v≋w
\end{code}
%</proj₁∘splitAt-last≋>


%<*≋-++-prefix>
\AgdaTarget{≋-++-prefix}
\begin{code}
≋-++-prefix : {v₁ w₁ : Vec α m} {v₂ w₂ : Vec α n} → v₁ ++ v₂ ≋ w₁ ++ w₂ → v₁ ≋ w₁
≋-++-prefix {α} {v₁ = []}     {w₁ = []}     _                   = []-cong   {A = α}
≋-++-prefix {α} {v₁ = _ ∷ _}  {w₁ = _ ∷ _}  (x≡y ∷-cong xs≋ys)  = _∷-cong_  {A = α} x≡y (≋-++-prefix xs≋ys)
\end{code}
%</≋-++-prefix>


%<*≋-++-suffix>
\AgdaTarget{≋-++-suffix}
\begin{code}
≋-++-suffix : {v₁ w₁ : Vec α m} {v₂ w₂ : Vec α n} → v₁ ++ v₂ ≋ w₁ ++ w₂ → v₂ ≋ w₂
≋-++-suffix {v₁ = []}       {w₁ = []}       v₂≋w₂               = v₂≋w₂
≋-++-suffix {v₁ = _ ∷ xs₁}  {w₁ = _ ∷ ys₁}  (x≡y ∷-cong xs≋ys)  = ≋-++-suffix {v₁ = xs₁} {ys₁} xs≋ys
\end{code}
%</≋-++-suffix>


%<*++-assoc>
\AgdaTarget{++-assoc}
\begin{code}
++-assoc : (xs : Vec α m) (ys : Vec α n) (zs : Vec α k) → (xs ++ ys) ++ zs ≋ xs ++ (ys ++ zs)
++-assoc      []         _   _   = ≋-refl
++-assoc {α}  (x ∷ xs′)  ys  zs  = _∷-cong_ {A = α} refl (++-assoc xs′ ys zs)
\end{code}
%</++-assoc>


\begin{code}
module _ where
 pattern _⹁_ l r = l , r , refl
 private ╍ = splitAt
\end{code}


%<*++-assoc-split₁>
\AgdaTarget{++-assoc-split₁}
\begin{code}
 ++-assoc-split₁ : {v : Vec α ((n + m) + o)} {w : Vec α (n + (m + o))} → v ≋ w → (₁ ∘ ╍ n ∘ ₁) (╍ (n + m) v) ≋ ₁ (╍ n w)
 ++-assoc-split₁ {n} {m} {v} {w}  v≋w with ╍ (n + m) v  | (╍ n ∘ ₁) (╍ (n + m) v)  | ╍ n w
 ++-assoc-split₁                  v≋w |    ._ ⹁ vₒ      | vₙ ⹁ vₘ                  | _ ⹁ _ =
   ≋-++-prefix $ ≋-trans (≋-sym $ ++-assoc vₙ vₘ vₒ) v≋w
\end{code}
%</++-assoc-split₁>
 
%<*++-assoc-split₂>
\AgdaTarget{++-assoc-split₂}
\begin{code}
 ++-assoc-split₂ : {v : Vec α ((n + m) + o)} {w : Vec α (n + (m + o))} → v ≋ w → (₂′ ∘ ╍ n ∘ ₁) (╍ (n + m) v) ≋ (₁ ∘ ╍ m ∘ ₂′) (╍ n w)
 ++-assoc-split₂ {n} {m} {v} {w}  v≋w with ╍ (n + m) v  | (╍ n ∘ ₁) (╍ (n + m) v)  | ╍ n w    | (╍ m ∘ ₂′) (╍ n w)
 ++-assoc-split₂                  v≋w | ._ ⹁ _          | vₙ ⹁ _                   | wₙ ⹁ ._  | wₘ ⹁ wₒ =
   ≋-++-suffix {v₁ = vₙ} {wₙ} $ ≋-++-prefix $ ≋-trans v≋w (≋-sym $ ++-assoc wₙ wₘ wₒ)
\end{code}
%</++-assoc-split₂>

%<*++-assoc-split₃>
\AgdaTarget{++-assoc-split₃}
\begin{code}
 ++-assoc-split₃ : {v : Vec α ((n + m) + o)} {w : Vec α (n + (m + o))} → v ≋ w → ₂′ (╍ (n + m) v) ≋ (₂′ ∘ ╍ m ∘ ₂′) (╍ n w)
 ++-assoc-split₃ {n} {m} {v} {w}  v≋w with ╍ (n + m) v  | ╍ n w    | ╍ m (₂′ $ ╍ n w)
 ++-assoc-split₃                  v≋w | vₙₘ ⹁ vₒ        | wₙ ⹁ ._  | wₘ ⹁ wₒ =
   ≋-++-suffix {v₁ = vₙₘ} {w₁ = wₙ ++ wₘ} $ ≋-trans v≋w (≋-sym $ ++-assoc wₙ wₘ wₒ)
\end{code}
%</++-assoc-split₃>


%<*split-++′>
\AgdaTarget{split-++′}
\begin{code}
split-++≋ : (v₁ w₁ : Vec α n) (v₂ w₂ : Vec α m) → v₁ ++ v₂ ≋ w₁ ++ w₂ → v₁ ≋ w₁ × v₂ ≋ w₂
split-++≋ {α}  []        []        _   _   v₂≋w₂               = []-cong {A = α} , v₂≋w₂
split-++≋      (_ ∷ xs)  (_ ∷ ys)  zs  ts  (_    ∷-cong rest)  with split-++≋ xs ys zs ts rest
split-++≋ {α}  (_ ∷ _)   (_ ∷ _)   _   _   (x≋y  ∷-cong rest)  | xs≋ys , zs≋ts = (_∷-cong_ {A = α} x≋y xs≋ys) , zs≋ts
\end{code}
%</split-++′>


%<*split-++>
\AgdaTarget{split-++}
\begin{code}
split-++ : (v₁ w₁ : Vec α n) (v₂ w₂ : Vec α m) → v₁ ++ v₂ ≡ w₁ ++ w₂ → v₁ ≡ w₁ × v₂ ≡ w₂
split-++ v₁ v₁′ v₂ v₂′ p = map× ≋⇒≡ ≋⇒≡ (split-++≋ v₁ v₁′ v₂ v₂′ $ ≡⇒≋ p)
\end{code}
%</split-++>


%<*concat-singleton-snoc>
\begin{code}
++-∷ʳ : (xs : Vec α n) (x : α) → xs ∷ʳ x ≋ xs ++ [ x ]
++-∷ʳ      []         _ = ≋-refl
++-∷ʳ {α}  (_ ∷ ys′)  x = _∷-cong_ {A = α} refl (++-∷ʳ ys′ x)
\end{code}
%</concat-singleton-snoc>


%<*map-cong>
\begin{code}
map-cong : (f : α → β) {xs : Vec α m} {ys : Vec α n} → xs ≋ ys → map f xs ≋ map f ys
map-cong {α} _ []-cong              = []-cong   {A = α}
map-cong {α} f (refl ∷-cong xs≋ys)  = _∷-cong_  {A = α} refl (map-cong f xs≋ys)
\end{code}
%</map-cong>


%<*tabulate-ext>
\AgdaTarget{tabulate-ext}
\begin{code}
tabulate-ext : {f g : Fin n → α} → (∀ x → f x ≡ g x) → tabulate f ≡ tabulate g
tabulate-ext {n = zero}       _ = refl
tabulate-ext {n = suc _} {g}  p rewrite p Fz = cong (g Fz ∷_) (tabulate-ext (p ∘ Fs))
\end{code}
%</tabulate-ext>


%<*drop-initLast>
\AgdaTarget{drop-initLast}
\begin{code}
drop-initLast : (x : α) (xs : Vec α (suc n)) → initLast′ (x ∷ xs) ≡ map× (x ∷_) id (initLast′ xs)
drop-initLast _ xs           with initLast xs
drop-initLast _ .(ys′ ∷ʳ y)  | ys′ , y , refl = refl
\end{code}
%</drop-initLast>


%<*snoc-injective>
\AgdaTarget{∷ʳ-injective}
\begin{code}
∷ʳ-injective : {x y : α} (xs′ ys′ : Vec α n) → (xs′ ∷ʳ x) ≡ (ys′ ∷ʳ y) → (x ≡ y) × (xs′ ≡ ys′)
∷ʳ-injective []        []         refl  = refl , refl
∷ʳ-injective (x ∷ xs′)  (y ∷ ys′)   p     with ∷-injective p
∷ʳ-injective (x ∷ xs′)  (.x ∷ ys′)  p     | refl , p′ = map× id (cong₂ _∷_ refl) (∷ʳ-injective xs′ ys′ p′)
\end{code}
%</snoc-injective>


%<*map-replicate>
\begin{code}
map-replicate : ∀ (f : α → β) n (x : α) → map f (replicate n x) ≡ replicate n (f x)
map-replicate _ zero      _ = refl
map-replicate f (suc n′)  x = cong (f x ∷_) (map-replicate f n′ x)
\end{code}
%</map-replicate>


%<*zipWith-replicate>
\begin{code}
zipWith-replicate : ∀ (f : α → β → γ) n (x : α) (y : β) → replicate n (f x y) ≡ zipWith f (replicate n x) (replicate n y)
zipWith-replicate _ zero      _ _ = refl
zipWith-replicate f (suc n′)  x y = cong (f x y ∷_) (zipWith-replicate f n′ x y)
\end{code}
%</zipWith-replicate>


%<*suc-injective>
\AgdaTarget{suc-injective}
\begin{code}
suc-injective : suc m ≡ suc n → m ≡ n
suc-injective {m = zero}    {n = zero}     refl = refl
suc-injective {m = suc m′}  {n = suc .m′}  refl = refl
suc-injective {m = zero}    {n = suc _}    ()
suc-injective {m = suc _}   {n = zero}     ()
\end{code}
%</suc-injective>


%<*comma-injective>
\AgdaTarget{,-injective}
\begin{code}
,-injective :  {q r : α × β}
               → (proj₁ q , proj₂ q) ≡ (proj₁ r , proj₂ r)
               → (proj₁ q ≡ proj₁ r) × (proj₂ q ≡ proj₂ r)
,-injective refl = refl , refl
\end{code}
%</comma-injective>


%<*append-injective>
\AgdaTarget{++-injective}
\begin{code}
++-injective : {xs₁ xs₂ : Vec α m} {ys₁ ys₂ : Vec α n} → xs₁ ++ ys₁ ≡ xs₂ ++ ys₂ → (xs₁ ≡ xs₂) × (ys₁ ≡ ys₂)
++-injective {xs₁ = []}        {xs₂ = []}                     p = refl , p
++-injective {xs₁ = _ ∷ _}     {xs₂ = _ ∷ _}                  p with ∷-injective p
++-injective {xs₁ = _ ∷ xs₁′}  {xs₂ = _  ∷ xs₂′} {ys₁} {ys₂}  _ | x≡ , xs++ys≡ with ++-injective {xs₁ = xs₁′} {xs₂′} {ys₁} {ys₂} xs++ys≡
++-injective                                                  _ | x≡ , xs++ys≡ | xs≡ , ys≡ = (cong₂ _∷_ x≡ xs≡) , ys≡
\end{code}
%</append-injective>


%<*splitAt-noproof-append-inverse>
\AgdaTarget{splitAt′-++-inverse}
\begin{code}
splitAt′-++-inverse : {xs : Vec α m} {ys : Vec α n} → splitAt′ m (xs ++ ys) ≡ (xs , ys)
splitAt′-++-inverse               {xs = []}            = refl
splitAt′-++-inverse {m = suc m′}  {xs = _ ∷ xs′} {ys}  with splitAt m′ (xs′ ++ ys)
splitAt′-++-inverse               {xs = _ ∷ xs′} {ys}  | xs″   , ys″ , p     with ++-injective {xs₁ = xs′} {xs″} {ys} {ys″} p
splitAt′-++-inverse               {xs = _ ∷ xs′} {ys}  | .xs′  , .ys , refl  | refl , refl = refl
\end{code}
%</splitAt-noproof-append-inverse>


%<*tabulate-cong>
\AgdaTarget{tabulate-cong}
\begin{code}
tabulate-cong : {f g : Fin n → α} → f ≗ g → tabulate f ≡ tabulate g
tabulate-cong {n = zero}   _    = refl
tabulate-cong {n = suc _}  f≗g  = cong₂ _∷_ (f≗g Fz) $ tabulate-cong (f≗g ∘ Fs)
\end{code}
%</tabulate-cong>
