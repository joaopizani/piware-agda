\begin{code}
module Data.List.Properties.Extra where

open import Function using (_∘_)
open import Data.List using (List; []; _∷_; zipWith; map; zip; length; unzip)
open import Data.Product using (uncurry′; _,_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; cong₂)

open import Data.Vec.Properties.Extra using (,-injective; suc-injective)
\end{code}

\begin{code}
open import Level using (Level)

private
 variable
  ℓ ℓ₁ ℓ₂ ℓ₃  : Level
  α β γ       : Set ℓ
\end{code}


%<*zipWith-map-zip>
\AgdaTarget{zipWith-map-zip}
\begin{code}
zipWith-map-zip : (f : α → β → γ) (xs : List α) (ys : List β) → zipWith f xs ys ≡ map (uncurry′ f) (zip xs ys)
zipWith-map-zip _ []         _          = refl
zipWith-map-zip _ (_ ∷ _)    []         = refl
zipWith-map-zip f (x ∷ xs′)  (y ∷ ys′)  = cong (f x y ∷_) (zipWith-map-zip f xs′ ys′)
\end{code}
%</zipWith-map-zip>


%<*unzip-zip-inverse>
\AgdaTarget{unzip-zip-inverse}
\begin{code}
unzip-zip-inverse :  (xs : List α) (ys : List β)
                     → length xs ≡ length ys → (unzip ∘ (uncurry′ zip)) ( xs , ys ) ≡ ( xs , ys )
unzip-zip-inverse []        []        _ = refl

unzip-zip-inverse []        (_ ∷ _)   ()
unzip-zip-inverse (_ ∷ _)   []        ()

unzip-zip-inverse (_ ∷ xs′)  (_ ∷ ys′)  p with ,-injective (unzip-zip-inverse xs′ ys′ (suc-injective p))
unzip-zip-inverse (x ∷ xs′)  (y ∷ ys′)  p | xs≡ , ys≡ = cong₂ _,_ (cong (x ∷_) xs≡) (cong (y ∷_) ys≡)
\end{code}
%</unzip-zip-inverse>
