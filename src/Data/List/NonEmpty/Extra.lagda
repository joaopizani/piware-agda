\begin{code}
module Data.List.NonEmpty.Extra where

open import Data.Nat.Base using (_+_)
open import Data.Vec.Base using (Vec)
open import Data.List.Base using (List; []; _∷_; reverse; _∷ʳ_; zipWith; unzip)
open import Data.List.NonEmpty using (List⁺; _∷_; _∷⁺_) renaming (map to map⁺; [_] to [_]⁺; _∷ʳ_ to _∷ʳ⁺_)
open import Data.Product.Base using (_×_; _,_; map)

open import Data.Vec.Extra using (splitAt′)

open import Notation.JP.Base using (α; β; γ)
\end{code}


%<*unzip-nonempty>
\AgdaTarget{unzip⁺}
\begin{code}
unzip⁺ : List⁺ (α × β) → List⁺ α × List⁺ β
unzip⁺ ((x , y) ∷ zs′) = map (_∷_ x) (_∷_ y) (unzip zs′)
\end{code}
%</unzip-nonempty>


%<*splitAt-nonempty>
\AgdaTarget{splitAt⁺}
\begin{code}
splitAt⁺ : ∀ m {n} → List⁺ (Vec α (m + n)) → List⁺ (Vec α m × Vec α n)
splitAt⁺ m = map⁺ (splitAt′ m)
\end{code}
%</splitAt-nonempty>


%<*uncurry-nonempty>
\AgdaTarget{uncurry⁺}
\begin{code}
uncurry⁺ : (α → List α → γ) → List⁺ α → γ
uncurry⁺ f (x ∷ xs′) = f x xs′
\end{code}
%</uncurry-nonempty>


%<*tails-nonempty-curried>
\AgdaTarget{tails⁺′}
\begin{code}
tails⁺′ : α → List α → List⁺ (List⁺ α)
tails⁺′ x []          = [ x ∷ [] ]⁺
tails⁺′ x (x′ ∷ xs″)  = let (t′ ∷ ts″) = tails⁺′ x′ xs″  in  (x ∷ x′ ∷ xs″) ∷ (t′ ∷ ts″)
\end{code}
%</tails-nonempty-curried>

-- Needs to use the trick with tails⁺′ and uncurry⁺ to "convince" the termination checker
%<*tails-nonempty>
\AgdaTarget{tails⁺}
\begin{code}
tails⁺ : List⁺ α → List⁺ (List⁺ α)
tails⁺ = uncurry⁺ tails⁺′
\end{code}
%</tails-nonempty>


%<*reverse-nonempty-prime>
\AgdaTarget{reverse⁺′}
\begin{code}
reverse⁺′ : List⁺ α → List⁺ α
reverse⁺′ (x ∷ xs′) = reverse xs′ ∷ʳ⁺ x
\end{code}
%</reverse-nonempty-prime>


%<*inits-nonempty-curried>
\AgdaTarget{inits⁺′}
\begin{code}
inits⁺′ : List α → α → List α → List⁺ (List⁺ α)
inits⁺′ acc x  []          = [ acc ∷ʳ⁺ x ]⁺
inits⁺′ acc x  (x′ ∷ xs″)  = (acc ∷ʳ⁺ x) ∷⁺ inits⁺′ (acc ∷ʳ x) x′ xs″
\end{code}
%</inits-nonempty-curried>

%<*inits-nonempty>
\AgdaTarget{inits⁺}
\begin{code}
inits⁺ : List⁺ α → List⁺ (List⁺ α)
inits⁺ (x ∷ xs′) = inits⁺′ [] x xs′
\end{code}
%</inits-nonempty>


%<*zipWith-nonempty>
\AgdaTarget{zipWith⁺}
\begin{code}
zipWith⁺ : (α → β → γ) → List⁺ α → List⁺ β → List⁺ γ
zipWith⁺ f (x ∷ xs′) (y ∷ ys′) = f x y ∷ zipWith f xs′ ys′
\end{code}
%</zipWith-nonempty>


%<*zip-nonempty>
\AgdaTarget{zip⁺}
\begin{code}
zip⁺ : List⁺ α → List⁺ β → List⁺ (α × β)
zip⁺ = zipWith⁺ _,_
\end{code}
%</zip-nonempty>
