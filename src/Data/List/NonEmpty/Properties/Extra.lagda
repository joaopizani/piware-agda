\begin{code}
module Data.List.NonEmpty.Properties.Extra where

open import Function.Base using (_∘_)
open import Data.Product.Base using (uncurry′; _,_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; cong; cong₂)
open import Data.List.NonEmpty using (List⁺; _∷_) renaming (map to map⁺; length to length⁺)

open import Data.List.Properties.Extra using (zipWith-map-zip; unzip-zip-inverse)
open import Data.List.NonEmpty.Extra using (zipWith⁺; zip⁺; unzip⁺)
open import Data.Vec.Properties.Extra using (,-injective; suc-injective)

open import Notation.JP.Base using (α; β; γ)
\end{code}


%<*zipWith+-map+-zip+>
\AgdaTarget{zipWith⁺-map⁺-zip⁺}
\begin{code}
zipWith⁺-map⁺-zip⁺ :  (f : α → β → γ) (xs : List⁺ α) (ys : List⁺ β)
                      → zipWith⁺ f xs ys ≡ map⁺ (uncurry′ f) (zip⁺ xs ys)
zipWith⁺-map⁺-zip⁺ f (x ∷ xs′) (y ∷ ys′) = cong (f x y ∷_) (zipWith-map-zip f xs′ ys′)
\end{code}
%</zipWith+-map+-zip+>


%<*unzip+-zip+-inverse>
\AgdaTarget{unzip⁺-zip⁺-inverse}
\begin{code}
unzip⁺-zip⁺-inverse :  (xs : List⁺ α) (ys : List⁺ β)
                       → length⁺ xs ≡ length⁺ ys
                       → (unzip⁺ ∘ (uncurry′ zip⁺)) ( xs , ys ) ≡ ( xs , ys )
unzip⁺-zip⁺-inverse (_ ∷ xs′)  (_ ∷ ys′)  p with ,-injective (unzip-zip-inverse xs′ ys′ (suc-injective p))
unzip⁺-zip⁺-inverse (x ∷ _)    (y ∷ _)    p | xs≡ , ys≡ = cong₂ _,_ (cong (x ∷_) xs≡) (cong (y ∷_) ys≡)
\end{code}
%</unzip+-zip+-inverse>
