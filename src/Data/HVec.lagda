\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.HVec where

open import Level using (Lift; lift; _⊔_) renaming (suc to lsuc)

open import Function.Base using (flip)
open import Data.Unit.Base using (⊤; tt)
open import Data.Product.Base using (_×_; _,_)
open import Data.Nat.Base using (zero; suc)
open import Data.Fin.Base using (Fin) renaming (zero to Fz; suc to Fs)
open import Data.Vec.Base using (Vec; []; _∷_; replicate; lookup; [_]; _++_; zipWith; map)
open import Relation.Binary.PropositionalEquality.Core using (subst)

open import Function.Extra using (fun)
open import Data.Vec.Properties.Extra using (zipWith-replicate)

open import Notation.JP.Base using (ℓ; ℓ₁; ℓ₂; α; m; n)
\end{code}


-- Type-heterogenenous vectors.
%<*vec-het>
\AgdaTarget{vec↑}
\begin{code}
vec↑ : Vec (Set ℓ) n → Set (lsuc ℓ)
vec↑ {ℓ}  []        = Lift (lsuc ℓ) ⊤
vec↑      (α ∷ αs)  = α × vec↑ αs
\end{code}
%</vec-het>


%<*head-het>
\AgdaTarget{head↑}
\begin{code}
head↑ : {αs : Vec (Set ℓ) n} → vec↑ (α ∷ αs) → α
head↑ (x , _) = x
\end{code}
%</head-het>


%<*tail-het>
\AgdaTarget{tail↑}
\begin{code}
tail↑ : {αs : Vec (Set ℓ) n} → vec↑ (α ∷ αs) → vec↑ αs
tail↑ (_ , xs) = xs
\end{code}
%</tail-het>


%<*singleton-het>
\AgdaTarget{[\_]↑}
\begin{code}
[_]↑ : α → vec↑ [ α ]
[ x ]↑ = x , lift tt
\end{code}
%</singleton-het>


%<*lookup-het>
\AgdaTarget{lookup↑}
\begin{code}
lookup↑ : {αs : Vec (Set ℓ) n} → vec↑ αs → (k : Fin n) → lookup αs k
lookup↑ {αs = []}     _          ()
lookup↑ {αs = _ ∷ _}  (x , _)    Fz      = x
lookup↑ {αs = _ ∷ _}  (_ , xs′)  (Fs k)  = lookup↑ xs′ k
\end{code}
%</lookup-het>


\begin{code}
infixr 5 _⧺↑_
\end{code}

%<*append-het>
\AgdaTarget{\_⧺↑\_}
\begin{code}
_⧺↑_ : {αs : Vec (Set ℓ) m} {βs : Vec (Set ℓ) n} → vec↑ αs → vec↑ βs → vec↑ (αs ++ βs)
_⧺↑_ {αs = []}     _          ys = ys
_⧺↑_ {αs = _ ∷ _}  (x , xs′)  ys = x , (xs′ ⧺↑ ys)
\end{code}
%</append-het>


%<*foldrFs>
\AgdaTarget{foldrFs}
\begin{code}
foldrFs : (β : Set ℓ₂) (αs : Vec (Set ℓ₁) n) → Vec (Set (ℓ₁ ⊔ ℓ₂)) n
foldrFs β αs = map (flip fun (β → β)) αs
\end{code}
%<foldrFs>


\begin{code}
infixr 3 _⊛↑_
\end{code}

%<*swierstra-star-het>
\AgdaTarget{\_⊛↑\_}
\begin{code}
_⊛↑_ : {αs βs : Vec (Set ℓ) n} → vec↑ (zipWith fun αs βs) → vec↑ αs → vec↑ βs
_⊛↑_ {αs = []}     {βs = []}     _         _         = lift tt
_⊛↑_ {αs = _ ∷ _}  {βs = _ ∷ _}  (f , fs)  (x , xs)  = f x , (fs ⊛↑ xs)
\end{code}
%</swierstra-star-het>


%<*replicate-het>
\AgdaTarget{replicate↑}
\begin{code}
replicate↑ : ∀ n {α : Set ℓ} → α → vec↑ (replicate n α)
replicate↑ zero      _ = lift tt
replicate↑ (suc n′)  x = x , replicate↑ n′ x
\end{code}
%</replicate-het>


%<*map-het>
\AgdaTarget{map↑}
\begin{code}
map↑ : {α β : Set ℓ} → (α → β) → vec↑ (replicate n α) → vec↑ (replicate n β)
map↑ {n} {α} {β} f xs = (subst vec↑ (zipWith-replicate fun n α β) (replicate↑ n f)) ⊛↑ xs
\end{code}
%</map-het>


%<*tops-het>
\AgdaTarget{⊤s}
\begin{code}
⊤s : vec↑ (replicate n ⊤)
⊤s {n} = replicate↑ n tt
\end{code}
%</tops-het>


%<*hetvectoVec>
\AgdaTarget{vec↑⇒Vec}
\begin{code}
vec↑⇒Vec : vec↑ (replicate n α) → Vec α n
vec↑⇒Vec {n = zero}     _         = []
vec↑⇒Vec {n = suc _n′}  (x , xs)  = x ∷ vec↑⇒Vec xs
\end{code}
%</hetvectoVec>
