\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.Finite.Forall where

open import Function.Base using (_∘_)
open import Data.Vec.Base using (tabulate)
open import Relation.Binary.PropositionalEquality.Core using (subst)

open import Data.HVec using (vec↑)
open import Data.Fin.Forall using (∀-Fin)
open import Data.Finite.Base using (Finite)
open Finite ⦃ … ⦄ using (left-inverse-of; from; to)

open import Notation.JP.Base using (α)
\end{code}


%<*forall-Finite-type>
\AgdaTarget{∀-Finite}
\begin{code}
∀-Finite : {P : α → Set} ⦃ fin : Finite α ⦄ {ps : vec↑ (tabulate (P ∘ from))} → (∀ x → P x)
\end{code}
%</forall-Finite-type>
\begin{code}
∀-Finite {P} {ps} x = subst  P
                             (left-inverse-of x)
                             (∀-Fin {P = P ∘ from} {ps} (to x))
\end{code}
