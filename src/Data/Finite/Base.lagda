\begin{code}
module Data.Finite.Base where

open import Data.Nat.Base using (ℕ)
open import Data.Fin.Base using (Fin)

open import Relation.Binary.Definitions using (DecidableEquality)

open import Function.Bijection.Sets using (module Inverse′; _↔′_)
open import Data.Fin.Properties.Extra using (eq?ⁿ)
\end{code}

\begin{code}
open import Level using (Level)

private
 variable
  ℓ : Level
\end{code}


%<*Finite>
\begin{AgdaMultiCode}
\AgdaTarget{Finite, #α, mapping}
\begin{code}
record Finite (α : Set ℓ) : Set ℓ where
  field  #α       : ℕ
         mapping  : α ↔′ Fin #α
\end{code}
\begin{code}[hide]
  open Inverse′ mapping public
\end{code}
\end{AgdaMultiCode}
%</Finite>

\begin{code}
  infix 4 _≟ⁿ_
\end{code}

%<*dec-eq-n>
\AgdaTarget{\_≟ⁿ\_}
\begin{code}
  _≟ⁿ_ : DecidableEquality α
  _≟ⁿ_ = eq?ⁿ injection
\end{code}
%</dec-eq-n>
