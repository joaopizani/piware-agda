\begin{code}
module Data.Finite.Bool where

open import Data.Bool.Base using (Bool; false; true)
open import Data.Fin.Base using (Fin) renaming (zero to Fz; suc to Fs)

open import Relation.Binary.PropositionalEquality.Core using (refl)

open import Function.Bijection.Sets using (_InverseOf′_; _LeftInverseOf′_; _RightInverseOf′_)
open import Data.Finite.Base using (Finite)
\end{code}


%<*patterns>
\AgdaTarget{#0; #1; #2+}
\begin{code}
pattern #0   = Fz
pattern #1   = Fs Fz
pattern #2+  = Fs (Fs ())
\end{code}
%</patterns>


%<*boolFrom>
\AgdaTarget{boolFrom}
\begin{code}
boolFrom : Fin 2 → Bool
boolFrom #0   = false
boolFrom #1   = true
boolFrom #2+
\end{code}
%</boolFrom>

%<*boolTo>
\AgdaTarget{boolTo}
\begin{code}
boolTo : Bool → Fin 2
boolTo false  = #0
boolTo true   = #1
\end{code}
%</boolTo>

%<*boolLeftInverse>
\AgdaTarget{boolLeftInverse}
\begin{code}
boolLeftInverse : boolFrom LeftInverseOf′ boolTo
boolLeftInverse true   = refl
boolLeftInverse false  = refl
\end{code}
%</boolLeftInverse>

%<*boolRightInverse>
\AgdaTarget{boolRightInverse}
\begin{code}
boolRightInverse : boolFrom RightInverseOf′ boolTo
boolRightInverse #0   = refl
boolRightInverse #1   = refl
boolRightInverse #2+
\end{code}
%</boolRightInverse>

%<*boolFrom-boolTo>
\AgdaTarget{boolFrom⇆boolTo}
\begin{code}
boolFrom⇆boolTo : boolFrom InverseOf′ boolTo
boolFrom⇆boolTo = record  { left-inverse-of   = boolLeftInverse
                          ; right-inverse-of  = boolRightInverse }
\end{code}
%</boolFrom-boolTo>


\begin{code}
instance
\end{code}
%<*Finite-Bool>
\AgdaTarget{Finite-Bool}
\begin{code}
 Finite-Bool : Finite Bool
 Finite-Bool = record  { #α       = 2
                       ; mapping  = record  { to          = boolTo
                                            ; from        = boolFrom
                                            ; inverse-of  = boolFrom⇆boolTo } }
\end{code}
%</Finite-Bool>
