\begin{code}
module Data.IVec where

open import Level using (_⊔_) renaming (suc to lsuc)

open import Data.Nat.Base using (zero; suc; _+_; ℕ)
open import Data.Product.Base using (_×_; _,_; uncurry)
open import Data.Fin.Base using (Fin) renaming (zero to Fz; suc to Fs)
open import Data.Vec.Base using (Vec; []; _∷_; lookup; [_]; _++_; replicate; zip)

open import Function.Extra using (fun₁; fun₂)

open import Notation.JP.Base using (ℓ; ℓ₁; ℓ₂; m; n)
\end{code}

\begin{code}
private variable I I₁ I₂  : Set ℓ
\end{code}


\begin{code}
infixr 30 _∷ᵢ_
\end{code}

-- Index-heterogeneous vectors
%<*VecI>
\AgdaTarget{Vecᵢ, []ᵢ, \_∷ᵢ\_}
\begin{code}
data Vecᵢ {I : Set ℓ₁} (C : I → Set ℓ₂) : ∀ {n} → Vec I n → Set (lsuc (ℓ₁ ⊔ ℓ₂)) where
  []ᵢ   :                                     Vecᵢ C []
  _∷ᵢ_  : ∀ {k i is} → C i → Vecᵢ C {k} is →  Vecᵢ C (i ∷ is)
\end{code}
%</VecI>


%<*headI>
\AgdaTarget{headᵢ}
\begin{code}
headᵢ : {C : I → Set ℓ₂} {i : I} {is : Vec I n} → Vecᵢ C (i ∷ is) → C i
headᵢ (x ∷ᵢ _) = x
\end{code}
%</headI>


%<*tailI>
\AgdaTarget{tailᵢ}
\begin{code}
tailᵢ : {C : I → Set ℓ₂} {i : I} {is : Vec I n} → Vecᵢ C (i ∷ is) → Vecᵢ C is
tailᵢ (_ ∷ᵢ xs) = xs
\end{code}
%</tailI>


%<*singletonI>
\AgdaTarget{[\_]ᵢ}
\begin{code}
[_]ᵢ : {C : I → Set ℓ₂} {i : I} → C i → Vecᵢ C [ i ]
[ x ]ᵢ = x ∷ᵢ []ᵢ
\end{code}
%</singletonI>


%<*lookupI>
\AgdaTarget{lookupᵢ}
\begin{code}
lookupᵢ : {C : I → Set ℓ₂} {is : Vec I n} → Vecᵢ C is → (k : Fin n) → C (lookup is k)
lookupᵢ (x ∷ᵢ _)   Fz      = x
lookupᵢ (_ ∷ᵢ xs)  (Fs k)  = lookupᵢ xs k
\end{code}
%</lookupI>


\begin{code}
infixr 5 _⧺ᵢ_
\end{code}

%<*appendI>
\AgdaTarget{\_⧺ᵢ\_}
\begin{code}
_⧺ᵢ_ : {C : I → Set ℓ₂} {is : Vec I m} {js : Vec I n} → Vecᵢ C {m} is → Vecᵢ C {n} js → Vecᵢ C (is ++ js)
[]ᵢ        ⧺ᵢ ys = ys
(x ∷ᵢ xs)  ⧺ᵢ ys = x ∷ᵢ (xs ⧺ᵢ ys)
\end{code}
%</appendI>


%<*swierstra-starI>
\AgdaTarget{\_⊛ᵢ\_}
\begin{code}
_⊛ᵢ_ : {C : I → Set ℓ₂} {is js : Vec I n} → Vecᵢ (uncurry (fun₁ C)) (zip is js) → Vecᵢ C is → Vecᵢ C js
_⊛ᵢ_ {is = []}     {js = []}     _           _           = []ᵢ
_⊛ᵢ_ {is = _ ∷ _}  {js = _ ∷ _}  (f ∷ᵢ fs′)  (x ∷ᵢ xs′)  = f x ∷ᵢ (fs′ ⊛ᵢ xs′)
\end{code}
%</swierstra-starI>


%<*replicateI>
\AgdaTarget{replicateᵢ}
\begin{code}
replicateᵢ : ∀ n {C : I → Set ℓ₂} {i : I} → C i → Vecᵢ C {n} (replicate n i)
replicateᵢ zero      _ = []ᵢ
replicateᵢ (suc n′)  x = x ∷ᵢ replicateᵢ n′ x
\end{code}
%</replicateI>


%<*mapI>
\AgdaTarget{mapᵢ}
\begin{code}
mapᵢ : {C : I → Set ℓ₂} {i j : I} → (C i → C j) → Vecᵢ C (replicate n i) → Vecᵢ C (replicate n j)
mapᵢ _ []ᵢ        = []ᵢ
mapᵢ f (x ∷ᵢ xs)  = f x ∷ᵢ mapᵢ f xs
\end{code}
%</mapI>


%<*VecItoVec>
\AgdaTarget{Vecᵢ⇒Vec}
\begin{code}
Vecᵢ⇒Vec : {C : I → Set ℓ₂} {i : I} → Vecᵢ C (replicate n i) → Vec (C i) n
Vecᵢ⇒Vec []ᵢ        = []
Vecᵢ⇒Vec (x ∷ᵢ xs)  = x ∷ Vecᵢ⇒Vec xs
\end{code}
%</VecItoVec>




%<*Vec2>
\AgdaTarget{Vec₂, []₂, \_∷₂\_}
\begin{code}
data Vec₂ {I₁ I₂ : Set ℓ₁} (C : I₁ → I₂ → Set ℓ₂) : ∀ {n} → Vec I₁ n → Vec I₂ n → Set (lsuc (ℓ₁ ⊔ ℓ₂)) where
  []₂   :                                               Vec₂ C [] []
  _∷₂_  : ∀ {k i j is js} → C i j → Vec₂ C {k} is js →  Vec₂ C (i ∷ is) (j ∷ js)
\end{code}
%</Vec2>


%<*head2>
\AgdaTarget{head₂}
\begin{code}
head₂ :  {C : I₁ → I₂ → Set ℓ₂} {i : I₁} {j : I₂} {is : Vec I₁ n} {js : Vec I₂ n}
         → Vec₂ C (i ∷ is) (j ∷ js) → C i j
head₂ (x ∷₂ _) = x
\end{code}
%</head2>


%<*tail2>
\AgdaTarget{tail₂}
\begin{code}
tail₂ :  {C : I₁ → I₂ → Set ℓ₂} {i : I₁} {j : I₂} {is : Vec I₁ n} {js : Vec I₂ n}
         → Vec₂ C (i ∷ is) (j ∷ js) → Vec₂ C is js
tail₂ (_ ∷₂ xs′) = xs′
\end{code}
%</tail2>


%<*singleton2>
\AgdaTarget{[\_]₂}
\begin{code}
[_]₂ : {C : I₁ → I₂ → Set ℓ₂} {i : I₁} {j : I₂} → C i j → Vec₂ C [ i ] [ j ]
[ x ]₂ = x ∷₂ []₂
\end{code}
%</singleton2>


%<*lookup2>
\AgdaTarget{lookup₂}
\begin{code}
lookup₂ :  {C : I₁ → I₂ → Set ℓ₂} {is : Vec I₁ n} {js : Vec I₂ n}
           → Vec₂ C is js → (k : Fin n) → C (lookup is k) (lookup js k)
lookup₂  (x ∷₂ _)    Fz      = x
lookup₂  (_ ∷₂ xs′)  (Fs k)  = lookup₂ xs′ k
\end{code}
%</lookup2>


\begin{code}
infixr 5 _⧺₂_
\end{code}

%<*append2>
\AgdaTarget{\_⧺₂\_}
\begin{code}
_⧺₂_ :  {C : I₁ → I₂ → Set ℓ₂} {is₁ : Vec I₁ m} {js₁ : Vec I₂ m} {is₂ : Vec I₁ n} {js₂ : Vec I₂ n}
        → Vec₂ C {m} is₁ js₁ → Vec₂ C {n} is₂ js₂ → Vec₂ C (is₁ ++ is₂) (js₁ ++ js₂)
[]₂         ⧺₂ ys = ys
(x ∷₂ xs′)  ⧺₂ ys = x ∷₂ (xs′ ⧺₂ ys)
\end{code}
%</append2>


%<*swierstra-star2>
\AgdaTarget{\_⊛₂\_}
\begin{code}
_⊛₂_ :  {C : I₁ → I₂ → Set ℓ₂} {is₁ is₂ : Vec I₁ n} {js₁ js₂ : Vec I₂ n}
        → Vec₂ (fun₂ C) (zip is₁ is₂) (zip js₁ js₂) → Vec₂ C is₁ js₁ → Vec₂ C is₂ js₂
_⊛₂_ {is₁ = []}     {is₂ = []}     {js₁ = []}     {js₂ = []}     _           _           = []₂
_⊛₂_ {is₁ = _ ∷ _}  {is₂ = _ ∷ _}  {js₁ = _ ∷ _}  {js₂ = _ ∷ _}  (f ∷₂ fs′)  (x ∷₂ xs′)  = f x ∷₂ (fs′ ⊛₂ xs′)
\end{code}
%</swierstra-star2>


%<*replicate2>
\AgdaTarget{replicate₂}
\begin{code}
replicate₂ : {C : I₁ → I₂ → Set ℓ₂} {i : I₁} {j : I₂} (n : ℕ) → C i j → Vec₂ C (replicate n i) (replicate n j)
replicate₂ zero      _ = []₂
replicate₂ (suc n′)  x = x ∷₂ replicate₂ n′ x
\end{code}
%</replicate2>


%<*map2>
\AgdaTarget{map₂}
\begin{code}
map₂ :  {C : I₁ → I₂ → Set ℓ₂} {i₁ i₂ : I₁} {j₁ j₂ : I₂}
        → (C i₁ j₁ → C i₂ j₂) → Vec₂ C (replicate n i₁) (replicate n j₁) → Vec₂ C (replicate n i₂) (replicate n j₂)
map₂ _ []₂         = []₂
map₂ f (x ∷₂ xs′)  = f x ∷₂ map₂ f xs′
\end{code}
%</mapI2>


%<*VecI2-to-Vec>
\AgdaTarget{Vec₂⇒Vec}
\begin{code}
Vec₂⇒Vec :  {C : I₁ → I₂ → Set ℓ₂} {i : I₁} {j : I₂}
            → Vec₂ C (replicate n i) (replicate n j) → Vec (C i j) n
Vec₂⇒Vec []₂         = []
Vec₂⇒Vec (x ∷₂ xs′)  = x ∷ Vec₂⇒Vec xs′
\end{code}
%</VecI2-to-Vec>
