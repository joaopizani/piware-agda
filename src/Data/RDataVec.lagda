\begin{code}
module Data.RDataVec where

open import Level using (_⊔_; Lift; lift) renaming (suc to lsuc)

open import Data.Unit.Base using (⊤; tt)
open import Data.Product.Base using (_,_)
open import Data.Nat.Base using (ℕ; suc)
open import Data.Fin.Base using (Fin) renaming (zero to Fz; suc to Fs)
open import Data.Vec.Base using (Vec; lookup; last; _++_; [_]; initLast; _∷ʳ_) renaming ([] to ε; _∷_ to _◁_)

open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
open import Relation.Nullary.Decidable.Core using (yes; no)
open import Relation.Binary.Definitions using (Decidable)

open import Notation.JP.Base using (ℓ; ℓ₁; ℓ₂; α; m; n)
\end{code}



\begin{code}
_◁?_ : {I : Set ℓ} → I → Vec I n → Set ℓ
_ ◁? ε        = Lift _ ⊤
x ◁? (y ◁ _)  = x ≡ y
\end{code}


\begin{code}
infixr 30 _∷⁼[_]_
\end{code}

\begin{code}
data Vec⁼ {I : Set ℓ₁} (C : I → I → Set ℓ₂) : ∀ {n} (is js : Vec I n) → Set (ℓ₁ ⊔ ℓ₂) where
  ε⁼       : Vec⁼ C ε ε
  _∷⁼[_]_  : ∀ {n i j} {is js : Vec I n} (x : C i j) (p : j ◁? is) → Vec⁼ C is js → Vec⁼ C (i ◁ is) (j ◁ js)
\end{code}


\begin{code}
head⁼ : ∀ {I : Set ℓ₁} {C : I → I → Set ℓ₂} {i₀ j₀} {is js : Vec I n} → Vec⁼ C (i₀ ◁ is) (j₀ ◁ js) → C i₀ j₀
head⁼ (x ∷⁼[ _ ] _) = x
\end{code}


\begin{code}
tail⁼ : ∀ {I : Set ℓ₁} {C : I → I → Set ℓ₂} {i₀ j₀} {is js : Vec I n} → Vec⁼ C (i₀ ◁ is) (j₀ ◁ js) → Vec⁼ C is js
tail⁼ (_ ∷⁼[ _ ] xs) = xs
\end{code}


\begin{code}
lookup⁼ : {I : Set ℓ₁} {C : I → I → Set ℓ₂} {is js : Vec I n} (k : Fin n) → Vec⁼ C is js → C (lookup is k) (lookup js k)
lookup⁼ Fz     (x ∷⁼[ _ ] _)  = x
lookup⁼ (Fs k) (_ ∷⁼[ _ ] xs) = lookup⁼ k xs
\end{code}


\begin{code}
[_]⁼ : ∀ {I : Set ℓ₁} {C : I → I → Set ℓ₂} {i₀ j₀} → C i₀ j₀ → Vec⁼ C [ i₀ ] [ j₀ ]
[ x ]⁼ = x ∷⁼[ _ ] ε⁼
\end{code}


\begin{code}
_⧺?_ : {I : Set ℓ} → Vec I m → Vec I n → Set ℓ
ε         ⧺? _   = Lift _ ⊤
(x ◁ xs)  ⧺? ys  = last (x ◁ xs) ◁? ys
\end{code}


\begin{code}
last∘◁ : (x : α) (xs : Vec α (suc n)) → last (x ◁ xs) ≡ last xs
last∘◁ x (y ◁ ε) = refl
last∘◁ x (y ◁ z ◁ zs)          with initLast (x ◁ y ◁ z ◁ zs)
last∘◁ x (y ◁ z ◁ .ε)          | (.x ◁ .y ◁ ε)        , .z  , refl = refl
last∘◁ x (y ◁ z ◁ .(zs ∷ʳ l))  | (.x ◁ .y ◁ .z ◁ zs)  , l   , refl = undefined
  where postulate undefined : _  -- TODO
\end{code}


\begin{code}
tail-⧺ : {I : Set ℓ} {x : I} {xs : Vec I m} {ys : Vec I n} → (x ◁ xs) ⧺? ys → xs ⧺? ys
tail-⧺ {x = _} {xs = ε}      _ = lift tt
tail-⧺ {x = x} {xs = z ◁ zs} p rewrite last∘◁ x (z ◁ zs) = p
\end{code}


\begin{code}
-- This maybe not true? Maybe a more specific version...
postulate concat-◁ : ∀ {m n ℓ} {I : Set ℓ} {x : I} {xs : Vec I m} {ys : Vec I n} → x ◁? xs → x ◁? (xs ++ ys)

--infixr 5 _⧺↑⁼_

--_⧺↑⁼_ :  ∀ {m n ℓ₁ ℓ₂} {I : Set ℓ₁} {C : I → I → Set ℓ₂} {is₁ js₁ : Vec I m} {is₂ js₂ : Vec I n}
--          → Vec⁼ C is₁ js₁ → Vec⁼ C is₂ js₂ → {p : js₁ ⧺? is₂} → Vec⁼ C (is₁ ++ is₂) (js₁ ++ js₂)
--_⧺↑⁼_ {m = 0}      {n = n′}     ε⁼ ys = ys
--_⧺↑⁼_ {m = suc m′} {n = 0}      (x ∷⁼[ x◁? ] xs) ε⁼               = {!!}
--_⧺↑⁼_ {m = suc m′} {n = suc n′} (x ∷⁼[ x◁? ] xs) (y ∷⁼[ y◁? ] ys) = {!!}
\end{code}


postulate ℂ : ℕ → ℕ → Set
postulate Gate : ∀ i o → ℂ i o

test1Vec⁼ : Vec⁼ ℂ  (1 ◁ 5 ◁ 2 ◁ ε)
                     (5 ◁ 2 ◁ 3 ◁ ε)
test1Vec⁼ = Gate 1 5 ∷⁼[ refl ] Gate 5 2 ∷⁼[ refl ] Gate 2 3 ∷⁼[ _ ] ε⁼
\end{code}
