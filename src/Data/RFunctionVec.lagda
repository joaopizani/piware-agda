\begin{code}
module Data.RFunctionVec where
\end{code}


vec↑⁼ : ∀ {n ℓ₁ ℓ₂} {I : Set ℓ₁} (C : I → I → Set ℓ₂) (d : Decidable {A = I} _≡_) (is js : Vec I n) → Set (ℓ₁ ⊔ ℓ₂)
vec↑⁼ {zero}         C d ε               ε               = Lift ⊤
vec↑⁼ {suc zero}     C d (i ◁ ε)         (j ◁ ε)         = C i j × vec↑⁼ C d ε ε
vec↑⁼ {suc (suc n)}  C d (i₀ ◁ i₁ ◁ is)  (j₀ ◁ j₁ ◁ js)  with d j₀ i₁
vec↑⁼ {suc (suc n)}  C d (i₀ ◁ i₁ ◁ is)  (j₀ ◁ j₁ ◁ js)  | yes  p = C i₀ j₀ × C i₁ j₁ × vec↑⁼ C d is js
vec↑⁼ {suc (suc n)}  C d (i₀ ◁ i₁ ◁ is)  (j₀ ◁ j₁ ◁ js)  | no   _ = Lift ⊥


head↑′ :  ∀ {n ℓ₁ ℓ₂} {I : Set ℓ₁} {C : I → I → Set ℓ₂} {i j : I} {is js : Vec I n} {d : Decidable {A = I} _≡_}
          → vec↑⁼ C d (i ◁ is) (j ◁ js) → C i j
head↑′ {zero}   {is = ε}       {js = ε}       (x , _) = x
head↑′ {suc n}  {is = i ◁ is}  {js = j ◁ js}  x = {!!}
