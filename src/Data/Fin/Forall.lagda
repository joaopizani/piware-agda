\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Data.Fin.Forall where

open import Function.Base using (id)
open import Data.Fin.Base using (Fin)
open import Data.Vec.Base using (tabulate)
open import Data.Vec.Properties using (lookup∘tabulate)
open import Relation.Binary.PropositionalEquality.Core using (subst)

open import Data.HVec using (vec↑; lookup↑)

open import Notation.JP.Base using (n)
\end{code}


%<*forall-Fin>
\AgdaTarget{∀-Fin}
\begin{code}
∀-Fin : {P : Fin n → Set} {ps : vec↑ (tabulate P)} → (∀ i → P i)
∀-Fin {P} {ps} i = subst id (lookup∘tabulate P i) (lookup↑ ps i)
\end{code}
%</forall-Fin>
