\begin{code}
module Data.Fin.Properties.Extra where

open import Data.Fin.Base using (Fin)
open import Data.Fin.Properties using (_≟_)
open import Relation.Binary.Definitions using (Decidable)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)

open import Function.Bijection.Sets using (_↣′_)
open import Relation.Nullary.Decidable.Extra using (via-injection′)

open import Notation.JP.Base using (α; n)
\end{code}


%<*eq-dec-via-injection-fin>
\AgdaTarget{eq?ⁿ}
\begin{code}
eq?ⁿ : (α ↣′ Fin n) → Decidable {A = α} _≡_
eq?ⁿ inj = via-injection′ inj _≟_
\end{code}
%</eq-dec-via-injection-fin>
