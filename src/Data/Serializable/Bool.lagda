\begin{code}
module Data.Serializable.Bool where

open import Data.Bool.Base using (Bool)
open import Data.Vec.Base using (head; [_])

open import Data.Serializable using (_⇕_; _⇓⇑_)
\end{code}


-- basic instance
\begin{code}
instance
\end{code}
%<*Serializable-Bool>
\AgdaTarget{⇕Bool}
\begin{code}
 ⇕Bool : Bool ⇕ Bool
 ⇕Bool = [_] ⇓⇑ head
\end{code}
%</Serializable-Bool>
