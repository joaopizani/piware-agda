\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module WholeLibrary where
\end{code}


\begin{code}
import Function.Bijection.Sets
import Function.Extra

import Category.Functor.Extra

import Relation.Binary.Indexed.Core.Extra
import Relation.Binary.Indexed.Extra
import Relation.Binary.Indexed.Equality.Core
import Relation.Binary.Indexed.PreorderReasoning
import Relation.Binary.Indexed.EqReasoning

import Relation.Nullary.Decidable.Extra

import Codata.Musical.Stream.Extra
import Codata.Musical.Stream.Causal
import Codata.Musical.Stream.Properties
import Codata.Musical.Stream.DistantReasoning
import Codata.Musical.Stream.Equality.FromPointwise
import Codata.Musical.Stream.Equality.FromPrefix
import Codata.Musical.Stream.Equality.NAD
import Codata.Musical.Stream.Equality.WithTrans

import Data.Fin.Forall
import Data.Fin.Properties.Extra

import Data.Finite.Base
import Data.Finite
import Data.Finite.Forall
import Data.Finite.Bool
import Data.FiniteInhabited.Base
import Data.FiniteInhabited
import Data.FiniteInhabited.Bool

import Data.HVec
import Data.IVec

import Data.List.NonEmpty.Extra
import Data.List.NonEmpty.Properties.Extra

import Data.RDataVec

import Data.Serializable
import Data.Serializable.Bool

import Data.Vec.Extra
import Data.Vec.Forall
import Data.Vec.Padding
import Data.Vec.Properties.Extra

import PiWare.Atomic
import PiWare.Atomic.Bool
import PiWare.Gates
import PiWare.Gates.BoolTrio
import PiWare.Gates.Nand
import PiWare.Circuit.Notation
import PiWare.Circuit
import PiWare.Circuit.Algebra

import PiWare.Patterns
import PiWare.Patterns.Fan
import PiWare.Patterns.Id
import PiWare.Plugs.Core
import PiWare.Plugs

import PiWare.Semantics.AreaAnalysis
import PiWare.Semantics.AreaAnalysis.Nand
import PiWare.Semantics.AreaAnalysis.BoolTrio

import PiWare.Semantics.Simulation
import PiWare.Semantics.Simulation.Compliance
import PiWare.Semantics.Simulation.Equivalence

import PiWare.Semantics.Simulation.Properties
import PiWare.Semantics.Simulation.Properties.Extension
import PiWare.Semantics.Simulation.Properties.Plugs
import PiWare.Semantics.Simulation.Semigroup.Base
import PiWare.Semantics.Simulation.Semigroup.Op2

import PiWare.Semantics.Simulation.Nand
import PiWare.Semantics.Simulation.BoolTrio

import PiWare.Semantics.Simulation.Properties.Sequential

import PiWare.Semantics.Simulation.Testing

import PiWare.Semantics.SimulationState
import PiWare.Semantics.SimulationState.Properties.Sequential

import PiWare.Samples.BoolTrioComb
import PiWare.Samples.Muxes

import PiWare.Samples.BoolTrioSeq
import PiWare.Samples.RegProperties
import PiWare.Samples.RegNRec
import PiWare.Samples.RegNRecProperties
--import PiWare.Samples.RegNPars

import PiWare.Typed.Circuit.Notation
import PiWare.Typed.Circuit
import PiWare.Typed.Plugs
import PiWare.Typed.Patterns
import PiWare.Typed.Semantics.Simulation
import PiWare.Typed.Semantics.ProofLifting
import PiWare.Typed.Samples.BoolTrioComb
import PiWare.Typed.Samples.BoolTrioSeq
import PiWare.Typed.Samples.Muxes
\end{code}
