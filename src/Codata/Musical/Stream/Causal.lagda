\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Causal where

open import Level using (_⊔_)
open import Function.Base using (const)
open import Data.List.Base using (_∷_; [])
open import Data.List.NonEmpty using (List⁺) renaming (_∷_ to _∷⁺_)
open import Data.List.NonEmpty.Extra using (tails⁺)
open import Codata.Musical.Notation using (♯_; ♭)
open import Codata.Musical.Stream using (Stream; _≈_; repeat) renaming (_∷_ to _∷ₛ_)
open import Relation.Binary.PropositionalEquality.Core using (refl)

open import Notation.JP.Base using (ℓ; ℓ₁; ℓ₂; α; β)
\end{code}


-- Causal context: present × past
%<*causal-context>
\AgdaTarget{Γᶜ}
\begin{code}
Γᶜ : (α : Set ℓ) → Set ℓ
Γᶜ = List⁺
\end{code}
%</causal-context>


-- Causal step function: causal context → next value
%<*causal-step>
\AgdaTarget{\_⇒ᶜ\_}
\begin{code}
_⇒ᶜ_ : (α : Set ℓ₁) (β : Set ℓ₂) → Set (ℓ₁ ⊔ ℓ₂)
α ⇒ᶜ β = Γᶜ α → β
\end{code}
%</causal-step>


%<*pasts>
\AgdaTarget{pasts}
\begin{code}
pasts : Γᶜ α → List⁺ (Γᶜ α)
pasts = tails⁺
\end{code}
%</pasts>


%<*run-causal-prime>
\AgdaTarget{runᶜ′}
\begin{code}
runᶜ′ : (fᶜ : α ⇒ᶜ β) (x⁰x⁻ : Γᶜ α) (x¹x⁺ : Stream α) → Stream β
runᶜ′ fᶜ (x⁰ ∷⁺ x⁻) (x¹ ∷ₛ x⁺) = fᶜ (x⁰ ∷⁺ x⁻) ∷ₛ ♯ runᶜ′ fᶜ (x¹ ∷⁺ x⁰ ∷ x⁻) (♭ x⁺)
\end{code}
%</run-causal-prime>

%<*run-causal>
\AgdaTarget{runᶜ}
\begin{code}
runᶜ : (fᶜ : α ⇒ᶜ β) → (Stream α → Stream β)
runᶜ fᶜ (x⁰ ∷ₛ x⁺) = runᶜ′ fᶜ (x⁰ ∷⁺ []) (♭ x⁺)
\end{code}
%</run-causal>


%<*runᶜ′-const>
\AgdaTarget{runᶜ′-const}
\begin{code}
runᶜ′-const : {x⁰⁻ : List⁺ α} {x⁺ : Stream α} (c : β) → runᶜ′ (const c) x⁰⁻ x⁺ ≈ repeat c
runᶜ′-const {x⁺ = _ ∷ₛ _} c = refl ∷ₛ ♯ runᶜ′-const c
\end{code}
%</runᶜ′-const>


%<*runᶜ-const>
\AgdaTarget{runᶜ-const}
\begin{code}
runᶜ-const : {xs : Stream α} (c : β) → runᶜ (const c) xs ≈ repeat c
runᶜ-const {xs = _ ∷ₛ _} = runᶜ′-const
\end{code}
%</runᶜ-const>
