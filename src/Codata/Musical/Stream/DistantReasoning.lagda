\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.DistantReasoning where

open import Data.Nat.Base using (_+_; zero; suc)
open import Data.Nat.Properties using (+-comm)
open import Data.Product.Base using (_,_; proj₂)
open import Data.Vec.Base using (Vec; _++_; _∷_; splitAt; []; [_]) renaming (drop to dropᵥ)
open import Data.Vec.Properties using (∷-injective)
open import Codata.Musical.Stream using (Stream; head; _∷_; drop; take)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; cong₂; refl)

open import Codata.Musical.Stream.Properties using (drop-∘)

open import Notation.JP.Base using (α)
\end{code}


%<*joinProofs>
\AgdaTarget{joinProofs}
\begin{code}
joinProofs :  ∀ {d n m ℓ} {α : Set ℓ} {xs : Vec α n} {ys : Vec α m} {s : Stream α}
              → take n        (drop d s)        ≡ xs
              → take m        (drop (d + n) s)  ≡ ys
              → take (n + m)  (drop d s)        ≡ xs ++ ys
joinProofs {d = zero}    {n = zero}                            refl  p2  = p2
joinProofs {d = suc d′}                            {s = _ ∷ _} p1    p2  = joinProofs {d′} p1 p2
joinProofs {d = zero}    {n = suc _} {xs = _ ∷ _}  {s = _ ∷ _} p1    _   with ∷-injective p1
joinProofs {d = zero}    {n = suc _} {xs = _ ∷ _}  {s = _ ∷ _} p1    p2  | x≡ , xs≡ = cong₂ _∷_ x≡ (joinProofs {zero} xs≡ p2)
\end{code}
%</joinProofs>


%<*selectProof⁻′>
\AgdaTarget{selectProof⁻′}
\begin{code}
selectProof⁻′ :  ∀ {m n ℓ} {α : Set ℓ} {xs : Vec α (m + n)} {s : Stream α}
                 → take (m + n) s ≡ xs
                 → take n (drop m s) ≡ dropᵥ m xs
selectProof⁻′ {m = zero}                                        p = p
selectProof⁻′ {m = suc m′} {n} {xs = x ∷ xs}           {_ ∷ _}  p with splitAt m′ xs  | selectProof⁻′ {m′} {n} (proj₂ (∷-injective p))
selectProof⁻′ {m = suc m′} {n} {xs = x ∷ .(ys ++ zs)}  {_ ∷ _}  p | ys , zs , refl    | q = q
\end{code}
%</selectProof⁻′>


%<*selectProof⁻>
\AgdaTarget{selectProof⁻}
\begin{code}
selectProof⁻ :  ∀ {d m n ℓ} {α : Set ℓ} {xs : Vec α (m + n)} {s : Stream α}
                → take (m + n) (drop d s) ≡ xs
                → take n (drop (d + m) s) ≡ dropᵥ m xs
selectProof⁻ {d} {m} {n} {α = α} {xs} {s} p rewrite +-comm d m | drop-∘ {α = α} {m} {d} {s} = selectProof⁻′ {m} {n} {s = drop d s} p
\end{code}
%</selectProof⁻>


%<*selectProof⁺>
\AgdaTarget{selectProof⁺}
\begin{code}
selectProof⁺ : ∀ {n k ℓ} {α : Set ℓ} {xs : Vec α n} {ys : Vec α k} {s : Stream α} → take (n + k) s ≡ xs ++ ys → take n s ≡ xs
selectProof⁺ {n = zero}   {xs = []}                p = refl
selectProof⁺ {n = suc _}  {xs = _ ∷ _} {s = _ ∷ _} p with ∷-injective p
selectProof⁺ {n = suc _}  {xs = _ ∷ _} {s = _ ∷ _} p | x≡ , xs≡ = cong₂ _∷_ x≡ (selectProof⁺ xs≡)
\end{code}
%</selectProof⁺>


%<*takeDrop-cong>
\AgdaTarget{takeDrop-cong}
\begin{code}
takeDrop-cong : ∀ {t d₁ d₂} {s : Stream α} → d₁ ≡ d₂ → take t (drop d₁ s) ≡ take t (drop d₂ s)
takeDrop-cong refl = refl
\end{code}
%</takeDrop-cong>


%<*take-1-head>
\AgdaTarget{take-1-head}
\begin{code}
take-1-head : {x : α} {s : Stream α} → take 1 s ≡ [ x ] → head s ≡ x
take-1-head {x = x} {.x ∷ _} refl = refl
\end{code}
%</take-1-head>
