\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Equality.WithTrans where

open import Codata.Musical.Notation using (∞; ♭; ♯_)
open import Codata.Musical.Stream using (Stream; _∷_; _≈_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)

open import Notation.JP.Base using (α)
\end{code}


\begin{code}
infix  4 _≈ₚ_
\end{code}

\begin{code}
data _≈ₚ_ {α} : Stream α → Stream α → Set where
  _∷ₚ_   : ∀ {x y xs ys}  → x ≡ y → ∞ (♭ xs ≈ₚ ♭ ys) → x ∷ xs ≈ₚ y ∷ ys
  transₚ : ∀ {s₁ s₂ s₃}   → s₁ ≈ₚ s₂ → s₂ ≈ₚ s₃ → s₁ ≈ₚ s₃
\end{code}


%<*reflₚ>
\AgdaTarget{reflₚ}
\begin{code}
reflₚ : {s : Stream α} → s ≈ₚ s
reflₚ {s = _ ∷ _} = refl ∷ₚ ♯ reflₚ
\end{code}
%</reflₚ>


\begin{code}
infix 4 _≈ₕ_
\end{code}

\begin{code}
data _≈ₕ_ {α} : Stream α → Stream α → Set where
  _∷ₕ_ : ∀ {x y xs ys} → x ≡ y → ♭ xs ≈ₚ ♭ ys → x ∷ xs ≈ₕ y ∷ ys
\end{code}


%<*reflₕ>
\AgdaTarget{reflₕ}
\begin{code}
reflₕ : {xs : Stream α} → xs ≈ₕ xs
reflₕ {xs = _ ∷ _} = refl ∷ₕ reflₚ
\end{code}
%</reflₕ>


%<*transₕ>
\AgdaTarget{transₕ}
\begin{code}
transₕ : {s₁ s₂ s₃ : Stream α} → s₁ ≈ₕ s₂ → s₂ ≈ₕ s₃ → s₁ ≈ₕ s₃
transₕ {s₁ = x ∷ _} {s₂ = .x ∷ _} (refl ∷ₕ s₁≈ₚs₂) (refl ∷ₕ s₂≈ₚs₃) = refl ∷ₕ (transₚ s₁≈ₚs₂ s₂≈ₚs₃)
\end{code}
%</transₕ>


%<*whnf>
\AgdaTarget{whnf}
\begin{code}
whnf : {s₁ s₂ : Stream α} → s₁ ≈ₚ s₂ → s₁ ≈ₕ s₂
whnf (x≡y ∷ₚ xs≈ₚys)         = x≡y ∷ₕ (♭ xs≈ₚys)
whnf (transₚ s₁≈ₚs₂ s₂≈ₚs₃)  = transₕ (whnf s₁≈ₚs₂) (whnf s₂≈ₚs₃)
\end{code}
%</whnf>


%<*soundₕ-soundₚ-types>
\AgdaTarget{soundₕ, soundₚ}
\begin{code}
soundₕ : {s₁ s₂ : Stream α} → s₁ ≈ₕ s₂ → s₁ ≈ s₂
soundₚ : {s₁ s₂ : Stream α} → s₁ ≈ₚ s₂ → s₁ ≈ s₂
\end{code}
%</soundₕ-soundₚ-types>

%<*soundₕ-soundₚ-defs>
\begin{code}
soundₕ (x≡y ∷ₕ xs≈ₚys) = x≡y ∷ ♯ (soundₚ xs≈ₚys)
soundₚ s₁≈ₚs₂ = soundₕ (whnf s₁≈ₚs₂)
\end{code}
%</soundₕ-soundₚ-defs>


%<*≈-to-≈ₚ>
\AgdaTarget{≈-to-≈ₚ}
\begin{code}
≈-to-≈ₚ : {s₁ s₂ : Stream α} → s₁ ≈ s₂ → s₁ ≈ₚ s₂
≈-to-≈ₚ (refl ∷ xs′≈) = refl ∷ₚ ♯ (≈-to-≈ₚ (♭ xs′≈))
\end{code}
%</≈-to-≈ₚ>


%<*≈ₚ-to-≈>
\AgdaTarget{≈ₚ-to-≈}
\begin{code}
≈ₚ-to-≈ : {s₁ s₂ : Stream α} → s₁ ≈ₚ s₂ → s₁ ≈ s₂
≈ₚ-to-≈ = soundₚ
\end{code}
%</≈ₚ-to-≈>
