\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Equality.FromPrefix where

open import Function.Base using (_∘_)
open import Data.Nat.Base using (suc; zero)
open import Data.Vec.Base using (Vec; _∷_)
open import Codata.Musical.Notation using (∞; ♭; ♯_)
open import Codata.Musical.Stream using (Stream; take; _≈_; _∷_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)

open import Notation.JP.Base using (α; n)
\end{code}


%<*vtail-cong>
\AgdaTarget{vtail-cong}
\begin{code}
vtail-cong : ∀ {x y} {xs′ ys′ : Vec α n} → (x ∷ xs′) ≡ (y ∷ ys′) → xs′ ≡ ys′
vtail-cong refl = refl
\end{code}
%</vtail-cong>

%<*_∷-cong_>
\AgdaTarget{\_∷-cong\_}
\begin{code}
_∷-cong_ : ∀ {x y} {xs′ ys′ : Vec α n} → x ≡ y → xs′ ≡ ys′ → (x ∷ xs′) ≡ (y ∷ ys′)
_∷-cong_ refl refl = refl
\end{code}
%</_∷-cong_>


%<*take-tail≡>
\AgdaTarget{take-tail≡}
\begin{code}
take-tail≡ :  ∀ {x y} (xs′ ys′ : ∞ (Stream α))
              → take (suc  n)  (x ∷  xs′) ≡ take (suc  n)  (y ∷  ys′)
              → take       n   (♭    xs′) ≡ take       n   (♭    ys′)
take-tail≡ _ _ p = vtail-cong p
\end{code}
%</take-tail≡>


%<*take-head≡>
\AgdaTarget{take-head≡}
\begin{code}
take-head≡ :  ∀ {x y} (xs′ ys′ : ∞ (Stream α))
              → (∀ n → take n (x ∷ xs′) ≡ take n (y ∷ ys′))
              → x ≡ y
take-head≡ _ _ p with p 1
take-head≡ _ _ p | refl = refl
\end{code}
%</take-head≡>


%<*take≗⇒≈>
\AgdaTarget{take≗⇒≈}
\begin{code}
take≗⇒≈ : (xs ys : Stream α) → (∀ n → take n xs ≡ take n ys) → xs ≈ ys
take≗⇒≈ (_ ∷ xs) (_ ∷ ys) p = take-head≡ _ _ p ∷ ♯ take≗⇒≈ (♭ xs) (♭ ys) (take-tail≡ xs ys ∘ (p ∘ suc))
\end{code}
%</take≗⇒≈>


%<*≈⇒take≗>
\AgdaTarget{≈⇒take≗}
\begin{code}
≈⇒take≗ : {xs ys : Stream α} → xs ≈ ys → (∀ n → take n xs ≡ take n ys)
≈⇒take≗ (_ ∷ _)        zero     = refl
≈⇒take≗ (x≡y ∷ xs≈ys)  (suc n)  = x≡y ∷-cong (≈⇒take≗ (♭ xs≈ys) n)
\end{code}
%</≈⇒take≗>
