\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Equality.FromPointwise where

open import Function.Base using (_∘_)
open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Nat.Base using (suc; zero)
open import Codata.Musical.Stream using (Stream; lookup; _≈_; _∷_)
open import Relation.Binary.PropositionalEquality.Core using (_≗_)

open import Notation.JP.Base using (α)
\end{code}


%<*lookup≗⇒≈>
\AgdaTarget{lookup≗⇒≈}
\begin{code}
lookup≗⇒≈ : (xs ys : Stream α) → (lookup xs ≗ lookup ys) → xs ≈ ys
lookup≗⇒≈ (x ∷ xs′) (y ∷ ys′) p = p 0 ∷ ♯ lookup≗⇒≈ (♭ xs′) (♭ ys′) (p ∘ suc)
\end{code}
%</lookup≗⇒≈>


%<*≈⇒lookup≗>
\AgdaTarget{≈⇒lookup≗}
\begin{code}
≈⇒lookup≗ : (xs ys : Stream α) → xs ≈ ys → lookup xs ≗ lookup ys
≈⇒lookup≗ (_ ∷ _)    (_ ∷ _)    (x≡y ∷ _)      zero      = x≡y
≈⇒lookup≗ (_ ∷ xs′)  (_ ∷ ys′)  (_ ∷ xs′≈ys′)  (suc n′)  = ≈⇒lookup≗ (♭ xs′) (♭ ys′) (♭ xs′≈ys′) n′
\end{code}
%</≈⇒lookup≗>
