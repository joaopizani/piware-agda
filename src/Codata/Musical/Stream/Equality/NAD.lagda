\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Equality.NAD where

open import Codata.Musical.Notation using (∞; ♭; ♯_)
open import Codata.Musical.Stream using (Stream; _∷_; _≈_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)

open import Notation.JP.Base using (α)
\end{code}


\begin{code}
infix  4 _≈ₚ_
\end{code}

%<*_≈ₚ_>
\AgdaTarget{\_≈ₚ\_}
\begin{code}
data _≈ₚ_ {α} : Stream α → Stream α → Set where
  _∷ₚ_    : ∀ {xs ys} x   → ∞ (♭ xs ≈ₚ ♭ ys)     →  x ∷ xs  ≈ₚ x ∷ ys
  transₚ  : ∀ {s₁ s₂ s₃}  → s₁ ≈ₚ s₂ → s₂ ≈ₚ s₃  →  s₁      ≈ₚ s₃
  reflₚ   : ∀ {s}         →                         s       ≈ₚ s
\end{code}
%</_≈ₚ_>


\begin{code}
infix 4 _≈ₕ_
\end{code}

%<*_≈ₕ_>
\AgdaTarget{\_≈ₕ\_}
\begin{code}
data _≈ₕ_ {α} : Stream α → Stream α → Set where
  _∷ₕ_ : ∀ {xs ys} x → ♭ xs ≈ₚ ♭ ys → x ∷ xs ≈ₕ x ∷ ys
\end{code}
%</_≈ₕ_>


%<*reflₕ>
\AgdaTarget{reflₕ}
\begin{code}
reflₕ : {s : Stream α} → s ≈ₕ s
reflₕ {s = x ∷ xs′} = x ∷ₕ reflₚ {s = ♭ xs′}
\end{code}
%</reflₕ>


%<*transₕ>
\AgdaTarget{transₕ}
\begin{code}
transₕ : {s₁ s₂ s₃ : Stream α} → s₁ ≈ₕ s₂ → s₂ ≈ₕ s₃ → s₁ ≈ₕ s₃
transₕ {s₁ = x ∷ xs′} {s₃ = .x ∷ zs′} (.x ∷ₕ s₁≈s₂) (.x ∷ₕ s₂≈s₃) = x ∷ₕ transₚ s₁≈s₂ s₂≈s₃
\end{code}
%</transₕ>


%<*whnf>
\AgdaTarget{whnf}
\begin{code}
whnf : {s₁ s₂ : Stream α} → s₁ ≈ₚ s₂ → s₁ ≈ₕ s₂
whnf (x ∷ₚ s₁≈ₚs₂)           = x ∷ₕ (♭ s₁≈ₚs₂)
whnf (transₚ s₁≈ₚs₂ s₂≈ₚs₃)  = transₕ (whnf s₁≈ₚs₂) (whnf s₂≈ₚs₃)
whnf reflₚ                   = reflₕ
\end{code}
%</whnf>


%<*soundₕ-soundₚ>
\AgdaTarget{soundₕ, soundₚ}
\begin{code}
soundₕ : {s₁ s₂ : Stream α} → s₁ ≈ₕ s₂ → s₁ ≈ s₂
soundₚ : {s₁ s₂ : Stream α} → s₁ ≈ₚ s₂ → s₁ ≈ s₂

soundₕ (x ∷ₕ s₁≈ₚs₂)  = refl ∷ ♯ (soundₚ s₁≈ₚs₂)
soundₚ xs≈ys          = soundₕ (whnf xs≈ys)
\end{code}
%</soundₕ-soundₚ>


%<*≈-to-≈ₚ>
\AgdaTarget{≈-to-≈ₚ}
\begin{code}
≈-to-≈ₚ : {s₁ s₂ : Stream α} → s₁ ≈ s₂ → s₁ ≈ₚ s₂
≈-to-≈ₚ {s₁ = x ∷ xs′} (refl ∷ xs≈) = x ∷ₚ ♯ ≈-to-≈ₚ (♭ xs≈)
\end{code}
%</≈-to-≈ₚ>


%<*≈ₚ-to-≈>
\AgdaTarget{≈ₚ-to-≈}
\begin{code}
≈ₚ-to-≈ : {xs ys : Stream α} → xs ≈ₚ ys → xs ≈ ys
≈ₚ-to-≈ = soundₚ
\end{code}
%</≈ₚ-to-≈>
