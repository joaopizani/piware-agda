\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Properties where

open import Function.Base using (_⟨_⟩_; _∘_; _$_; id)
open import Algebra.Definitions using (Involutive)
open import Algebra.Bundles using (Monoid)
open import Data.Nat.Base using (zero; suc; _+_)
open import Data.Nat.Properties using (+-identityʳ; +-comm)
open import Data.Vec.Base using (_∷_)
open import Data.List.Base using (List; _∷ʳ_; _++_; []; _∷_; map; reverse; [_])
open import Relation.Binary.Bundles using (Setoid)
open import Relation.Binary.PropositionalEquality using (_≡_; _≗_; refl; cong; cong₂; subst₂; trans; sym; module ≡-Reasoning)
open import Codata.Musical.Notation using (♯_; ♭)
open import Codata.Musical.Stream
  using (Stream; _∷_; lookup; take; drop; tail; _≈_; repeat)
  renaming (map to mapₛ; head to headₛ; setoid to setoidₛ)
open import Data.List.Properties
  using (map-cong; map-++; reverse-map; unfold-reverse; reverse-++; reverse-involutive; ++-monoid)
  renaming (map-id to map-idₗ; map-∘ to map-∘ₗ)
open import Data.List.NonEmpty
  using (List⁺; toList; _∷_; _∷⁺_; _⁺∷ʳ_; _∷ʳ′_; _⁺++⁺_; snocView) renaming (head to head⁺; tail to tail⁺; map to map⁺; _∷ʳ_ to _∷⁺ʳ_)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-⟨; step-≡-∣; _∎)

open import Data.List.NonEmpty.Extra using (inits⁺′; inits⁺; reverse⁺′; tails⁺)
open import Codata.Musical.Stream.Extra using (take⁺) renaming (inits⁺′ to initsₛ′; inits⁺ to initsₛ)

open import Notation.JP.Base using (ℓ; α; β; γ; m; n)
\end{code}


%<*Stream-Setoid-EqReasoning>
\begin{code}
module Setoidₛ {ℓ} {α : Set ℓ} = Setoid (setoidₛ α)
open Setoidₛ using () renaming (refl to reflₛ; trans to transₛ)
\end{code}
%</Stream-Setoid-EqReasoning>

\begin{code}
import Relation.Binary.Reasoning.Setoid as EqReasoning
module EqReasoningₛ {ℓ} {α : Set ℓ} = EqReasoning (setoidₛ α)
\end{code}


%<*≡-to-≈>
\AgdaTarget{≡-to-≈}
\begin{code}
≡-to-≈ : {s₁ s₂ : Stream α} → s₁ ≡ s₂ → s₁ ≈ s₂
≡-to-≈ p = subst₂ _≈_ refl p reflₛ
\end{code}
%</≡-to-≈>


%<*map-id>
\AgdaTarget{map-id}
\begin{code}
map-id : {s : Stream α} → mapₛ id s ≈ s
map-id {s = _ ∷ _} = refl ∷ ♯ map-id
\end{code}
%</map-id>


%<*map-compose>
\AgdaTarget{map-∘}
\begin{code}
map-∘ : {f : α → β} {g : β → γ} {s : Stream α} → mapₛ (g ∘ f) s ≈ (mapₛ g ∘ mapₛ f) s
map-∘ {s = _ ∷ _} = refl ∷ ♯ map-∘
\end{code}
%</map-compose>


%<*map-cong-fun>
\AgdaTarget{map-cong-fun}
\begin{code}
map-cong-fun : {f g : α → β} {s : Stream α} → (∀ x → f x ≡ g x) → mapₛ f s ≈ mapₛ g s
map-cong-fun {s = x ∷ _} p = (p x) ∷ ♯ map-cong-fun p
\end{code}
%</map-cong-fun>


%<*take-cong>
\AgdaTarget{take-cong}
\begin{code}
take-cong : {xs ys : Stream α} → xs ≈ ys → take n xs ≡ take n ys
take-cong {n = zero}                        p           = refl
take-cong {n = suc _} {xs = _ ∷ _} {_ ∷ _}  (x≡ ∷ xs≈)  = cong₂ _∷_ x≡ (take-cong (♭ xs≈))
\end{code}
%</take-cong>


%<*drop-cong>
\AgdaTarget{drop-cong}
\begin{code}
drop-cong : {xs ys : Stream α} → xs ≈ ys → drop n xs ≈ drop n ys
drop-cong {n = zero}                         p          = p
drop-cong {n = suc n′} {xs = _ ∷ _} {_ ∷ _}  (_ ∷ xs≈)  = drop-cong {n = n′} (♭ xs≈)
\end{code}
%</drop-cong>


%<*map-repeat>
\AgdaTarget{map-repeat}
\begin{code}
map-repeat : {f : α → β} {x : α} → mapₛ f (repeat x) ≈ repeat (f x)
map-repeat = refl ∷ ♯ map-repeat
\end{code}
%</map-repeat>


%<*drop-repeat>
\AgdaTarget{drop-repeat}
\begin{code}
drop-repeat : {x : α} → drop n (repeat x) ≈ repeat x
drop-repeat {n = zero}    = reflₛ
drop-repeat {n = suc n′}  = drop-repeat {n = n′}
\end{code}
%</drop-repeat>


%<*id-comm>
\AgdaTarget{id-comm}
\begin{code}
id-comm : {f : Stream α → Stream β} {s : Stream α} → (f ∘ id) s ≈ (id ∘ f) s
id-comm {s = _ ∷ _} = reflₛ
\end{code}
%</id-comm>


%<*tail-map>
\AgdaTarget{tail-map}
\begin{code}
tail-map : {f : α → β} {s : Stream α} → (tail ∘ mapₛ f) s ≈ (mapₛ f ∘ tail) s
tail-map {s = _ ∷ _} = reflₛ
\end{code}
%</tail-map>


%<*drop-compose>
\AgdaTarget{drop-∘}
\begin{code}
drop-∘ : {s : Stream α} → drop (n + m) s ≡ (drop n ∘ drop m) s
drop-∘ {n = zero}                                                                          = refl
drop-∘ {n = suc n′} {m = zero}                  rewrite +-identityʳ n′                     = refl
drop-∘ {n = suc n′} {m = suc m′}  {s = _ ∷ xs}  rewrite +-comm n′ (suc m′) | +-comm m′ n′  = drop-∘ {n = suc n′} {m′} {♭ xs}
\end{code}
%</drop-compose>


%<*inits⁺′-take⁺-inits⁺′>
\AgdaTarget{inits⁺′-take⁺-inits⁺′}
\begin{code}
inits⁺′-take⁺-inits⁺′ :  ∀ {s : Stream α} {acc}
                         → inits⁺′ (toList acc) (head⁺ (take⁺ n s)) (tail⁺ (take⁺ n s)) ≡ take⁺ n (initsₛ′ acc s)
inits⁺′-take⁺-inits⁺′ {n = zero}    {_ ∷ _}    = refl
inits⁺′-take⁺-inits⁺′ {n = suc n′}  {_ ∷ xs′}  = cong₂ _∷_ refl (cong toList (inits⁺′-take⁺-inits⁺′ {n = n′} {♭ xs′}))
\end{code}
%</inits⁺′-take⁺-inits⁺′>


%<*inits⁺-take⁺-inits⁺>
\AgdaTarget{inits⁺-take⁺-inits⁺}
\begin{code}
inits⁺-take⁺-inits⁺ : {s : Stream α} → inits⁺ (take⁺ n s) ≡ take⁺ n (initsₛ s)
inits⁺-take⁺-inits⁺  {n = zero}   {s = _ ∷ _}    = refl
inits⁺-take⁺-inits⁺  {n = suc n′} {s = _ ∷ xs′}  = cong₂ _∷_ refl (cong toList (inits⁺′-take⁺-inits⁺′ {n = n′} {s = ♭ xs′}))
\end{code}
%</inits⁺-take⁺-inits⁺>


%<*lemma-massage-∷ʳ-++-toList>
\AgdaTarget{lemma-massage-∷ʳ-++-toList}
\begin{code}
lemma-massage-∷ʳ-++-toList : {xs : List α} {y : α} {zs : List⁺ α} → (xs ∷ʳ y) ++ (toList zs) ≡ xs ++ toList (y ∷⁺ zs)
lemma-massage-∷ʳ-++-toList {xs = []}       = refl
lemma-massage-∷ʳ-++-toList {xs = _ ∷ xs′}  = cong₂ _∷_ refl (lemma-massage-∷ʳ-++-toList {xs = xs′})
\end{code}
%</lemma-massage-∷ʳ-++-toList>


%<*lemma-massage-⁺∷ʳ-⁺++⁺>
\AgdaTarget{lemma-massage-⁺∷ʳ-⁺++⁺}
\begin{code}
lemma-massage-⁺∷ʳ-⁺++⁺ : {xs : List⁺ α} {y : α} {zs : List⁺ α} → (xs ⁺∷ʳ y) ⁺++⁺ zs ≡ xs ⁺++⁺ y ∷⁺ zs
lemma-massage-⁺∷ʳ-⁺++⁺ {xs = _ ∷ []}           = refl
lemma-massage-⁺∷ʳ-⁺++⁺ {xs = _ ∷ _ ∷ xs″} {y}  = cong₂  _∷_
                                                        refl
                                                        (cong₂ _∷_ refl (lemma-massage-∷ʳ-++-toList {xs = xs″} {y}))
\end{code}
%</lemma-massage-⁺∷ʳ-⁺++⁺>


%<*lookup-inits⁺′-take⁺>
\AgdaTarget{lookup-inits⁺′-take⁺}
\begin{code}
lookup-inits⁺′-take⁺ : ∀ {s : Stream α} {acc} → lookup (initsₛ′ acc s) n ≡ acc ⁺++⁺ take⁺ n s
lookup-inits⁺′-take⁺ {n = zero}    {_ ∷ _}          = refl
lookup-inits⁺′-take⁺ {n = suc n′}  {x ∷ xs′} {acc}  = begin
  lookup (initsₛ′ acc (x ∷ xs′)) (suc n′)      ≡⟨ lookup-inits⁺′-take⁺ {n = n′} {♭ xs′} ⟩
  (acc ⁺∷ʳ x)  ⁺++⁺ take⁺ n′        (♭ xs′)    ≡⟨ lemma-massage-⁺∷ʳ-⁺++⁺ ⟩
  acc          ⁺++⁺ take⁺ (suc n′)  (x ∷ xs′)  ∎
\end{code}
%</lookup-inits⁺′-take⁺>


%<*lookup-inits⁺-take⁺>
\AgdaTarget{lookup-inits⁺-take⁺}
\begin{code}
lookup-inits⁺-take⁺ : {s : Stream α} → lookup (initsₛ s) n ≡ take⁺ n s
lookup-inits⁺-take⁺ {n = zero}    {_ ∷ _}    = refl
lookup-inits⁺-take⁺ {n = suc n′}  {_ ∷ xs′}  = lookup-inits⁺′-take⁺ {n = n′} {♭ xs′}
\end{code}
%</lookup-inits⁺-take⁺>


%<*drop-tail>
\AgdaTarget{drop-tail}
\begin{code}
drop-tail : {s : Stream α} → drop (suc n) s ≡ tail (drop n s)
drop-tail {n = zero}    {_ ∷ _}    = refl
drop-tail {n = suc n′}  {_ ∷ xs′}  = drop-tail {s = ♭ xs′}
\end{code}
%</drop-tail>


%<*lemma-lookup-drop>
\AgdaTarget{lemma-lookup-drop}
\begin{code}
lemma-lookup-drop : {s : Stream α} → lookup s n ≡ headₛ (drop n s)
lemma-lookup-drop {n = zero}    {_ ∷ _}    = refl
lemma-lookup-drop {n = suc n′}  {_ ∷ xs′}  = lemma-lookup-drop {n = n′} {♭ xs′}
\end{code}
%</lemma-lookup-drop>


%<*lemma-lookup-map>
\AgdaTarget{lemma-lookup-map}
\begin{code}
lemma-lookup-map : {s : Stream α} {f : α → β} → lookup (mapₛ f s) n ≡ f (lookup s n)
lemma-lookup-map  {n = zero}    {_ ∷ _}    = refl
lemma-lookup-map  {n = suc n′}  {_ ∷ xs′}  = lemma-lookup-map {n = n′} {♭ xs′}
\end{code}
%</lemma-lookup-map>


%<*take⁺-cong>
\AgdaTarget{take⁺-cong}
\begin{code}
take⁺-cong : {s₁ s₂ : Stream α} → s₁ ≈ s₂ → take⁺ n s₁ ≡ take⁺ n s₂
take⁺-cong {n = zero}    {x ∷ _} {.x ∷ _}  (refl ∷ _)  = refl
take⁺-cong {n = suc n′}  {_ ∷ _} {_ ∷ _}   (x≡ ∷ xs≈)  = cong₂ _∷_ x≡ (cong toList (take⁺-cong {n = n′} (♭ xs≈)))
\end{code}
%</take⁺-cong>


%<*map⁺-take⁺-mapₛ>
\AgdaTarget{map⁺-take⁺-mapₛ}
\begin{code}
map⁺-take⁺-mapₛ : {f : α → β} {s : Stream α} → (map⁺ f ∘ take⁺ n) s ≡ (take⁺ n ∘ mapₛ f) s
map⁺-take⁺-mapₛ {n = zero}    {_} {_ ∷ _}    = refl
map⁺-take⁺-mapₛ {n = suc n′}  {f} {x ∷ xs′}  = cong (f x ∷_) (cong toList (map⁺-take⁺-mapₛ {n = n′} {f} {♭ xs′}))
\end{code}
%</map⁺-take⁺-mapₛ>


%<*unfold-take⁺>
\AgdaTarget{unfold-take⁺}
\begin{code}
unfold-take⁺ : ∀ (s : Stream α) n → take⁺ (suc n) s ≡ take⁺ n s ⁺∷ʳ lookup s (suc n)
unfold-take⁺ (_ ∷ xs′)   zero      with ♭ xs′
unfold-take⁺ (_ ∷ _)     zero      | _ ∷ _   = refl
unfold-take⁺ (x ∷ xs′)   (suc n′)            = cong (x ∷⁺_) (unfold-take⁺ (♭ xs′) n′)
\end{code}
%</unfold-take⁺>


%<*lookup-drop>
\AgdaTarget{lookup-drop}
\begin{code}
lookup-drop : ∀ (s : Stream α) n → drop n s ≈ lookup s n ∷ ♯ drop (suc n) s
lookup-drop (_ ∷ _)    zero      = refl ∷ ♯ reflₛ
lookup-drop (_ ∷ xs′)  (suc n′)  = lookup-drop (♭ xs′) n′  ⟨ transₛ ⟩  refl ∷ ♯ reflₛ
\end{code}
%</lookup-drop>


%<*map⁺-id>
\AgdaTarget{map⁺-id}
\begin{code}
map⁺-id : map⁺ id ≗ id {A = List⁺ α}
map⁺-id (x ∷ xs′) = cong (x ∷_) (map-idₗ xs′)
\end{code}
%</map⁺-id>


%<*map⁺-cong>
\AgdaTarget{map⁺-cong}
\begin{code}
map⁺-cong : {f g : α → β} → f ≗ g → map⁺ f ≗ map⁺ g
map⁺-cong p (x ∷ xs′) = cong₂ _∷_ (p x) (map-cong p xs′)
\end{code}
%</map⁺-cong>


%<*map⁺-compose>
\AgdaTarget{map⁺-∘}
\begin{code}
map⁺-∘ : {f : β → γ} {g : α → β} → map⁺ (f ∘ g) ≗ map⁺ f ∘ map⁺ g
map⁺-∘ {f = f} {g} (x ∷ xs′) = cong ((f ∘ g) x ∷_) (map-∘ₗ xs′)
\end{code}
%</map⁺-compose>


%<*map⁺-∷ʳ-commute>
\AgdaTarget{map⁺-∷ʳ-commute}
\begin{code}
map⁺-∷ʳ-commute : (f : α → β) (xs : List α) (y : α) → map⁺ f (xs ∷⁺ʳ y) ≡ map f xs ∷⁺ʳ f y
map⁺-∷ʳ-commute _ []         _ = refl
map⁺-∷ʳ-commute f (x ∷ xs′)  y = cong (f x ∷_) (map-++ f xs′ [ y ])
\end{code}
%</map⁺-∷ʳ-commute>


%<*∷⁺-toList>
\AgdaTarget{∷⁺-toList}
\begin{code}
∷⁺-toList : (x : α) (xs′ : List⁺ α) → x ∷⁺ xs′ ≡ x ∷ toList xs′
∷⁺-toList _ _ = refl
\end{code}
%</∷⁺-toList>


%<*∷ʳ-toList>
\AgdaTarget{∷ʳ-toList}
\begin{code}
∷ʳ-toList : (y : α) (xs : List α) → toList (xs ∷⁺ʳ y) ≡ xs ∷ʳ y
∷ʳ-toList _ []       = refl
∷ʳ-toList _ (_ ∷ _)  = refl
\end{code}
%</∷ʳ-toList>


%<*reverse⁺′-map⁺-commute>
\AgdaTarget{reverse⁺′-map⁺-commute}
\begin{code}
reverse⁺′-map⁺-commute : (f : α → β) (xs : List⁺ α) → (map⁺ f ∘ reverse⁺′) xs ≡ (reverse⁺′ ∘ map⁺ f) xs
reverse⁺′-map⁺-commute f (x ∷ xs′) = begin
  map⁺  f  (reverse⁺′ (x ∷ xs′))   ≡⟨⟩
  map⁺  f  ((reverse xs′) ∷⁺ʳ x)   ≡⟨ map⁺-∷ʳ-commute f (reverse xs′) x ⟩
  (map f (reverse xs′))  ∷⁺ʳ f x   ≡⟨ cong (_∷⁺ʳ f x) (reverse-map f xs′) ⟩
  reverse (map f xs′)    ∷⁺ʳ f x   ≡⟨⟩
  reverse⁺′  (f x ∷ map f xs′)     ≡⟨⟩
  reverse⁺′  (map⁺ f (x ∷ xs′))    ∎
\end{code}
%</reverse⁺′-map⁺-commute>


%<*unfold⁺-reverse⁺′>
\AgdaTarget{unfold⁺-reverse⁺′}
\begin{code}
unfold⁺-reverse⁺′ : (x x′ : α) (xs″ : List α) → reverse⁺′ (x ∷ (x′ ∷ xs″)) ≡ reverse⁺′ (x′ ∷ xs″) ⁺∷ʳ x
unfold⁺-reverse⁺′ x x′ xs″ = begin
  reverse⁺′ (x ∷ x′ ∷ xs″)     ≡⟨⟩
  reverse (x′ ∷ xs″)     ∷⁺ʳ x  ≡⟨ cong (_∷⁺ʳ x) (unfold-reverse x′ xs″) ⟩
  (reverse xs″ ∷ʳ   x′)  ∷⁺ʳ x  ≡⟨ cong (_∷⁺ʳ x) (sym (∷ʳ-toList x′ (reverse xs″))) ⟩
  (reverse xs″ ∷⁺ʳ  x′)  ⁺∷ʳ x  ≡⟨⟩
  reverse⁺′ (x′ ∷ xs″)   ⁺∷ʳ x  ∎
\end{code}
%</unfold⁺-reverse⁺′>


%<*unfold'-reverse⁺′>
\AgdaTarget{unfold'-reverse⁺′}
\begin{code}
unfold'-reverse⁺′ : (y : α) (xs : List α) → reverse⁺′ (xs ∷⁺ʳ y) ≡ y ∷ reverse xs
unfold'-reverse⁺′ _ []         = refl
unfold'-reverse⁺′ y (x ∷ xs′)  = begin
  reverse⁺′ (x ∷ (xs′ ∷ʳ y))  ≡⟨⟩
  reverse (xs′ ∷ʳ y) ∷⁺ʳ x    ≡⟨ cong (_∷⁺ʳ x)  (reverse-++ xs′ [ y ]) ⟩
  y ∷ reverse xs′ ++ [ x ]    ≡⟨ cong (y ∷_)    (sym $ reverse-++ [ x ] xs′) ⟩
  y ∷ reverse (x ∷ xs′)       ∎
\end{code}
%</unfold'-reverse⁺′>


%<*reverse⁺′-toList-commute>
\AgdaTarget{reverse⁺′-toList-commute}
\begin{code}
reverse⁺′-toList-commute : (xs : List⁺ α) → toList (reverse⁺′ xs) ≡ reverse (toList xs)
reverse⁺′-toList-commute xs           with snocView xs
reverse⁺′-toList-commute .(xs ∷⁺ʳ y)  | xs ∷ʳ′ y = begin
  toList  (reverse⁺′ (xs ∷⁺ʳ y))  ≡⟨ cong toList (unfold'-reverse⁺′ y xs) ⟩
  toList  (  y ∷ reverse xs)      ≡⟨⟩
             y ∷ reverse xs       ≡⟨ sym $ reverse-++ xs [ y ] ⟩
  reverse           (xs ∷ʳ y)     ≡⟨ cong reverse (sym $ ∷ʳ-toList y xs) ⟩
  reverse  (toList  (xs ∷⁺ʳ y))   ∎
\end{code}
%</reverse⁺′-toList-commute>


%<*unfold'⁺-reverse⁺′>
\AgdaTarget{unfold'⁺-reverse⁺′}
\begin{code}
unfold'⁺-reverse⁺′ : (y : α) (xs : List⁺ α) → reverse⁺′ (xs ⁺∷ʳ y) ≡ y ∷⁺ reverse⁺′ xs
unfold'⁺-reverse⁺′ y xs = begin
  reverse⁺′ (xs ⁺∷ʳ y)         ≡⟨ unfold'-reverse⁺′ y (toList xs) ⟩
  y ∷  reverse (toList xs)     ≡⟨ cong (y ∷_) (sym $ reverse⁺′-toList-commute xs) ⟩
  y ∷  toList (reverse⁺′ xs)   ≡⟨ sym $ ∷⁺-toList y (reverse⁺′ xs) ⟩
  y ∷⁺ reverse⁺′ xs            ∎
\end{code}
%</unfold'⁺-reverse⁺′>


%<*reverse⁺′-involutive>
\AgdaTarget{reverse⁺′-involutive}
\begin{code}
reverse⁺′-involutive : Involutive (_≡_) (reverse⁺′ {_} {α})
reverse⁺′-involutive (x ∷ xs′) = begin
  reverse⁺′ (reverse⁺′ (x ∷ xs′))    ≡⟨⟩
  reverse⁺′ ((reverse xs′) ∷⁺ʳ x)    ≡⟨ unfold'-reverse⁺′ x (reverse xs′) ⟩
  x ∷  (reverse ∘ reverse) xs′       ≡⟨ cong (x ∷_) (reverse-involutive xs′) ⟩
  x ∷  xs′                           ∎
\end{code}
%</reverse⁺′-involutive>


%<*unfold-tails⁺>
\AgdaTarget{unfold-tails⁺}
\begin{code}
unfold-tails⁺ : ∀ x y (zs : List α) → tails⁺ (x ∷⁺ (y ∷ zs)) ≡ (x ∷⁺ (y ∷ zs)) ∷⁺ tails⁺ (y ∷ zs)
unfold-tails⁺ _ _ _ = refl
\end{code}
%</unfold-tails⁺>


%<*unfold-inits⁺′>
\AgdaTarget{unfold-inits⁺′}
\begin{code}
unfold-inits⁺′ :  ∀ (s : List α) x y (ys : List α)
                  → inits⁺′ s y (ys ∷ʳ x) ≡ inits⁺′ s y ys ⁺∷ʳ ((s ++ (y ∷ ys)) ∷⁺ʳ x)
unfold-inits⁺′ s x y []        = refl
unfold-inits⁺′ s x y (z ∷ zs)  = cong ((s ∷⁺ʳ y) ∷⁺_) $ begin
  inits⁺′ (s ∷ʳ y) z (zs ∷ʳ x)                                  ≡⟨ unfold-inits⁺′ (s ∷ʳ y) x z zs ⟩
  inits⁺′ (s ∷ʳ y) z zs ⁺∷ʳ (((s ∷ʳ y) ++ (z ∷ zs)) ∷⁺ʳ x)      ≡⟨⟩
  inits⁺′ (s ∷ʳ y) z zs ⁺∷ʳ (((s ++ [ y ]) ++ (z ∷ zs)) ∷⁺ʳ x)  ≡⟨ cong (λ a → inits⁺′ (s ∷ʳ y) z zs ⁺∷ʳ (a ∷⁺ʳ x)) (assoc s [ y ] (z ∷ zs)) ⟩
  inits⁺′ (s ∷ʳ y) z zs ⁺∷ʳ ((s ++ y ∷ z ∷ zs) ∷⁺ʳ x)           ∎
  where open module LM {ℓ} {α : Set ℓ} = Monoid (++-monoid α) using (assoc)
\end{code}
%</unfold-inits⁺′>


%<*unfold-inits⁺>
\AgdaTarget{unfold-inits⁺}
\begin{code}
unfold-inits⁺ : ∀ x y (zs : List α) → inits⁺ ((y ∷ zs) ⁺∷ʳ x) ≡ inits⁺ (y ∷ zs) ⁺∷ʳ ((y ∷ zs) ⁺∷ʳ x)
unfold-inits⁺ = unfold-inits⁺′ []
\end{code}
%</unfold-inits⁺>


%<*lemma-tails⁺-inits⁺>
\AgdaTarget{lemma-tails⁺-inits⁺}
\begin{code}
lemma-tails⁺-inits⁺ : ∀ {x} {xs : List α} → map⁺ reverse⁺′ (tails⁺ (x ∷ xs)) ≡ reverse⁺′ (inits⁺ (reverse⁺′ (x ∷ xs)))
lemma-tails⁺-inits⁺ {x} {xs = []}      = refl
lemma-tails⁺-inits⁺ {x} {xs = y ∷ zs}  = begin
  map⁺ reverse⁺′ (tails⁺ (x ∷ y ∷ zs))                                    ≡⟨ cong (map⁺ reverse⁺′)                                       eq0 ⟩
  map⁺ reverse⁺′ ((x ∷ y ∷ zs) ∷⁺ tails⁺ (y ∷ zs))                        ≡⟨⟩
  reverse⁺′ (x ∷ y ∷ zs) ∷⁺ map⁺ reverse⁺′ (tails⁺ (y ∷ zs))              ≡⟨ cong (reverse⁺′ (x ∷ y ∷ zs) ∷⁺_)                           eq2 ⟩
  reverse⁺′ (x ∷ y ∷ zs) ∷⁺ reverse⁺′ (inits⁺ (reverse⁺′ (y ∷ zs)))       ≡⟨ unfold'⁺-reverse⁺′ (reverse⁺′ (x ∷ y ∷ zs)) (inits⁺ (reverse⁺′ (y ∷ zs))) ⟨
  reverse⁺′ (inits⁺ (reverse⁺′ (y ∷ zs)) ⁺∷ʳ reverse⁺′ (x ∷ y ∷ zs))      ≡⟨ cong (λ w → reverse⁺′ (inits⁺ (reverse⁺′ (y ∷ zs)) ⁺∷ʳ w))  eq4 ⟩
  reverse⁺′ (inits⁺ (reverse⁺′ (y ∷ zs)) ⁺∷ʳ (reverse⁺′ (y ∷ zs) ⁺∷ʳ x))  ≡⟨ cong reverse⁺′                                              eq5 ⟩
  reverse⁺′ (inits⁺ (reverse⁺′ (y ∷ zs) ⁺∷ʳ x))                           ≡⟨ cong (λ w → reverse⁺′ (inits⁺ w))                           eq6 ⟩
  reverse⁺′ (inits⁺ (reverse⁺′ (x ∷ (y ∷ zs))))                           ∎
  where
    eq0 = unfold-tails⁺ x y zs
    eq2 = lemma-tails⁺-inits⁺ {x = y} {zs}
    eq4 = unfold⁺-reverse⁺′ x y zs
    eq5 = sym $ unfold-inits⁺ x (head⁺ (reverse⁺′ (y ∷ zs))) (tail⁺ (reverse⁺′ (y ∷ zs)))
    eq6 = sym (unfold⁺-reverse⁺′ x y zs)
\end{code}
%</lemma-tails⁺-inits⁺>


%<*tails⁺-inits⁺>
\AgdaTarget{tails⁺-inits⁺}
\begin{code}
tails⁺-inits⁺ : {xs : List⁺ α} → map⁺ reverse⁺′ (tails⁺ (reverse⁺′ xs)) ≡ reverse⁺′ (inits⁺ xs)
tails⁺-inits⁺ {xs} = begin
  map⁺ reverse⁺′ (tails⁺ (reverse⁺′ xs))             ≡⟨ lemma-tails⁺-inits⁺ {x = head⁺ (reverse⁺′ xs)} {tail⁺ (reverse⁺′ xs)} ⟩
  reverse⁺′ (inits⁺ (reverse⁺′ (reverse⁺′ xs)))      ≡⟨⟩
  (reverse⁺′ ∘ inits⁺) ((reverse⁺′ ∘ reverse⁺′) xs)  ≡⟨ cong (reverse⁺′ ∘ inits⁺) (reverse⁺′-involutive xs) ⟩
  (reverse⁺′ ∘ inits⁺) xs                            ∎
\end{code}
%</tails⁺-inits⁺>
