\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Codata.Musical.Stream.Extra where

open import Codata.Musical.Notation using (♭; ♯_)
open import Codata.Musical.Stream using (Stream; _∷_; zipWith)
open import Data.Product.Base using (_,_; _×_)
open import Data.Nat.Base using (ℕ; zero; suc)
open import Data.List.NonEmpty using (List⁺; _∷⁺_; _⁺∷ʳ_) renaming ([_] to [_]⁺)

open import Notation.JP.Base using (α; β)
\end{code}


%<*take⁺>
\AgdaTarget{take⁺}
\begin{code}
take⁺ : ℕ → Stream α → List⁺ α
take⁺ zero     (x ∷ _)   = [ x ]⁺
take⁺ (suc n)  (x ∷ xs)  = x ∷⁺ take⁺ n (♭ xs)
\end{code}
%</take⁺>


%<*inits⁺′>
\AgdaTarget{inits⁺′}
\begin{code}
inits⁺′ : List⁺ α → Stream α → Stream (List⁺ α)
inits⁺′ acc (x ∷ xs) = (acc ⁺∷ʳ x) ∷ ♯ inits⁺′ (acc ⁺∷ʳ x) (♭ xs)
\end{code}
%</inits⁺′>


%<*inits⁺>
\AgdaTarget{inits⁺}
\begin{code}
inits⁺ : Stream α → Stream (List⁺ α)
inits⁺ (x ∷ xs) = [ x ]⁺ ∷ ♯ inits⁺′ [ x ]⁺ (♭ xs)
\end{code}
%</inits⁺>


%<*zip>
\AgdaTarget{zip}
\begin{code}
zip : Stream α → Stream β → Stream (α × β)
zip = zipWith _,_
\end{code}
%</zip>
