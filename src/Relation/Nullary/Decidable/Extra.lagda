\begin{code}
module Relation.Nullary.Decidable.Extra where

open import Function.Base using (_∘_)
open import Relation.Nullary.Decidable.Core using (yes; no)
open import Relation.Binary.Definitions using (Decidable)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; cong)

open import Function.Bijection.Sets using (module Injection′; _↣′_)
open Injection′ using (to; injective)

open import Notation.JP.Base using (α; β)
\end{code}


%<*via-injection-sets>
\AgdaTarget{via-injection′}
\begin{code}
via-injection′ : (α ↣′ β) → Decidable {A = β} _≡_ → Decidable {A = α} _≡_
via-injection′ inj _≟β_ x y with (to inj x) ≟β (to inj y)
...  | yes  injX≡injY = yes  (injective inj injX≡injY)
...  | no   injX≢injY = no   (injX≢injY ∘ cong (to inj))
\end{code}
%</via-injection-sets>
