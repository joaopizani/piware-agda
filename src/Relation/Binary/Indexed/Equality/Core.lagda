\begin{code}
module Relation.Binary.Indexed.Equality.Core where

open import Relation.Binary.Indexed.Heterogeneous.Definitions using (Symmetric; Transitive)
open import Relation.Binary.Indexed.Heterogeneous.Structures using (IsIndexedEquivalence)

open import Relation.Binary.Indexed.Core.Extra using (_≡_; refl)
\end{code}


%<*sym-indexed>
\begin{code}
sym : ∀ {i a} {I : Set i} {A : I → Set a} → Symmetric A (_≡_ {A = A})
sym refl = refl
\end{code}
%</sym-indexed>


\begin{code}
trans : ∀ {i a} {I : Set i} {A : I → Set a} → Transitive A (_≡_ {A = A})
trans refl eq = eq
\end{code}


\begin{code}
isEquivalence : ∀ {i a} {I : Set i} {A : I → Set a} → IsIndexedEquivalence A (_≡_ {A = A})
isEquivalence = record  { refl   = refl
                        ; sym    = sym
                        ; trans  = trans }
\end{code}
