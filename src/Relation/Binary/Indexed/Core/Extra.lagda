\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module Relation.Binary.Indexed.Core.Extra where

open import Level using (Level; suc; _⊔_)
open import Function.Base using (flip)
open import Data.Product.Base using (_×_)
open import Relation.Nullary.Negation.Core using (¬_)

open import Relation.Binary.Indexed.Heterogeneous.Core using (IREL; IRel)
open import Relation.Binary.Indexed.Heterogeneous.Bundles using (IndexedSetoid)

open import Notation.JP.Base using (ℓ₁; ℓ₂)
\end{code}

\begin{code}
private variable i i₁ i₂ : Level
\end{code}


%<*rel-impl-generalized>
\AgdaTarget{_⇒_}
\begin{code}
_⇒_ : ∀ {a b} {I₁ : Set i₁} {I₂ : Set i₂} {A : I₁ → Set a} {B : I₂ → Set b} → IREL A B ℓ₁ → IREL A B ℓ₂ → Set _
_⇒_ {A} {B} P Q = ∀ {i₁ i₂} {x : A i₁} {y : B i₂} → P x y → Q x y 
\end{code}
%</rel-impl-generalized>

%<*rel-impl-specific>
\begin{code}
_⇒′_ : ∀ {a} {I : Set i} {A : I → Set a} → IRel A ℓ₁ → IRel A ℓ₂ → Set _
_⇒′_ {A} P Q = ∀ {i₁ i₂} {x : A i₁} {y : A i₂} → P x y → Q x y
\end{code}
%</rel-impl-specific>


%<*indexed-pred>
\begin{code}
Pred : ∀ {a} {I : Set i} → (I → Set a) → (ℓ : Level) → Set (i ⊔ a ⊔ suc ℓ)
Pred A ℓ = ∀ {i} → A i → Set ℓ
\end{code}
%</indexed-pred>


%<*pred-respects-rel>
\begin{code}
_Respects_ : ∀ {a} {I : Set i} {A : I → Set a} → Pred A ℓ₁ → IRel A ℓ₂ → Set _
_Respects_ {A} P _∼_ = ∀ {i₁ i₂} {x : A i₁} {y : A i₂} → x ∼ y → P x → P y
\end{code}
%</pred-respects-rel>


%<*pred-respects-rel-binary>
\begin{code}
_Respects₂_ : ∀ {a} {I : Set i} {A : I → Set a} → IRel A ℓ₁ → IRel A ℓ₂ → Set (i ⊔ a ⊔ ℓ₁ ⊔ ℓ₂)
_Respects₂_ {A} R _∼_ =     (∀ {i} {x : A i} → _Respects_ {A = A} (R x)      _∼_)
                         ×  (∀ {i} {y : A i} → _Respects_ {A = A} (flip R y) _∼_)
\end{code}
%</pred-respects-rel-binary>


\begin{code}
infix 4 _≡_
\end{code}

%<*prop-eq-indexed>
\begin{code}
data _≡_ {i} {a} {I : Set i} {A : I → Set a} : ∀ {i₁ i₂} (x : A i₁) (y : A i₂) → Set (a ⊔ i) where
  refl : ∀ {i₁} {x : A i₁} → x ≡ x
\end{code}
%</prop-eq-indexed>


\begin{code}
infix 4 _≢_
\end{code}

%<*prop-neq-indexed>
\begin{code}
_≢_ : ∀ {i} {a} {I : Set i} {A : I → Set a} {i₁ i₂} (x : A i₁) (y : A i₂) → Set _
_≢_ {A} x y = ¬ (_≡_ {A = A} x y)
\end{code}
%</prop-neq-indexed>


\begin{code}
setoidIsReflexive :  ∀ {c} {I : Set ℓ₁} (S : IndexedSetoid I c ℓ₂)
                     →  let open IndexedSetoid S using (_≈_; Carrier)
                        in _⇒′_ {A = Carrier} (_≡_ {A = Carrier}) _≈_
setoidIsReflexive S refl = let open IndexedSetoid S using (_≈_) renaming (refl to refl≈) in refl≈
\end{code}
