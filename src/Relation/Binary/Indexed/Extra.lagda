\begin{code}
module Relation.Binary.Indexed.Extra where

open import Level using (suc; _⊔_)
open import Data.Product.Base using (_,_)
open import Relation.Binary.Indexed.Heterogeneous.Core using (IRel)
open import Relation.Binary.Indexed.Heterogeneous.Definitions using (Transitive; Reflexive)
open import Relation.Binary.Indexed.Heterogeneous.Structures using (IsIndexedEquivalence)
open import Relation.Binary.Indexed.Heterogeneous.Bundles using (IndexedSetoid)

open import Relation.Binary.Indexed.Core.Extra using (_⇒′_; _Respects₂_; _≡_; setoidIsReflexive)
open import Relation.Binary.Indexed.Equality.Core using () renaming (isEquivalence to ≡-isEquivalence)
\end{code}


\begin{code}
record IsIndexedPreorder {i a ℓ₁ ℓ₂} {I : Set i} (A : I → Set a) (_≈_ : IRel A ℓ₁) (_∼_ : IRel A ℓ₂) : Set (i ⊔ a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isEquivalence  : IsIndexedEquivalence A _≈_
    reflexive      : _⇒′_ {A = A} _≈_ _∼_
    trans          : Transitive A _∼_

  module Eq = IsIndexedEquivalence isEquivalence

  refl : Reflexive A _∼_
  refl = reflexive Eq.refl
  
  ∼-resp-≈ : _Respects₂_ {A = A} _∼_ _≈_
  ∼-resp-≈ =     (λ x≈y z∼x → trans z∼x (reflexive x≈y))
              ,  (λ x≈y x∼z → trans (reflexive (Eq.sym x≈y)) x∼z)
\end{code}


\begin{code}
record IndexedPreorder {i} (I : Set i) c ℓ₁ ℓ₂ : Set (suc (i ⊔ c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix 4 _≈_ _∼_
  field
    Carrier     : I → Set c
    _≈_         : IRel Carrier ℓ₁
    _∼_         : IRel Carrier ℓ₂
    isPreorder  : IsIndexedPreorder Carrier _≈_ _∼_

  open IsIndexedPreorder isPreorder public
\end{code}


\begin{code}
setoidIsPreorder :  ∀ {i c ℓ} {I : Set i} (S : IndexedSetoid I c ℓ) → let open IndexedSetoid S using (Carrier; _≈_)
                    in IsIndexedPreorder Carrier (_≡_ {A = Carrier}) _≈_
setoidIsPreorder S =  let open IndexedSetoid S using (trans)
                      in record  { isEquivalence  = ≡-isEquivalence
                                 ; reflexive      = setoidIsReflexive S
                                 ; trans          = trans }
\end{code}


\begin{code}
setoidPreorder : ∀ {i c ℓ} {I : Set i} (S : IndexedSetoid I c ℓ) → IndexedPreorder I c (c ⊔ i) ℓ 
setoidPreorder S = let open IndexedSetoid S in record  { Carrier     = Carrier
                                                ; _≈_         = (_≡_ {A = Carrier})
                                                ; _∼_         = _≈_
                                                ; isPreorder  = setoidIsPreorder S }
\end{code}
