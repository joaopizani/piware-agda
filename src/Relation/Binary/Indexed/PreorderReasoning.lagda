\begin{code}
open import Relation.Binary.Indexed.Extra using (IndexedPreorder)

module Relation.Binary.Indexed.PreorderReasoning {i} {I : Set i} {c} {ℓ₁} {ℓ₂} (P : IndexedPreorder I c ℓ₁ ℓ₂) where

open import Level using (suc)
open IndexedPreorder P using (_∼_; _≈_; refl; trans; reflexive) renaming (Carrier to Cr)
\end{code}


\begin{code}
infix 4 _IsRelatedTo_
\end{code}

\begin{code}
data _IsRelatedTo_ {i₁ i₂} (x : Cr i₁) (y : Cr i₂) : Set (suc ℓ₂) where
  relTo : (x∼y : x ∼ y) → x IsRelatedTo y
\end{code}


\begin{code}
infix 1 begin_
\end{code}

\begin{code}
begin_ : ∀ {i₁ i₂} {x : Cr i₁} {y : Cr i₂} → x IsRelatedTo y → x ∼ y
begin (relTo x∼y) = x∼y
\end{code}


\begin{code}
infixr 2 _∼⟨_⟩_
\end{code}

\begin{code}
_∼⟨_⟩_ : ∀ {i₁ i₂ i₃} (x : Cr i₁) {y : Cr i₂} {z : Cr i₃} → x ∼ y → y IsRelatedTo z → x IsRelatedTo z
_ ∼⟨ x∼y ⟩ (relTo y∼z) = relTo (trans x∼y y∼z) 
\end{code}


\begin{code}
infixr 2 _≈⟨_⟩_
\end{code}

\begin{code}
_≈⟨_⟩_ : ∀ {i₁ i₂ i₃} (x : Cr i₁) {y : Cr i₂} {z : Cr i₃} → x ≈ y → y IsRelatedTo z → x IsRelatedTo z
_ ≈⟨ x≈y ⟩ (relTo y∼z) = relTo (trans (reflexive x≈y) y∼z)
\end{code}


\begin{code}
infixr 2 _≈⟨⟩_
\end{code}

\begin{code}
_≈⟨⟩_ : ∀ {i₁ i₂} (x : Cr i₁) {y : Cr i₂} → x IsRelatedTo y → x IsRelatedTo y
_ ≈⟨⟩ r = r
\end{code}


\begin{code}
infix 3 _∎
\end{code}

\begin{code}
_∎ : ∀ {i₁} (x : Cr i₁) → x IsRelatedTo x
_∎ _ = relTo refl
\end{code}
