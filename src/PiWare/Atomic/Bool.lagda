\begin{code}
module PiWare.Atomic.Bool where

open import Data.Bool using (Bool) renaming (_≟_ to _≟Bool_)

open import Data.FiniteInhabited.Bool using (FiniteInhabited-Bool)
open import PiWare.Atomic using (Atomic)
\end{code}


%<*Atomic-Bool>
\AgdaTarget{Atomic-Bool}
\begin{code}
Atomic-Bool : Atomic
Atomic-Bool = record  { Atom = Bool
                      ; enum = FiniteInhabited-Bool
                      ; _≟A_ = _≟Bool_ }
\end{code}
%</Atomic-Bool>
