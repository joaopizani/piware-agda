\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.SimulationState (A : Atomic) where

open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Nat.Base using (_+_)
open import Codata.Musical.Stream using (Stream; _∷_)
open import Data.Product.Base using (_×_; _,_; proj₂; proj₁)
open import Data.Vec.Base using (replicate; _++_)
open import Data.Vec.Extra using (splitAt′)

open Atomic A using (default)
open import PiWare.Gates using (Gates)
open import PiWare.Circuit using (IsComb; σ; ω; ℂ[_]; Gate; Plug; _⟫_; _∥_; DelayLoop)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Plugs.Core using (_⤪_)

open import PiWare.Semantics.Simulation A using (W; W⟶W; plugσ)

open import Notation.JP.Base using (γ; α; β)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; i₁; i₂; m; o; o₁; o₂; l)
open import PiWare.Circuit using (s)
\end{code}


\begin{code}
infixl 4 _⟫ₛ_
infixr 5 _∥ₛ_
\end{code}

%<*ℂₛ[_]>
\AgdaTarget{ℂₛ[\_]}
\begin{code}
data ℂₛ[_] (G : Gates) : (c : ℂ[ G ] {s} i o) → Set where
  Gateₛ : ∀ {g}          → ℂₛ[ G ] {s} (Gate g)
  Plugₛ : ∀ {p : i ⤪ o}  → ℂₛ[ G ] {s} (Plug p)

  _⟫ₛ_ : {c₁ : ℂ[ G ] {s} i m}    {c₂ : ℂ[ G ] {s} m o}    → ℂₛ[ G ] c₁ → ℂₛ[ G ] c₂ → ℂₛ[ G ] (c₁ ⟫ c₂)
  _∥ₛ_ : {c₁ : ℂ[ G ] {s} i₁ o₁}  {c₂ : ℂ[ G ] {s} i₂ o₂}  → ℂₛ[ G ] c₁ → ℂₛ[ G ] c₂ → ℂₛ[ G ] (c₁ ∥ c₂)

  DelayLoopₛ : {c : ℂ[ G ] {σ} (i + l) (o + l)} → W l → ℂₛ[ G ] c → ℂₛ[ G ] {ω} {i} {o} (DelayLoop c)
\end{code}
%</ℂₛ>


%<*defaultₛ>
\AgdaTarget{defaultₛ}
\begin{code}
defaultₛ : (c : ℂ[ G ] {s} i o) → ℂₛ[ G ] c
defaultₛ (Gate g)       = Gateₛ
defaultₛ (Plug x)       = Plugₛ
defaultₛ (c₁ ⟫ c₂)      = defaultₛ c₁ ⟫ₛ defaultₛ c₂
defaultₛ (c₁ ∥ c₂)      = defaultₛ c₁ ∥ₛ defaultₛ c₂
defaultₛ (DelayLoop c)  = DelayLoopₛ (replicate _ default) (defaultₛ c)
\end{code}
%</defaultₛ>


%<*⟦_⟧ₛ[_]>
\AgdaTarget{⟦\_⟧ₛ[\_]}
\begin{code}
⟦_⟧ₛ[_] : (c : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) → (ℂₛ[ G ] c → W i → (ℂₛ[ G ] c × W o))
⟦ Gate h      ⟧ₛ[ g ] s           i           = s , g h i
⟦ Plug x      ⟧ₛ[ g ] s           i           = s , plugσ x i

⟦ c₁ ⟫ c₂     ⟧ₛ[ g ] (s₁ ⟫ₛ s₂)  i =
  let  s₁′ , r₁ = ⟦ c₁ ⟧ₛ[ g ] s₁ i
       s₂′ , r₂ = ⟦ c₂ ⟧ₛ[ g ] s₂ r₁
  in (s₁′ ⟫ₛ s₂′) , r₂

⟦ c₁ ∥ c₂     ⟧ₛ[ g ] (s₁ ∥ₛ s₂) i =
  let  i₁   , i₂ = splitAt′ _ i
       s₁′  , r₁ = ⟦ c₁ ⟧ₛ[ g ] s₁ i₁
       s₂′  , r₂ = ⟦ c₂ ⟧ₛ[ g ] s₂ i₂
  in (s₁′ ∥ₛ s₂′) , r₁ ++ r₂

⟦ DelayLoop c ⟧ₛ[ g ] (DelayLoopₛ sm sc) i =
  let  sc′ , r++s   = ⟦ c ⟧ₛ[ g ] sc (i ++ sm)
       r   , sm′    = splitAt′ _ r++s
  in (DelayLoopₛ sm′ sc′) , r
\end{code}
%</⟦_⟧ₛ[_]>


%<*mealy>
\AgdaTarget{mealy}
\begin{code}
mealy : (γ → α → (γ × β)) → γ → Stream α → Stream β
mealy f s (x ∷ xs) = proj₂ (f s x) ∷ ♯ mealy f (proj₁ (f s x)) (♭ xs)
\end{code}
%</mealy>


%<*⟦_⟧ω'[_]>
\AgdaTarget{⟦_⟧ω'[_]}
\begin{code}
⟦_⟧ω'[_] : ℂ[ G ] {s} i o → TyGate G W⟶W → (Stream (W i) → Stream (W o))
⟦ c ⟧ω'[ g ] = mealy ⟦ c ⟧ₛ[ g ] (defaultₛ c)
\end{code}
%</⟦_⟧ω'[_]>



\begin{code}
module WithGates {G} (g : TyGate G W⟶W) where
\end{code}

%<*ℂₛ>
\AgdaTarget{ℂₛ}
\begin{code}
 ℂₛ : (c : ℂ[ G ] {s} i o) → Set
 ℂₛ = ℂₛ[ G ]
\end{code}
%<*ℂₛ>

%<*⟦_⟧ₛ>
\AgdaTarget{⟦\_⟧ₛ}
\begin{code}
 ⟦_⟧ₛ : (c : ℂ[ G ] {s} i o) → (ℂₛ[ G ] c → W i → (ℂₛ[ G ] c × W o))
 ⟦_⟧ₛ = ⟦_⟧ₛ[ g ]
\end{code}
%</⟦_⟧ₛ>

%<*⟦_⟧ω'>
\AgdaTarget{⟦\_⟧ω'}
\begin{code}
 ⟦_⟧ω' : (c : ℂ[ G ] {s} i o) → (Stream (W i) → Stream (W o))
 ⟦_⟧ω' = ⟦_⟧ω'[ g ]
\end{code}
%</⟦_⟧ω'>
