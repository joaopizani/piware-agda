\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation (A : Atomic) where

open import Function.Base using (_∘′_; flip)
open import Data.Nat.Base using (ℕ; _+_)
open import Data.Product.Base using (uncurry′) renaming (map to map×)
open import Data.List.Base using (List; []; _∷_)
open import Data.List.NonEmpty using (head) renaming (map to map⁺)
open import Data.Vec.Base using (Vec; _++_; lookup; replicate; drop; tabulate; take)
open import Relation.Binary.Definitions using (DecidableEquality)
open import Data.Vec.Properties using (≡-dec)
open import Codata.Musical.Stream using (Stream)

open import Data.Vec.Extra using (splitAt′)
open import Codata.Musical.Stream.Causal using (_⇒ᶜ_; runᶜ; pasts)
open import Data.List.NonEmpty.Extra using (unzip⁺; splitAt⁺; uncurry⁺)

open import PiWare.Circuit using (ℂ[_]; σ; ω) renaming (module WithGates to CircuitWithGates)
open import PiWare.Circuit.Algebra using (ℂσA; cataℂσ; ℂA; cataℂ; TyGate; TyPlug; Ty⟫; Ty∥)
open Atomic A using (Atom; _≟A_) renaming (default to defaultAtom)

open import Notation.JP.Base using (n)
open import PiWare.Gates using (G)
open import PiWare.Circuit using (s)
open import PiWare.Circuit.Notation using (i; o; l)
\end{code}


%<*Word>
\begin{code}
W = Vec Atom
\end{code}
%</Word>

%<*Word-dec-eq>
\AgdaTarget{\_≟W\_}
\begin{code}
_≟W_ : DecidableEquality (W n)
_≟W_ = ≡-dec _≟A_
\end{code}
%</Word-dec-eq>


%<*Word-function>
\begin{code}
W⟶W : ℕ → ℕ → Set
W⟶W i o = W i → W o
\end{code}
%</Word-function>


%<*combinator-Word-function-types>
\begin{code}
plugσ  : TyPlug  W⟶W
seqσ   : Ty⟫     W⟶W
parσ   : Ty∥     W⟶W
\end{code}
%</combinator-Word-function-types>

%<*combinator-Word-function-defs>
\AgdaTarget{plugσ,seqσ,parσ}
\begin{code}
plugσ p ins  = tabulate (lookup ins ∘′ lookup p)
seqσ         = flip _∘′_
parσ f₁ f₂   = uncurry′ _++_ ∘′ map× f₁ f₂ ∘′ splitAt′ _
\end{code}
%</combinator-Word-function-defs>


%<*simulation-sigma-algebra>
\AgdaTarget{simσ[\_]}
\begin{code}
simσ[_] : TyGate G W⟶W → ℂσA W⟶W
simσ[ g ] = record { GateA = g;  PlugA = plugσ;  _⟫A_ = seqσ;  _∥A_ = parσ }
\end{code}
%</simulation-sigma-algebra>


%<*simulation-combinational>
\AgdaTarget{⟦\_⟧[_]}
\begin{code}
⟦_⟧[_] : ℂ[ G ] {σ} i o → (g : TyGate G W⟶W) → W⟶W i o
⟦ c ⟧[ g ] = cataℂσ W⟶W simσ[ g ] c
\end{code}
%</simulation-combinational>



%<*Word-causal-function>
\AgdaTarget{W⇒ᶜW}
\begin{code}
W⇒ᶜW : ℕ → ℕ → Set
W⇒ᶜW i o = W i ⇒ᶜ W o
\end{code}
%</Word-causal-function>


%<*delay-prime>
\AgdaTarget{delay′}
\begin{code}
delay′ : ∀ o → W⟶W (i + l) (o + l) → W i → List (W i) → W (o + l)
delay′ {l = l}  _ f w⁰ []          = f (w⁰ ++ replicate l defaultAtom)
delay′          o f w⁰ (w⁻¹ ∷ w⁻)  = f (w⁰ ++ drop o (delay′ o f w⁻¹ w⁻))
\end{code}
%</delay-prime>

%<*delay>
\AgdaTarget{delay}
\begin{code}
delay : ∀ o → W⟶W (i + l) (o + l) → W⇒ᶜW i (o + l)
delay o = uncurry⁺ ∘′ delay′ o
\end{code}
%</delay>

\begin{code}
delayᶜ : W⟶W (i + l) (o + l) → W⇒ᶜW i o
\end{code}
%<*delay-causal>
\AgdaTarget{delayᶜ}
\begin{code}
delayᶜ {o = o} f = take o ∘′ delay o f
\end{code}
%</delay-causal>


\begin{code}
gateᶜ[_] : TyGate G W⟶W → TyGate G W⇒ᶜW
\end{code}
%<*gate-causal>
\AgdaTarget{gateᶜ[_]}
\begin{code}
gateᶜ[ g ] g# = g g# ∘′ head
\end{code}
%</gate-causal>


%<*combinator-causal-function-types>
\begin{code}
plugᶜ  : TyPlug  W⇒ᶜW
seqᶜ   : Ty⟫     W⇒ᶜW
parᶜ   : Ty∥     W⇒ᶜW
\end{code}
%</combinator-causal-function-types>

%<*combinator-causal-function-defs>
\AgdaTarget{plugᶜ,seqᶜ,parᶜ}
\begin{code}
plugᶜ f     = plugσ f ∘′ head
seqᶜ f₁ f₂  = f₂ ∘′ map⁺ f₁ ∘′ pasts
parᶜ f₁ f₂  = uncurry′ _++_ ∘′ map× f₁ f₂ ∘′ unzip⁺ ∘′ splitAt⁺ _
\end{code}
%</combinator-causal-function-defs>


\begin{code}
simᶜ[_] : TyGate G W⟶W → ℂA W⟶W W⇒ᶜW
\end{code}
%<*simulation-causal-algebra>
\AgdaTarget{simᶜ[\_]}
\begin{code}
simᶜ[ g ] = record { GateA = gateᶜ[ g ]; PlugA = plugᶜ; _⟫A_ = seqᶜ; _∥A_ = parᶜ; DelayLoopA = delayᶜ }
\end{code}
%</simulation-causal-algebra>


%<*simulation-causal>
\AgdaTarget{⟦\_⟧ᶜ[\_]}
\begin{code}
⟦_⟧ᶜ[_] : ℂ[ G ] {s} i o → (g : TyGate G W⟶W) → W⇒ᶜW i o
⟦ c ⟧ᶜ[ g ] = cataℂ W⟶W W⇒ᶜW simσ[ g ] simᶜ[ g ] c
\end{code}
%</simulation-causal>


%<*simulation-sequential>
\AgdaTarget{⟦\_⟧ω[\_]}
\begin{code}
⟦_⟧ω[_] : ℂ[ G ] {s} i o → (g : TyGate G W⟶W) → (Stream (W i) → Stream (W o))
⟦ c ⟧ω[ g ] = runᶜ ⟦ c ⟧ᶜ[ g ]
\end{code}
%</simulation-sequential>



\begin{code}
module WithGates {G} (g : TyGate G W⟶W) where
 open CircuitWithGates G using (ℂ)
\end{code}

%<*simulation-sigma-algebra-with-gates>
\AgdaTarget{simσ}
\begin{code}
 simσ : ℂσA W⟶W
 simσ = simσ[ g ]
\end{code}
%</simulation-sigma-algebra-with-gates>

%<*simulation-combinational-with-gates>
\AgdaTarget{⟦\_⟧}
\begin{code}
 ⟦_⟧ : ℂ {σ} i o → W⟶W i o
 ⟦_⟧ = ⟦_⟧[ g ]
\end{code}
%</simulation-combinational-with-gates>

%<*gate-causal-function-with-gates>
\AgdaTarget{gateᶜ}
\begin{code}
 gateᶜ = gateᶜ[ g ]
\end{code}
%</gate-causal-function-with-gates>

%<*simulation-causal-algebra-with-gates>
\AgdaTarget{simᶜ}
\begin{code}
 simᶜ : ℂA W⟶W W⇒ᶜW
 simᶜ = simᶜ[ g ]
\end{code}
%</simulation-causal-algebra-with-gates>

%<*simulation-causal-with-gates>
\AgdaTarget{⟦\_⟧ᶜ}
\begin{code}
 ⟦_⟧ᶜ : ℂ {s} i o → W⇒ᶜW i o
 ⟦_⟧ᶜ = ⟦_⟧ᶜ[ g ]
\end{code}
%</simulation-causal-with-gates>

%<*simulation-sequential-with-gates>
\AgdaTarget{⟦\_⟧ω}
\begin{code}
 ⟦_⟧ω : ℂ {s} i o → (Stream (W i) → Stream (W o))
 ⟦_⟧ω = ⟦_⟧ω[ g ]
\end{code}
%</simulation-sequential-with-gates>
