\begin{code}
module PiWare.Semantics.AreaAnalysis where

open import Data.Nat.Base using (ℕ; _+_)

open import PiWare.Circuit using (ℂ[_]; σ)
open import PiWare.Circuit.Algebra using (ℂσA; ℂA; cataℂσ; cataℂ; TyGate; TyPlug; Ty⟫; Ty∥)

open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o; l)
open import PiWare.Circuit using (s)
\end{code}


%<*Area>
\AgdaTarget{Area}
\begin{code}
Area : ℕ → ℕ → Set
Area _ _ = ℕ
\end{code}
%</Area>


%<*combinator-area-types>
\begin{code}
plugA      : TyPlug  Area
seqA       : Ty⟫     Area
parA       : Ty∥     Area
\end{code}
%</combinator-area-types>

-- TODO: should depend on the gate (spec should be A PARAMETER TYPE)
%<*combinator-area-defs>
\AgdaTarget{plug, seq, par}
\begin{code}
plugA     _      = 0
seqA      a₁ a₂  = a₁ + a₂
parA      a₁ a₂  = a₁ + a₂
\end{code}
%</combinator-area-defs>


%<*area-delayLoop>
\AgdaTarget{delayLoop}
\begin{code}
delayLoop  : Area (i + l) (o + l) → Area i o
delayLoop a = 1 + a
\end{code}
%</area-delayLoop>


-- TODO: the need for bogus being passed here
%<*bogus-dont-care>
\begin{code}
X = 42
\end{code}
%</bogus-dont-care>


%<*area-combinational-algebra>
\AgdaTarget{areaσ[\_]}
\begin{code}
areaσ[_] : TyGate G Area → ℂσA {G} Area
areaσ[ g ] = record  { GateA = g
                     ; PlugA = plugA
                     ; _⟫A_ = seqA {X} {X} {X}
                     ; _∥A_ = parA {X} {X} {X} {X} }
\end{code}
%</area-combinational-algebra>


%<*area-combinational>
\AgdaTarget{⟦\_⟧ₐ[\_]}
\begin{code}
⟦_⟧ₐ[_] : ℂ[ G ] {σ} i o → TyGate G Area → Area i o
⟦ c ⟧ₐ[ g ] = cataℂσ Area areaσ[ g ] c
\end{code}
%</area-combinational>


%<*area-sequential-algebra>
\AgdaTarget{area[\_]}
\begin{code}
area[_] : TyGate G Area → ℂA {G} Area Area
area[_] {G} g = record  { GateA       = GateAσ
                        ; PlugA       = PlugAσ
                        ; _⟫A_        = _⟫Aσ_ {X} {X} {X}
                        ; _∥A_        = _∥Aσ_ {X} {X} {X} {X}
                        ; DelayLoopA  = delayLoop {X} {X} {X}
                        } where open ℂσA {G} areaσ[ g ] using () renaming  ( GateA to GateAσ; PlugA to PlugAσ
                                                                            ; _⟫A_ to _⟫Aσ_; _∥A_ to _∥Aσ_ )
\end{code}
%</area-sequential-algebra>


%<*area-sequential>
\AgdaTarget{⟦\_⟧ωₐ[\_]}
\begin{code}
⟦_⟧ωₐ[_] : ℂ[ G ] {s} i o → TyGate G Area → Area i o
⟦ c ⟧ωₐ[ g ] = cataℂ Area Area areaσ[ g ] area[ g ] c
\end{code}
%</area-sequential>



\begin{code}
module WithGates {G} (g : TyGate G Area) where
\end{code}

%<*area-combinational-algebra-with-gates>
\AgdaTarget{areaσ}
\begin{code}
 areaσ : ℂσA {G} Area
 areaσ = areaσ[ g ]
\end{code}
%</area-combinational-algebra-with-gates>

%<*area-combinational-with-gates>
\AgdaTarget{⟦\_⟧ₐ}
\begin{code}
 ⟦_⟧ₐ : ℂ[ G ] {σ} i o → Area i o
 ⟦_⟧ₐ = ⟦_⟧ₐ[ g ]
\end{code}
%</area-combinational-with-gates>

%<*area-sequential-algebra-with-gates>
\AgdaTarget{area}
\begin{code}
 area : ℂA {G} Area Area
 area = area[ g ]
\end{code}
%</area-sequential-algebra-with-gates>


%<*area-sequential-with-gates>
\AgdaTarget{⟦\_⟧ωₐ}
\begin{code}
 ⟦_⟧ωₐ : ℂ[ G ] {s} i o → Area i o
 ⟦_⟧ωₐ = ⟦_⟧ωₐ[ g ]
\end{code}
%</area-sequential-with-gates>
