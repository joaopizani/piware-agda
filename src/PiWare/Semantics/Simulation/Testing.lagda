\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Testing (A : Atomic) where

open import Function using (_∘′_)
open import Data.Vec.Base using (tabulate)
open import Data.Fin.Base using (Fin)

open Atomic A using () renaming (finite to finiteAtom)
open import Data.HVec using (vec↑)
open import Data.Finite using (Finite; Finite-Vec)
open Finite using (#α) renaming (from to fromFin)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Circuit using (ℂ[_]; σ) renaming (module WithGates to CircuitWithGates)
open import PiWare.Semantics.Simulation A using (W; W⟶W; _≟W_; ⟦_⟧[_]) renaming (module WithGates to SimulationWithGates)
open import Relation.Nullary.Decidable.Core using (True)

open import PiWare.Semantics.Simulation.Compliance A using (∀-W)

open import Notation.JP.Base using (n)
open import PiWare.Circuit.Notation using (i; o)
\end{code}


%<*decidable-compliance-at-word>
\AgdaTarget{\_⊑?[\_]\_at\_}
\begin{code}
_⊑?[_]_at_ : (c : ℂ[ G ] {σ} i o) (g : TyGate G W⟶W) (f : W⟶W i o) → W i → Set
c ⊑?[ g ] f at w = True (⟦ c ⟧[ g ] w ≟W f w)
\end{code}
%</decidable-compliance-at-word>

%<*decidable-compliance>
\AgdaTarget{\_⊑?[\_]\_}
\begin{code}
_⊑?[_]_ : (c : ℂ[ G ] {σ} i o) (g : TyGate G W⟶W) (f : W⟶W i o) → Set
c ⊑?[ g ] f = ∀ w → c ⊑?[ g ] f at w
\end{code}
%</decidable-compliance>


%<*fromW>
\AgdaTarget{fromW}
\begin{code}
fromW : (k : Fin (#α (Finite-Vec ⦃ finiteAtom ⦄ {n}))) → W n
fromW {n} k = fromFin (Finite-Vec ⦃ finiteAtom ⦄ {n}) k
\end{code}
%</fromW>


%<*check-decidable-compliance>
\AgdaTarget{check⊑[\_]}
\begin{code}
check⊑[_] : (g : TyGate G W⟶W) (c : ℂ[ G ] i o) (f : W⟶W i o) {ps : vec↑ (tabulate (c ⊑?[ g ] f at_ ∘′ fromW))} → c ⊑?[ g ] f
check⊑[ g ] c f {ps} = ∀-W {P = c ⊑?[ g ] f at_} {ps}
\end{code}
%</check-decidable-compliance>



\begin{code}
module WithGates (g : TyGate G W⟶W) where
 open CircuitWithGates G using (ℂ)
\end{code}


%<*decidable-compliance-at-word-with-gates>
\AgdaTarget{\_⊑?\_at\_}
\begin{code}
 _⊑?_at_ : (c : ℂ i o) (f : W⟶W i o) → W i → Set
 c ⊑? f at w = c ⊑?[ g ] f at w
\end{code}
%</decidable-compliance-at-word-with-gates>


%<*decidable-compliance-with-gates>
\AgdaTarget{\_⊑?\_}
\begin{code}
 _⊑?_ : (c : ℂ i o) (f : W⟶W i o) → Set
 c ⊑? f = c ⊑?[ g ] f
\end{code}
%</decidable-compliance-at-word-with-gates>


%<*check-decidable-compliance-with-gates>
\AgdaTarget{check⊑}
\begin{code}
 check⊑ : (c : ℂ i o) (f : W⟶W i o) {ps : vec↑ (tabulate (c ⊑? f at_ ∘′ fromW))} → c ⊑? f
 check⊑ = check⊑[ g ]
\end{code}
%</check-decidable-compliance-with-gates>
