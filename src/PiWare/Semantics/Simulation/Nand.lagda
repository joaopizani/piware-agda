\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Semantics.Simulation.Nand where

open import Data.Vec using ([]; _∷_; [_])
open import Data.Bool.Base using (not; _∧_)

open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Gates.Nand using (Nand; ⊼ℂ')
open import PiWare.Semantics.Simulation Atomic-Bool using (W⟶W)
\end{code}


%<*spec-nand>
\AgdaTarget{spec-⊼ℂ}
\begin{code}
spec-⊼ℂ : W⟶W 2 1
spec-⊼ℂ (x ∷ y ∷ []) = [ not (x ∧ y) ]
\end{code}
%</spec-nand>

%<*spec>
\AgdaTarget{spec}
\begin{code}
spec : TyGate Nand W⟶W
spec ⊼ℂ' = spec-⊼ℂ
\end{code}
%</spec>
