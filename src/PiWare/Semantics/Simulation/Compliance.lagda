\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Compliance (A : Atomic) where

open import Function.Base using (_∘′_)
open import Data.Vec.Base using (tabulate)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
open import Data.Vec.Forall using (∀-Vec-Finite)
open import Data.Finite using (Finite-Vec)
open Atomic A using () renaming (finite to finiteAtom)
open import Data.Finite.Base using (module Finite)
open Finite using () renaming (from to fromFin)
open import Data.HVec using (vec↑)

open import Data.Vec.Relation.Binary.Equality.Propositional using (_≋_; ≋-refl; ≋⇒≡)
open import Data.Vec.Relation.Binary.Pointwise.Inductive using (length-equal)

open import PiWare.Circuit using (ℂ[_]; σ)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W; W⟶W; ⟦_⟧[_])

open import Notation.JP.Base using (n)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o; i′; o′)
\end{code}


%<*forall-words-type>
\AgdaTarget{∀-W}
\begin{code}
∀-W :  let fromW = fromFin (Finite-Vec ⦃ finiteAtom ⦄)  in
       {P : W n → Set} {ps : vec↑ (tabulate (P ∘′ fromW))} → (∀ w → P w)
\end{code}
%</forall-words-type>
%<*forall-words-def>
\begin{code}
∀-W {P} {ps} = ∀-Vec-Finite {P = P} ⦃ finiteAtom ⦄ {ps}
\end{code}
%</forall-words-def>



%<*implements-pointwise-at>
\AgdaTarget{\_⫃[\_]\_at\_}
\begin{code}
_⫃[_]_at_ : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i o) → (W i → Set)
c ⫃[ g ] f at w = ⟦ c ⟧[ g ] w ≡ f w
\end{code}
%</implements-pointwise-at>

\begin{code}
infixl 3 _⫃[_]_
\end{code}

%<*implements-pointwise>
\AgdaTarget{\_⫃[\_]\_}
\begin{code}
_⫃[_]_ : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i o) → Set
c ⫃[ g ] f = ∀ w → c ⫃[ g ] f at w
\end{code}
%</implements-pointwise>


%<*implements-pointwise-c-at>
\AgdaTarget{\_⫃ᶜ[\_]\_at\_}
\begin{code}
\end{code}
%</implements-pointwise-c-at>

\begin{code}
--infixl 3 _⫃ᶜ[_]_
\end{code}

%<*implements-pointwise-c>
\AgdaTarget{\_⫃ᶜ[\_]\_}
\begin{code}
\end{code}
%</implements-pointwise-c>


%<*implements-pointwise-omega-at>
\AgdaTarget{\_⫃ω[\_]\_at\_}
\begin{code}
\end{code}
%</implements-pointwise-omega-at>

\begin{code}
--infixl 3 _⫃ω[_]_
\end{code}

%<*implements-pointwise-omega>
\AgdaTarget{\_⫃ω[\_]\_}
\begin{code}
\end{code}
%</implements-pointwise-omega>



%<*implements-semihet-at>
\AgdaTarget{\_⫅[\_]\_at\_}
\begin{code}
_⫅[_]_at_ : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i o′) → (W i → Set)
c ⫅[ g ] f at w = ⟦ c ⟧[ g ] w ≋ f w
\end{code}
%</implements-semihet-at>

\begin{code}
infixl 3 _⫅[_]_
\end{code}

%<*implements-semihet>
\AgdaTarget{\_⫅[\_]\_}
\begin{code}
_⫅[_]_ : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i o′) → Set
c ⫅[ g ] f = ∀ w → c ⫅[ g ] f at w
\end{code}
%</implements-semihet>


%<*implements-semihet-c-at>
\AgdaTarget{\_⫅ᶜ[\_]\_at\_}
\begin{code}
\end{code}
%</implements-semihet-c-at>

\begin{code}
--infixl 3 _⫅ᶜ[_]_
\end{code}

%<*implements-semihet-c>
\AgdaTarget{\_⫅ᶜ[\_]\_}
\begin{code}
\end{code}
%</implements-semihet-c>


%<*implements-semihet-omega-at>
\AgdaTarget{\_⫅ω[\_]\_at\_}
\begin{code}
\end{code}
%</implements-semihet-omega-at>

\begin{code}
--infixl 3 _⫅ω[_]_
\end{code}

%<*implements-semihet-omega>
\AgdaTarget{\_⫅ω[\_]\_}
\begin{code}
\end{code}
%</implements-semihet-omega>



%<*implements-preserve-semihet-at>
\AgdaTarget{\_⫇[\_]\_at\_/\_}
\begin{code}
_⫇[_]_at_/_ : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i′ o′) → (W i → W i′ → Set)
c ⫇[ g ] f at w / w′ = w ≋ w′ → ⟦ c ⟧[ g ] w ≋ f w′
\end{code}
%</implements-preserve-semihet-at>

\begin{code}
infixl 3 _⫇[_]_
\end{code}

%<*implements-preserve-semihet>
\AgdaTarget{\_⫇[\_]\_}
\begin{code}
_⫇[_]_ : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i′ o′) → Set
c ⫇[ g ] f = ∀ {w w′} → c ⫇[ g ] f at w / w′
\end{code}
%</implements-preserve-semihet>


%<*implements-preserve-semihet-c-at>
\AgdaTarget{\_⫇ᶜ[\_]\_at\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-c-at>

\begin{code}
--infixl 3 _⫇ᶜ[_]_
\end{code}

%<*implements-preserve-semihet-c>
\AgdaTarget{\_⫇ᶜ[\_]\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-c>


%<*implements-preserve-semihet-omega-at>
\AgdaTarget{\_⫇ω[\_]\_at\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-omega-at>

\begin{code}
--infixl 3 _⫇ω[_]_
\end{code}

%<*implements-preserve-semihet-omega>
\AgdaTarget{\_⫇ω[\_]\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-omega>



\begin{code}
module ⫇-unsound where
\end{code}
%<*implements-preserve-semihet-unsound>
\begin{code}
 ⫇-unsound : (g : TyGate G W⟶W) (c : ℂ[ G ] 0 0) (f : W⟶W 1 1) → c ⫇[ g ] f
 ⫇-unsound _ _ _ ()
\end{code}
%</implements-preserve-semihet-unsound>



\begin{code}
infixl 3 _⫉[_]_
\end{code}

%<*implements>
\AgdaTarget{\_⫉[\_]\_}
\begin{code}
data _⫉[_]_ {i o i′ o′ G} : (c : ℂ[ G ] i o) (g : TyGate G W⟶W) (f : W⟶W i′ o′) → Set where
  refl⫉ : {c : ℂ[ G ] i o} {f : W⟶W i′ o′} {g : TyGate G W⟶W} (i≡ : i ≡ i′) → c ⫇[ g ] f → c ⫉[ g ] f
\end{code}
%</implements>


%<*implements-i-equal>
\AgdaTarget{⫉⇒i≡}
\begin{code}
⫉⇒i≡ : {c : ℂ[ G ] i o} {f : W⟶W i′ o′} {g : TyGate G W⟶W} → c ⫉[ g ] f → i ≡ i′
⫉⇒i≡ (refl⫉ refl _) = refl
\end{code}
%</implements-i-equal>


%<*implements-o-equal>
\AgdaTarget{⫉⇒o≡}
\begin{code}
⫉⇒o≡ : {c : ℂ[ G ] i o} {f : W⟶W i′ o′} {g : TyGate G W⟶W} → c ⫉[ g ] f → o ≡ o′
⫉⇒o≡ {i} (refl⫉ refl c⫇f) = length-equal (c⫇f {w = dummy} ≋-refl) where postulate dummy : W i
\end{code}
%</implements-o-equal>


%<*implements-semihet-to-eq>
\AgdaTarget{⫅⇒⫉}
\begin{code}
⫅⇒⫉ : {c : ℂ[ G ] i o} {f : W⟶W i o′} {g : TyGate G W⟶W} → c ⫅[ g ] f → c ⫉[ g ] f
⫅⇒⫉ {c} {f} {g} c⫅f = refl⫉ refl h
  where  h : ∀ {w w′} → w ≋ w′ → ⟦ c ⟧[ g ] w ≋ f w′
         h w≋w′ rewrite ≋⇒≡ w≋w′ = c⫅f _
\end{code}
%</implements-semihet-to-eq>



\begin{code}
module WithGates {G} (g : TyGate G W⟶W) where
\end{code}

%<*implements-pointwise-at-with-gates>
\AgdaTarget{\_⫃\_at\_}
\begin{code}
 _⫃_at_ : (c : ℂ[ G ] i o) (f : W⟶W i o) → (W i → Set)
 _⫃_at_ = _⫃[ g ]_at_
\end{code}
%</implements-pointwise-at-with-gates>

\begin{code}
 infixl 3 _⫃_
\end{code}

%<*implements-pointwise-with-gates>
\AgdaTarget{\_⫃\_}
\begin{code}
 _⫃_ : (c : ℂ[ G ] i o) (f : W⟶W i o) → Set
 _⫃_ = _⫃[ g ]_
\end{code}
%</implements-pointwise-with-gates>

%<*implements-pointwise-c-at-with-gates>
\AgdaTarget{\_⫃ᶜ\_at\_}
\begin{code}
\end{code}
%</implements-pointwise-c-at-with-gates>

\begin{code}
--infixl 3 _⫃ᶜ_
\end{code}

%<*implements-pointwise-c-with-gates>
\AgdaTarget{\_⫃ᶜ\_}
\begin{code}
\end{code}
%</implements-pointwise-c-with-gates>

%<*implements-pointwise-omega-at-with-gates>
\AgdaTarget{\_⫃ω\_at\_}
\begin{code}
\end{code}
%</implements-pointwise-omega-at-with-gates>

\begin{code}
--infixl 3 _⫃ω_
\end{code}

%<*implements-pointwise-omega-with-gates>
\AgdaTarget{\_⫃ω\_}
\begin{code}
\end{code}
%</implements-pointwise-omega-with-gates>


%<*implements-semihet-at-with-gates>
\AgdaTarget{\_⫅\_at\_}
\begin{code}
 _⫅_at_ : (c : ℂ[ G ] {σ} i o) (f : W⟶W i o′) → (W i → Set)
 _⫅_at_ = _⫅[ g ]_at_
\end{code}
%</implements-semihet-at-with-gates>

\begin{code}
 infixl 3 _⫅_
\end{code}

%<*implements-semihet-with-gates>
\AgdaTarget{\_⫅\_}
\begin{code}
 _⫅_ : (c : ℂ[ G ] {σ} i o) (f : W⟶W i o′) → Set
 _⫅_ = _⫅[ g ]_
\end{code}
%</implements-semihet-with-gates>

%<*implements-semihet-c-at-with-gates>
\AgdaTarget{\_⫅ᶜ\_at\_}
\begin{code}
\end{code}
%</implements-semihet-c-at-with-gates>

\begin{code}
--infixl 3 _⫅ᶜ_
\end{code}

%<*implements-semihet-c-with-gates>
\AgdaTarget{\_⫅ᶜ\_}
\begin{code}
\end{code}
%</implements-semihet-c-with-gates>

%<*implements-semihet-omega-at-with-gates>
\AgdaTarget{\_⫅ω\_at\_}
\begin{code}
\end{code}
%</implements-semihet-omega-at-with-gates>

\begin{code}
--infixl 3 _⫅ω_
\end{code}

%<*implements-semihet-omega-with-gates>
\AgdaTarget{\_⫅ω\_}
\begin{code}
\end{code}
%</implements-semihet-omega-with-gates>


%<*implements-preserve-semihet-at-with-gates>
\AgdaTarget{\_⫇\_at\_/\_}
\begin{code}
 _⫇_at_/_ : (c : ℂ[ G ] i o) (f : W⟶W i′ o′) → (W i → W i′ → Set)
 _⫇_at_/_ = _⫇[ g ]_at_/_
\end{code}
%</implements-preserve-semihet-at-with-gates>

\begin{code}
 infixl 3 _⫇_
\end{code}

%<*implements-preserve-semihet-with-gates>
\AgdaTarget{\_⫇\_}
\begin{code}
 _⫇_ : (c : ℂ[ G ] i o) (f : W⟶W i′ o′) → Set
 _⫇_ = _⫇[ g ]_
\end{code}
%</implements-preserve-semihet-with-gates>

%<*implements-preserve-semihet-c-at-with-gates>
\AgdaTarget{\_⫇ᶜ\_at\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-c-at-with-gates>

\begin{code}
--infixl 3 _⫇ᶜ_
\end{code}

%<*implements-preserve-semihet-c-with-gates>
\AgdaTarget{\_⫇ᶜ\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-c-with-gates>

%<*implements-preserve-semihet-omega-at-with-gates>
\AgdaTarget{\_⫇ω\_at\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-omega-at-with-gates>

\begin{code}
--infixl 3 _⫇ω_
\end{code}

%<*implements-preserve-semihet-omega-with-gates>
\AgdaTarget{\_⫇ω\_}
\begin{code}
\end{code}
%</implements-preserve-semihet-omega-with-gates>


\begin{code}
 infixl 3 _⫉_
\end{code}

%<*implements-with-gates>
\AgdaTarget{\_⫉\_}
\begin{code}
 _⫉_ : (c : ℂ[ G ] i o) (f : W⟶W i′ o′) → Set
 _⫉_ = _⫉[ g ]_
\end{code}
%</implements-with-gates>
