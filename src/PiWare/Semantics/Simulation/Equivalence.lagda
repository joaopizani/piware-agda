\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Equivalence (A : Atomic) where

open import Function.Base using (_∘_; _∘′_)
open import Data.Nat.Base using (ℕ)
open import Data.Product.Base using (_×_; uncurry′)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
open import Codata.Musical.Stream using (Stream) renaming (_≈_ to _≈ₛ_)

open import Relation.Binary.Indexed.Heterogeneous.Structures using () renaming (IsIndexedEquivalence to IsEquivalence)
open import Relation.Binary.Indexed.Heterogeneous.Bundles using () renaming (IndexedSetoid to Setoid)
open import Data.Vec.Relation.Binary.Equality.Propositional using (_≋_; ≋-refl; ≋-sym; ≋-trans; ≡⇒≋; ≋⇒≡; length-equal)

open import Codata.Musical.Stream.Causal using (Γᶜ)

open import PiWare.Circuit using (ℂ[_]; ω)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W; W⟶W; ⟦_⟧[_]; ⟦_⟧ᶜ[_]; ⟦_⟧ω[_])
open import PiWare.Semantics.Simulation.Compliance A using (_⫃[_]_at_; _⫃[_]_; _⫅[_]_at_; _⫅[_]_; _⫇[_]_at_/_; _⫇[_]_; _⫉[_]_)
open Atomic A using (default)

open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o; i₁; o₁; i₂; o₂; i′; o′)
open import PiWare.Circuit using (s)
\end{code}

\begin{code}
private variable i₃ o₃ : ℕ
\end{code}


%<*reindex-setoid>
\AgdaTarget{reindex-setoid}
\begin{code}
reindex-setoid : ∀ {ℓⁱ ℓʲ ℓ₁ ℓ₂} {I : Set ℓⁱ} {J : Set ℓʲ} → Setoid I ℓ₁ ℓ₂ → (J → I) → Setoid J ℓ₁ ℓ₂
reindex-setoid s f = record  { Carrier        = Carrier₀ ∘′ f
                             ; _≈_            = _≈₀_
                             ; isEquivalence  = record { refl = refl₀; sym = sym₀; trans = trans₀ } }
\end{code}
%</reindex-setoid>
\begin{code}
  where  open Setoid s using () renaming (isEquivalence to isEquiv₀; Carrier to Carrier₀; _≈_ to _≈₀_)
         open IsEquivalence isEquiv₀ using () renaming (refl to refl₀; sym to sym₀; trans to trans₀)
\end{code}



%<*eq-pointwise-at>
\AgdaTarget{\_≗[\_]\_at\_}
\begin{code}
_≗[_]_at_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i o) → (W i → Set)
c₁ ≗[ g ] c₂ at w = c₁ ⫃[ g ] ⟦ c₂ ⟧[ g ] at w
\end{code}
%</eq-pointwise-at>

\begin{code}
infix 3 _≗[_]_
\end{code}

%<*eq-pointwise>
\AgdaTarget{\_≗[\_]\_}
\begin{code}
_≗[_]_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i o) → Set
c₁ ≗[ g ] c₂ = c₁ ⫃[ g ] ⟦ c₂ ⟧[ g ]
\end{code}
%</eq-pointwise>


%<*eq-pointwise-c-at>
\AgdaTarget{\_≗ᶜ[\_]\_at\_}
\begin{code}
_≗ᶜ[_]_at_ : (c₁ : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] {s} i o) → (Γᶜ (W i) → Set)
c₁ ≗ᶜ[ g ] c₂ at γ = ⟦ c₁ ⟧ᶜ[ g ] γ ≡ ⟦ c₂ ⟧ᶜ[ g ] γ
\end{code}
%</eq-pointwise-c-at>

\begin{code}
infix 3 _≗ᶜ[_]_
\end{code}

%<*eq-pointwise-c>
\AgdaTarget{\_≗ᶜ[\_]\_}
\begin{code}
_≗ᶜ[_]_ : (c₁ : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] {s} i o) → Set
c₁ ≗ᶜ[ g ] c₂ = ∀ γ → c₁ ≗ᶜ[ g ] c₂ at γ
\end{code}
%</eq-pointwise-c>


%<*eq-pointwise-omega-at>
\AgdaTarget{\_≗ω[\_]\_at\_}
\begin{code}
_≗ω[_]_at_ : (c₁ : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] {s} i o) → (Stream (W i) → Set)
c₁ ≗ω[ g ] c₂ at ins = ⟦ c₁ ⟧ω[ g ] ins ≈ₛ ⟦ c₂ ⟧ω[ g ] ins
\end{code}
%</eq-pointwise-omega-at>

\begin{code}
infix 3 _≗ω[_]_
\end{code}

%<*eq-pointwise-omega>
\AgdaTarget{\_≗ω[\_]\_}
\begin{code}
_≗ω[_]_ : (c₁ : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i o) → Set
c₁ ≗ω[ g ] c₂ = ∀ ins → c₁ ≗ω[ g ] c₂ at ins
\end{code}
%</eq-pointwise-omega>



%<*eq-semihet-at>
\AgdaTarget{\_≅[\_]\_at\_}
\begin{code}
_≅[_]_at_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i o′) → (W i → Set)
c₁ ≅[ g ] c₂ at w = c₁ ⫅[ g ] ⟦ c₂ ⟧[ g ] at w
\end{code}
%</eq-semihet-at>

\begin{code}
infix 3 _≅[_]_
\end{code}

%<*eq-semihet>
\AgdaTarget{\_≅[\_]\_}
\begin{code}
_≅[_]_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i o′) → Set
c₁ ≅[ g ] c₂ = c₁ ⫅[ g ] ⟦ c₂ ⟧[ g ]
\end{code}
%</eq-semihet>


%<*eq-semihet-c-at>
\AgdaTarget{\_≅ᶜ[\_]\_at\_}
\begin{code}
_≅ᶜ[_]_at_ : (c₁ : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] {s} i o′) → (Γᶜ (W i) → Set)
c₁ ≅ᶜ[ g ] c₂ at γ = ⟦ c₁ ⟧ᶜ[ g ] γ ≋ ⟦ c₂ ⟧ᶜ[ g ] γ
\end{code}
%</eq-semihet-c-at>

\begin{code}
infix 3 _≅ᶜ[_]_
\end{code}

%<*eq-semihet-c>
\AgdaTarget{\_≅ᶜ[\_]\_}
\begin{code}
_≅ᶜ[_]_ : (c₁ : ℂ[ G ] {s} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] {s} i o′) → Set
c₁ ≅ᶜ[ g ] c₂ = ∀ γ → c₁ ≅ᶜ[ g ] c₂ at γ
\end{code}
%</eq-semihet-c>


%<*eq-semihet-omega-at>
\AgdaTarget{\_≅ᶜ[\_]\_at\_}
\begin{code}
_≅ω[_]_at_ : (c₁ : ℂ[ G ] {ω} i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] {ω} i o′) → (Stream (W i) → Set)
c₁ ≅ω[ g ] c₂ at ins = ⊥ where postulate ⊥ : _  -- ⟦ c₁ ⟧ω[ g ] ins ≈ₛ {- TODO -} ⟦ c₂ ⟧ω[ g ] ins
\end{code}
%</eq-semihet-omega-at>

\begin{code}
infix 3 _≅ω[_]_
\end{code}

%<*eq-semihet-omega>
\AgdaTarget{\_≅ω[\_]\_}
\begin{code}
_≅ω[_]_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i o′) → Set
c₁ ≅ω[ g ] c₂ = ∀ ins → c₁ ≅ω[ g ] c₂ at ins
\end{code}
%</eq-semihet-omega>



%<*eq-preserve-semihet-at>
\AgdaTarget{\_≊[\_]\_at\_/\_}
\begin{code}
_≊[_]_at_/_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i′ o′) → (W i → W i′ → Set)
c₁ ≊[ g ] c₂ at w / w′ = c₁ ⫇[ g ] ⟦ c₂ ⟧[ g ] at w / w′
\end{code}
%</eq-preserve-semihet-at>

\begin{code}
infix 3 _≊[_]_
\end{code}

%<*eq-preserve-semihet>
\AgdaTarget{\_≊[\_]\_}
\begin{code}
_≊[_]_ : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i′ o′) → Set
c₁ ≊[ g ] c₂ = c₁ ⫇[ g ] ⟦ c₂ ⟧[ g ]
\end{code}
%</eq-preserve-semihet>


%<*eq-preserve-semihet-c-at>
\AgdaTarget{\_≊ᶜ[\_]\_at\_/\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-c-at>

\begin{code}
\end{code}
infix 3 _≊ᶜ[_]_

%<*eq-preserve-semihet-c>
\AgdaTarget{\_≊[\_]\_}
\begin{code}
\end{code}
_≊ᶜ[_]_ : ℂ[ G ] {ω} i o → TyGate G W⟶W → ℂ[ G ] {ω} i′ o′ → Set
c₁ ≊ᶜ[ g ] c₂ = ∀ {w w′} → w ≋ w′ → ⟦ c₁ ⟧ᶜ[ g ] w ≋ ⟦ c₂ ⟧ᶜ[ g ] w′
%</eq-preserve-semihet-c>


%<*eq-preserve-semihet-omega-at>
\AgdaTarget{\_≊ω[\_]\_at\_/\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-omega-at>

\begin{code}
\end{code}
infix 3 _≊ω[_]_

%<*eq-preserve-semihet-omega>
\AgdaTarget{\_≊ω[\_]\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-omega>



\begin{code}
module ≊-unsound where
\end{code}
%<*eq-preserve-semihet-unsound>
\AgdaTarget{≊-unsound}
\begin{code}
 ≊-unsound : (g : TyGate G W⟶W) (c₁ : ℂ[ G ] 0 0) (c₂ : ℂ[ G ] 1 1) → c₁ ≊[ g ] c₂
 ≊-unsound _ _ _ ()
\end{code}
%</eq-preserve-semihet-unsound>



\begin{code}
infix 3 _≋[_]_
\end{code}

%<*eq>
\AgdaTarget{\_≋[\_]\_}
\begin{code}
data _≋[_]_ {i o i′ o′ G} : (c₁ : ℂ[ G ] i o) (g : TyGate G W⟶W) (c₂ : ℂ[ G ] i′ o′) → Set where
  refl≋ : {c₁ : ℂ[ G ] i o} {c₂ : ℂ[ G ] i′ o′} {g : TyGate G W⟶W} (i≡ : i ≡ i′) → c₁ ≊[ g ] c₂ → c₁ ≋[ g ] c₂
\end{code}
%</eq>


%<*eq-i-equal>
\AgdaTarget{≋⇒i≡}
\begin{code}
≋⇒i≡ : {c₁ : ℂ[ G ] i o} {c₂ : ℂ[ G ] i′ o′} {g : TyGate G W⟶W} → c₁ ≋[ g ] c₂ → i ≡ i′
≋⇒i≡ (refl≋ refl _) = refl
\end{code}
%</eq-i-equal>

%<*eq-o-equal>
\AgdaTarget{≋⇒o≡}
\begin{code}
≋⇒o≡ : {c₁ : ℂ[ G ] i o} {c₂ : ℂ[ G ] i′ o′} {g : TyGate G W⟶W} → c₁ ≋[ g ] c₂ → o ≡ o′
≋⇒o≡ {i} {o} (refl≋ refl c₁≊c₂) = length-equal {A = W o} (c₁≊c₂ {w = dummy} ≋-refl)
  where postulate dummy : W i
\end{code}
%</eq-o-equal>


%<*semihet-to-eq>
\AgdaTarget{≅⇒≋}
\begin{code}
≅⇒≋ : {c₁ : ℂ[ G ] i o} {c₂ : ℂ[ G ] i o′} {g : TyGate G W⟶W} → c₁ ≅[ g ] c₂ → c₁ ≋[ g ] c₂
≅⇒≋ {c₁} {c₂} {g} c₁≅c₂ = refl≋ refl f
  where  f : ∀ {w₁ w₂} → w₁ ≋ w₂ → ⟦ c₁ ⟧[ g ] w₁ ≋ ⟦ c₂ ⟧[ g ] w₂
         f w₁≋w₂ rewrite ≋⇒≡ w₁≋w₂ = c₁≅c₂ _
\end{code}
%</semihet-to-eq>


%<*eq-refl>
\AgdaTarget{≋c-refl}
\begin{code}
≋c-refl : {c : ℂ[ G ] i o} {g : TyGate G W⟶W} → c ≋[ g ] c
≋c-refl = ≅⇒≋ (λ _ → ≋-refl)
\end{code}
%</eq-refl>

%<*eq-sym>
\AgdaTarget{≋c-sym}
\begin{code}
≋c-sym : {c₁ : ℂ[ G ] i o} {c₂ : ℂ[ G ] i′ o′} {g : TyGate G W⟶W} → c₁ ≋[ g ] c₂ → c₂ ≋[ g ] c₁
≋c-sym (refl≋ refl c₁≊c₂) = refl≋ refl (≋-sym ∘ c₁≊c₂ ∘ ≋-sym)
\end{code}
%</eq-sym>

%<*eq-trans>
\AgdaTarget{≋c-trans}
\begin{code}
≋c-trans :  {c₁ : ℂ[ G ] i₁ o₁} {c₂ : ℂ[ G ] i₂ o₂} {c₃ : ℂ[ G ] i₃ o₃} {g : TyGate G W⟶W}
            → c₁ ≋[ g ] c₂ → c₂ ≋[ g ] c₃ → c₁ ≋[ g ] c₃
≋c-trans (refl≋ refl c₁≊c₂) (refl≋ refl c₂≊c₃) = ≅⇒≋ (λ _ → ≋-trans (c₁≊c₂ ≋-refl) (c₂≊c₃ ≋-refl))
\end{code}
%</eq-trans>


%<*eq-isEquivalence>
\AgdaTarget{≋c-isEquivalence}
\begin{code}
≋c-isEquivalence : {g : TyGate G W⟶W} → IsEquivalence (uncurry′ ℂ[ G ]) _≋[ g ]_
≋c-isEquivalence = record  { refl   = ≋c-refl
                           ; sym    = ≋c-sym
                           ; trans  = ≋c-trans }
\end{code}
%</eq-isEquivalence>


%<*eq-setoid>
\AgdaTarget{≋c-setoid[\_]}
\begin{code}
≋c-setoid[_] : (g : TyGate G W⟶W) → Setoid (ℕ × ℕ) _ _
≋c-setoid[_] {G} g = record  { Carrier        = uncurry′ ℂ[ G ]
                             ; _≈_            = _≋[ g ]_
                             ; isEquivalence  = ≋c-isEquivalence }
\end{code}
%</eq-setoid>


%<*reindex-setoid-eq>
\AgdaTarget{reindex-setoid-≋c[\_]}
\begin{code}
reindex-setoid-≋c[_] : ∀ {ℓʲ} {J : Set ℓʲ} {G} (g : TyGate G W⟶W) (f : J → ℕ × ℕ) → Setoid J _ _
reindex-setoid-≋c[ g ] = reindex-setoid ≋c-setoid[ g ]
\end{code}
%</reindex-setoid-eq>


%<*eq-reasoning>
\begin{code}
import Relation.Binary.Indexed.EqReasoning as IdxEqReasoning
module ≋c-Reasoning[_] {G} (g : TyGate G W⟶W) = IdxEqReasoning ≋c-setoid[ g ] renaming (_≈⟨_⟩_ to _≋⟨_⟩_)
\end{code}
%</eq-reasoning>



\begin{code}
module WithGates {G} (g : TyGate G W⟶W) where
\end{code}

%<*eq-pointwise-at-with-gates>
\AgdaTarget{\_≗\_at\_}
\begin{code}
 _≗_at_ : (c₁ c₂ : ℂ[ G ] i o) → (W i → Set)
 _≗_at_ = _≗[ g ]_at_
\end{code}
%</eq-pointwise-at-with-gates>

\begin{code}
 infix 3 _≗_
\end{code}

%<*eq-pointwise-with-gates>
\AgdaTarget{\_≗\_}
\begin{code}
 _≗_ : (c₁ c₂ :  ℂ[ G ] i o) → Set
 _≗_ = _≗[ g ]_
\end{code}
%</eq-pointwise-with-gates>

%<*eq-pointwise-c-at-with-gates>
\AgdaTarget{\_≗ᶜ\_at\_}
\begin{code}
\end{code}
%</eq-pointwise-c-at-with-gates>

\begin{code}
--infix 3 _≗ᶜ_
\end{code}

%<*eq-pointwise-c-with-gates>
\AgdaTarget{\_≗ᶜ\_}
\begin{code}
\end{code}
%</eq-pointwise-c-with-gates>

%<*eq-pointwise-omega-at-with-gates>
\AgdaTarget{\_≗ω\_at\_}
\begin{code}
\end{code}
%</eq-pointwise-omega-at-with-gates>

\begin{code}
--infix 3 _≗ω_
\end{code}

%<*eq-pointwise-omega-with-gates>
\AgdaTarget{\_≗ω\_}
\begin{code}
\end{code}
%</eq-pointwise-omega-with-gates>


%<*eq-semihet-at-with-gates>
\AgdaTarget{\_≅\_at\_}
\begin{code}
 _≅_at_ : (c₁ : ℂ[ G ] i o) (c₂ : ℂ[ G ] i o′) → (W i → Set)
 _≅_at_ = _≅[ g ]_at_
\end{code}
%</eq-semihet-at-with-gates>

\begin{code}
 infix 3 _≅_
\end{code}

%<*eq-semihet-with-gates>
\AgdaTarget{\_≅\_}
\begin{code}
 _≅_ : (c₁ : ℂ[ G ] i o) (c₂ : ℂ[ G ] i o′) → Set
 _≅_ = _≅[ g ]_
\end{code}
%</eq-semihet-with-gates>

%<*eq-semihet-c-at-with-gates>
\AgdaTarget{\_≅ᶜ\_at\_}
\begin{code}
\end{code}
%</eq-semihet-c-at-with-gates>

\begin{code}
\end{code}
infix 3 _≅ᶜ_

%<*eq-semihet-c-with-gates>
\AgdaTarget{\_≅ᶜ\_}
\begin{code}
\end{code}
%</eq-semihet-c-with-gates>

%<*eq-semihet-omega-at-with-gates>
\AgdaTarget{\_≅ω\_at\_}
\begin{code}
\end{code}
%</eq-semihet-omega-at-with-gates>

\begin{code}
\end{code}
infix 3 _≅ω_

%<*eq-semihet-omega-with-gates>
\AgdaTarget{\_≅ω\_}
\begin{code}
\end{code}
%</eq-semihet-omega-with-gates>


%<*eq-preserve-semihet-at-with-gates>
\AgdaTarget{\_≊\_at\_/\_}
\begin{code}
 _≊_at_/_ : (c₁ : ℂ[ G ] i o) (c₂ : ℂ[ G ] i′ o′) → (W i → W i′ → Set)
 _≊_at_/_ = _≊[ g ]_at_/_
\end{code}
%</eq-preserve-semihet-at-with-gates>

\begin{code}
 infix 3 _≊_
\end{code}

%<*eq-preserve-semihet-with-gates>
\AgdaTarget{\_≊\_}
\begin{code}
 _≊_ : (c₁ : ℂ[ G ] i o) (c₂ : ℂ[ G ] i′ o′) → Set
 _≊_ = _≊[ g ]_
\end{code}
%</eq-preserve-semihet-with-gates>

%<*eq-preserve-semihet-c-at-with-gates>
\AgdaTarget{\_≊ᶜ\_at\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-c-at-with-gates>

\begin{code}
\end{code}
infix 3 _≊ᶜ_

%<*eq-preserve-semihet-c-with-gates>
\AgdaTarget{\_≊ᶜ\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-c-with-gates>

%<*eq-preserve-semihet-omega-at-with-gates>
\AgdaTarget{\_≊ω\_at\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-omega-at-with-gates>

\begin{code}
\end{code}
infix 3 _≊ω_

%<*eq-preserve-semihet-omega-with-gates>
\AgdaTarget{\_≊ω\_}
\begin{code}
\end{code}
%</eq-preserve-semihet-omega-with-gates>


\begin{code}
 infix 3 _≋c_
\end{code}

%<*eq-with-gates>
\AgdaTarget{\_≋c\_}
\begin{code}
 _≋c_ : (c₁ : ℂ[ G ] i o) (c₂ : ℂ[ G ] i′ o′) → Set
 _≋c_ = _≋[ g ]_
\end{code}
%</eq-with-gates>

%<*eq-setoid>
\AgdaTarget{≋c-setoid}
\begin{code}
 ≋c-setoid : Setoid (ℕ × ℕ) _ _
 ≋c-setoid = record  { Carrier        = uncurry′ ℂ[ G ]
                     ; _≈_            = _≋c_
                     ; isEquivalence  = ≋c-isEquivalence }
\end{code}
%</eq-setoid>

%<*reindex-setoid-eq-with-gates>
\AgdaTarget{reindex-setoid-≋c}
\begin{code}
 reindex-setoid-≋c : ∀ {ℓʲ} {J : Set ℓʲ} (f : J → ℕ × ℕ) → Setoid J _ _
 reindex-setoid-≋c = reindex-setoid ≋c-setoid
\end{code}
%</reindex-setoid-eq-with-gates>

%<*eq-reasoning-with-gates>
\AgdaTarget{\_≋⟨\_⟩\_,\_≡⟨\_⟩\_,\_≡⟨⟩\_; begin\_; \_∎}
\begin{code}
 open ≋c-Reasoning[_] g using (begin_; _∎; _≡⟨_⟩_; _≡⟨⟩_; _≋⟨_⟩_) public
\end{code}
%</eq-reasoning-with-gates>
