\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Semigroup.Base (A : Atomic) where

open import PiWare.Gates using (Gates)
open import PiWare.Plugs using (id⤨₁)
open import PiWare.Circuit using (C[_]; _⟫_; _∥_)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W⟶W)
open import PiWare.Semantics.Simulation.Equivalence A using (_≋[_]_)

open import PiWare.Gates using (G)
\end{code}


%<*C₂[_]>
\AgdaTarget{C₂[\_]}
\begin{code}
C₂[_] : Gates → Set
C₂[ G ] = C[ G ] 2 1
\end{code}
%</C₂[_]>


%<*IsSemigroupC2>
\AgdaTarget{IsSemigroupC₂}
\begin{code}
IsSemigroupC₂[_] : (g : TyGate G W⟶W) → C₂[ G ] → Set
IsSemigroupC₂[ g ] c = c ∥ id⤨₁ ⟫ c ≋[ g ] id⤨₁ ∥ c ⟫ c
\end{code}
%</IsSemigroupC2>


%<*SemigroupC2>
\AgdaTarget{SemigroupC₂}
\begin{code}
record SemigroupC₂ {G} (g : TyGate G W⟶W) : Set where
  field  ⊕C           : C₂[ G ]
         isSemigroup  : IsSemigroupC₂[ g ] ⊕C
\end{code}
%</SemigroupC2>
