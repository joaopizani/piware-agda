\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Semigroup.Op2 (A : Atomic) where

open import Function.Base using (const)
open import Algebra.Core using (Op₂)
open import Algebra.Structures using (IsSemigroup)
open import Data.Vec.Base using (Vec; []; _∷_; [_])
open import Data.Vec.Relation.Binary.Equality.Propositional using (_≋_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)

open Atomic A using (Atom)
open import PiWare.Gates using (Gates)
open import PiWare.Circuit using (Gate; _⟫_; _∥_)
open import PiWare.Plugs using (id⤨₁)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W⟶W)
open import PiWare.Semantics.Simulation.Semigroup.Base A using (C₂[_]; IsSemigroupC₂[_]; SemigroupC₂)
open import PiWare.Semantics.Simulation.Equivalence A using (_≅[_]_; ≅⇒≋)

open import Notation.JP.Base using (α; n)
\end{code}


\begin{code}
W₂⟶W₁ = W⟶W 2 1
\end{code}

%<*Op2-Gate-identifier>
\AgdaTarget{Gate#⊕}
\begin{code}
data Gate#⊕ : Set where ⊕' : Gate#⊕
\end{code}
%</Op2-Gate-identifier>

%<*Op2Gates>
\AgdaTarget{Op₂Gates}
\begin{code}
Op₂Gates : Gates
Op₂Gates = record { Gate# = Gate#⊕; #in = const 2; #out = const 1 }
\end{code}
%</Op2Gates>

%<*Op2W2toW1>
\AgdaTarget{Op₂W₂⟶W₁}
\begin{code}
Op₂W₂⟶W₁ : Op₂ Atom → W₂⟶W₁
Op₂W₂⟶W₁ _⊕_ (x ∷ y ∷ []) = [_] (_⊕_ x y)
\end{code}
%</Op2W2toW1>

%<*Op2TyGate>
\AgdaTarget{Op₂TyGate}
\begin{code}
Op₂TyGate : Op₂ Atom → TyGate Op₂Gates W⟶W
Op₂TyGate _⊕_ = const (Op₂W₂⟶W₁ _⊕_)
\end{code}
%</Op2TyGate>



%<*Op2C>
\AgdaTarget{⊕C}
\begin{code}
⊕C : C₂[ Op₂Gates ]
⊕C = Gate ⊕'
\end{code}
%</Op2C>


\begin{code}
postulate []-cong  : _≋_ {A = α} [] []
postulate ∷-cong   : {x y : α} {xs′ ys′ : Vec α n} → x ≡ y → xs′ ≋ ys′ → x ∷ xs′ ≋ y ∷ ys′
\end{code}


\begin{code}
module _ (_⊕_ : Op₂ Atom) where
\end{code}

%<*Op2IsSemigroupC2-VecEq>
\AgdaTarget{Op₂IsSemigroupC₂≅}
\begin{code}
 Op₂IsSemigroupC₂≅ : ⦃ sg : IsSemigroup _≡_ _⊕_ ⦄ → ⊕C ∥ id⤨₁ ⟫ ⊕C ≅[ Op₂TyGate _⊕_ ] id⤨₁ ∥ ⊕C ⟫ ⊕C
 Op₂IsSemigroupC₂≅ ⦃ sg ⦄ (x ∷ y ∷ z ∷ []) = ∷-cong (assoc x y z) []-cong
   where open IsSemigroup sg using (assoc)
\end{code}
%</Op2IsSemigroupC2-VecEq>

%<*Op2IsSemigroupC2>
\AgdaTarget{Op₂IsSemigroupC₂}
\begin{code}
 Op₂IsSemigroupC₂ : ⦃ sg : IsSemigroup _≡_ _⊕_ ⦄ → IsSemigroupC₂[ Op₂TyGate _⊕_ ] ⊕C
 Op₂IsSemigroupC₂ ⦃ sg ⦄ = ≅⇒≋ (Op₂IsSemigroupC₂≅ ⦃ sg ⦄)
\end{code}
%</Op2IsSemigroupC2>
 
%<*Op2SemigroupC2>
\AgdaTarget{Op₂SemigroupC₂}
\begin{code}
 Op₂SemigroupC₂ : ⦃ sg : IsSemigroup _≡_ _⊕_ ⦄ → SemigroupC₂ (Op₂TyGate _⊕_)
 Op₂SemigroupC₂ ⦃ sg ⦄ = record { ⊕C = ⊕C; isSemigroup = Op₂IsSemigroupC₂ ⦃ sg ⦄ }
\end{code}
%</Op2SemigroupC2>
