\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Properties (A : Atomic) where

open import Function.Base using (_∘_)
open import Data.Nat.Base using (_+_)
open import Data.Vec.Base using (Vec; splitAt; _++_)
open import Data.Nat.Properties using (+-assoc)
open import Data.Vec.Extra using (₁; ₂′)
open import Data.Vec.Properties.Extra using (proj₁∘splitAt-last≋; ++-assoc-split₁; ++-assoc-split₂; ++-assoc-split₃; splitAt++; ++-assoc)
open import Data.Vec.Relation.Binary.Equality.Propositional using (_≋_; ≋⇒≡; ≡⇒≋; ≋-refl; ≋-sym)

open import Relation.Binary.PropositionalEquality using (_≡_; refl; subst; sym; cong; setoid; module ≡-Reasoning)
open ≡-Reasoning

open import PiWare.Circuit using (ℂ[_]; _⟫_; _∥_)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Plugs using (id⤨; nil⤨₀)
open import PiWare.Patterns.Id using (adaptEqI; adaptEqO; adaptEqIO)
open import PiWare.Semantics.Simulation A using (W⟶W; ⟦_⟧[_])
open import PiWare.Semantics.Simulation.Equivalence A using (_≅[_]_; _≊[_]_; _≋[_]_; refl≋; ≅⇒≋; ≋c-refl)
open import PiWare.Semantics.Simulation.Properties.Plugs A using (id⤨⫃id[_]; id⤨⫅id[_])

open import Notation.JP.Base using (n)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; i₁; i₂; i₃; i′; i₁′; i₂′; o; o₁; o₂; o₃; o′; o₁′; o₂′; m; m₁; m₂)
\end{code}


%<*adaptEqI-noop>
\AgdaTarget{adaptEqI-noop}
\begin{code}
adaptEqI-noop : {g : TyGate G W⟶W} {i≡ : i ≡ i′} (c : ℂ[ G ] i o) → adaptEqI i≡ c ≋[ g ] c
adaptEqI-noop {i≡} c rewrite i≡ = ≋c-refl
\end{code}
%</adaptEqI-noop>


%<*adaptEqO-noop>
\AgdaTarget{adaptEqO-noop}
\begin{code}
adaptEqO-noop : {g : TyGate G W⟶W} {o≡ : o ≡ o′} (c : ℂ[ G ] i o) → adaptEqO o≡ c ≋[ g ] c
adaptEqO-noop {o≡} c rewrite o≡ = ≋c-refl
\end{code}
%</adaptEqO-noop>


%<*adaptEqIO-noop>
\AgdaTarget{adaptEqIO-noop}
\begin{code}
adaptEqIO-noop : {g : TyGate G W⟶W} {i≡ : i ≡ i′} {o≡ : o ≡ o′} (c : ℂ[ G ] i o) → adaptEqIO i≡ o≡ c ≋[ g ] c
adaptEqIO-noop {i≡} {o≡} c rewrite i≡ | o≡ = ≋c-refl
\end{code}
%</adaptEqIO-noop>


\begin{code}
infixl 4 _⟫-cong_
\end{code}

%<*seq-cong>
\AgdaTarget{⟫-cong}
\begin{code}
_⟫-cong_ :  {g : TyGate G W⟶W} {c₁ : ℂ[ G ] i₁ m₁} {d₁ : ℂ[ G ] m₁ o₁} {c₂ : ℂ[ G ] i₂ m₂} {d₂ : ℂ[ G ] m₂ o₂}
            → c₁ ≋[ g ] c₂ → d₁ ≋[ g ] d₂ → (c₁ ⟫ d₁) ≋[ g ] (c₂ ⟫ d₂)
(refl≋ refl c₁≊c₂) ⟫-cong (refl≋ refl d₁≊d₂) = ≅⇒≋ (d₁≊d₂ ∘ c₁≊c₂ ∘ λ _ → ≋-refl)
\end{code}
%</seq-cong>


%<*seq-right-identity>
\AgdaTarget{⟫-right-identity}
\begin{code}
⟫-right-identity : {g : TyGate G W⟶W} (c : ℂ[ G ] i o) → c ⟫ id⤨ ≋[ g ] c
⟫-right-identity {g} c = ≅⇒≋ (id⤨⫅id[ g ] ∘ ⟦ c ⟧[ g ])
\end{code}
%</seq-right-identity>


%<*seq-left-identity>
\AgdaTarget{⟫-left-identity}
\begin{code}
⟫-left-identity : {g : TyGate G W⟶W} (c : ℂ[ G ] i o) → id⤨ ⟫ c ≋[ g ] c
⟫-left-identity {g} c = ≅⇒≋ (≡⇒≋ ∘ cong ⟦ c ⟧[ g ] ∘ id⤨⫃id[ g ])
\end{code}
%</seq-left-identity>


%<*seq-assoc>
\AgdaTarget{⟫-assoc}
\begin{code}
⟫-assoc : {g : TyGate G W⟶W} (c₁ : ℂ[ G ] i m) (c₂ : ℂ[ G ] m n) (c₃ : ℂ[ G ] n o) → (c₁ ⟫ c₂) ⟫ c₃ ≋[ g ] c₁ ⟫ (c₂ ⟫ c₃)
⟫-assoc c₁ c₂ c₃ = ≅⇒≋ (≡⇒≋ ∘ λ _ → refl)
\end{code}
%</seq-assoc>


\begin{code}
infixr 5 _∥-cong_
\end{code}
%<*par-cong>

\AgdaTarget{_∥-cong_}
\begin{code}
postulate _∥-cong_ :  {g : TyGate G W⟶W} {c₁ : ℂ[ G ] i₁ o₁} {d₁ : ℂ[ G ] i₁′ o₁′} {c₂ : ℂ[ G ] i₂ o₂} {d₂ : ℂ[ G ] i₂′ o₂′}
                      → c₁ ≋[ g ] c₂ → d₁ ≋[ g ] d₂ → (c₁ ∥ d₁) ≋[ g ] (c₂ ∥ d₂)
\end{code}
(refl≋ refl c₁≊c₂) ∥-cong (refl≋ refl d₁≊d₂) = ≅⇒≋ (λ v → ++-cong (c₁≊c₂ (≋-refl _)) (d₁≊d₂ (≋-refl _)))
  where postulate ++-cong : ∀ {ℓ n m} {α : Set ℓ} {xs xs′ : Vec α n} {ys ys′ : Vec α m} → xs ≋ xs′ → ys ≋ ys′ → xs ++ ys ≋ xs′ ++ ys′
%</par-cong>


%<*par-left-identity>
\AgdaTarget{∥-left-identity}
\begin{code}
∥-left-identity : {g : TyGate G W⟶W} (c : ℂ[ G ] i o) → nil⤨₀ ∥ c ≋[ g ] c
∥-left-identity _ = ≅⇒≋ (λ _ → ≋-refl)
\end{code}
%</par-left-identity>


%<*par-right-identity-prime>
\AgdaTarget{∥-right-identity≊}
\begin{code}
\end{code}
∥-right-identity≊ : {g : TyGate G W⟶W} (c : ℂ[ G ] i o) → c ∥ nil⤨₀ ≊[ g ] c
∥-right-identity≊ {g = g} c w≈w′ rewrite ≡⇒≋ (proj₁∘splitAt-last≋ w≈w′) = xs++[]=xs (⟦ c ⟧[ g ] _)
  where postulate xs++[]=xs : _
%</par-right-identity-prime>

%<*par-right-identity>
\AgdaTarget{∥-right-identity}
\begin{code}
\end{code}
∥-right-identity : {g : TyGate G W⟶W} (c : ℂ[ G ] i o) → c ∥ nil⤨₀ ≋[ g ] c
∥-right-identity {i = i} {o} c = refl≋ (+-identityʳ i) (∥-right-identity≊ c)
%</par-right-identity>


%<*par-assoc-prime>
\AgdaTarget{∥-assoc≊}
\begin{code}
∥-assoc≊ : {g : TyGate G W⟶W} (c₁ : ℂ[ G ] i₁ o₁) (c₂ : ℂ[ G ] i₂ o₂) (c₃ : ℂ[ G ] i₃ o₃) → (c₁ ∥ c₂) ∥ c₃ ≊[ g ] c₁ ∥ (c₂ ∥ c₃)
∥-assoc≊ {i₁} {i₂} {g} c₁ c₂ c₃ {w} {w′} w≈w′
  rewrite  ≋⇒≡ (++-assoc-split₁ {n = i₁} w≈w′) | ≋⇒≡ (++-assoc-split₂ {n = i₁} w≈w′) | sym (≋⇒≡ (++-assoc-split₃ {n = i₁} w≈w′))
           = ++-assoc (⟦ c₁ ⟧[ g ] w₁) (⟦ c₂ ⟧[ g ] w₂) (⟦ c₃ ⟧[ g ] w₃)
  where  w₁ = (₁ ∘ splitAt i₁) w′
         w₂ = (₁ ∘ splitAt i₂ ∘ ₂′ ∘ splitAt i₁) w′
         w₃ = (₂′ ∘ splitAt (i₁ + i₂)) w
\end{code}
%</par-assoc-prime>

%<*par-assoc>
\AgdaTarget{∥-assoc}
\begin{code}
∥-assoc : {g : TyGate G W⟶W} {c₁ : ℂ[ G ] i₁ o₁} {c₂ : ℂ[ G ] i₂ o₂} {c₃ : ℂ[ G ] i₃ o₃} → (c₁ ∥ c₂) ∥ c₃ ≋[ g ] c₁ ∥ (c₂ ∥ c₃)
∥-assoc {i₁} {i₂} {i₃} {c₁} {c₂} {c₃} = refl≋ (+-assoc i₁ i₂ i₃) (∥-assoc≊ c₁ c₂ c₃)
\end{code}
%</par-assoc>


%<*rows-to-cols-prime>
\AgdaTarget{rows-to-cols≡}
\begin{code}
rows-to-cols≡ :  {g : TyGate G W⟶W} (f₁ : ℂ[ G ] i₁ m₁) (f₂ : ℂ[ G ] i₂ m₂) (g₁ : ℂ[ G ] m₁ o₁) (g₂ : ℂ[ G ] m₂ o₂)
                 → ∀ w → ⟦ (f₁ ∥ f₂) ⟫ (g₁ ∥ g₂) ⟧[ g ] w ≡ ⟦ (f₁ ⟫ g₁) ∥ (f₂ ⟫ g₂) ⟧[ g ] w
rows-to-cols≡ {i₁} {g} f₁ f₂ _ _ w rewrite splitAt++ ((⟦ f₁ ⟧[ g ] ∘ ₁ ∘ splitAt i₁) w) ((⟦ f₂ ⟧[ g ] ∘ ₂′ ∘ splitAt i₁) w) = refl
\end{code}
%</rows-to-cols-prime>

-- Circuits in "row-major order" to "column-major order"
%<*rows-to-cols>
\AgdaTarget{rows-to-cols}
\begin{code}
rows-to-cols :  {g : TyGate G W⟶W} (f₁ : ℂ[ G ] i₁ m₁) (f₂ : ℂ[ G ] i₂ m₂) (g₁ : ℂ[ G ] m₁ o₁) (g₂ : ℂ[ G ] m₂ o₂)
                → (f₁ ∥ f₂) ⟫ (g₁ ∥ g₂) ≋[ g ] (f₁ ⟫ g₁) ∥ (f₂ ⟫ g₂)
rows-to-cols {g} f₁ f₂ g₁ g₂ = ≅⇒≋ (≡⇒≋ ∘ (rows-to-cols≡ f₁ f₂ g₁ g₂))
\end{code}
%</rows-to-cols>


%<*cols-to-rows>
\AgdaTarget{cols-to-rows}
\begin{code}
\end{code}
cols-to-rows :  {g : TyGate G W⟶W} (f₁ : ℂ[ G ] i₁ m₁) (f₂ : ℂ[ G ] i₂ m₂) (g₁ : ℂ[ G ] m₁ o₁) (g₂ : ℂ[ G ] m₂ o₂)
                → (f₁ ⟫ g₁) ∥ (f₂ ⟫ g₂) ≋[ g ] (f₁ ∥ f₂) ⟫ (g₁ ∥ g₂)
cols-to-rows f₁ f₂ g₁ g₂ = ≋-sym (rows-to-cols f₁ f₂ g₁ g₂)
%</cols-to-rows>
