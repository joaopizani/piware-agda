\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Properties.Plugs (A : Atomic) where

open import Function.Base using (_$_; _∘_; _∘′_; id; _⟨_⟩_)
open import Data.Nat.Base using (_+_)
open import Data.Product.Base using (_,_)
open import Codata.Musical.Notation using (♯_)
open import Codata.Musical.Stream using (_∷_; map; _≈_)
open import Data.List.NonEmpty using (_∷_; head)
open import Data.Vec.Base using (Vec; _∷_; lookup; tabulate; allFin; splitAt)
open import Data.Vec.Properties using (tabulate∘lookup; lookup∘tabulate; lookup-allFin)
open import Data.Vec.Properties.Extra using (tabulate-cong; tabulate-ext)
open import Data.Vec.Relation.Binary.Equality.Propositional using (≡⇒≋) renaming (_≋_ to _≈ᵥ_)
open import Effect.Functor using (module Morphism)
open import Relation.Binary.PropositionalEquality using (refl; _≡_; sym; cong; module ≡-Reasoning; _≗_)
open Morphism using (op)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-∣; _∎)

open import Data.Vec.Extra using (VecMorphism)
open import Function.Bijection.Sets using (_RightInverseOf′_)
open import Codata.Musical.Stream.Equality.WithTrans using (_≈ₚ_; _∷ₚ_; ≈ₚ-to-≈; reflₚ; transₚ)
open import Codata.Musical.Stream.Properties using (module Setoidₛ)
open Setoidₛ using () renaming (refl to reflₛ; trans to transₛ)

open import Category.Functor.Extra using (_∘⇓_)
open import Codata.Musical.Stream.Causal using (runᶜ′)
open import PiWare.Plugs.Core using (eq⤪)
open import PiWare.Circuit using (_⟫_; _∥_; Plug)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Plugs using (plug-Vecη; id⤨)
open import PiWare.Semantics.Simulation A using (W; W⟶W; ⟦_⟧[_]; ⟦_⟧ᶜ[_]; ⟦_⟧ω[_]) renaming (module WithGates to SimulationWithGates)
open import PiWare.Semantics.Simulation.Compliance A using (_⫅[_]_; _⫃[_]_) renaming (module WithGates to ComplianceWithGates)
open import PiWare.Semantics.Simulation.Equivalence A using (_≅[_]_; _≋[_]_; ≅⇒≋; ≋c-refl; ≋c-trans) renaming (module WithGates to EqWithGates)

open import Notation.JP.Base using (n; n′; α)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o; m; m₁; m₂; m′)
open import PiWare.Circuit using (s)
\end{code}


%<*plug-Vec-implements-eta-identical>
\AgdaTarget{plug-Vecη⫃op[\_]}
\begin{code}
postulate plug-Vecη⫃op[_] : (g : TyGate G W⟶W) (η : VecMorphism i o) → plug-Vecη η ⫃[ g ] op η
\end{code}
plug-Vecη⫃op[_] {i} g η w = begin
  tabulate (λ i → lookup w (lookupf i $ op η (allFin _)))  ≡⟨ tabulate-ext (λ i → sym $ op-<$> (app-NaturalT (lookup-morphism i)) (lookup w) _) ⟩
  tabulate (lookup (mapᵥ (lookup w) (op η (allFin _))))    ≡⟨ tabulate-ext (λ i → sym $ cong (λ u → lookup u i) (op-<$> η (lookup w) _)) ⟩
  tabulate (lookup (op η (mapᵥ (lookup w) (allFin _))))    ≡⟨ tabulate∘lookup _ ⟩
  op η (mapᵥ (lookup w) (allFin _))                        ≡⟨ cong (op η) (map-lookup-allFin _) ⟩
  op η w                                                   ∎
%</plug-Vec-implements-eta-identical>

%<*plug-Vec-implements-eta-one-input>
\AgdaTarget{plug-Vecη⫅op[\_]}
\begin{code}
plug-Vecη⫅op[_] : (g : TyGate G W⟶W) (η : VecMorphism i o) → plug-Vecη η ⫅[ g ] op η
plug-Vecη⫅op[_] {i} g η w = ≡⇒≋ (plug-Vecη⫃op[ g ] η w)
\end{code}
%</plug-Vec-implements-eta-one-input>


%<*id-plug-implements-id-identical>
\AgdaTarget{id⤨⫃id[\_]}
\begin{code}
id⤨⫃id[_] : (g : TyGate G W⟶W) → id⤨ {m = m} ⫃[ g ] id
id⤨⫃id[ g ] w = begin
  ⟦ id⤨ ⟧[ g ] w                                ≡⟨⟩
  tabulate (lookup w ∘′ lookup (tabulate id))   ≡⟨ tabulate-ext (cong (lookup w) ∘ lookup∘tabulate id) ⟩
  tabulate (lookup w)                           ≡⟨ tabulate∘lookup w ⟩
  id w                                          ∎
\end{code}
%</id-plug-implements-id-identical>

%<*id-plug-implements-id-one-input>
\AgdaTarget{id⤨⫅id[\_]}
\begin{code}
id⤨⫅id[_] : (g : TyGate G W⟶W) → id⤨ {m = m} ⫅[ g ] id
id⤨⫅id[ g ] w = ≡⇒≋ (id⤨⫃id[ g ] w)
\end{code}
%</id-plug-implements-id-one-input>


%<*id-plug-cong>
\AgdaTarget{id⤨-cong}
\begin{code}
id⤨-cong : {g : TyGate G W⟶W} (p : m ≡ m′) → id⤨ {m = m} ≋[ g ] id⤨ {m = m′}
id⤨-cong refl = ≋c-refl
\end{code}
%</id-plug-cong>


%<*id-plug-par-fusion-identical>
\AgdaTarget{∥-id⤨≡[\_]}
\begin{code}
∥-id⤨≡[_] : ∀ (g : TyGate G W⟶W) {m₁} → ⟦ id⤨ {m = m₁} ∥ id⤨ {m = m₂} ⟧[ g ] ≗ ⟦ id⤨ {m = m₁ + m₂} ⟧[ g ]
∥-id⤨≡[_] _ {m₁}  w with splitAt m₁ w
∥-id⤨≡[_] g {_}   w | w₁ , w₂ , _        with id⤨⫃id[ g ] w₁ | id⤨⫃id[ g ] w₂ | id⤨⫃id[ g ] w
∥-id⤨≡[_] _ {_}   w | w₁ , w₂ , w≡w₁⧺w₂  | eq-w₁ | eq-w₂ | eq-w rewrite eq-w₁ | eq-w₂ | eq-w = sym w≡w₁⧺w₂
\end{code}
%</id-plug-par-fusion-identical>

%<*id-plug-par-fusion-one-input>
\AgdaTarget{∥-id⤨≅[\_]}
\begin{code}
∥-id⤨≅[_] : ∀ (g : TyGate G W⟶W) {m₁} → id⤨ {m = m₁} ∥ id⤨ {m = m₂} ≅[ g ] id⤨ {m = m₁ + m₂}
∥-id⤨≅[_] g {m₁} = ≡⇒≋ ∘ ∥-id⤨≡[_] g {m₁}
\end{code}
%</id-plug-par-fusion-one-input>

%<*id-plug-par-fusion>
\AgdaTarget{∥-id⤨≋[\_]}
\begin{code}
∥-id⤨≋[_] : ∀ (g : TyGate G W⟶W) {m₁} → (id⤨ {m = m₁} ∥ id⤨ {m = m₂}) ≋[ g ] (id⤨ {m = m₁ + m₂})
∥-id⤨≋[_] g {m₁} = ≅⇒≋ (∥-id⤨≅[_] g {m₁})
\end{code}
%</id-plug-par-fusion>


%<*plug-Vec-eta-id-one-input>
\AgdaTarget{plug-Vecη≅[\_]}
\begin{code}
plug-Vecη≅[_] :  ∀ (g : TyGate G W⟶W) {j} (η : VecMorphism j j)
                 → (∀ {X} (w : Vec X j) → op η w ≡ w) → plug-Vecη η ≅[ g ] id⤨
plug-Vecη≅[ g ] η η-id w = ≡⇒≋ $ begin
  ⟦ plug-Vecη η ⟧[ g ] w  ≡⟨ plug-Vecη⫃op[ g ] η w ⟩
  op η w                  ≡⟨ η-id w ⟩
  w                       ≡⟨ sym (id⤨⫃id[ g ] w) ⟩
  ⟦ id⤨ ⟧[ g ] w          ∎
\end{code}
%</plug-Vec-eta-id-one-input>

%<*plug-Vec-eta-id>
\AgdaTarget{plug-Vecη-id[\_]}
\begin{code}
plug-Vecη-id[_] :  ∀ (g : TyGate G W⟶W) {j} (η : VecMorphism j j)
                   → (∀ {X} (w : Vec X j) → op η w ≡ w) → plug-Vecη η ≋[ g ] id⤨
plug-Vecη-id[ g ] η p = ≅⇒≋ (plug-Vecη≅[ g ] η p)
\end{code}
%</plug-Vec-eta-id>


%<*plug-Vec-eta-comp-one-input>
\AgdaTarget{plug-Vecη-∘≅[\_]}
\begin{code}
plug-Vecη-∘≅[_] :  (g : TyGate G W⟶W) (η : VecMorphism i m) (ε : VecMorphism m o)
                   → plug-Vecη η ⟫ plug-Vecη ε ≅[ g ] plug-Vecη (ε ∘⇓ η)
plug-Vecη-∘≅[ g ] η ε w = ≡⇒≋ $ begin
  ⟦ plug-Vecη η ⟫ plug-Vecη ε ⟧[ g ] w            ≡⟨⟩
  ⟦ plug-Vecη ε ⟧[ g ] (⟦ plug-Vecη η ⟧[ g ] w)   ≡⟨ plug-Vecη⫃op[ g ] ε (⟦ plug-Vecη η ⟧[ g ] w) ⟩
  op ε (⟦ plug-Vecη η ⟧[ g ] w)                   ≡⟨ cong (op ε) (plug-Vecη⫃op[ g ] η w) ⟩
  (op ε ∘′ op η) w                                ≡⟨⟩  -- by definition of _∘⇓_
  op (ε ∘⇓ η) w                                   ≡⟨ sym (plug-Vecη⫃op[ g ] (ε ∘⇓ η) w) ⟩
  ⟦ plug-Vecη (ε ∘⇓ η) ⟧[ g ] w                   ∎
\end{code}
%</plug-Vec-eta-comp-one-input>

%<*plug-Vec-eta-comp>
\AgdaTarget{plug-Vecη-∘[\_]}
\begin{code}
plug-Vecη-∘[_] :  (g : TyGate G W⟶W) (η : VecMorphism i m) (ε : VecMorphism m o)
                  → plug-Vecη η ⟫ plug-Vecη ε ≋[ g ] plug-Vecη (ε ∘⇓ η)
plug-Vecη-∘[ g ] η ε = ≅⇒≋ (plug-Vecη-∘≅[ g ] η ε)
\end{code}
%</plug-Vec-eta-comp>


%<*plug-Vec-eta-ext-one-input>
\AgdaTarget{plug-Vecη-ext≅[\_]}
\begin{code}
plug-Vecη-ext≅[_] :  (g : TyGate G W⟶W) (η : VecMorphism i o) (ε : VecMorphism i o)
                     → (∀ {X} (w : Vec X i) → op η w ≡ op ε w) → plug-Vecη η ≅[ g ] plug-Vecη ε
plug-Vecη-ext≅[ g ] η ε η≈ε w = ≡⇒≋ $ begin
  ⟦ plug-Vecη η ⟧[ g ] w  ≡⟨ plug-Vecη⫃op[ g ] η w ⟩
  op η w                  ≡⟨ η≈ε w ⟩
  op ε w                  ≡⟨ sym (plug-Vecη⫃op[ g ] ε w) ⟩
  ⟦ plug-Vecη ε ⟧[ g ] w  ∎
\end{code}
%</plug-Vec-eta-ext-one-input>

%<*plug-Vec-eta-ext>
\AgdaTarget{plug-Vecη-ext[\_]}
\begin{code}
plug-Vecη-ext[_] :  (g : TyGate G W⟶W) {η : VecMorphism i o} {ε : VecMorphism i o}
                    → (∀ {X} (w : Vec X i) → op η w ≡ op ε w) → plug-Vecη η ≋[ g ] plug-Vecη ε
plug-Vecη-ext[ g ] {η} {ε} p = ≅⇒≋ (plug-Vecη-ext≅[ g ] η ε p)
\end{code}
%</plug-Vec-eta-ext>


%<*plugs-Vec-eta-inverse>
\AgdaTarget{plugs-Vecη⁻¹[\_]}
\begin{code}
plugs-Vecη⁻¹[_] :  (g : TyGate G W⟶W) (η : VecMorphism i o) (ε : VecMorphism o i)
                   → (∀ {X} (w : Vec X i) → (op ε ∘′ op η) w ≡ w) → plug-Vecη η ⟫ plug-Vecη ε ≋[ g ] id⤨ {m = i}
plugs-Vecη⁻¹[ g ] η ε p = plug-Vecη-∘[ g ] η ε  ⟨ ≋c-trans ⟩  plug-Vecη-id[ g ] (ε ∘⇓ η) p
\end{code}
%</plugs-Vec-eta-inverse>



\begin{code}
module WithGates {G} (g : TyGate G W⟶W) where
 open SimulationWithGates g using (⟦_⟧)
 open ComplianceWithGates g using (_⫅_; _⫃_)
 open EqWithGates g
\end{code}

%<*plug-Vec-implements-eta-identical-with-gates>
\AgdaTarget{plug-Vecη⫃op}
\begin{code}
 plug-Vecη⫃op : (η : VecMorphism i o) → plug-Vecη η ⫃ op η
 plug-Vecη⫃op = plug-Vecη⫃op[ g ]
\end{code}
%</plug-Vec-implements-eta-identical-with-gates>

%<*plug-Vec-implements-eta-one-input-with-gates>
\AgdaTarget{plug-Vecη⫅op}
\begin{code}
 plug-Vecη⫅op : (η : VecMorphism i o) → plug-Vecη η ⫅ op η
 plug-Vecη⫅op = plug-Vecη⫅op[ g ]
\end{code}
%</plug-Vec-implements-eta-one-input-with-gates>

%<*id-plug-implements-id-identical-with-gates>
\AgdaTarget{id⤨⫃id}
\begin{code}
 id⤨⫃id : id⤨ {m = i} ⫃ id
 id⤨⫃id = id⤨⫃id[ g ]
\end{code}
%</id-plug-implements-id-identical-with-gates>

%<*id-plug-implements-id-one-input-with-gates>
\AgdaTarget{id⤨⫅id}
\begin{code}
 id⤨⫅id : id⤨ {m = i} ⫅ id
 id⤨⫅id = id⤨⫅id[ g ]
\end{code}
%</id-plug-implements-id-one-input-with-gates>

%<*id-plug-par-fusion-identical-with-gates>
\AgdaTarget{∥-id⤨≡}
\begin{code}
\end{code}
 ∥-id⤨≡ : ⟦ id⤨ {m₁} ∥ id⤨ {m₂} ⟧ ≗ ⟦ id⤨ {m₁ + m₂} ⟧
 ∥-id⤨≡ {m₁} {m₂} w = ∥-id⤨≡[_] {m₁} {m₂} g w
%</id-plug-par-fusion-identical-with-gates>

%<*id-plug-par-fusion-one-input-with-gates>
\AgdaTarget{∥-id⤨≅}
\begin{code}
 ∥-id⤨≅ : id⤨ {m = m₁} ∥ id⤨ {m = m₂} ≅[ g ] id⤨
 ∥-id⤨≅ {m₁} {m₂} w = ∥-id⤨≅[_] {m₂ = m₂} g {m₁} w
\end{code}
%</id-plug-par-fusion-identical-with-gates>

%<*id-plug-par-fusion-with-gates>
\AgdaTarget{∥-id⤨≋}
\begin{code}
 ∥-id⤨≋ : id⤨ {m = m₁} ∥ id⤨ {m = m₂} ≋c id⤨
 ∥-id⤨≋ = ∥-id⤨≋[ g ]
\end{code}
%</id-plug-par-fusion-with-gates>

%<*plug-Vec-eta-id-prime-with-gates>
\AgdaTarget{plug-Vecη≅}
\begin{code}
 plug-Vecη≅ :  ∀ {j} (η : VecMorphism j j)
               → (∀ {X} (w : Vec X j) → op η w ≡ w) → plug-Vecη η ≅ id⤨
 plug-Vecη≅ = plug-Vecη≅[ g ]
\end{code}
%</plug-Vec-eta-id-prime-with-gates>

%<*plug-Vec-eta-id-with-gates>
\AgdaTarget{plug-Vecη-id}
\begin{code}
 plug-Vecη-id :  ∀ {j} (η : VecMorphism j j)
                 → (∀ {X} (w : Vec X j) → op η w ≡ w) → plug-Vecη η ≋c id⤨
 plug-Vecη-id = plug-Vecη-id[ g ]
\end{code}
%</plug-Vec-eta-id-with-gates>

%<*plug-Vec-eta-comp-prime-with-gates>
\AgdaTarget{plug-Vecη-∘≅}
\begin{code}
 plug-Vecη-∘≅ :  (η : VecMorphism i m) (ε : VecMorphism m o)
                 → plug-Vecη η ⟫ plug-Vecη ε ≅ plug-Vecη (ε ∘⇓ η)
 plug-Vecη-∘≅ = plug-Vecη-∘≅[ g ]
\end{code}
%</plug-Vec-eta-comp-prime-with-gates>

%<*plug-Vec-eta-comp-with-gates>
\AgdaTarget{plug-Vecη-∘}
\begin{code}
 plug-Vecη-∘ :  (η : VecMorphism i m) (ε : VecMorphism m o)
                → plug-Vecη η ⟫ plug-Vecη ε ≋c plug-Vecη (ε ∘⇓ η)
 plug-Vecη-∘ = plug-Vecη-∘[ g ]
\end{code}
%</plug-Vec-eta-comp-with-gates>

%<*plug-Vec-eta-ext-prime-with-gates>
\AgdaTarget{plug-Vecη-ext≅}
\begin{code}
 plug-Vecη-ext≅ :  (η : VecMorphism i o) (ε : VecMorphism i o)
                   → (∀ {X} (w : Vec X i) → op η w ≡ op ε w) → plug-Vecη η ≅ plug-Vecη ε
 plug-Vecη-ext≅ = plug-Vecη-ext≅[ g ]
\end{code}
%</plug-Vec-eta-ext-prime-with-gates>

%<*plug-Vec-eta-ext-with-gates>
\AgdaTarget{plug-Vecη-ext}
\begin{code}
 plug-Vecη-ext :  {η : VecMorphism i o} {ε : VecMorphism i o}
                  → (∀ {X} (w : Vec X i) → op η w ≡ op ε w) → plug-Vecη η ≋c plug-Vecη ε
 plug-Vecη-ext {η} {ε} = plug-Vecη-ext[_] g {η} {ε}
\end{code}
%</plug-Vec-eta-ext-with-gates>

%<*plugs-Vec-eta-inverse-with-gates>
\AgdaTarget{plugs-Vecη⁻¹}
\begin{code}
 plugs-Vecη⁻¹ :  (η : VecMorphism i o) (ε : VecMorphism o i)
                 → (∀ {X} (w : Vec X i) → (op ε ∘′ op η) w ≡ w)
                 → plug-Vecη η ⟫ plug-Vecη ε ≋c id⤨ {m = i}
 plugs-Vecη⁻¹ = plugs-Vecη⁻¹[ g ]
\end{code}
%</plugs-Vec-eta-inverse-with-gates>




%<*vecCoerce>
\AgdaTarget{vecCoerce}
\begin{code}
vecCoerce : n ≡ n′ → Vec α n → Vec α n′
vecCoerce refl v = v
\end{code}
%</vecCoerce>


%<*unfold-vecCoerce>
\AgdaTarget{unfold-vecCoerce}
\begin{code}
vecCoerce-step :  ∀ eq₁ eq₂ {x} {xs : Vec α n} → vecCoerce eq₁ (x ∷ xs) ≡ x ∷ vecCoerce {n′ = n′} eq₂ xs
vecCoerce-step refl refl = refl
\end{code}
%</unfold-vecCoerce>


%<*vecCoerce-rightInverse>
\AgdaTarget{vecCoerce-rightInverse}
\begin{code}
vecCoerce-rightInverse : (p : n ≡ n′) → vecCoerce {α = α} (sym p) RightInverseOf′ vecCoerce p
vecCoerce-rightInverse refl x = refl
\end{code}
%</vecCoerce-rightInverse>


%<*eq⤨⫃[ᶜ]id>
\AgdaTarget{eq⤨⫃[ᶜ]id}
\begin{code}
eq⤨⫃[ᶜ]id : ∀ {g : TyGate G W⟶W} {p x⁰ x⁻} → ⟦ Plug {G} {i} {o} (eq⤪ p) {s} ⟧ᶜ[ g ] (x⁰ ∷ x⁻) ≡ vecCoerce p x⁰
eq⤨⫃[ᶜ]id {p = refl} {x⁰} = begin
  tabulate (lookup x⁰ ∘′ lookup (allFin _))  ≡⟨ tabulate-cong (λ z → cong (lookup x⁰) (lookup-allFin z) ) ⟩
  tabulate (lookup x⁰)                       ≡⟨ tabulate∘lookup x⁰ ⟩
  x⁰                                         ≡⟨⟩
  vecCoerce refl x⁰                          ∎
\end{code}
%</eq⤨⫃[ᶜ]id>


-- TODO: One property of plugs and one of identity plugs:
-- plugs always comply with a function over only "now" (no dependency on past)

-- For the special case of an identity plug, this function is the identity
%<*eq⤨⫅[ᶜ]id∘head>
\AgdaTarget{eq⤨⫅[ᶜ]id∘head}
\begin{code}
eq⤨⫅[ᶜ]id∘head : ∀ {g : TyGate G W⟶W} {p x⁰⁻} → ⟦ Plug {G} {i} {o} (eq⤪ p) {s} ⟧ᶜ[ g ] x⁰⁻ ≈ᵥ (id ∘ head) x⁰⁻
eq⤨⫅[ᶜ]id∘head {s} {g} {p = refl} {x⁰ ∷ x⁻} = ≡⇒≋ $ eq⤨⫃[ᶜ]id {s = s} {g} {refl} {x⁰} {x⁻}
\end{code}
%</eq⤨⫅[ᶜ]id∘head>


%<*eq⤨⊑[runᶜ′]id>
\AgdaTarget{eq⤨⊑[runᶜ′]id}
\begin{code}
eq⤨⊑[runᶜ′]id :  ∀ {g : TyGate G W⟶W} {p x⁰ x⁻ x⁺}
                 → runᶜ′ ⟦ Plug {G} {i} {o} (eq⤪ p) {s} ⟧ᶜ[ g ] (x⁰ ∷ x⁻) x⁺ ≈ₚ vecCoerce p x⁰ ∷ ♯ map (vecCoerce p) x⁺
eq⤨⊑[runᶜ′]id {s} {g} {p} {x⁻} {x⁰ ∷ x⁺} =
  eq⤨⫃[ᶜ]id {s = s} {g} {p} {x⁻ = x⁻} ∷ₚ ♯ transₚ (eq⤨⊑[runᶜ′]id {s = s} {g}) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</eq⤨⊑[runᶜ′]id>


%<*eq⤨⊑ωid>
\AgdaTarget{eq⤨⊑ωid}
\begin{code}
eq⤨⊑ωid :  ∀ {st} {g : TyGate G W⟶W} {p x⁰⁺}
           → ⟦ Plug {G} {i} {o} (eq⤪ p) {s} ⟧ω[ g ] x⁰⁺ ≈ map (vecCoerce p) x⁰⁺
eq⤨⊑ωid {st} {g} {p} {x⁰ ∷ x⁺} = ≈ₚ-to-≈ (eq⤨⊑[runᶜ′]id {s = st} {g})  ⟨ transₛ ⟩  refl ∷ ♯ reflₛ
\end{code}
%</eq⤨⊑ωid>
