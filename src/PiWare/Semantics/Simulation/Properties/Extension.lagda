\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Properties.Extension (A : Atomic) where

open import Function.Base using (_$_; _⟨_⟩_)
open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Fin.Base using () renaming (zero to Fz; suc to Fs)
open import Data.Vec.Base using ([]; _∷_)
open import Data.List.NonEmpty using (_∷_)
open import Codata.Musical.Stream using (Stream; _∷_; _≈_; map)
open import Relation.Binary.PropositionalEquality.Core using (_≗_; refl)

open import Codata.Musical.Stream.Equality.WithTrans using (_≈ₚ_; _∷ₚ_; ≈ₚ-to-≈; reflₚ; transₚ)
open import Codata.Musical.Stream.Causal using (runᶜ′)

open import PiWare.Gates using (module Gates)
open Gates using (#in; #out)
open import PiWare.Gates.BoolTrio using (B₃)
open import PiWare.Plugs.Core using (_⤪_)
open import PiWare.Circuit using (C[_]; Gate; Plug; σ; ω)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W⟶W; ⟦_⟧[_]; ⟦_⟧ω[_]; ⟦_⟧ᶜ[_])

open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o)
open import PiWare.Circuit using (s)
\end{code}


%<*runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug>
\AgdaTarget{runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug}
\begin{code}
runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug :  ∀ {g : TyGate G W⟶W} {p : i ⤪ o} {f}
                    → ⟦ Plug p ⟧[ g ] ≗ f
                    → (∀ {x⁰ x⁻} x⁺ → runᶜ′ (⟦_⟧ᶜ[_] {s = s} (Plug p) g) (x⁰ ∷ x⁻) x⁺ ≈ₚ f x⁰ ∷ ♯ map f x⁺)
runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug {s} {g} {p} eq {x⁰} (_ ∷ x²⁺) rewrite eq x⁰ =
  refl ∷ₚ ♯ transₚ (runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug {s = s} {g} {p} eq (♭ x²⁺)) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug>

%<*⟦⟧ω⇒⟦⟧-Plug>
\AgdaTarget{⟦⟧ω⇒⟦⟧-Plug}
\begin{code}
⟦⟧ω⇒⟦⟧-Plug :  ∀ {st} {g : TyGate G W⟶W} {p : i ⤪ o} {f}
               → ⟦ Plug p ⟧[ g ] ≗ f
               → (∀ xs → ⟦ Plug p {s} ⟧ω[ g ] xs ≈ map f xs)
⟦⟧ω⇒⟦⟧-Plug {st} {g} {p} eq (x⁰ ∷ x⁺) =
  ≈ₚ-to-≈ $ runᶜ′⟦⟧ᶜ⇒⟦⟧-Plug {s = st} {g} {p} eq (♭ x⁺) ⟨ transₚ ⟩ (refl ∷ₚ ♯ reflₚ)
\end{code}
%</⟦⟧ω⇒⟦⟧-Plug>


%<*runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate>
\AgdaTarget{runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate}
\begin{code}
runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate :  ∀ {h} {g : TyGate G W⟶W} {f : W⟶W (#in G h) (#out G h)}
                    → ⟦ Gate h ⟧[ g ] ≗ f
                    → (∀ {x⁰ x⁻} x⁺ → runᶜ′ (⟦_⟧ᶜ[_] {s = s} (Gate h) g) (x⁰ ∷ x⁻) x⁺ ≈ₚ f x⁰ ∷ ♯ map f x⁺)
runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate {s} {g} eq {x⁰} (_ ∷ x²⁺) rewrite eq x⁰ = refl ∷ₚ ♯ transₚ (runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate {s = s} {g = g} eq (♭ x²⁺)) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate>

%<*⟦⟧ω⇒⟦⟧-Gate>
\AgdaTarget{⟦⟧ω⇒⟦⟧-Gate}
\begin{code}
⟦⟧ω⇒⟦⟧-Gate :  ∀ {st h} {g : TyGate G W⟶W} {f : W⟶W (#in G h) (#out G h)}
               → ⟦ Gate h ⟧[ g ] ≗ f → (∀ xs → ⟦ Gate h {s} ⟧ω[ g ] xs ≈ map f xs)
⟦⟧ω⇒⟦⟧-Gate {st} {g} eq (x⁰ ∷ x⁺) = ≈ₚ-to-≈ $ runᶜ′⟦⟧ᶜ⇒⟦⟧-Gate {s = st} {g = g} eq (♭ x⁺) ⟨ transₚ ⟩ (refl ∷ₚ ♯ reflₚ)
\end{code}
%</⟦⟧ω⇒⟦⟧-Gate>



%<*badC>
\AgdaTarget{badC}
\begin{code}
badC : C[ B₃ ] 2 1
badC {s = σ} = Plug (Fz ∷ [])
badC {s = ω} = Plug (Fs Fz ∷ [])
\end{code}
%</badC>


-- The property we would actually like to have
%<*ω-extends-σ>
\AgdaTarget{ω-extends-σ}
\begin{code}
ω-extends-σ : Set₁
ω-extends-σ = ∀ {i o s G} {g : TyGate G W⟶W} {c : C[ G ] i o} {f} → ⟦ c ⟧[ g ] ≗ f → (∀ xs → ⟦ c {s} ⟧ω[ g ] xs ≈ map f xs)
\end{code}
%</ω-extends-σ>


%<*¬ω-extends-σ>
\AgdaTarget{¬ω-extends-σ}
\begin{code}
\end{code}
¬ω-extends-σ : ¬ ω-extends-σ
¬ω-extends-σ e with e {g = b₃} {c = badC} {f = [_] ∘′ head} (λ {(_ ∷ _ ∷ []) → refl}) (repeat (𝕋 ∷ 𝔽 ∷ []))
¬ω-extends-σ _ | () ∷ xs≈
%</¬ω-extends-σ>
