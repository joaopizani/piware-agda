\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Semantics.Simulation.Properties.Sequential (A : Atomic) where

open import Function using (_∘′_; id)
open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Vec using (_++_)
open import Data.Product using (uncurry′; _,_) renaming (map to map×)
open import Data.Nat.Base using (zero; suc)
open import Data.List.NonEmpty using (_∷_; _∷⁺_; _⁺∷ʳ_; length) renaming (map to map⁺)
open import Codata.Musical.Stream using (_∷_; _≈_; tail; lookup; drop; tail-cong; head-cong; map; zipWith)
open import Relation.Binary.PropositionalEquality using (_≡_; _≗_; refl; sym; trans; cong; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-⟨; step-≡-∣; _∎)

open import Data.Vec.Extra using (splitAt′)
open import Data.Vec.Properties.Extra using (splitAt′-++-inverse)
open import Data.List.NonEmpty.Extra using (zipWith⁺; tails⁺; reverse⁺′; inits⁺; unzip⁺; splitAt⁺; zip⁺)
open import Data.List.NonEmpty.Properties.Extra using (zipWith⁺-map⁺-zip⁺; unzip⁺-zip⁺-inverse)
open import Codata.Musical.Stream.Causal using (_⇒ᶜ_; runᶜ′; runᶜ)
open import Codata.Musical.Stream.Extra using (take⁺) renaming (inits⁺ to sinits⁺)
open import Codata.Musical.Stream.Equality.FromPointwise using (lookup≗⇒≈)
open import Codata.Musical.Stream.Properties using
  ( ≡-to-≈; module Setoidₛ; module EqReasoningₛ; lookup-drop; unfold'⁺-reverse⁺′; unfold-take⁺; drop-tail
  ; lemma-lookup-drop; lookup-inits⁺-take⁺; lemma-lookup-map; map⁺-id; map⁺-cong; reverse⁺′-involutive; map⁺-∘
  ; tails⁺-inits⁺; reverse⁺′-map⁺-commute ; inits⁺-take⁺-inits⁺; map⁺-take⁺-mapₛ; take⁺-cong )

open import PiWare.Circuit using (ℂ[_]; _⟫_; _∥_)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W⟶W; ⟦_⟧ω[_]; ⟦_⟧ᶜ[_])

open import Notation.JP.Base using (α; β)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; i₁; i₂; o; o₁; o₂; m)
open import PiWare.Circuit using (s)
\end{code}


\begin{code}
open Setoidₛ       using ()          renaming (refl to reflₛ; sym to symₛ)
open EqReasoningₛ  using (step-≈-⟩)  renaming (begin_ to beginₛ_; _∎ to _∎ₛ)
\end{code}


%<*runᶜ′-cong>
\AgdaTarget{runᶜ′-cong}
\begin{code}
runᶜ′-cong : ∀ {f : α ⇒ᶜ β} {x⁰⁻ x⁺ y⁺} → x⁺ ≈ y⁺ → (runᶜ′ f x⁰⁻) x⁺ ≈ (runᶜ′ f x⁰⁻) y⁺
runᶜ′-cong {f} {x⁺ = x¹ ∷ _} {.x¹ ∷ _} (refl ∷ x²⁺≈y²⁺) = refl ∷ ♯ runᶜ′-cong {f = f} (♭ x²⁺≈y²⁺)
\end{code}
%</runᶜ′-cong>


%<*runᶜ-cong>
\AgdaTarget{runᶜ-cong}
\begin{code}
runᶜ-cong : ∀ {f : α ⇒ᶜ β} {xs ys} → xs ≈ ys → (runᶜ f) xs ≈ (runᶜ f) ys
runᶜ-cong {f} {xs = x ∷ _} {.x ∷ _} (refl ∷ xs′≈ys′) = runᶜ′-cong (♭ xs′≈ys′)
\end{code}
%</runᶜ-cong>


%<*⟦⟧ω[–]-cong>
\AgdaTarget{⟦⟧ω[–]-cong}
\begin{code}
⟦⟧ω[–]-cong : ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i o) {xs ys} → xs ≈ ys → ⟦ c ⟧ω[ g ] xs ≈ ⟦ c ⟧ω[ g ] ys
⟦⟧ω[–]-cong _ _ xs≈ys = runᶜ-cong xs≈ys
\end{code}
%</⟦⟧ω[–]-cong>



%<*⟦⟧ᶜ-∥⇒zipWith++>
\AgdaTarget{⟦⟧ᶜ-∥⇒zipWith++}
\begin{code}
⟦⟧ᶜ-∥⇒zipWith++ :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i₁ o₁) (d : ℂ[ G ] i₂ o₂) {x⁰⁻ y⁰⁻}
                  → length x⁰⁻ ≡ length y⁰⁻ → ⟦ c ∥ d ⟧ᶜ[ g ] (zipWith⁺ _++_ x⁰⁻ y⁰⁻) ≡ ⟦ c ⟧ᶜ[ g ] x⁰⁻ ++ ⟦ d ⟧ᶜ[ g ] y⁰⁻
⟦⟧ᶜ-∥⇒zipWith++ g c d {x⁰⁻} {y⁰⁻} p =
  begin
    ⟦ c ∥ d ⟧ᶜ[ g ]                                                  (zipWith⁺ _++_ x⁰⁻ y⁰⁻)  ≡⟨⟩  -- definition of ⟦ c ∥ d ⟧
    (pre ∘′ unzip⁺ ∘′ splitAt⁺ _)                                    (zipWith⁺ _++_ x⁰⁻ y⁰⁻)  ≡⟨ eq0 ⟩
    (pre ∘′ unzip⁺ ∘′ map⁺ (splitAt′ _)  ∘′ map⁺ (  uncurry′ _++_))  (zip⁺ x⁰⁻ y⁰⁻)           ≡⟨ eq1 ⟨
    (pre ∘′ unzip⁺ ∘′ map⁺ (splitAt′ _   ∘′         uncurry′ _++_))  (zip⁺ x⁰⁻ y⁰⁻)           ≡⟨ eq2 ⟩
    (pre ∘′ unzip⁺ ∘′ map⁺ id)                                       (zip⁺ x⁰⁻ y⁰⁻)           ≡⟨ eq3 ⟩
    (pre ∘′ unzip⁺)                                                  (zip⁺ x⁰⁻ y⁰⁻)           ≡⟨ eq4 ⟩
    pre (x⁰⁻ , y⁰⁻)                                                                           ≡⟨⟩  -- definition of ⟦ c ∥ d ⟧
    ⟦ c ⟧ᶜ[ g ] x⁰⁻ ++ ⟦ d ⟧ᶜ[ g ] y⁰⁻
  ∎
  where
    pre = uncurry′ _++_ ∘′ map× ⟦ c ⟧ᶜ[ g ] ⟦ d ⟧ᶜ[ g ]
    eq0 = cong  (  pre ∘′ unzip⁺ ∘′ splitAt⁺ _)  (zipWith⁺-map⁺-zip⁺ _++_ x⁰⁻ y⁰⁻)
    eq1 = cong  (  pre ∘′ unzip⁺)                (map⁺-∘                                 (zip⁺ x⁰⁻ y⁰⁻))
    eq2 = cong  (  pre ∘′ unzip⁺)                (map⁺-cong (λ _ → splitAt′-++-inverse)  (zip⁺ x⁰⁻ y⁰⁻))
    eq3 = cong  (  pre ∘′ unzip⁺)                (map⁺-id                                (zip⁺ x⁰⁻ y⁰⁻))
    eq4 = cong     pre                           (unzip⁺-zip⁺-inverse x⁰⁻ y⁰⁻ p)
\end{code}
%</⟦⟧ᶜ-∥⇒zipWith++>


%<*runᶜ′⟦⟧ᶜ-∥⇒zipWith++>
\AgdaTarget{runᶜ′⟦⟧ᶜ-∥⇒zipWith++}
\begin{code}
runᶜ′⟦⟧ᶜ-∥⇒zipWith++ :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i₁ o₁) (d : ℂ[ G ] i₂ o₂) {x⁰⁻ y⁰⁻ x⁺ y⁺}
                        →  length x⁰⁻ ≡ length y⁰⁻
                        →  runᶜ′ ⟦ c ∥ d ⟧ᶜ[ g ] (zipWith⁺ _++_ x⁰⁻ y⁰⁻) (zipWith _++_ x⁺ y⁺)
                           ≈ zipWith _++_ (runᶜ′ ⟦ c ⟧ᶜ[ g ] x⁰⁻ x⁺) (runᶜ′ ⟦ d ⟧ᶜ[ g ] y⁰⁻ y⁺)
runᶜ′⟦⟧ᶜ-∥⇒zipWith++ g c d {_ ∷ _} {_ ∷ _} {_ ∷ _} {_ ∷ _} p = ⟦⟧ᶜ-∥⇒zipWith++ g c d p ∷ ♯ runᶜ′⟦⟧ᶜ-∥⇒zipWith++ g c d (cong suc p)
\end{code}
%</runᶜ′⟦⟧ᶜ-∥⇒zipWith++>


%<*⟦⟧ω[–]-∥⇒zipWith++>
\AgdaTarget{⟦⟧ω[–]-∥⇒zipWith++}
\begin{code}
⟦⟧ω[–]-∥⇒zipWith++ :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i₁ o₁) (d : ℂ[ G ] i₂ o₂) {xs ys}
                      → ⟦ c ∥ d ⟧ω[ g ] (zipWith _++_ xs ys) ≈ zipWith _++_ (⟦ c ⟧ω[ g ] xs) (⟦ d ⟧ω[ g ] ys)
⟦⟧ω[–]-∥⇒zipWith++ g c d {_ ∷ _} {_ ∷ _} = runᶜ′⟦⟧ᶜ-∥⇒zipWith++ g c d refl
\end{code}
%</⟦⟧ω[–]-∥⇒zipWith++>




%<*lemma-drop-ω′>
\AgdaTarget{lemma-drop-ω′}
\begin{code}
lemma-drop-ω′ :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i o) {xs} {n}
                 →  tail (  runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs))        (drop (suc n) xs))
                    ≈       runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ (suc n) xs))  (drop (suc (suc n)) xs)
lemma-drop-ω′ g c {xs} {n} = beginₛ
  tail (runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs)) (drop (suc n) xs))
    ≈⟨ tail-cong (runᶜ′-cong {f = ⟦ c ⟧ᶜ[ g ]} (lookup-drop xs (suc n))) ⟩
  runᶜ′ ⟦ c ⟧ᶜ[ g ] (lookup xs (suc n) ∷⁺ reverse⁺′ (take⁺ n xs)) (drop (suc (suc n)) xs)
    ≈⟨ ≡-to-≈ (cong (λ zs → runᶜ′ ⟦ c ⟧ᶜ[ g ] zs (drop (suc (suc n)) xs)) (sym (unfold'⁺-reverse⁺′ (lookup xs (suc n)) (take⁺ n xs)))) ⟩
  runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs ⁺∷ʳ lookup xs (suc n))) (drop (suc (suc n)) xs)
    ≈⟨ ≡-to-≈ (cong (λ zs → runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ zs) (drop (suc (suc n)) xs)) (sym (unfold-take⁺ xs n))) ⟩
  runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ (suc n) xs)) (drop (suc (suc n)) xs) ∎ₛ
\end{code}
%</lemma-drop-ω′>


%<*lemma-drop-ω>
\AgdaTarget{lemma-drop-ω}
\begin{code}
lemma-drop-ω :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i o) {xs} {n}
                → drop n (⟦ c ⟧ω[ g ] xs) ≈ runᶜ′ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs)) (drop (suc n) xs)
lemma-drop-ω _ _ {_ ∷ _}  {n = zero}    = reflₛ
lemma-drop-ω g c {xs}     {n = suc n′}  = beginₛ
  drop (suc n′) (⟦ c ⟧ω[ g ] xs)                                                         ≈⟨ ≡-to-≈ (drop-tail {n = n′} {⟦ c ⟧ω[ g ] xs}) ⟩
  tail (drop n′ (⟦ c ⟧ω[ g ] xs))                                                        ≈⟨ tail-cong (lemma-drop-ω g c {xs}) ⟩
  tail (runᶜ′  ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n′        xs))  (drop (suc      n′)   xs))  ≈⟨ lemma-drop-ω′ g c {xs} ⟩
  runᶜ′        ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ (suc n′)  xs))  (drop (suc (suc n′))  xs)   ∎ₛ
\end{code}
%</lemma-drop-ω>


%<*lookup-ω>
\AgdaTarget{lookup-ω}
\begin{code}
lookup-ω :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i o) {xs} {n}
            → lookup (⟦ c ⟧ω[ g ] xs) n ≡ ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs))
lookup-ω g c {xs} {n} with reverse⁺′ (take⁺ n xs) | drop (suc n) xs | trans (lemma-lookup-drop {n = n}) (head-cong (lemma-drop-ω g c {xs} {n}))
lookup-ω g c {xs} {n} | _ ∷ _                     | _ ∷ _           | p = p
\end{code}
%</lookup-ω>


%<*lemma-lookup-ω-map>
\AgdaTarget{lemma-lookup-ω-map}
\begin{code}
lemma-lookup-ω-map :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i o) {xs} {n}
                      → lookup (⟦ c ⟧ω[ g ] xs) n ≡ lookup (map (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (sinits⁺ xs)) n
lemma-lookup-ω-map g c {xs} {n} = begin
  lookup (⟦ c ⟧ω[ g ] xs) n                               ≡⟨ lookup-ω g c {xs} ⟩
  ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs))                    ≡⟨ cong (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (sym (lookup-inits⁺-take⁺ {s = xs})) ⟩
  ⟦ c ⟧ᶜ[ g ] (reverse⁺′ (lookup (sinits⁺ xs) n))         ≡⟨ lemma-lookup-map {n = n} {sinits⁺ xs} {⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′} ⟨
  lookup (map (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (sinits⁺ xs)) n  ∎
\end{code}
%</lemma-lookup-ω-map>


%<*lemma-ω-map>
\AgdaTarget{lemma-ω-map}
\begin{code}
lemma-ω-map :  ∀ {i o G} (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i o) {xs}
               → ⟦ c ⟧ω[ g ] xs ≈ map (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (sinits⁺ xs)
lemma-ω-map g c {xs} = lookup≗⇒≈  (⟦ c ⟧ω[ g ] xs)
                                  (map (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (sinits⁺ xs))
                                  (λ n → lemma-lookup-ω-map g c {xs} {n})
\end{code}
%</lemma-ω-map>


%<*lookup-⟫⇒∘>
\AgdaTarget{lookup-⟫⇒∘}
\begin{code}
lookup-⟫⇒∘ :  ∀ (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i m) (d : ℂ[ G ] m o) {xs} {n}
              → lookup (⟦ c ⟫ d ⟧ω[ g ] xs) n ≡ lookup ((⟦ d ⟧ω[ g ] ∘′ ⟦ c ⟧ω[ g ]) xs) n
lookup-⟫⇒∘ g c d {xs} {n} =
  begin
    lookup (⟦ c ⟫ d ⟧ω[ g ] xs) n                                                                        ≡⟨ eq0 ⟩
    ⟦ c ⟫ d ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n xs))                                                             ≡⟨⟩
    ⟦ d ⟧ᶜ[ g ] (map⁺ ⟦ c ⟧ᶜ[ g ] (tails⁺ (reverse⁺′ (take⁺ n xs))))                                     ≡⟨ eq1 ⟩
    ⟦ d ⟧ᶜ[ g ] (map⁺ ⟦ c ⟧ᶜ[ g ] (map⁺ (reverse⁺′ ∘′ reverse⁺′) (tails⁺ (reverse⁺′ (take⁺ n xs)))))     ≡⟨ eq2 ⟩
    ⟦ d ⟧ᶜ[ g ] (map⁺ ⟦ c ⟧ᶜ[ g ] (map⁺ reverse⁺′ (map⁺ reverse⁺′ (tails⁺ (reverse⁺′ (take⁺ n xs))))))   ≡⟨ eq3 ⟩
    ⟦ d ⟧ᶜ[ g ] (map⁺ (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (map⁺ reverse⁺′ (tails⁺ (reverse⁺′ (take⁺ n xs)))))     ≡⟨ eq4 ⟩
    ⟦ d ⟧ᶜ[ g ] (map⁺ (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (reverse⁺′ (inits⁺ (take⁺ n xs))))                      ≡⟨ eq5 ⟩
    ⟦ d ⟧ᶜ[ g ] (reverse⁺′ (map⁺ (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (inits⁺ (take⁺ n xs))))                      ≡⟨ eq6 ⟩
    ⟦ d ⟧ᶜ[ g ] (reverse⁺′ (map⁺ (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (take⁺ n (sinits⁺ xs))))                     ≡⟨ eq7 ⟩
    ⟦ d ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n (map (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (sinits⁺ xs))))                      ≡⟨ eq8 ⟩
    ⟦ d ⟧ᶜ[ g ] (reverse⁺′ (take⁺ n (⟦ c ⟧ω[ g ] xs)))                                                   ≡⟨ eq9 ⟨
    lookup (⟦ d ⟧ω[ g ] (⟦ c ⟧ω[ g ] xs)) n
  ∎
  where
    eq0 = lookup-ω g (c ⟫ d) {n = n}
    tai = tails⁺ (reverse⁺′ (take⁺ n xs))
    eq1 = cong (λ x → ⟦ d ⟧ᶜ[ g ] (map⁺ ⟦ c ⟧ᶜ[ g ] x))  (trans (sym (map⁺-id tai)) (map⁺-cong (λ x → sym (reverse⁺′-involutive x)) tai))
    eq2 = cong (λ x → ⟦ d ⟧ᶜ[ g ] (map⁺ ⟦ c ⟧ᶜ[ g ] x))  (map⁺-∘ (tails⁺ (reverse⁺′ (take⁺ n xs))))
    eq3 = cong (λ x → ⟦ d ⟧ᶜ[ g ] x)                     (sym (map⁺-∘ (map⁺ reverse⁺′ (tails⁺ (reverse⁺′ (take⁺ n xs))))))
    eq5 = cong (λ x → ⟦ d ⟧ᶜ[ g ] x)                     (reverse⁺′-map⁺-commute (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) (inits⁺ (take⁺ n xs)))
    eq7 = cong (λ x → ⟦ d ⟧ᶜ[ g ] (reverse⁺′ x))         (map⁺-take⁺-mapₛ {n = n} {s = sinits⁺ xs})
    eq8 = cong (λ x → ⟦ d ⟧ᶜ[ g ] (reverse⁺′ x))         (take⁺-cong {n = n} (symₛ (lemma-ω-map g c {xs})))
    eq9 = lookup-ω g d {n = n}

    eq6 = cong (λ x → ⟦ d ⟧ᶜ[ g ] (reverse⁺′ (map⁺ (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) x)))  (inits⁺-take⁺-inits⁺ {n = n} {xs})
    eq4 = cong (λ x → ⟦ d ⟧ᶜ[ g ] (map⁺ (⟦ c ⟧ᶜ[ g ] ∘′ reverse⁺′) x))              (tails⁺-inits⁺ {xs = take⁺ n xs})
\end{code}
%</lookup-⟫⇒∘>


%<*⟦⟧ω[–]-⟫⇒∘>
\AgdaTarget{⟦⟧ω[–]-⟫⇒∘}
\begin{code}
⟦⟧ω[–]-⟫⇒∘ :  ∀ {i m o G} (g : TyGate G W⟶W) (c : ℂ[ G ] {s} i m) (d : ℂ[ G ] m o) {xs}
              → ⟦ c ⟫ d ⟧ω[ g ] xs ≈ (⟦ d ⟧ω[ g ] ∘′ ⟦ c ⟧ω[ g ]) xs
⟦⟧ω[–]-⟫⇒∘ g c d {xs} = lookup≗⇒≈  (⟦ c ⟫ d ⟧ω[ g ] xs)
                                   ((⟦ d ⟧ω[ g ] ∘′ ⟦ c ⟧ω[ g ]) xs)
                                   (λ n → lookup-⟫⇒∘ g c d {xs} {n})
\end{code}
%</⟦⟧ω[–]-⟫⇒∘>
