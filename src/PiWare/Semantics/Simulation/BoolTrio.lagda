\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Semantics.Simulation.BoolTrio where

open import Function.Base using (const)
open import Data.Bool.Base using (not; _∧_; _∨_; false; true)
open import Data.Vec.Base using ([]; _∷_; [_])

open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Gates.BoolTrio using (B₃; ⊥ℂ'; ⊤ℂ'; ¬ℂ'; ∧ℂ'; ∨ℂ')
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Semantics.Simulation Atomic-Bool using (W⟶W)
\end{code}


%<*specs-type>
\begin{code}
spec-¬ℂ          : W⟶W 1 1
spec-∧ℂ spec-∨ℂ  : W⟶W 2 1
\end{code}
%</specs-type>

%<*specs-def>
\AgdaTarget{spec-¬ℂ, spec-∧ℂ, spec-∨ℂ}
\begin{code}
spec-¬ℂ (x ∷ [])      = [_] (not x)
spec-∧ℂ (x ∷ y ∷ [])  = [_] (_∧_ x y)
spec-∨ℂ (x ∷ y ∷ [])  = [_] (_∨_ x y)
\end{code}
%</specs-def>


%<*spec>
\AgdaTarget{spec}
\begin{code}
spec : TyGate B₃ W⟶W
spec ⊥ℂ' = const [ false ]
spec ⊤ℂ' = const [ true  ]
spec ¬ℂ' = spec-¬ℂ
spec ∧ℂ' = spec-∧ℂ
spec ∨ℂ' = spec-∨ℂ
\end{code}
%</spec>
