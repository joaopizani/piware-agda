\begin{code}
module PiWare.Semantics.AreaAnalysis.Nand where

open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Gates.Nand using (Nand; ⊼ℂ')
open import PiWare.Semantics.AreaAnalysis using (Area)
\end{code}


%<*spec>
\AgdaTarget{spec}
\begin{code}
spec : TyGate Nand Area
spec ⊼ℂ' = 1
\end{code}
%</spec>
