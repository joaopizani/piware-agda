\begin{code}
module PiWare.Semantics.AreaAnalysis.BoolTrio where

open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Gates.BoolTrio using (B₃; ⊥ℂ'; ⊤ℂ'; ¬ℂ'; ∧ℂ'; ∨ℂ')
open import PiWare.Semantics.AreaAnalysis using (Area)
\end{code}


%<*spec>
\AgdaTarget{spec}
\begin{code}
spec : TyGate B₃ Area
spec ⊥ℂ' = 0
spec ⊤ℂ' = 0
spec ¬ℂ' = 1
spec ∧ℂ' = 2
spec ∨ℂ' = 2
\end{code}
%</spec>
