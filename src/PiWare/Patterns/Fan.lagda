\begin{code}
module PiWare.Patterns.Fan where

open import Function.Base using (_∘′_; id)
open import Data.Nat.Base using (suc; _+_)
open import Data.Vec.Base using (Vec; head; _∷_; [_]; map; _⊛_; _++_)
open import Data.Product.Base using (uncurry′) renaming (map to map×)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)

open import Data.Vec.Extra using (initLast′; VecMorphism)

open import Notation.JP.Base using (ℓ; α; β; n)
\end{code}


%<*fan-binop-M-op>
\AgdaTarget{fan-⊕-M-op}
\begin{code}
fan-⊕-M-op : Vec α (suc n) → Vec α (n + 2)
fan-⊕-M-op xs = (uncurry′ _++_ ∘′ map× id ((head xs ∷_) ∘′ [_]) ∘′ initLast′) xs
\end{code}
%</fan-binop-M-op>


%<*fan-binop-M>
\AgdaTarget{fan-⊕-M}
\begin{code}
fan-⊕-M : ∀ n → VecMorphism {ℓ} (suc n) (n + 2)
fan-⊕-M n = record { op = fan-⊕-M-op; op-<$> = fan-⊕-M-<$> }
  where -- TODO by parametricity
    postulate fan-⊕-M-<$> : (f : α → β) (xs : Vec α (suc n)) → fan-⊕-M-op (map f xs) ≡ map f (fan-⊕-M-op xs)
\end{code}
%<fan-binop-M>
