\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module PiWare.Patterns.Id where

open import Function.Base using (flip; _∘′_)
open import Data.Nat.Base using (_+_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; subst)

open import PiWare.Circuit using (ℂ[_]; _⟫_; _∥_)
open import PiWare.Plugs using (eq⤨)

open import PiWare.Gates using (G)
open import PiWare.Circuit using (s)
open import PiWare.Circuit.Notation using (i; m; o; i₁; i₂; o₁; o₂; i′; i₁′; i₂′; o′; o₁′; o₂′; m′)
\end{code}


%<*adaptEqI>
\AgdaTarget{adaptEqI}
\begin{code}
adaptEqI : (≡ᵢ : i ≡ i′) → ℂ[ G ] {s} i o → ℂ[ G ] {s} i′ o
adaptEqI {G} {s} {o} = subst (flip (ℂ[ G ] {s}) o)
\end{code}
%</adaptEqI>


%<*adaptEqO>
\AgdaTarget{adaptEqO}
\begin{code}
adaptEqO : (≡ₒ : o ≡ o′) → ℂ[ G ] {s} i o → ℂ[ G ] {s} i o′
adaptEqO {G} {s} {i} = subst (ℂ[ G ] {s} i)
\end{code}
%</adaptEqO>


%<*adaptEqIO>
\AgdaTarget{adaptEqIO}
\begin{code}
adaptEqIO : (≡ᵢ : i ≡ i′) (≡ₒ : o ≡ o′) → ℂ[ G ] {s} i o → ℂ[ G ] {s} i′ o′
adaptEqIO ≡ᵢ ≡ₒ = adaptEqO ≡ₒ ∘′ adaptEqI ≡ᵢ
\end{code}
%</adaptEqIO>



\begin{code}
infixl 4 _⟫[_]_
\end{code}

%<*seq-het>
\AgdaTarget{\_⟫[\_]\_}
\begin{code}
_⟫[_]_ : (c₁ : ℂ[ G ] {s} i m) (eq : m ≡ m′) (c₂ : ℂ[ G ] {s} m′ o) → ℂ[ G ] {s} i o
c₁ ⟫[ eq ] c₂ = c₁ ⟫ eq⤨ eq ⟫ c₂
\end{code}
%</seq-het>


\begin{code}
infixr 3 [_]_[_]∥_
\end{code}

%<*par-het-left>
\AgdaTarget{[\_]\_[\_]∥\_}
\begin{code}
[_]_[_]∥_ : (≡ᵢ : i₁ ≡ i₁′) (c₁ : ℂ[ G ] {s} i₁ o₁) (≡ₒ : o₁ ≡ o₁′) (c₂ : ℂ[ G ] {s} i₂ o₂) → ℂ[ G ] {s} (i₁′ + i₂) (o₁′ + o₂)
[ ≡ᵢ ] c₁ [ ≡ₒ ]∥ c₂ = adaptEqIO ≡ᵢ ≡ₒ c₁ ∥ c₂
\end{code}
%</par-het-left>


\begin{code}
infixr 3 _∥[_]_[_]
\end{code}

%<*par-het-right>
\AgdaTarget{\_∥[\_]\_[\_]}
\begin{code}
_∥[_]_[_] : (c₁ : ℂ[ G ] {s} i₁ o₁) (≡ᵢ : i₂ ≡ i₂′) (c₂ : ℂ[ G ] {s} i₂ o₂) (≡ₒ : o₂ ≡ o₂′) → ℂ[ G ] {s} (i₁ + i₂′) (o₁ + o₂′)
c₁ ∥[ ≡ᵢ ] c₂ [ ≡ₒ ] = c₁ ∥ adaptEqIO ≡ᵢ ≡ₒ c₂
\end{code}
%</par-het-right>


\begin{code}
infixr 3 [_]_∥[_]_
\end{code}

%<*par-het-input>
\AgdaTarget{[\_]\_∥[\_]\_}
\begin{code}
[_]_∥[_]_ : (≡₁ : i₁ ≡ i₁′) (c₁ : ℂ[ G ] {s} i₁ o₁) (≡₂ : i₂ ≡ i₂′) (c₂ : ℂ[ G ] {s} i₂ o₂) → ℂ[ G ] {s} (i₁′ + i₂′) (o₁ + o₂)
[ ≡₁ ] c₁ ∥[ ≡₂ ] c₂ = adaptEqI ≡₁ c₁ ∥ adaptEqI ≡₂ c₂ 
\end{code}
%</par-het-input>


\begin{code}
infixr 3 _[_]∥_[_]
\end{code}

%<*par-het-output>
\AgdaTarget{\_[\_]∥\_[\_]}
\begin{code}
_[_]∥_[_] : (c₁ : ℂ[ G ] {s} i₁ o₁) (≡₁ : o₁ ≡ o₁′) (c₂ : ℂ[ G ] {s} i₂ o₂) (≡₂ : o₂ ≡ o₂′) → ℂ[ G ] {s} (i₁ + i₂) (o₁′ + o₂′)
c₁ [ ≡₁ ]∥ c₂ [ ≡₂ ] = adaptEqO ≡₁ c₁ ∥ adaptEqO ≡₂ c₂
\end{code}
%</par-het-output>
