\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.RegNRec where

open import Function.Base using (_∘′_; _$_)
open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Fin using (#_; _↑ʳ_) renaming (suc to Fs)
open import Data.Bool.Base using () renaming (false to 𝔽; true to 𝕋)
open import Data.Nat.Base using (ℕ; zero; suc; _+_)
open import Data.Product.Base using (_,_; proj₁; proj₂)
open import Data.List.NonEmpty using (_∷_)
open import Data.Vec.Base using (Vec; []; _∷_; _++_; [_]; tabulate; lookup; splitAt; replicate)
open import Data.Vec.Properties using (lookup∘tabulate; tabulate∘lookup)
open import Codata.Musical.Stream using (Stream; _∷_; zipWith; repeat; _≈_; map; zipWith-cong)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-⟩; _∎)

open import Data.Vec.Extra using (splitAt′)
open import Data.Vec.Properties.Extra using (tabulate-cong)
open import Codata.Musical.Stream.Causal using (runᶜ′; runᶜ-const)
open import Codata.Musical.Stream.Equality.WithTrans using (_≈ₚ_; _∷ₚ_; transₚ; reflₚ; ≈ₚ-to-≈)
open import Codata.Musical.Stream.Properties using (module EqReasoningₛ; module Setoidₛ)
open EqReasoningₛ using (step-≈-⟩) renaming (begin_ to beginₛ_; _∎ to _∎ₛ)
open Setoidₛ using () renaming (refl to reflₛ; trans to transₛ)

open import PiWare.Circuit using (Plug; _⟫_; _∥_; ω; IsComb) renaming (module WithGates to CircuitWithGates)
open import PiWare.Gates.BoolTrio using (B₃)
open CircuitWithGates B₃ using (ℂ; C)
open import PiWare.Plugs using (nil⤨)
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Semantics.Simulation Atomic-Bool using (module WithGates)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to b₃)
open WithGates b₃ using (⟦_⟧ω; ⟦_⟧ᶜ; ⟦_⟧)
open import PiWare.Semantics.Simulation.Properties.Sequential Atomic-Bool using (⟦⟧ω[–]-cong; ⟦⟧ω[–]-∥⇒zipWith++; ⟦⟧ω[–]-⟫⇒∘)
open import PiWare.Samples.BoolTrioSeq using (reg; proofReg-never; proofReg-always)

open import Notation.JP.Base using (α; n)
\end{code}


%<*regn-plug>
\AgdaTarget{regn-plug}
\begin{code}
regn-plug : ∀ n → C (2 + n) (3 + n)
regn-plug _ = Plug ((# 1) ∷ (# 0) ∷ (# 0) ∷ tabulate (2 ↑ʳ_))  -- load (bit #0) duplicated
\end{code}
%</regn-plug>


-- An n-sized register
-- load ∷ data → out
-- (note that this is different from the wiring scheme of reg, input ∷ load → out!)
%<*regn>
\AgdaTarget{regn}
\begin{code}
regn : ∀ n → ℂ {ω} (1 + n) n
regn zero     = nil⤨
regn (suc n)  = regn-plug n ⟫ (reg ∥ regn n)
\end{code}
%</regn>



-- TODO: move? (to where?)
%<*regn-plug-tabulates-lookups>
\AgdaTarget{regn-plug-tabulates-lookups}
\begin{code}
regn-plug-tabulates-lookups : ∀ x y {zs : Vec α n} → tabulate (lookup (x ∷ y ∷ zs) ∘′ lookup (tabulate (Fs ∘′ Fs))) ≡ zs
regn-plug-tabulates-lookups x y {zs} = begin
  tabulate (lookup (x ∷ y ∷ zs) ∘′ lookup (tabulate (Fs ∘′ Fs)))  ≡⟨ tabulate-cong (λ k → cong (lookup (x ∷ y ∷ zs)) (lookup∘tabulate (Fs ∘′ Fs) k)) ⟩
  (tabulate ∘′ lookup) zs                                         ≡⟨ tabulate∘lookup zs ⟩
  zs ∎
\end{code}
%</regn-plug-tabulates-lookups>


%<*regn-plug-runᶜ′>
\AgdaTarget{regn-plug-runᶜ′}
\begin{code}
regn-plug-runᶜ′ : ∀ s v x {cs xs}
  →  runᶜ′ ⟦ regn-plug n {s} ⟧ᶜ ((v ∷ x) ∷ cs) (zipWith _∷_ (repeat v) xs)
     ≈ₚ zipWith _++_  (zipWith _++_  (map (proj₁ ∘′ splitAt′ 1) (x ∷ ♯ xs))  (repeat [ v ]))
                      (zipWith _∷_   (repeat v)                              (map (proj₂ ∘′ splitAt′ 1) (x ∷ ♯ xs)))

regn-plug-runᶜ′ _s  _v  x   {xs = _ ∷ _} with splitAt 1 x
regn-plug-runᶜ′ s   v   _x  {xs = _ ∷ _} | xh ∷ [] , _ , refl =
  cong (λ z → xh ∷ v ∷ v ∷ z) (regn-plug-tabulates-lookups v xh) ∷ₚ ♯ transₚ (regn-plug-runᶜ′ s v _) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</regn-plug-runᶜ′>


%<*regn-plug-ω>
\AgdaTarget{regn-plug-ω}
\begin{code}
regn-plug-ω : ∀ s v xs
  →  ⟦ regn-plug n {s} ⟧ω (zipWith _∷_ (repeat v) xs)
     ≈ zipWith _++_  (zipWith _++_  (map (proj₁ ∘′ splitAt′ 1) xs)  (repeat [ v ]))
                     (zipWith _∷_   (repeat v)                      (map (proj₂ ∘′ splitAt′ 1) xs))

regn-plug-ω s v (x ∷ _) = ≈ₚ-to-≈ $ transₚ (regn-plug-runᶜ′ s v x) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</regn-plug-ω>


%<*regN-never-data𝔽>
\AgdaTarget{regN-never-data𝔽}
\begin{code}
regN-never-data𝔽 : zipWith _++_ (repeat [ 𝔽 ]) (repeat (replicate n 𝔽)) ≈ repeat (replicate (suc n) 𝔽)
regN-never-data𝔽 = refl ∷ ♯ regN-never-data𝔽
\end{code}
%</regN-never-data𝔽>


%<*proofRegN-never>
\AgdaTarget{proofRegN-never}
\begin{code}
proofRegN-never : ∀ xs {s : IsComb} → ⟦ regn n ⟧ω (zipWith _∷_ (repeat 𝔽) xs) ≈ repeat (replicate n 𝔽)
proofRegN-never {zero}    xs      = runᶜ-const  {xs = zipWith _∷_ (repeat 𝔽) xs} (replicate zero 𝔽)
proofRegN-never {suc n′}  xs {s}  = beginₛ
  ⟦ regn (suc n′) ⟧ω (zipWith _∷_ (repeat 𝔽) xs)
    ≈⟨ reflₛ ⟩
  ⟦ regn-plug n′ ⟫ (reg ∥ regn n′) ⟧ω (zipWith _∷_ (repeat 𝔽) xs)
    ≈⟨ ⟦⟧ω[–]-⟫⇒∘ b₃ (regn-plug n′) (reg ∥ regn n′) {zipWith _∷_ (repeat 𝔽) xs} ⟩
  ⟦ reg ∥ regn n′ ⟧ω (⟦ regn-plug n′ {s} ⟧ω (zipWith _∷_ (repeat 𝔽) xs))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (reg ∥ regn n′) (regn-plug-ω s _ xs) ⟩
  ⟦ reg ∥ regn n′ ⟧ω (zipWith _++_ (zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (repeat [ 𝔽 ])) (zipWith _∷_ (repeat 𝔽) (map (proj₂ ∘′ splitAt′ 1) xs)))
    ≈⟨ ⟦⟧ω[–]-∥⇒zipWith++ b₃ reg (regn n′) {zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (repeat [ 𝔽 ])} {zipWith _∷_ (repeat 𝔽) (map (proj₂ ∘′ splitAt′ 1) xs)} ⟩
  zipWith _++_ (⟦ reg ⟧ω (zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (repeat [ 𝔽 ]))) (⟦ regn n′ ⟧ω (zipWith _∷_ (repeat 𝔽) (map (proj₂ ∘′ splitAt′ 1) xs)))
    ≈⟨ zipWith-cong _++_ (proofReg-never {map (proj₁ ∘′ splitAt′ 1) xs}) (proofRegN-never {n′} (map (proj₂ ∘′ splitAt′ 1) xs) {s}) ⟩
  zipWith _++_ (repeat [ 𝔽 ]) (repeat (replicate n′ 𝔽))
    ≈⟨ regN-never-data𝔽 {n′} ⟩
  repeat (replicate (suc n′) 𝔽) ∎ₛ
\end{code}
%</proofRegN-never>


%<*repeat[]≈xs-Vec0>
\AgdaTarget{repeat[]≈xs-Vec0}
\begin{code}
repeat[]≈xs-Vec0 : (xs : Stream (Vec α zero)) → repeat [] ≈ xs
repeat[]≈xs-Vec0 ([] ∷ xs′) = refl ∷ ♯ repeat[]≈xs-Vec0 (♭ xs′)
\end{code}
%</repeat[]≈xs-Vec0>


%<*zipWith++-map-splitAt>
\AgdaTarget{zipWith++-map-splitAt}
\begin{code}
zipWith++-map-splitAt : (xs : Stream (Vec α (suc n))) → zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (map (proj₂ ∘′ splitAt′ 1) xs) ≈ xs
zipWith++-map-splitAt (x ∷ _)              with splitAt 1 x
zipWith++-map-splitAt (.(_h ++ _t) ∷ xs′)  | _h , _t , refl = refl ∷ ♯ zipWith++-map-splitAt (♭ xs′)
\end{code}
%</zipWith++-map-splitAt>


%<*proofRegN-always>
\AgdaTarget{proofRegN-always}
\begin{code}
proofRegN-always : ∀ xs {s : IsComb} → ⟦ regn n ⟧ω (zipWith _∷_ (repeat 𝕋) xs) ≈ xs
proofRegN-always {zero}    xs      = transₛ (runᶜ-const {xs = zipWith _∷_ (repeat 𝕋) xs} []) (repeat[]≈xs-Vec0 xs)
proofRegN-always {suc n′}  xs {s}  = beginₛ
  ⟦ regn (suc n′) ⟧ω (zipWith _∷_ (repeat 𝕋) xs)
    ≈⟨ reflₛ ⟩
  ⟦ regn-plug n′ ⟫ (reg ∥ regn n′) ⟧ω (zipWith _∷_ (repeat 𝕋) xs)
    ≈⟨ ⟦⟧ω[–]-⟫⇒∘ b₃ (regn-plug n′) (reg ∥ regn n′) {zipWith _∷_ (repeat 𝕋) xs} ⟩
  ⟦ reg ∥ regn n′ ⟧ω (⟦ regn-plug n′ {s} ⟧ω (zipWith _∷_ (repeat 𝕋) xs))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (reg ∥ regn n′) (regn-plug-ω ω _ xs) ⟩
  ⟦ reg ∥ regn n′ ⟧ω (zipWith _++_ (zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (repeat [ 𝕋 ])) (zipWith _∷_ (repeat 𝕋) (map (proj₂ ∘′ splitAt′ 1) xs)))
    ≈⟨ ⟦⟧ω[–]-∥⇒zipWith++ b₃ reg (regn n′) {zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (repeat [ 𝕋 ])} {zipWith _∷_ (repeat 𝕋) (map (proj₂ ∘′ splitAt′ 1) xs)} ⟩
  zipWith _++_ (⟦ reg ⟧ω (zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (repeat [ 𝕋 ]))) (⟦ regn n′ ⟧ω (zipWith _∷_ (repeat 𝕋) (map (proj₂ ∘′ splitAt′ 1) xs)))
    ≈⟨ zipWith-cong _++_ (proofReg-always {xs = map (proj₁ ∘′ splitAt′ 1) xs}) (proofRegN-always {n′} (map (proj₂ ∘′ splitAt′ 1) xs) {s}) ⟩
  zipWith _++_ (map (proj₁ ∘′ splitAt′ 1) xs) (map (proj₂ ∘′ splitAt′ 1) xs)
    ≈⟨ zipWith++-map-splitAt xs ⟩
  xs ∎ₛ
\end{code}
%</proofRegN-always>
