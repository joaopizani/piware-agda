\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.BoolTrioComb where

open import Function.Base using (_∘′_)
open import Data.Product.Base using (proj₂)
open import Data.Nat.Base using (zero; suc; _+_; _*_)
open import Data.Nat.Properties using (+-identityʳ)
open import Data.Bool.Base using (not; _∧_; _∨_; _xor_) renaming (false to 𝔽; true to 𝕋)
open import Data.Vec.Base using ([]; _∷_; [_]; replicate)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; cong)
open import Data.Bool.Properties using (∨-∧-commutativeSemiring)
open import Algebra.Bundles using (module CommutativeSemiring)
open CommutativeSemiring ∨-∧-commutativeSemiring using () renaming (+-identity to ∨-identity)

open import PiWare.Plugs using (id⤨; id⤨₁; fork×⤨; swap⤨)
open import PiWare.Gates.BoolTrio using (⊥ℂ'; ⊤ℂ'; ¬ℂ'; ∧ℂ'; ∨ℂ'; B₃)
open import PiWare.Circuit using (σ; C[_]; Gate; _⟫_; _∥_) renaming (module WithGates to CircuitWithGates)
open CircuitWithGates B₃ using (ℂ; C)
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Patterns Atomic-Bool using (linear-reductor)
open import PiWare.Semantics.Simulation Atomic-Bool using (W; W⟶W) renaming (module WithGates to SimulationWithGates)
open import PiWare.Semantics.Simulation.Compliance Atomic-Bool using () renaming (module WithGates to ComplianceWithGates)
open import PiWare.Semantics.Simulation.BoolTrio using (spec-∧ℂ) renaming (spec to spec-B₃)
open SimulationWithGates spec-B₃ using (⟦_⟧)
open ComplianceWithGates spec-B₃ using (_⫃_)

open import PiWare.Gates using (G)
\end{code}

\begin{code}
open import Data.Nat.Base using (ℕ)
private variable k : ℕ
\end{code}


%<*fundamentals-type>
\begin{code}
⊥ℂ ⊤ℂ  : C 0 1
∧ℂ ∨ℂ  : C 2 1
¬ℂ     : C 1 1
\end{code}
%</fundamentals-type>

%<*fundamentals-def>
\AgdaTarget{⊥ℂ, ⊤ℂ, ¬ℂ, ∧ℂ, ∨ℂ}
\begin{code}
⊥ℂ = Gate ⊥ℂ'
⊤ℂ = Gate ⊤ℂ'
¬ℂ = Gate ¬ℂ'
∧ℂ = Gate ∧ℂ'
∨ℂ = Gate ∨ℂ'
\end{code}
%</fundamentals-def>


%<*nand>
\AgdaTarget{⊼ℂ}
\begin{code}
⊼ℂ : C 2 1
⊼ℂ = ∧ℂ ⟫ ¬ℂ
\end{code}
%</nand>


%<*xor>
\AgdaTarget{⊻ℂ}
\begin{code}
⊻ℂ : C 2 1
⊻ℂ =     fork×⤨
      ⟫  (¬ℂ ∥ id⤨₁ ⟫ ∧ℂ)  ∥  (id⤨₁ ∥ ¬ℂ ⟫ ∧ℂ)
      ⟫  ∨ℂ
\end{code}
%</xor>


%<*andN>
\AgdaTarget{andN}
\begin{code}
andN : ∀ n → ℂ {σ} (n * 1) 1
andN n = (linear-reductor n) ∧ℂ ⊤ℂ
\end{code}
%</andN>


-- a × b → s × c
%<*hadd>
\AgdaTarget{hadd}
\begin{code}
hadd : C 2 2
hadd =     fork×⤨
        ⟫  ⊻ℂ ∥ ∧ℂ
\end{code}
%</hadd>


-- cin × a × b → s × c
%<*fadd>
\AgdaTarget{fadd}
\begin{code}
fadd : C (1 + 2) (1 + 1)
fadd =     id⤨₁  ∥  hadd
        ⟫  hadd  ∥  id⤨₁
        ⟫  id⤨₁  ∥  ∨ℂ
\end{code}
%</fadd>


\begin{code}
row : ∀ n → C[ G ] (k + (k + k)) (k + k) → C[ G ] (k + (n * k + n * k)) (n * k + k)
row {k} zero      _ rewrite +-identityʳ k = id⤨
row {k} (suc n′)  c = ⊥ where postulate ⊥ : _
--     id⤨ {k + k}  ∥ swap⤨ {n * k} {k + n * k}
--  ⟫  c            ∥ id⤨
--  ⟫  id⤨ {k}      ∥ row n
\end{code}

-- cin × a × b → s × co
%<*ripple>
\AgdaTarget{ripple}
\begin{code}
ripple : ∀ n → C (1 + (n + n)) (n + 1)
ripple zero    = id⤨₁
ripple (suc n′) =     id⤨ {m = 1 + 1}  ∥  swap⤨   {a = n′} {1 + n′}
                   ⟫  fadd             ∥  id⤨     {m = n′ + n′}
                   ⟫  id⤨₁             ∥  ripple  n′
\end{code}
%</ripple>


%<*and-func>
\AgdaTarget{∧-func}
\begin{code}
∧-func : W⟶W 2 1
∧-func (x ∷ y ∷ []) = [_] (_∧_ x y)
\end{code}
%</and-func>


%<*and-proof-func>
\AgdaTarget{∧ℂ-proof-func}
\begin{code}
∧ℂ-proof-func : ∧ℂ ⫃ ∧-func
∧ℂ-proof-func (_ ∷ _ ∷ []) = refl
\end{code}
%</and-proof-func>


%<*xor-table>
\AgdaTarget{⊻-table}
\begin{code}
⊻-table : W⟶W 2 1
⊻-table (𝔽 ∷ 𝔽 ∷ []) = [ 𝔽 ]
⊻-table (𝔽 ∷ 𝕋 ∷ []) = [ 𝕋 ]
⊻-table (𝕋 ∷ 𝔽 ∷ []) = [ 𝕋 ]
⊻-table (𝕋 ∷ 𝕋 ∷ []) = [ 𝔽 ]
\end{code}
%</xor-table>


%<*xor-proof-table>
\AgdaTarget{⊻ℂ-proof-table}
\begin{code}
⊻ℂ-proof-table : ⊻ℂ ⫃ ⊻-table
⊻ℂ-proof-table (𝔽 ∷ 𝔽 ∷ []) = refl
⊻ℂ-proof-table (𝔽 ∷ 𝕋 ∷ []) = refl
⊻ℂ-proof-table (𝕋 ∷ 𝔽 ∷ []) = refl
⊻ℂ-proof-table (𝕋 ∷ 𝕋 ∷ []) = refl
\end{code}
%</xor-proof-table>


%<*xor-func>
\AgdaTarget{⊻-func}
\begin{code}
⊻-func : W⟶W 2 1
⊻-func (x ∷ y ∷ []) = [_] (_xor_ x y)
\end{code}
%</xor-func>


%<*xor-proof-func>
\AgdaTarget{⊻-proof-func}
\begin{code}
⊻ℂ-proof-func : ⊻ℂ ⫃ ⊻-func
⊻ℂ-proof-func (𝕋 ∷ _ ∷ []) = refl
⊻ℂ-proof-func (𝔽 ∷ y ∷ []) = cong [_] ((proj₂ ∨-identity) y)
\end{code}
%</xor-proof-func>


%<*hadd-func>
\AgdaTarget{hadd-func}
\begin{code}
hadd-func : W⟶W 2 2
hadd-func (x ∷ y ∷ []) = (_xor_ x y) ∷ (_∧_ x y) ∷ []
\end{code}
%</hadd-func>


%<*xor-lemma>
\AgdaTarget{⊻-lemma}
\begin{code}
⊻-lemma : ∀ a b → (not a ∧ b) ∨ (a ∧ not b) ≡ a xor b
⊻-lemma 𝕋  _ = refl
⊻-lemma 𝔽  b = (proj₂ ∨-identity) b
\end{code}
%</xor-lemma>

%<*hadd-proof-func>
\AgdaTarget{hadd-proof-func}
\begin{code}
hadd-proof-func : hadd ⫃ hadd-func
hadd-proof-func (x ∷ y ∷ []) = cong (_∷ [ x ∧ y ]) (⊻-lemma x y)
\end{code}
%</hadd-proof-func>


%<*fadd-table>
\AgdaTarget{fadd-table}
\begin{code}
fadd-table : W⟶W 3 2
fadd-table (𝔽 ∷ 𝔽 ∷ 𝔽 ∷ []) = 𝔽 ∷ 𝔽 ∷ []
fadd-table (𝔽 ∷ 𝔽 ∷ 𝕋 ∷ []) = 𝕋 ∷ 𝔽 ∷ []
fadd-table (𝔽 ∷ 𝕋 ∷ 𝔽 ∷ []) = 𝕋 ∷ 𝔽 ∷ []
fadd-table (𝔽 ∷ 𝕋 ∷ 𝕋 ∷ []) = 𝔽 ∷ 𝕋 ∷ []
fadd-table (𝕋 ∷ 𝔽 ∷ 𝔽 ∷ []) = 𝕋 ∷ 𝔽 ∷ []
fadd-table (𝕋 ∷ 𝔽 ∷ 𝕋 ∷ []) = 𝔽 ∷ 𝕋 ∷ []
fadd-table (𝕋 ∷ 𝕋 ∷ 𝔽 ∷ []) = 𝔽 ∷ 𝕋 ∷ []
fadd-table (𝕋 ∷ 𝕋 ∷ 𝕋 ∷ []) = 𝕋 ∷ 𝕋 ∷ []
\end{code}
%</fadd-table>


%<*fadd-proof-table>
\AgdaTarget{fadd-proof-table}
\begin{code}
fadd-proof-table : fadd ⫃ fadd-table
fadd-proof-table (𝕋 ∷ 𝕋 ∷ 𝕋 ∷ []) = refl
fadd-proof-table (𝕋 ∷ 𝕋 ∷ 𝔽 ∷ []) = refl
fadd-proof-table (𝕋 ∷ 𝔽 ∷ 𝕋 ∷ []) = refl
fadd-proof-table (𝕋 ∷ 𝔽 ∷ 𝔽 ∷ []) = refl
fadd-proof-table (𝔽 ∷ 𝕋 ∷ 𝕋 ∷ []) = refl
fadd-proof-table (𝔽 ∷ 𝕋 ∷ 𝔽 ∷ []) = refl
fadd-proof-table (𝔽 ∷ 𝔽 ∷ 𝕋 ∷ []) = refl
fadd-proof-table (𝔽 ∷ 𝔽 ∷ 𝔽 ∷ []) = refl
\end{code}
%</fadd-proof-table>


%<*proof-andN-alltrue>
\AgdaTarget{proof-andN-alltrue}
\begin{code}
proof-andN-alltrue : ∀ n → ⟦ andN n ⟧ (replicate (n * 1) 𝕋) ≡ [ 𝕋 ]
proof-andN-alltrue zero     = refl
proof-andN-alltrue (suc n)  = cong (spec-∧ℂ ∘′ (𝕋 ∷_)) (proof-andN-alltrue n)
\end{code}
%</proof-andN-alltrue>
