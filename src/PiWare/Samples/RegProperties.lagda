\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.RegProperties where

open import Function.Base using (_$_)
open import Data.Bool.Base using (Bool) renaming (false to 𝔽; true to 𝕋)
open import Data.Product.Base using (_,_; proj₂; proj₁)
open import Codata.Musical.Notation using (♯_; ♭)
open import Codata.Musical.Stream using (_∷_; head; take; drop)
open import Relation.Binary.PropositionalEquality using (_≡_; cong; refl; module ≡-Reasoning)
open import Data.List.Base using ([]; _∷_)
open import Data.List.NonEmpty using (_∷_)
open import Data.Nat.Base using (zero; suc; _+_)
open import Data.Nat.Properties using (+-comm)
open import Data.Vec.Base using (_∷_; []; [_])
open import Data.Vec.Properties using (∷-injective)
open ≡-Reasoning using (begin_; step-≡-⟩; _∎)

open import Algebra.Bundles using (module CommutativeSemiring)
open import Algebra.Structures using (module IsCommutativeMonoid)
open import Data.Bool.Properties using (∨-∧-commutativeSemiring)
open CommutativeSemiring ∨-∧-commutativeSemiring using () renaming (+-isCommutativeMonoid to ∨-isCommutativeMonoid)
open IsCommutativeMonoid ∨-isCommutativeMonoid using () renaming (identity to ∨-identity)

open import Codata.Musical.Stream.Causal using (runᶜ′)
open import Codata.Musical.Stream.DistantReasoning using (joinProofs; selectProof⁺; selectProof⁻; takeDrop-cong; take-1-head)

open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to spec-B₃)
open import PiWare.Semantics.Simulation Atomic-Bool using (module WithGates)
open WithGates spec-B₃ using (⟦_⟧ω; ⟦_⟧ᶜ; ⟦_⟧)
open import PiWare.Samples.BoolTrioSeq using (reg; regCore)

open import Data.Serializable using (_⇕_; ⇕⊤; ⇕×)
open import Data.Serializable.Bool using (⇕Bool)
open _⇕_ ⇕Bool using (⇓)
open _⇕_ (⇕× ⦃ ⇕Bool ⦄ ⦃ ⇕Bool ⦄) using () renaming (⇓ to ⇓²)
\end{code}


%<*regW,R-runᶜ>
\AgdaTarget{regW,R-runᶜ}
\begin{code}
regW,R-runᶜ : ∀ {d⁰ d¹ x⁻ x²⁺} → take 2 (runᶜ′ ⟦ reg ⟧ᶜ (⇓² (d⁰ , 𝕋) ∷ x⁻) (⇓² (d¹ , 𝔽) ∷ x²⁺)) ≡ ⇓ d⁰ ∷ ⇓ d⁰ ∷ []
regW,R-runᶜ {d⁰} {x²⁺ = x²⁺}        with ♭ x²⁺
regW,R-runᶜ {d⁰} {d¹} {x⁻ = []}     | x² ∷ x³⁺  = cong (λ x → [ d⁰ ] ∷ [ x ] ∷ []) $ (proj₂ ∨-identity) d⁰
regW,R-runᶜ {d⁰} {d¹} {x⁻ = _ ∷ _}  | x² ∷ x³⁺  = cong (λ x → [ d⁰ ] ∷ [ x ] ∷ []) $ (proj₂ ∨-identity) d⁰
\end{code}
%</regW,R-runᶜ>


%<*regW,R-runᶜ-I⇒O>
\AgdaTarget{regW,R-runᶜ-I⇒O}
\begin{code}
regW,R-runᶜ-I⇒O :  ∀ {n d⁰ d¹ x⁻ x⁰ x⁺}
                   → take 2 (drop n (x⁰ ∷ x⁺))                         ≡ ⇓² (d⁰ , 𝕋) ∷ ⇓² (d¹ , 𝔽) ∷ []
                   → take 2 (drop n (runᶜ′ ⟦ reg ⟧ᶜ (x⁰ ∷ x⁻) (♭ x⁺)))  ≡ ⇓ d⁰ ∷ ⇓ d⁰ ∷ []
regW,R-runᶜ-I⇒O {zero} {x⁺ = x⁺}                               _      with ♭ x⁺
regW,R-runᶜ-I⇒O {zero} {d⁰} {d¹} {_}   {.(⇓² (d⁰ , 𝕋))}       refl  | .(⇓² (d¹ , 𝔽)) ∷ x²⁺ with ♭ x²⁺
regW,R-runᶜ-I⇒O {zero} {d⁰} {d¹} {x⁻}  {.(⇓² (d⁰ , 𝕋))} {x⁺}  refl  | .(⇓² (d¹ , 𝔽)) ∷ x²⁺ | _ ∷ _ =
  regW,R-runᶜ {d¹ = d¹} {x⁻} {♯ ((d⁰ ∷ 𝕋 ∷ []) ∷ x⁺)}

regW,R-runᶜ-I⇒O {suc _} {x⁺ = x⁺} p with ♭ x⁺
regW,R-runᶜ-I⇒O {suc n} p | _ ∷ _ = regW,R-runᶜ-I⇒O {n} p
\end{code}
%</regW,R-runᶜ-I⇒O>


%<*regW,R-I⇒O>
\AgdaTarget{regW,R-I⇒O}
\begin{code}
regW,R-I⇒O : ∀ {n d⁰ d¹ xs} → take 2 (drop n xs) ≡ ⇓² (d⁰ , 𝕋) ∷ ⇓² (d¹ , 𝔽) ∷ [] → take 2 (drop n (⟦ reg ⟧ω xs)) ≡ ⇓ d⁰ ∷ ⇓ d⁰ ∷ []
regW,R-I⇒O {n} {xs = _ ∷ _} p = regW,R-runᶜ-I⇒O {n} p
\end{code}
%</regW,R-I⇒O>



%<*regR,R-runᶜ>
\AgdaTarget{regR,R-runᶜ}
\begin{code}
regR,R-runᶜ :  ∀ {w d⁰ d¹ x⁻ x⁺}
               → head    (runᶜ′ ⟦ reg ⟧ᶜ (⇓² (d⁰ , 𝔽) ∷ x⁻) (⇓² (d¹ , 𝔽) ∷ x⁺)) ≡ ⇓ w
               → take 2  (runᶜ′ ⟦ reg ⟧ᶜ (⇓² (d⁰ , 𝔽) ∷ x⁻) (⇓² (d¹ , 𝔽) ∷ x⁺)) ≡ ⇓ w ∷ ⇓ w ∷ []
regR,R-runᶜ {x⁺ = x⁺}           _     with ♭ x⁺
regR,R-runᶜ {.𝔽}  {x⁻ = []}     refl  | _ ∷ _ = refl
regR,R-runᶜ {w}   {x⁻ = _ ∷ _}  ph    | _ ∷ _ rewrite proj₁ (∷-injective ph) | (proj₂ ∨-identity) w = refl
\end{code}
%</regR,R-runᶜ>


%<*regR,R-runᶜ-I⇒O>
\AgdaTarget{regR,R-runᶜ-I⇒O}
\begin{code}
regR,R-runᶜ-I⇒O :  ∀ {n w d⁰ d¹ x⁻ x⁰ x⁺}
                   → take 2  (drop n (x⁰ ∷ x⁺))                         ≡ ⇓² (d⁰ , 𝔽) ∷ ⇓² (d¹ , 𝔽) ∷ []  -- in
                   → head    (drop n (runᶜ′ ⟦ reg ⟧ᶜ (x⁰ ∷ x⁻) (♭ x⁺)))  ≡ ⇓ w  -- output head
                   → take 2  (drop n (runᶜ′ ⟦ reg ⟧ᶜ (x⁰ ∷ x⁻) (♭ x⁺)))  ≡ ⇓ w ∷ ⇓ w ∷ []  -- output prefix
regR,R-runᶜ-I⇒O {zero} {x⁺ = x⁺}                                         _    _   with ♭ x⁺
regR,R-runᶜ-I⇒O {zero} {d⁰ = d⁰} {d¹}       {x⁰ = .(⇓² (d⁰ , 𝔽))}  {_}   refl ph  | .(⇓² (d¹ , 𝔽)) ∷ x²⁺  with ♭ x²⁺
regR,R-runᶜ-I⇒O {zero} {d⁰ = d⁰} {d¹} {x⁻}  {.(⇓² (d⁰ , 𝔽))}       {x⁺}  refl ph  | .(⇓² (d¹ , 𝔽)) ∷ _    | _ ∷ _ =
  regR,R-runᶜ {d⁰ = d⁰} {d¹} {x⁻} {♯ ((d¹ ∷ 𝔽 ∷ []) ∷ x⁺)} ph

regR,R-runᶜ-I⇒O {suc _} {x⁺ = x⁺} _  _   with ♭ x⁺
regR,R-runᶜ-I⇒O {suc n}           pp ph  | _ ∷ _ = regR,R-runᶜ-I⇒O {n} pp ph
\end{code}
%</regR,R-runᶜ-I⇒O>


%<*regR,R-I⇒O>
\AgdaTarget{regR,R-I⇒O}
\begin{code}
regR,R-I⇒O :  ∀ {n w d⁰ d¹ xs}
              → take 2  (drop n xs) ≡ ⇓² (d⁰ , 𝔽) ∷ ⇓² (d¹ , 𝔽) ∷ []
              → head    (drop n (⟦ reg ⟧ω xs)) ≡ ⇓ w
              → take 2  (drop n (⟦ reg ⟧ω xs)) ≡ ⇓ w ∷ ⇓ w ∷ []
regR,R-I⇒O {n} {w} {xs = _ ∷ _} pp ph = regR,R-runᶜ-I⇒O {n} pp ph
\end{code}
%</regR,R-I⇒O>



%<*regW,R,W,R-I⇒O>
\AgdaTarget{regW,R,W,R-I⇒O}
\begin{code}
regW,R,W,R-I⇒O :  ∀ {n w₁ z₁ w₂ z₂ xs}
                  → take 4 (drop n xs)             ≡ ⇓² (w₁ , 𝕋) ∷ ⇓² (z₁ , 𝔽) ∷ ⇓² (w₂ , 𝕋) ∷ ⇓² (z₂ , 𝔽) ∷ []
                  → take 4 (drop n (⟦ reg ⟧ω xs))  ≡ ⇓ w₁ ∷ ⇓ w₁ ∷ ⇓ w₂ ∷ ⇓ w₂ ∷ []
regW,R,W,R-I⇒O {n} {xs = xs} p = joinProofs {n}  (regW,R-I⇒O {n}      {xs = xs} (selectProof⁺ p))
                                                 (regW,R-I⇒O {n + 2}  {xs = xs} (selectProof⁻ {n} p))
\end{code}
%</regW,R,W,R-I⇒O>



\begin{code}
private
\end{code}
%<*n+2≡1+[n+1]>
\AgdaTarget{n+2≡1+[n+1]}
\begin{code}
  n+2≡1+[n+1] : ∀ {n} → n + 2 ≡ suc (n + 1)
  n+2≡1+[n+1] {zero}   = refl
  n+2≡1+[n+1] {suc n}  = cong suc (n+2≡1+[n+1] {n})
\end{code}
%</n+2≡1+[n+1]>


%<*regW,R,R-I⇒O>
\AgdaTarget{regW,R,R-I⇒O}
\begin{code}
regW,R,R-I⇒O :  ∀ {n w d⁰ d¹ xs}
                → take 3 (drop n xs)             ≡ ⇓² (w , 𝕋)  ∷ ⇓² (d⁰ , 𝔽)  ∷ ⇓² (d¹ , 𝔽)  ∷ []  -- in
                → take 3 (drop n (⟦ reg ⟧ω xs))  ≡ ⇓ w         ∷ ⇓ w          ∷ ⇓ w          ∷ []  -- out
regW,R,R-I⇒O {n} {w} {d⁰} {d¹} {xs} I[n~3] = joinProofs {n} p₁ p₂
 where
  p₁  = regW,R-I⇒O {n} {d⁰ = w} {d¹ = d⁰} {xs} (selectProof⁺ I[n~3])
  eq₁ = begin
         take 2 (drop (1 + n) xs)            ≡⟨ takeDrop-cong {d₁ = 1 + n} {n + 1} (+-comm 1 n) ⟩
         take 2 (drop (n + 1) xs)            ≡⟨ selectProof⁻ {n} {1} I[n~3] ⟩
         (d⁰ ∷ 𝔽 ∷ []) ∷ (d¹ ∷ 𝔽 ∷ []) ∷ []  ∎
  eq₂ = take-1-head $ begin
         take 1 (drop (1 + n) (⟦ reg ⟧ω xs))  ≡⟨ takeDrop-cong {d₁ = 1 + n} {d₂ = n + 1} (+-comm 1 n) ⟩
         take 1 (drop (n + 1) (⟦ reg ⟧ω xs))  ≡⟨ selectProof⁻ {n} {1} p₁ ⟩
         [ w ] ∷ []                           ∎
  p₂  = begin
         take 1 (drop (n + 2)        (⟦ reg ⟧ω xs))  ≡⟨ takeDrop-cong {d₁ = n + 2} {1 + (n + 1)} (n+2≡1+[n+1] {n}) ⟩
         take 1 (drop (1 + (n + 1))  (⟦ reg ⟧ω xs))  ≡⟨ selectProof⁻ {suc n} {s = ⟦ reg ⟧ω xs} (regR,R-I⇒O {suc n} {w} {d⁰} {d¹} {xs} eq₁ eq₂)⟩
         ⇓ w ∷ []                                    ∎
\end{code}
%</regW,R,R-I⇒O>
