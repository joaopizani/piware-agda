\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.BoolTrioSeq where

open import Function.Base using (_$_; _⟨_⟩_; _∘′_; id)
open import Data.Unit.Base using (⊤; tt)
open import Data.Nat.Base using (_+_)
open import Data.Product.Base using (∃; _,_)
open import Data.Fin using (#_)
open import Data.List.Base using ([]; _∷_)
open import Data.List.Relation.Unary.All using (All; _∷_; [])
open import Data.List.NonEmpty.Base using (_∷_) renaming ([_] to [_]⁺)
open import Data.Bool.Base using (Bool; not) renaming (false to 𝔽; true to 𝕋)
open import Data.Vec.Base using (Vec; _++_; []; _∷_; [_]; lookup; head)
open import Data.Vec.Properties.Extra using (singleton∘head)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; inspect; cong) renaming ([_] to [_]ᵣ)
open import Codata.Musical.Notation using (♯_; ♭)
open import Codata.Musical.Stream using (Stream; _∷_; repeat; take; iterate; tail; _≈_; map; zipWith)

open import Codata.Musical.Stream.Causal using (runᶜ′)
open import Codata.Musical.Stream.Extra using (zip)
open import Codata.Musical.Stream.Properties using (≡-to-≈; map-repeat; map-∘; map-cong-fun; map-id; module Setoidₛ; module EqReasoningₛ)
open import Codata.Musical.Stream.Equality.WithTrans using (_≈ₚ_; _∷ₚ_; ≈ₚ-to-≈; reflₚ; transₚ)
open Setoidₛ using () renaming (refl to reflₛ; sym to symₛ; trans to transₛ)
open EqReasoningₛ using (step-≈-⟩) renaming (begin_ to beginₛ_; _∎ to _∎ₛ)

open import PiWare.Plugs.Core using (_⤪_; _⟫⤪_; _|⤪_; id⤪₁; swap⤪₁)
open import PiWare.Plugs using (swap⤨₁; fork×⤨)
open import PiWare.Gates.BoolTrio using (B₃)
open import PiWare.Circuit using (Plug; DelayLoop; _⟫_) renaming (module WithGates to CircuitWithGates)
open CircuitWithGates B₃ using (ℂ; C)
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Semantics.Simulation Atomic-Bool using (module WithGates; delay′; W)
open import PiWare.Semantics.SimulationState Atomic-Bool using (mealy; DelayLoopₛ; Plugₛ; Gateₛ; _⟫ₛ_) renaming (module WithGates to WithGatesₛ)
open import PiWare.Semantics.Simulation.Properties.Sequential Atomic-Bool using (⟦⟧ω[–]-cong)
open import PiWare.Samples.BoolTrioComb using (¬ℂ; ⊥ℂ; ∨ℂ)
open import PiWare.Samples.Muxes using (mux)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to b₃)
open WithGates b₃ using (⟦_⟧; ⟦_⟧ᶜ; ⟦_⟧ω)
open WithGatesₛ b₃ using (⟦_⟧ₛ; ⟦_⟧ω'; ℂₛ)

open import Data.Serializable using (_⇕_; ⇕⊤; ⇕×)
open import Data.Serializable.Bool using (⇕Bool)

open _⇕_ ⇕Bool
open _⇕_ {⊤} {Bool} ⇕⊤ using () renaming (⇓ to ⇓⊤)
open _⇕_ ⇕Bool using () renaming (⇑ to ⇑¹; ⇓ to ⇓¹)
open _⇕_ (⇕× ⦃ ⇕Bool ⦄ ⦃ ⇕Bool ⦄) using () renaming (⇓ to ⇓²)

open import Notation.JP.Base using (α)
\end{code}


%<*shift>
\AgdaTarget{shift}
\begin{code}
shift : ℂ 1 1
shift = DelayLoop swap⤨₁
\end{code}
%</shift>


%<*lookup#1-singletons>
\begin{code}
lookup#1-singletons : (a b {c} : Vec α 1) → [ lookup (a ++ [ lookup (b ++ c) (# 0) ]) (# 1) ] ≡ b
lookup#1-singletons (_ ∷ []) (_ ∷ []) = refl
\end{code}
%</lookup#1-singletons>

%<*shift-runᶜ′>
\begin{code}
shift-runᶜ′ : ∀ {x⁻ x⁻¹ x⁰ x⁺} → runᶜ′ ⟦ shift ⟧ᶜ (x⁰ ∷ x⁻¹ ∷ x⁻) x⁺ ≈ₚ x⁻¹ ∷ ♯ (x⁰ ∷ ♯ x⁺)
shift-runᶜ′ {[]}        {x⁻¹} {x⁰} {x¹ ∷ x⁺} = lookup#1-singletons x⁰ x⁻¹ ∷ₚ ♯ transₚ (shift-runᶜ′ {x⁻¹ ∷ []}        {x⁰} {x¹} {♭ x⁺}) (refl ∷ₚ ♯ (refl ∷ₚ ♯ reflₚ))
shift-runᶜ′ {x⁻² ∷ x⁻}  {x⁻¹} {x⁰} {x¹ ∷ x⁺} = lookup#1-singletons x⁰ x⁻¹ ∷ₚ ♯ transₚ (shift-runᶜ′ {x⁻¹ ∷ x⁻² ∷ x⁻}  {x⁰} {x¹} {♭ x⁺}) (refl ∷ₚ ♯ (refl ∷ₚ ♯ reflₚ))
\end{code}
%</shift-runᶜ′>


%<*proofShift>
\AgdaTarget{proofShift}
\begin{code}
proofShift : ∀ {xs} → tail (⟦ shift ⟧ω xs) ≈ xs
proofShift {_ ∷ x⁺}  with ♭ x⁺   | inspect ♭ x⁺
proofShift {_ ∷ _}   | _ ∷ _     | [ xseq ]ᵣ =  transₛ  (≈ₚ-to-≈ shift-runᶜ′)
                                                        (refl ∷ ♯ transₛ (refl ∷ ♯ reflₛ) (symₛ (≡-to-≈ xseq)))
\end{code}
%</proofShift>


%<*singleton∘lookup1>
\AgdaTarget{singleton∘lookup1}
\begin{code}
singleton∘lookup1 : ∀ (x : Vec α 1) {c} → [ lookup (x ++ [ c ]) (# 1) ] ≡ [ c ]
singleton∘lookup1 (_ ∷ []) = refl
\end{code}
%</singleton∘lookup1>

%<*singleton∘lookup0>
\AgdaTarget{singleton∘lookup0}
\begin{code}
singleton∘lookup0 : ∀ (x : Vec α 1) {c} → [ lookup (x ++ [ c ]) (# 0) ] ≡ x
singleton∘lookup0 (_ ∷ []) = refl
\end{code}
%</singleton∘lookup0>

%<*lemmaShiftₚ>
\AgdaTarget{lemmaShiftₚ}
\begin{code}
lemmaShiftₚ : ∀ {c ins} → mealy ⟦ shift ⟧ₛ (DelayLoopₛ (⇓ c) Plugₛ) ins ≈ₚ ⇓ c ∷ ♯ ins
lemmaShiftₚ {_c} {x ∷ _xs′} = singleton∘lookup1 x ∷ₚ ♯ transₚ lemmaShiftₚ (singleton∘lookup0 x ∷ₚ ♯ reflₚ)
\end{code}
%</lemmaShiftₚ>


%<*lemmaShift>
\AgdaTarget{lemmaShift}
\begin{code}
lemmaShift : ∀ {c ins} → mealy ⟦ shift ⟧ₛ (DelayLoopₛ (⇓ c) Plugₛ) ins ≈ ⇓ c ∷ ♯ ins
lemmaShift = ≈ₚ-to-≈ $ lemmaShiftₚ ⟨ transₚ ⟩ (refl ∷ₚ ♯ reflₚ)
\end{code}
%</lemmaShift>


%<*proofShift′>
\AgdaTarget{proofShift′}
\begin{code}
proofShift′ : ∀ {ins} → ⟦ shift ⟧ω' ins ≈ ⇓ 𝔽 ∷ ♯ ins
proofShift′ {x ∷ _xs′} = singleton∘lookup1 x ∷ ♯ transₛ lemmaShift (singleton∘lookup0 x ∷ ♯ reflₛ)
\end{code}
%</proofShift′>




%<*toggle>
\AgdaTarget{toggle}
\begin{code}
toggle : ℂ 0 1 
toggle = ⊥ℂ ⟫ DelayLoop (∨ℂ ⟫ ¬ℂ ⟫ fork×⤨)
\end{code}
%</toggle>


%<*toggleᶜ-start>
\begin{code}
toggleᶜ-start : ∀ {x⁰} → ⟦ toggle ⟧ᶜ [ x⁰ ]⁺ ≡ [ 𝕋 ]
toggleᶜ-start = refl
\end{code}
%</toggleᶜ-start>


%<*[_]-injective>
\begin{code}
[_]-injective : {x y : α} → [ x ] ≡ [ y ] → x ≡ y
[_]-injective refl = refl
\end{code}
%</[_]-injective>

%<*toggleᶜ-𝔽⇒𝕋>
\begin{code}
toggleᶜ-𝔽⇒𝕋 : ∀ {x⁻ x⁻¹ x⁰} → ⟦ toggle ⟧ᶜ (x⁻¹ ∷ x⁻) ≡ ⇓ 𝔽 → ⟦ toggle ⟧ᶜ (x⁰ ∷ x⁻¹ ∷ x⁻) ≡ ⇓ 𝕋
toggleᶜ-𝔽⇒𝕋 {[]}     ()
toggleᶜ-𝔽⇒𝕋 {_ ∷ _}  p = cong ([_] ∘′ not) ([_]-injective p)
\end{code}
%</toggleᶜ-𝔽⇒𝕋>

%<*toggleᶜ-𝕋⇒𝔽>
\begin{code}
toggleᶜ-𝕋⇒𝔽 : ∀ {x⁻ x⁻¹ x⁰} → ⟦ toggle ⟧ᶜ (x⁻¹ ∷ x⁻) ≡ ⇓ 𝕋 → ⟦ toggle ⟧ᶜ (x⁰ ∷ x⁻¹ ∷ x⁻) ≡ ⇓ 𝔽
toggleᶜ-𝕋⇒𝔽 {[]}     _ = refl
toggleᶜ-𝕋⇒𝔽 {_ ∷ _}  p = cong ([_] ∘′ not) ([_]-injective p)
\end{code}
%</toggleᶜ-𝕋⇒𝔽>


%<*toggle-runᶜ′-𝕋-𝔽-types>
\begin{code}
toggle-runᶜ′-𝕋 : ∀ {x⁻ x⁰ x⁺} → ⟦ toggle ⟧ᶜ (x⁰ ∷ x⁻) ≡ ⇓ 𝕋 → runᶜ′ ⟦ toggle ⟧ᶜ (x⁰ ∷ x⁻) x⁺ ≈ map ⇓ (iterate not 𝕋)
toggle-runᶜ′-𝔽 : ∀ {x⁻ x⁰ x⁺} → ⟦ toggle ⟧ᶜ (x⁰ ∷ x⁻) ≡ ⇓ 𝔽 → runᶜ′ ⟦ toggle ⟧ᶜ (x⁰ ∷ x⁻) x⁺ ≈ map ⇓ (iterate not 𝔽)
\end{code}
%</toggle-runᶜ′-𝕋-𝔽-types>

%<*toggle-runᶜ′-𝕋-𝔽-defs>
\begin{code}
toggle-runᶜ′-𝕋 {x⁻} {x⁰} {x¹ ∷ _} p = p ∷ ♯ toggle-runᶜ′-𝔽 (toggleᶜ-𝕋⇒𝔽 {x⁻} {x⁰} {x¹} p)
toggle-runᶜ′-𝔽 {x⁻} {x⁰} {x¹ ∷ _} p = p ∷ ♯ toggle-runᶜ′-𝕋 (toggleᶜ-𝔽⇒𝕋 {x⁻} {x⁰} {x¹} p)
\end{code}
%</toggle-runᶜ′-𝕋-𝔽-defs>


%<*proofToggle>
\begin{code}
proofToggle : ⟦ toggle ⟧ω (map ⇓⊤ (repeat tt)) ≈ map ⇓ (iterate not 𝕋)
proofToggle = toggle-runᶜ′-𝕋 (toggleᶜ-start {[]})
\end{code}
%</proofToggle>


\begin{code}
stateToggle𝔽 stateToggle𝕋 : ℂₛ toggle
\end{code}
%<*stateToggle𝔽-𝕋>
\AgdaTarget{stateToggle𝔽, stateToggle𝕋}
\begin{code}
stateToggle𝔽 = Gateₛ ⟫ₛ DelayLoopₛ (⇓ 𝔽) ((Gateₛ ⟫ₛ Gateₛ) ⟫ₛ Plugₛ)
stateToggle𝕋 = Gateₛ ⟫ₛ DelayLoopₛ (⇓ 𝕋) ((Gateₛ ⟫ₛ Gateₛ) ⟫ₛ Plugₛ)
\end{code}
%</stateToggle𝔽-stateToggle𝕋>

%<*proofToggle𝔽-𝕋-types>
\AgdaTarget{proofToggle𝔽′, proofToggle𝕋′}
\begin{code}
proofToggle𝔽′ : ∀ {ins} → mealy ⟦ toggle ⟧ₛ stateToggle𝔽 ins ≈ₚ map ⇓ (iterate not 𝕋)
proofToggle𝕋′ : ∀ {ins} → mealy ⟦ toggle ⟧ₛ stateToggle𝕋 ins ≈ₚ map ⇓ (iterate not 𝔽)
\end{code}
%</proofToggle𝔽-𝕋-types>

%<*proofToggle𝔽-𝕋-defs>
\begin{code}
proofToggle𝔽′ {_ ∷ _} = refl ∷ₚ ♯ transₚ proofToggle𝕋′ (refl ∷ₚ ♯ reflₚ)
proofToggle𝕋′ {_ ∷ _} = refl ∷ₚ ♯ transₚ proofToggle𝔽′ (refl ∷ₚ ♯ reflₚ)
\end{code}
%</proofToggle𝔽-𝕋-defs>

%<*proofToggle′>
\AgdaTarget{proofToggle′}
\begin{code}
proofToggle′ : ∀ {ins} → ⟦ toggle ⟧ω' ins ≈ map ⇓ (iterate not 𝕋)
proofToggle′ = ≈ₚ-to-≈ proofToggle𝔽′
\end{code}
%</proofToggle′>




%<*rotateL-fin-3>
\AgdaTarget{rotateL⤪₃}
\begin{code}
rotateL⤪₃ : (1 + 2) ⤪ (2 + 1)
rotateL⤪₃ =      swap⤪₁  |⤪ id⤪₁
             ⟫⤪  id⤪₁    |⤪ swap⤪₁
\end{code}
%</rotateL-fin-3>

%<*regCore>
\AgdaTarget{regCore}
\begin{code}
regCore : C 3 2
regCore = Plug rotateL⤪₃ ⟫ mux ⟫ fork×⤨
\end{code}
%</regCore>

-- input × load → out
%<*reg>
\AgdaTarget{reg}
\begin{code}
reg : ℂ 2 1
reg = DelayLoop regCore
\end{code}
%</reg>


%<*reg-delay′-never>
\begin{code}
reg-delay′-never :  ∀ {d⁰ x⁻}
                    → All (λ x → ∃ (λ d → ⇓² (d , 𝔽) ≡ x)) x⁻
                    → delay′ 1 ⟦ regCore ⟧ (⇓² (d⁰ , 𝔽)) x⁻ ≡ 𝔽 ∷ 𝔽 ∷ []
reg-delay′-never {x⁻ = []}                    p = refl
reg-delay′-never {x⁻ = .(d⁻¹ ∷ 𝔽 ∷ []) ∷ xs}  ((d⁻¹ , refl) ∷ pxs) rewrite reg-delay′-never {d⁻¹} pxs = refl
\end{code}
%</reg-delay′-never>

%<*regᶜ-never>
\begin{code}
regᶜ-never :  ∀ {d⁰ x⁻}
              → All (λ x → ∃ (λ d → ⇓² (d , 𝔽) ≡ x)) x⁻
              → ⟦ reg ⟧ᶜ (⇓² (d⁰ , 𝔽) ∷ x⁻) ≡ ⇓ 𝔽
regᶜ-never {d⁰} {x⁻} p rewrite reg-delay′-never {d⁰} p = refl
\end{code}
%</regᶜ-never>

%<*reg-runᶜ′-never>
\begin{code}
reg-runᶜ′-never :  ∀ {d⁰ x⁺ x⁻}
                   → All (λ c → ∃ (λ i → ⇓² (i , 𝔽) ≡ c)) x⁻
                   → runᶜ′ ⟦ reg ⟧ᶜ (⇓² (d⁰ , 𝔽) ∷ x⁻) (map ⇓² (zip x⁺ (repeat 𝔽))) ≈ map ⇓ (repeat 𝔽)
reg-runᶜ′-never {d⁰} {x⁺ = _ ∷ _} p rewrite regᶜ-never {d⁰} p = refl ∷ ♯ reg-runᶜ′-never ((d⁰ , refl) ∷ p)
\end{code}
%</reg-runᶜ′-never>

%<*proofReg-never-typed>
\begin{code}
proofReg-never-typed : ∀ {xs} → ⟦ reg ⟧ω (map ⇓² (xs ⟨ zip ⟩ repeat 𝔽)) ≈ map ⇓ (repeat 𝔽)
proofReg-never-typed {_ ∷ _} = reg-runᶜ′-never []
\end{code}
%</proofReg-never-typed>


%<*reg-runᶜ′-always>
\begin{code}
reg-runᶜ′-always : ∀ {d⁰ x⁻ x⁺} → runᶜ′ ⟦ reg ⟧ᶜ (⇓² (d⁰ , 𝕋) ∷ x⁻) (map ⇓² (x⁺ ⟨ zip ⟩ repeat 𝕋)) ≈ₚ map ⇓ (d⁰ ∷ ♯ x⁺)
reg-runᶜ′-always {x⁻ = []}     {x⁺ = _ ∷ _} = refl ∷ₚ ♯ transₚ reg-runᶜ′-always (refl ∷ₚ ♯ reflₚ)
reg-runᶜ′-always {x⁻ = _ ∷ _}  {x⁺ = _ ∷ _} = refl ∷ₚ ♯ transₚ reg-runᶜ′-always (refl ∷ₚ ♯ reflₚ)
\end{code}
%</reg-runᶜ′-always>

%<*proofReg-always-typed>
\begin{code}
proofReg-always-typed : ∀ {xs} → ⟦ reg ⟧ω (map ⇓² (xs ⟨ zip ⟩ repeat 𝕋)) ≈ map ⇓ xs
proofReg-always-typed {x ∷ xs} = ≈ₚ-to-≈ $ transₚ reg-runᶜ′-always (refl ∷ₚ ♯ reflₚ)
\end{code}
%</proofReg-always-typed>



-- TODO: this holds for any serializable product
%<*map⇓²-zip-map⇑¹-repeat>
\AgdaTarget{map⇓²-zip-map⇑¹-repeat}
\begin{code}
map⇓²-zip-map⇑¹-repeat : ∀ {xs y} → zipWith _++_ xs (repeat [ y ]) ≈ map ⇓² (zip (map ⇑¹ xs) (repeat y))
map⇓²-zip-map⇑¹-repeat {x ∷ _} {y} = append-headCons x ∷ ♯ map⇓²-zip-map⇑¹-repeat
  where  append-headCons : ∀ (z : W 1) → z ++ [ y ] ≡ (head z) ∷ [ y ]
         append-headCons (_ ∷ []) = refl
\end{code}
%</map⇓²-zip-map⇑¹-repeat>


%<*proofReg-never>
\AgdaTarget{proofReg-never}
\begin{code}
proofReg-never : ∀ {xs} → ⟦ reg ⟧ω (zipWith _++_ xs (repeat [ 𝔽 ])) ≈ repeat [ 𝔽 ]
proofReg-never {xs} = beginₛ
  ⟦ reg ⟧ω (zipWith _++_ xs (repeat [ 𝔽 ]))               ≈⟨ ⟦⟧ω[–]-cong b₃ reg (map⇓²-zip-map⇑¹-repeat {xs = xs}) ⟩
  ⟦ reg ⟧ω (map ⇓² (zipWith _,_ (map ⇑¹ xs) (repeat 𝔽)))  ≈⟨ proofReg-never-typed {xs = map ⇑¹ xs} ⟩
  map ⇓¹ (repeat 𝔽)                                       ≈⟨ map-repeat ⟩
  repeat [ 𝔽 ]                                            ∎ₛ
\end{code}
%</proofReg-never>


%<*proofReg-always>
\AgdaTarget{proofReg-always}
\begin{code}
proofReg-always : ∀ {xs} → ⟦ reg ⟧ω (zipWith _++_ xs (repeat [ 𝕋 ])) ≈ xs
proofReg-always {xs} = beginₛ
  ⟦ reg ⟧ω (zipWith _++_ xs (repeat [ 𝕋 ]))               ≈⟨ ⟦⟧ω[–]-cong b₃ reg (map⇓²-zip-map⇑¹-repeat {xs = xs}) ⟩
  ⟦ reg ⟧ω (map ⇓² (zipWith _,_ (map ⇑¹ xs) (repeat 𝕋)))  ≈⟨ proofReg-always-typed {xs = map ⇑¹ xs} ⟩
  map ⇓¹ (map ⇑¹ xs)                                      ≈⟨ symₛ map-∘ ⟩
  map (⇓¹ ∘′ ⇑¹) xs                                       ≈⟨ map-cong-fun singleton∘head ⟩
  map id xs                                                ≈⟨ map-id ⟩
  xs                                                       ∎ₛ
\end{code}
%</proofReg-always>
