\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.Muxes where

open import Function.Base using (_∘_)
open import Data.Product.Base using (proj₂; _,_)
open import Data.Nat.Base using (zero; suc; _+_)
open import Data.Bool.Base using (true; false; if_then_else_)
open import Data.Vec.Base using (Vec; []; _∷_; [_]; splitAt; _++_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; cong)
open import Data.Bool.Properties using (∨-∧-commutativeSemiring)
open import Algebra.Bundles using (module CommutativeSemiring)
open CommutativeSemiring ∨-∧-commutativeSemiring using () renaming (+-identity to ∨-identity)

open import PiWare.Plugs.Core using (_⤪_; _⟫⤪_; _|⤪_; id⤪; fork×⤪; intertwine⤪; swap⤪₁)
open import PiWare.Plugs using (fork×⤨; nil⤨; id⤨₁; fst⤨; snd⤨)
open import PiWare.Circuit using (_⟫_; _∥_; Plug) renaming (module WithGates to CircuitWithGates)
open import PiWare.Gates.BoolTrio using (B₃)
open CircuitWithGates B₃ using (C)
open import PiWare.Samples.BoolTrioComb using (¬ℂ; ∧ℂ; ∨ℂ)
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Semantics.Simulation Atomic-Bool using (W⟶W) renaming (module WithGates to SimulationWithGates)
open import PiWare.Semantics.Simulation.Compliance Atomic-Bool using () renaming (module WithGates to ComplianceWithGates)
open import PiWare.Semantics.Simulation.Testing Atomic-Bool using () renaming (module WithGates to TestingWithGates)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to spec-B₃)
open SimulationWithGates spec-B₃ using (⟦_⟧)
open ComplianceWithGates spec-B₃ using (_⫃_)
open TestingWithGates spec-B₃ using (_⊑?_; check⊑)
\end{code}


%<*mux>
\AgdaTarget{mux}
\begin{code}
mux : C 3 1
mux =  fork×⤨
       --  ¬s     a              s        b
       ⟫   (¬ℂ ∥  fst⤨₁ ⟫ ∧ℂ) ∥  (id⤨₁ ∥  snd⤨₁ ⟫ ∧ℂ)
       ⟫   ∨ℂ
\end{code}
%</mux>
\begin{code}
  where  fst⤨₁  = fst⤨ {b = 1}
         snd⤨₁  = snd⤨ {a = 1}
\end{code}


%<*adapt-fin>
\AgdaTarget{adapt⤪}
\begin{code}
adapt⤪ : ∀ n → (1 + ((1 + n) + (1 + n))) ⤪ ((1 + 1 + 1) + (1 + (n + n)))
adapt⤪ n =      fork×⤪ {1}               |⤪  id⤪ {(1 + n) + (1 + n)}
            ⟫⤪  id⤪ {1 + 1}              |⤪  intertwine⤪ {1} {n} {1} {n}
            ⟫⤪  intertwine⤪₁             |⤪  id⤪ {n + n}
            ⟫⤪  (id⤪ {1 + 1} |⤪ swap⤪₁)  |⤪  id⤪ {n + n}
\end{code}
%</adapt-fin>
\begin{code}
  where intertwine⤪₁ = intertwine⤪ {1} {1} {1} {1}
\end{code}

%<*adapt-plug>
\AgdaTarget{adapt⤨}
\begin{code}
adapt⤨ : ∀ n → C (1 + ((1 + n) + (1 + n))) ((1 + 1 + 1) + (1 + (n + n)))
adapt⤨ = Plug ∘ adapt⤪
\end{code}
%</adapt-plug>

%<*muxN>
\AgdaTarget{muxN}
\begin{code}
muxN : ∀ n → C (1 + (n + n)) n
muxN zero      = nil⤨
muxN (suc n′)  = adapt⤨ n′  ⟫  (mux ∥ muxN n′)
\end{code}
%</muxN>



%<*test-mux1>
\AgdaTarget{test-mux₁}
\begin{code}
test-mux₁ : ⟦ mux ⟧ (false ∷ (true ∷ false ∷ [])) ≡ [ true ]
test-mux₁ = refl
\end{code}
%</test-mux1>

%<*test-mux2>
\AgdaTarget{test-mux₂}
\begin{code}
test-mux₂ : ⟦ mux ⟧ (true ∷ (true ∷ false ∷ [])) ≡ [ false ]
test-mux₂ = refl
\end{code}
%</test-mux2>


%<*ite>
\AgdaTarget{ite}
\begin{code}
ite : W⟶W 3 1
ite (s ∷ a ∷ b ∷ []) = [ if s then b else a ]
\end{code}
%</ite>


%<*mux-impl-ite>
\AgdaTarget{mux⊑ite}
\begin{code}
mux⊑ite : mux ⫃ ite
mux⊑ite (true   ∷ _ ∷ _ ∷ []) = refl
mux⊑ite (false  ∷ a ∷ _ ∷ []) = cong [_] ((proj₂ ∨-identity) a)
\end{code}
%</mux-impl-ite>


%<*mux-impl-ite-dec>
\AgdaTarget{mux⊑?ite}
\begin{code}
mux⊑?ite : mux ⊑? ite
mux⊑?ite = check⊑ mux ite
\end{code}
%</mux-impl-ite-dec>



%<*iteN>
\AgdaTarget{iteN}
\begin{code}
iteN : ∀ n → W⟶W (1 + (n + n)) n
iteN zero      _                = []
iteN (suc n′)  (s ∷ ab)         with splitAt (suc n′) ab
iteN (suc n′)  (s ∷ .(a ++ b))  | a , b , refl = if s then b else a
\end{code}
%</iteN>


%<*muxN-impl-iteN>
\AgdaTarget{muxN⊑iteN}
\begin{code}
muxN⊑iteN : ∀ n → muxN n ⫃ iteN n
muxN⊑iteN zero      (_ ∷ []) = refl
muxN⊑iteN (suc n′)  (s ∷ ab)         with splitAt (suc n′) ab
muxN⊑iteN (suc n′)  (s ∷ .(a ++ b))  | a , b , refl = muxN⊑iteN-ind
\end{code}
%</muxN-impl-iteN>
\begin{code}
  where postulate muxN⊑iteN-ind : _
\end{code}
