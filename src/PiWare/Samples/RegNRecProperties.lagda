\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.RegNRecProperties where

open import Function.Base using (_∘′_)
open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Bool.Base using () renaming (true to 𝕋; false to 𝔽)
open import Data.Fin.Base using () renaming (suc to Fs)
open import Data.List.NonEmpty using (_∷_)
open import Data.Nat.Base using (zero; suc; _+_)
open import Data.Product.Base using (proj₁; _×_; proj₂; _,_)
open import Data.Vec.Base using (Vec; _∷_; []; [_]; head; tail; _∷ʳ_; _++_; splitAt; tabulate; lookup) renaming (zipWith to zipWithᵥ)
open import Data.Vec.Properties using (tabulate∘lookup; lookup∘tabulate)
open import Codata.Musical.Stream using (Stream; take; drop; map; zipWith; _≈_; head-cong; _∷_) renaming (head to headₛ)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; cong; cong₂; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-∣; _∎)

open import Data.Vec.Extra using (splitAt′; ₁∘split1′; ₂∘split1′)
open import Data.Vec.Properties.Extra using (tabulate-cong; ++-injective)
open import Codata.Musical.Stream.Causal using (runᶜ′; runᶜ-const)
open import Codata.Musical.Stream.Equality.WithTrans using (_≈ₚ_; _∷ₚ_; ≈ₚ-to-≈; reflₚ; transₚ)
open import Codata.Musical.Stream.Properties using (module Setoidₛ; module EqReasoningₛ; take-cong; drop-cong; drop-repeat)
open Setoidₛ using () renaming (refl to reflₛ; trans to transₛ; sym to symₛ)
open EqReasoningₛ using (step-≈-⟩) renaming (begin_ to beginₛ_; _∎ to _∎ₛ)

open import PiWare.Circuit using (_⟫_; _∥_; ω)
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Semantics.Simulation Atomic-Bool using (module WithGates)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to b₃)
open WithGates b₃ using (⟦_⟧ᶜ; ⟦_⟧ω)
open import PiWare.Samples.BoolTrioSeq using (reg)
open import PiWare.Samples.RegProperties using (regW,R-I⇒O; regR,R-I⇒O)
open import PiWare.Samples.RegNRec using (regn-plug; regn)
open import PiWare.Semantics.Simulation.Properties.Sequential Atomic-Bool using (⟦⟧ω[–]-cong; ⟦⟧ω[–]-⟫⇒∘; ⟦⟧ω[–]-∥⇒zipWith++)
\end{code}

\begin{code}
open import Level using (Level)
open import Data.Nat.Base using (ℕ)

private
 variable
  ℓ : Level
  α : Set ℓ

  m n d : ℕ
\end{code}


%<*lemma-deconstruct-split>
\AgdaTarget{lemma-deconstruct-split}
\begin{code}
lemma-deconstruct-split : ∀ {y z : α} {ys zs : Vec α n} {xs}
  → take 2 (drop d xs) ≡ (y ∷ ys) ∷ (z ∷ zs) ∷ []
  → take 2 (drop d (map ₁∘split1′ xs)) ≡ [ y ] ∷ [ z ] ∷ [] × take 2 (drop d (map ₂∘split1′ xs)) ≡ ys ∷ zs ∷ []

lemma-deconstruct-split {d = zero}                    {xs = _ ∷ xs′}     p     with ♭ xs′
lemma-deconstruct-split {d = zero} {y} {z} {ys} {zs}  {.(y ∷ ys) ∷ xs′}  refl  | .(z ∷ zs) ∷ xs₁ = refl , refl
lemma-deconstruct-split {d = suc d′}                  {xs = _ ∷ _}       p = lemma-deconstruct-split {d = d′} p
\end{code}
%</lemma-deconstruct-split>


%<*lemma-deconstruct-head-tail>
\AgdaTarget{lemma-deconstruct-head-tail}
\begin{code}
lemma-deconstruct-head-tail : ∀ {y z : α} {ys zs : Vec α n} {xs}
  → take 2 (drop d xs) ≡ (y ∷ ys) ∷ (z ∷ zs) ∷ []
  → take 2 (drop d (map head xs)) ≡ y ∷ z ∷ [] × take 2 (drop d (map tail xs)) ≡ ys ∷ zs ∷ []

lemma-deconstruct-head-tail {d = zero}                    {xs = _ ∷ xs′}     p     with ♭ xs′
lemma-deconstruct-head-tail {d = zero} {y} {z} {ys} {zs}  {.(y ∷ ys) ∷ xs′}  refl  | .(z ∷ zs) ∷ xs₁ = refl , refl
lemma-deconstruct-head-tail {d = suc d}                   {xs = _ ∷ _}       p = lemma-deconstruct-head-tail {d = d} p
\end{code}
%</lemma-deconstruct-head-tail>


%<*lemma-construct-cons>
\AgdaTarget{lemma-construct-cons}
\begin{code}
lemma-construct-cons : ∀ {y z : α} {ys zs : Vec α n} {xs₁ : Stream α} {xs₂ : Stream (Vec α n)}
  → take 2 (drop d xs₁) ≡ y   ∷ z   ∷ []
  → take 2 (drop d xs₂) ≡ ys  ∷ zs  ∷ []
  → take 2 (drop d (zipWith {C = Vec α (1 + n)} _∷_ xs₁ xs₂)) ≡ (y ∷ ys) ∷ (z ∷ zs) ∷ []

lemma-construct-cons {d = zero}                    {xs₁ = _ ∷ xs₁′}  {_ ∷ xs₂′}   ph    pt    with ♭ xs₁′ | ♭ xs₂′
lemma-construct-cons {d = zero} {y} {z} {ys} {zs}  {.y ∷ is₁}        {.ys ∷ is₂}  refl  refl  | .z ∷ is'₁ | .zs ∷ is'₂ = refl
lemma-construct-cons {d = suc d}                   {xs₁ = _ ∷ _}     {_ ∷ _}      ph    pt = lemma-construct-cons {d = d} ph pt
\end{code}
%</lemma-construct-cons>


%<*lemma-construct-snoc>
\AgdaTarget{lemma-construct-snoc}
\begin{code}
lemma-construct-snoc : ∀ {y z : α} {ys zs : Vec α n} {xs₁ xs₂}
  → take 2 (drop d xs₁) ≡ ys ∷ zs ∷ []
  → take 2 (drop d xs₂) ≡ y ∷ z ∷ []
  → take 2 (drop d (zipWith _∷ʳ_ xs₁ xs₂)) ≡ (ys ∷ʳ y) ∷ (zs ∷ʳ z) ∷ []

lemma-construct-snoc {d = zero}                    {xs₁ = _ ∷ xs₁′}  {_ ∷ xs₂′} _    _     with ♭ xs₁′  | ♭ xs₂′
lemma-construct-snoc {d = zero} {y} {z} {ys} {zs}  {.ys ∷ _}         {.y ∷ _}   refl refl  | .zs ∷ _    | .z ∷ _ = refl
lemma-construct-snoc {d = suc d}                   {xs₁ = _ ∷ _}     {_ ∷ _} pi pl = lemma-construct-snoc {d = d} pi pl
\end{code}
%</lemma-construct-snoc>


%<*lemma-merge-splitAt>
\AgdaTarget{lemma-merge-splitAt}
\begin{code}
lemma-merge-splitAt : (xs : Stream (Vec α (m + n))) → zipWith _++_ (map (proj₁ ∘′ splitAt′ m) xs) (map (proj₂ ∘′ splitAt′ m) xs) ≈ xs
lemma-merge-splitAt {m = m}  (x ∷ _)                with splitAt m x
lemma-merge-splitAt          (.(_xₘ ++ _xₙ) ∷ xs′)  | _xₘ , _xₙ , refl = refl ∷ ♯ lemma-merge-splitAt (♭ xs′)
\end{code}
%</lemma-merge-splitAt>


%<*lemma-merge-head-tail>
\AgdaTarget{lemma-merge-head-tail}
\begin{code}
lemma-merge-head-tail : {xs : Stream (Vec α (suc n))} → zipWith _∷_ (map head xs) (map tail xs) ≈ xs
lemma-merge-head-tail {xs = (_ ∷ _) ∷ _} = refl ∷ ♯ lemma-merge-head-tail
\end{code}
%</lemma-merge-head-tail>


%<*lemma-drop-zipWith>
\AgdaTarget{lemma-drop-zipWith}
\begin{code}
lemma-drop-zipWith : {xs : Stream (Vec α n)} {ys : Stream (Vec α m)} → drop d (zipWith _++_ xs ys) ≈ zipWith _++_ (drop d xs) (drop d ys)
lemma-drop-zipWith {d = zero}                        = reflₛ
lemma-drop-zipWith {d = suc d} {xs = _ ∷ _} {_ ∷ _}  = lemma-drop-zipWith {d = d}
\end{code}
%</lemma-drop-zipWith>


%<*lemma-take-zipWith>
\AgdaTarget{lemma-take-zipWith}
\begin{code}
lemma-take-zipWith :  ∀ {n p q} {xs : Stream (Vec α p)} {ys : Stream (Vec α q)}
                      → take n (zipWith _++_ xs ys) ≡ zipWithᵥ _++_ (take n xs) (take n ys)
lemma-take-zipWith {n = zero}                             = refl
lemma-take-zipWith {n = suc n′} {xs = x ∷ xs′} {y ∷ ys′}  = cong ((x ++ y) ∷_) (lemma-take-zipWith {n = n′} {xs = ♭ xs′} {♭ ys′})
\end{code}
%</lemma-take-zipWith>


%<*lemma-plug-denotation>
\AgdaTarget{lemma-plug″}
\begin{code}
lemma-plug″ : ∀ {n x₀ x₁} {xs : Vec α n} → tabulate (λ k → lookup (x₀ ∷ x₁ ∷ xs) (lookup (tabulate (Fs ∘′ Fs)) k)) ≡ xs
lemma-plug″ {x₀ = x₀} {x₁} {xs} = begin
  tabulate (λ k → lookup (x₀ ∷ x₁ ∷ xs) (lookup (tabulate (Fs ∘′ Fs)) k))
    ≡⟨ tabulate-cong (λ x → cong (λ z → lookup (x₀ ∷ x₁ ∷ xs) z) (lookup∘tabulate (Fs ∘′ Fs) x)) ⟩
  tabulate (λ k → lookup xs k)
    ≡⟨ tabulate∘lookup xs ⟩
  xs ∎
\end{code}
%</lemma-plug-denotation>


%<*lemma-plug-runc>
\AgdaTarget{lemma-plug′}
\begin{code}
lemma-plug′ :  ∀ {s n l x cs ls xs}
               → runᶜ′ ⟦ regn-plug n {s} ⟧ᶜ ((l ∷ x) ∷ cs) (zipWith _∷_ ls xs)
                 ≈ₚ zipWith _++_  (zipWith _∷ʳ_ (map ₁∘split1′ (x ∷ ♯ xs)) (l ∷ ♯ ls))
                                  (zipWith _∷_ (l ∷ ♯ ls) (map ₂∘split1′ (x ∷ ♯ xs)))
lemma-plug′ {_} {x = x} {ls = l₁ ∷ ls₁} {xs = x₁ ∷ xs₁} with splitAt 1 x
lemma-plug′ {s} {l = l} {x = ._} {ls = l₁ ∷ ls₁} {xs = x₁ ∷ xs₁} | xh ∷ [] , xt , refl =
  cong (λ z → xh ∷ l ∷ l ∷ z) lemma-plug″ ∷ₚ (♯ transₚ (lemma-plug′ {s}) (refl ∷ₚ ♯ reflₚ))
\end{code}
%</lemma-plug-runc>


%<*lemma-plug>
\AgdaTarget{lemma-plug}
\begin{code}
lemma-plug :  ∀ {s n ls xs}
              → ⟦ regn-plug n {s} ⟧ω (zipWith _∷_ ls xs)
                ≈ zipWith _++_ (zipWith _∷ʳ_ (map ₁∘split1′ xs) ls) (zipWith _∷_ ls (map ₂∘split1′ xs))
lemma-plug {s} {ls = l ∷ ls} {x ∷ xs} = transₛ (≈ₚ-to-≈ (lemma-plug′ {s})) (refl ∷ (♯ reflₛ))
\end{code}
%</lemma-plug>



%<*regnW,R-I⇒O>
\AgdaTarget{regnW,R-I⇒O}
\begin{code}
regnW,R-I⇒O : ∀ {n d w w′ xs} → take 2 (drop d xs) ≡ (𝕋 ∷ w) ∷ (𝔽 ∷ w′) ∷ [] → take 2 (drop d (⟦ regn n ⟧ω xs)) ≡ w ∷ w ∷ []
regnW,R-I⇒O {zero}   {d} {[]}     {_}        {xs} p = take-cong (transₛ (drop-cong {n = d} (runᶜ-const {xs = xs} [])) (drop-repeat {n = d}))
regnW,R-I⇒O {suc n}  {d} {h ∷ t}  {h′ ∷ t′}  {xs} p with lemma-deconstruct-head-tail {n = suc n} {d} {𝕋} {𝔽} {h ∷ t} {h′ ∷ t′} p
regnW,R-I⇒O {suc n}  {d} {h ∷ t}  {h′ ∷ t′}  {xs} p | pls , pvs with lemma-deconstruct-split {n = n} {d} {h} {h′} {t} {t′} pvs
regnW,R-I⇒O {suc n}  {d} {h ∷ t}  {h′ ∷ t′}  {xs} p | pls , pvs | pvhs , pvts = begin
  take 2 (drop d (⟦ regn (suc n) ⟧ω xs))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-cong b₃ (regn (suc n)) {xs = xs} (symₛ (lemma-merge-head-tail {n = suc n})))) ⟩
  take 2 (drop d (⟦ regn (suc n) ⟧ω (zipWith _∷_ (map head xs) (map tail xs))))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-⟫⇒∘ b₃ (regn-plug n) (reg ∥ regn n) {zipWith _∷_ (map head xs) (map tail xs)})) ⟩
  take 2 (drop d (⟦ reg ∥ regn n ⟧ω (⟦ regn-plug n {ω} ⟧ω (zipWith _∷_ (map head xs) (map tail xs)))))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-cong b₃ (reg ∥ regn n) (lemma-plug {ω} {ls = map head xs} {map tail xs}))) ⟩
  take 2 (drop d (⟦ reg ∥ regn n ⟧ω (zipWith _++_  (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))
                                                   (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-∥⇒zipWith++ b₃  reg (regn n)
                                                            {zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)}
                                                            {zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))})) ⟩
  take 2 (drop d (zipWith _++_  (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))
                                (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ take-cong (lemma-drop-zipWith {d = d}) ⟩
  take 2  (zipWith _++_  (drop d (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))))
          (drop d (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ lemma-take-zipWith ⟩
  zipWithᵥ _++_  (take 2 (drop d (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))))
                 (take 2 (drop d (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ cong₂ (zipWithᵥ _++_)
         (regW,R-I⇒O {n = d} {d¹ = h′} {xs = zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)} (lemma-construct-snoc {d = d} pvhs pls))
         (regnW,R-I⇒O {d = d} {xs = zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))} (lemma-construct-cons {d = d} pls pvts)) ⟩
  zipWithᵥ _++_ ([ h ] ∷ [ h ] ∷ []) (t ∷ t ∷ [])
    ≡⟨ refl ⟩
  (h ∷ t) ∷ (h ∷ t) ∷ [] ∎
\end{code}
%</regnW,R-I⇒O>



%<*lemma-unfold-regn-bisim>
\AgdaTarget{lemma-unfold-regn′}
\begin{code}
lemma-unfold-regn′ : ∀ {n xs} → ⟦ regn (suc n) ⟧ω xs ≈ zipWith _++_
                                                         (⟦ reg ⟧ω     (zipWith _∷ʳ_  (map ₁∘split1′ (map tail xs)) (map head xs)))
                                                         (⟦ regn n ⟧ω  (zipWith _∷_   (map head xs) (map ₂∘split1′ (map tail xs))))
lemma-unfold-regn′ {n} {xs} = beginₛ
  ⟦ regn (suc n) ⟧ω xs                   ≈⟨ reflₛ ⟩
  ⟦ regn-plug n ⟫ (reg ∥ regn n) ⟧ω xs     ≈⟨ ⟦⟧ω[–]-⟫⇒∘ b₃ (regn-plug n) (reg ∥ regn n) {xs} ⟩
  ⟦ reg ∥ regn n ⟧ω (⟦ regn-plug n {ω} ⟧ω xs)
    ≈⟨ ⟦⟧ω[–]-cong b₃ (reg ∥ regn n) (⟦⟧ω[–]-cong {s = ω} b₃ (regn-plug n) {xs} (symₛ lemma-merge-head-tail)) ⟩
  ⟦ reg ∥ regn n ⟧ω (⟦ regn-plug n {ω} ⟧ω (zipWith _∷_ (map head xs) (map tail xs)))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (reg ∥ regn n) (lemma-plug {ω} {ls = map head xs} {map tail xs}) ⟩
  ⟦ reg ∥ regn n ⟧ω (zipWith _++_  (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))
                                   (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))
    ≈⟨ ⟦⟧ω[–]-∥⇒zipWith++ b₃  reg (regn n)  {zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)}
                                            {zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))}  ⟩
  zipWith _++_  (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))
                (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs)))) ∎ₛ
\end{code}
%</lemma-unfold-regn-bisim>


%<*lemma-head-zipWith>
\AgdaTarget{lemma-head-zipWith}
\begin{code}
lemma-head-zipWith : {xs : Stream (Vec α n)} {ys : Stream (Vec α m)} → headₛ (zipWith _++_ xs ys) ≡ headₛ xs ++ headₛ ys
lemma-head-zipWith {xs = x ∷ xs} {y ∷ ys} = refl
\end{code}
%</lemma-head-zipWith>


%<*lemma-unfold-regn>
\AgdaTarget{lemma-unfold-regn}
\begin{code}
lemma-unfold-regn : ∀ {n d h t xs}  → headₛ (drop d (⟦ regn (suc n) ⟧ω xs)) ≡ h ∷ t
                                    →     headₛ (drop d (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))) ≡ h ∷ []
                                       ×  headₛ (drop d (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))) ≡ t
lemma-unfold-regn {n} {d} {h} {t} {xs} p = ++-injective (begin
      headₛ (drop d (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))))
  ++  headₛ (drop d (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs)))))
    ≡⟨ sym (lemma-head-zipWith  {xs = drop d (⟦ reg ⟧ω     (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))}
                                {ys = drop d (⟦ regn n ⟧ω  (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))}) ⟩
  headₛ (zipWith _++_  (drop d (⟦ reg ⟧ω     (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))))
                       (drop d (⟦ regn n ⟧ω  (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ head-cong (symₛ (lemma-drop-zipWith {d = d})) ⟩
  headₛ (drop d (zipWith _++_  (⟦ reg ⟧ω     (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))
                               (⟦ regn n ⟧ω  (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ head-cong (drop-cong {n = d} (symₛ (lemma-unfold-regn′ {xs = xs}))) ⟩
  headₛ (drop d (⟦ regn (suc n) ⟧ω xs))
    ≡⟨ p ⟩
  h ∷ t ∎)
\end{code}
%</lemma-unfold-regn>



%<*regnR,R-I⇒O>
\AgdaTarget{regnR,R-I⇒O}
\begin{code}
regnR,R-I⇒O : ∀ {n d w a b xs}  → take 2 (drop d xs) ≡ (𝔽 ∷ a) ∷ (𝔽 ∷ b) ∷ [] → headₛ (drop d (⟦ regn n ⟧ω xs)) ≡ w
                                → take 2 (drop d (⟦ regn n ⟧ω xs)) ≡ w ∷ w ∷ []
regnR,R-I⇒O {zero}   {d} {[]}    {xs = xs} pp ph = take-cong (transₛ (drop-cong {n = d} (runᶜ-const {xs = xs} [])) (drop-repeat {n = d}))
regnR,R-I⇒O {suc n}  {d} {h ∷ t} {ah ∷ at} {bh ∷ bt} {xs} pp ph with lemma-deconstruct-head-tail {n = suc n} {d} {𝔽} {𝔽} {ah ∷ at} {bh ∷ bt} pp
regnR,R-I⇒O {suc n}  {d} {h ∷ t} {ah ∷ at} {bh ∷ bt} {xs} pp ph | pls , pvs with lemma-deconstruct-split {n = n} {d} {ah} {bh} {at} {bt} pvs
regnR,R-I⇒O {suc n}  {d} {h ∷ t} {ah ∷ at} {bh ∷ bt} {xs} pp ph | pls , pvs | pvhs , pvts with lemma-unfold-regn {n = n} {d} {xs = xs} ph
regnR,R-I⇒O {suc n}  {d} {h ∷ t} {ah ∷ at} {bh ∷ bt} {xs} pp ph | pls , pvs | pvhs , pvts | phhs , phts = begin
  take 2 (drop d (⟦ regn (suc n) ⟧ω xs))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-cong b₃ (regn (suc n)) {xs = xs} (symₛ (lemma-merge-head-tail {n = suc n})))) ⟩
  take 2 (drop d (⟦ regn (suc n) ⟧ω (zipWith _∷_ (map head xs) (map tail xs))))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-⟫⇒∘ b₃ (regn-plug n) (reg ∥ regn n) {zipWith _∷_ (map head xs) (map tail xs)})) ⟩
  take 2 (drop d (⟦ reg ∥ regn n ⟧ω (⟦ regn-plug n {ω} ⟧ω (zipWith _∷_ (map head xs) (map tail xs)))))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-cong b₃ (reg ∥ regn n) (lemma-plug {ω} {ls = map head xs} {map tail xs}))) ⟩
  take 2 (drop d (⟦ reg ∥ regn n ⟧ω (zipWith _++_  (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))
                                                   (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ take-cong (drop-cong {n = d} (⟦⟧ω[–]-∥⇒zipWith++ b₃  reg (regn n)  {zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)}
                                                                          {zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))})) ⟩
  take 2 (drop d (zipWith _++_  (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))
                                (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ take-cong (lemma-drop-zipWith {d = d}) ⟩
  take 2 (zipWith _++_  (drop d (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs))))
                        (drop d (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ lemma-take-zipWith ⟩
  zipWithᵥ _++_  (take 2 (drop d (⟦ reg ⟧ω (zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)))))
                 (take 2 (drop d (⟦ regn n ⟧ω (zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))))))
    ≡⟨ cong₂ (zipWithᵥ _++_)  (regR,R-I⇒O {n = d} {d⁰ = ah} {xs = zipWith _∷ʳ_ (map ₁∘split1′ (map tail xs)) (map head xs)}
                                (lemma-construct-snoc {d = d} pvhs pls) phhs)
                              (regnR,R-I⇒O {d = d} {xs = zipWith _∷_ (map head xs) (map ₂∘split1′ (map tail xs))}
                                (lemma-construct-cons {d = d} pls pvts) phts) ⟩
  zipWithᵥ _++_ ([ h ] ∷ [ h ] ∷ []) (t ∷ t ∷ [])
    ≡⟨⟩
  (h ∷ t) ∷ (h ∷ t) ∷ [] ∎
\end{code}
%</regnR,R-I⇒O>
