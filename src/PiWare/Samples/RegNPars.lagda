\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Samples.RegNPars where

open import Function using (_$_; _∘′_; _∘_; id; _⟨_⟩_)
open import Codata.Musical.Notation using (♯_; ♭)
open import Data.Bool.Base using (Bool) renaming (false to 𝔽; true to 𝕋)
open import Data.Fin.Base using (Fin; _↑ʳ_) renaming (zero to Fz; suc to Fs)
open import Data.Nat.Base using (ℕ; zero; suc; _+_; _*_; _+⋎_)
open import Data.Product using (_,_; proj₂)
open import Data.List.Base using ([]; _∷_)
open import Data.List.NonEmpty using (_∷_)
open import Data.Vec.Base using ([]; _∷_; _∷ʳ_; [_]; tabulate; _++_; head; Vec; _⋎_; replicate; splitAt; lookup)
open import Codata.Musical.Stream using (zipWith; repeat; _≈_; map; _∷_; Stream; zipWith-cong)

open import Algebra.Structures using (IsCommutativeSemiring; IsCommutativeMonoid)
open import Data.Nat.Properties using (+-suc; +-identityʳ; +-comm; +-*-isCommutativeSemiring)
open IsCommutativeSemiring +-*-isCommutativeSemiring using (*-isCommutativeMonoid)
open IsCommutativeMonoid *-isCommutativeMonoid using () renaming (identity to *-id; comm to *-comm)

open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; cong; cong₂; trans; _≗_; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-∣; step-≡-⟩; step-≡-⟨; _∎)

open import Data.Vec.Extra using (splitAt′; ₁∘split1′; ₂∘split1′)
open import Data.Vec.Properties.Extra using (tabulate-cong)
open import Codata.Musical.Stream.Causal using (runᶜ′)
open import Codata.Musical.Stream.Properties using (map-∘; map-repeat; map-cong-fun; map-id; module Setoidₛ; module EqReasoningₛ)
open import Codata.Musical.Stream.Equality.WithTrans using (_≈ₚ_; _∷ₚ_; transₚ; reflₚ; ≈ₚ-to-≈)
open Setoidₛ using () renaming (sym to symₛ; trans to transₛ; refl to reflₛ)
open EqReasoningₛ using (step-≈-⟩) renaming (begin_ to beginₛ_; _∎ to _∎ₛ)

open import PiWare.Circuit using (Plug; DelayLoop; _⟫_; ω) renaming (module WithGates to CircuitWithGates)
open import PiWare.Gates.BoolTrio using (B₃)
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Patterns Atomic-Bool using (parsN)
open import PiWare.Plugs using (eq⤨)
open import PiWare.Samples.BoolTrioSeq using (reg)
open import PiWare.Semantics.Simulation Atomic-Bool using (module WithGates; W)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to b₃)
open import PiWare.Semantics.Simulation.Properties.Plugs Atomic-Bool using (vecCoerce; vecCoerce-step; vecCoerce-rightInverse; eq⤨⊑ωid)
open import PiWare.Semantics.Simulation.Properties.Sequential Atomic-Bool using (⟦⟧ω[–]-cong; ⟦⟧ω[–]-⟫⇒∘)
open import PiWare.Samples.BoolTrioSeq using (proofReg-never-typed; proofReg-always-typed)
open CircuitWithGates B₃ using (ℂ; C)
open WithGates b₃ using (⟦_⟧ω; ⟦_⟧; ⟦_⟧ᶜ)

open import Data.Serializable using (_⇕_; ⇕×)
open import Data.Serializable.Bool using (⇕Bool)
open _⇕_ ⇕Bool using (⇑; ⇓)
open _⇕_ (⇕× ⦃ ⇕Bool ⦄ ⦃ ⇕Bool ⦄) using () renaming (⇓ to ⇓²)
\end{code}



%<*regn-distribute>
\AgdaTarget{regn-distribute}
\begin{code}
regn-distribute : ∀ n → C (suc n) (n +⋎ n)
regn-distribute n = Plug $ tabulate (1 ↑ʳ_) ⋎ replicate n Fz
\end{code}
%</regn-distribute>


%<*+⋎≡+>
\AgdaTarget{+⋎≡+}
\begin{code}
+⋎≡+ : ∀ n m → n +⋎ m ≡ n + m
+⋎≡+ zero m     = refl
+⋎≡+ (suc n) m  = cong suc (+⋎≡+ m n ⟨ trans ⟩ +-comm m n)
\end{code}
%</+⋎≡+>

%<*+⋎-*2>
\AgdaTarget{+⋎-*2}
\begin{code}
+⋎-*2 : ∀ n → n +⋎ n ≡ n * 2
+⋎-*2 n = begin
  n +⋎ n       ≡⟨ +⋎≡+ n n ⟩
  n + n        ≡⟨ cong (n +_) $ sym (+-identityʳ n) ⟩
  2 * n        ≡⟨ *-comm 2 n ⟩
  n * 2  ∎
\end{code}
%</+⋎-*2>

%<*regn-n+⋎n≡n*2>
\AgdaTarget{regn-n+⋎n≡n*2}
\begin{code}
regn-n+⋎n≡n*2 : ∀ n → C (n +⋎ n) (n * 2)
regn-n+⋎n≡n*2 n = eq⤨ (+⋎-*2 n)
\end{code}
%</regn-n+⋎n≡n*2>


%<*regn-n*1≡n>
\AgdaTarget{regn-n*1≡n}
\begin{code}
regn-n*1≡n : ∀ n → C (n * 1) n
regn-n*1≡n n = eq⤨ ((proj₂ *-id) n)
\end{code}
%</regn-n*1≡n>


%<*regn-regs>
\AgdaTarget{regn-regs}
\begin{code}
regn-regs : ∀ n → ℂ (n * 2) (n * 1)
regn-regs n = parsN n reg
\end{code}
%</regn-regs>


-- An n-sized register
-- load ∷ inputs → out
-- (note that this is different from the wiring scheme of reg, input × load → out!)
%<*regn>
\AgdaTarget{regn}
\begin{code}
regn : ∀ n → ℂ (suc n) n
regn n = regn-distribute n ⟫ (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n)
\end{code}
%</regn>




%<*finCoerce>
\AgdaTarget{finCoerce}
\begin{code}
finCoerce : ∀ {m n} (eq : m ≡ n) → Fin m → Fin n
finCoerce refl x = x
\end{code}
%</finCoerce>


\begin{code}
lookup-finCoerce :  ∀ {ℓ} {α : Set ℓ} {m n m≡n i j} {v : Vec α m} {w : Vec α n}
                    → j ≡ finCoerce m≡n i → w ≡ vecCoerce m≡n v → lookup v i ≡ lookup w j
lookup-finCoerce {m≡n = refl} refl refl = refl
\end{code}


\begin{code}
lookup-raise :  ∀ {m n ℓ} {α : Set ℓ} f (r x : α) (rs : Vec α n) (xs : Vec α m)
                → f ≗ _↑ʳ_ (suc n) → lookup (r ∷ rs ++ x ∷ xs) (f Fz) ≡ x
lookup-raise              _f  _r  _x  []        _xs  p rewrite p Fz = refl
lookup-raise {n = suc n′} f   _r  x   (r ∷ rs)  xs   p rewrite p Fz = lookup-raise (_↑ʳ_ (suc n′)) r x rs xs (λ _ → refl)
\end{code}


\begin{code}
unfold-finCoerce : ∀ {m n ≡₁ ≡₂ i} → finCoerce {suc m} ≡₁ (Fs i) ≡ Fs (finCoerce {n = n} ≡₂ i)
unfold-finCoerce {≡₁ = refl} {refl} = refl
\end{code}


%<*finCoerce-raise>
\AgdaTarget{finCoerce-raise}
\begin{code}
finCoerce-raise : ∀ {m n i} f → f ≗ _↑ʳ_ (suc n) → finCoerce (cong suc (+-suc n m)) (f (Fs i)) ≡ Fs (Fs (n ↑ʳ i))
finCoerce-raise {_} {zero}   {i} _f  p rewrite p (Fs i) = refl
finCoerce-raise {m} {suc n}  {i} f   p rewrite p (Fs i) =
  let ucoerce = unfold-finCoerce {≡₁ = cong suc (+-suc (suc n) m)} {≡₂ = cong suc (+-suc n m)}  in
  begin
    finCoerce (cong suc (cong suc (+-suc n m))) (Fs (Fs (n ↑ʳ Fs i)))  ≡⟨ ucoerce ⟩
    Fs (finCoerce (cong suc (+-suc n m))        (Fs (n ↑ʳ Fs i)))      ≡⟨ cong Fs (finCoerce-raise (suc n ↑ʳ_) (λ _ → refl)) ⟩
    Fs (Fs (Fs (n ↑ʳ i)))                                              ∎
\end{code}
%</finCoerce-raise>




-- TODO: This sort of plug lemmas are hell on earth
\begin{code}
lemma1'1 :  ∀ {m n ℓ} {α : Set ℓ} i (f : Fin (suc m) → Fin (suc (n + suc m))) (rs : Vec α n) (xs : Vec α m)
            →  lookup (tabulate {n = m} (λ j → finCoerce (cong suc (+-suc n m)) (f (Fs j))) ⋎ replicate m Fz) i
               ≡ finCoerce (cong suc (+-suc n m)) (lookup (tabulate (λ k → f (Fs k)) ⋎ replicate m Fz) i)
lemma1'1 {m} {n} _i _f _rs _xs with suc (n + m)  | cong suc (+-suc n m)
lemma1'1 {_} {_} _i _f _rs _xs | ._              | refl = refl
\end{code}

\begin{code}
lemma1'2 :  ∀ {m n ℓ} {α : Set ℓ} {r} x (rs : Vec α n) (xs : Vec α m)
            → r ∷ (rs ∷ʳ x) ++ xs ≡ vecCoerce (cong suc (+-suc n m)) (r ∷ rs ++ x ∷ xs)
lemma1'2                     _x  []         _xs  = refl
lemma1'2 {m} {suc n} {r = r} x   (r′ ∷ rs)  xs   =
  let coerceEq = vecCoerce-step (cong suc (cong suc (+-suc n m))) (cong suc (+-suc n m)) in
  begin
    r ∷ r′ ∷ (rs ∷ʳ x) ++ xs                                                   ≡⟨ cong (r ∷_) (lemma1'2 x rs xs) ⟩
    r ∷  vecCoerce (cong suc (+-suc n m))             (r′ ∷ rs ++ x ∷ xs)      ≡⟨ coerceEq ⟨
         vecCoerce (cong suc (cong suc (+-suc n m)))  (r ∷ r′ ∷ rs ++ x ∷ xs)  ∎
\end{code}

\begin{code}
lemma1 :  ∀ {m n ℓ} {α : Set ℓ} {v x i} (f : Fin (suc m) → Fin (suc (n + suc m))) (rs : Vec α n) (xs : Vec α m)
           → f ≗ _↑ʳ_ (suc n)
           →     lookup  (v ∷ rs         ++ x ∷  xs)  (lookup (tabulate (f ∘ Fs)                                             ⋎ replicate m Fz) i)
              ≡  lookup  (v ∷ (rs ∷ʳ x)  ++      xs)  (lookup (tabulate (λ k → finCoerce (cong suc (+-suc n m)) (f (Fs k)))  ⋎ replicate m Fz) i)
lemma1 {m} {n} {v = v} {x} {i} f rs xs p =
  lookup-finCoerce  {m≡n  = cong suc (+-suc n m)}
                    {i    = lookup (tabulate (λ j → f (Fs j))                                     ⋎ replicate m Fz) i}
                    {j    = lookup (tabulate (λ k → finCoerce (cong suc (+-suc n m)) (f (Fs k)))  ⋎ replicate m Fz) i}
                    {v    = v ∷ rs         ++ x ∷  xs}
                    {w    = v ∷ (rs ∷ʳ x)  ++      xs}
                    (lemma1'1 i f  rs xs)
                    (lemma1'2 x    rs xs)
\end{code}

%<*lemma-⋎-repl-raise>
\AgdaTarget{lemma-⋎-repl-raise}
\begin{code}
lemma-⋎-repl-raise : ∀ {ℓ m n} {α : Set ℓ} {v} f (rs : Vec α n) (xs : Vec α m) → f ≗ _↑ʳ_ (suc n)
                     → tabulate (λ y → lookup ((v ∷ rs) ++ xs) (lookup (tabulate {n = m} f ⋎ replicate m Fz) y)) ≡ xs ⋎ replicate m v
lemma-⋎-repl-raise {m = zero}               _f  _rs  []        _p  = refl
lemma-⋎-repl-raise {m = suc m} {n} {v = v}  f   rs   (x ∷ xs)  p   = cong₂ _∷_ (lookup-raise f v x rs xs p) (cong₂ _∷_ refl p′)
  where p′ = begin

               tabulate (λ y → lookup  (v ∷ rs ++ x ∷ xs)
                                       (lookup (tabulate (f ∘ Fs) ⋎ replicate m Fz) y))

             ≡⟨ tabulate-cong (λ i → lemma1 f rs xs p) ⟩

               tabulate (λ y → lookup  (v ∷ (rs ∷ʳ x) ++ xs)
                                       (lookup (tabulate (λ z → finCoerce (cong suc (+-suc n m)) (f (Fs z))) ⋎ replicate m Fz) y))

             ≡⟨ lemma-⋎-repl-raise  (finCoerce (cong suc (+-suc n m)) ∘ f ∘ Fs) (rs ∷ʳ x) xs (λ _i → finCoerce-raise f p) ⟩

               xs ⋎ replicate m v
             ∎
\end{code}
%</lemma-⋎-repl-raise>

%<*lemma-⋎-repl>
\AgdaTarget{lemma-⋎-repl}
\begin{code}
lemma-⋎-repl :  ∀ {ℓ n} {α : Set ℓ} (v : α) xs
                → tabulate (λ y → lookup (v ∷ xs) (lookup (tabulate {n = n} Fs ⋎ replicate n Fz) y)) ≡ xs ⋎ replicate n v
lemma-⋎-repl {n = n} _v xs = lemma-⋎-repl-raise {m = n} Fs [] xs (λ _ → refl)
\end{code}
%</lemma-⋎-repl>


%<*regn-distribute-runᶜ′>
\AgdaTarget{regn-distribute-runᶜ′}
\begin{code}
regn-distribute-runᶜ′ : ∀ {n} s v cs x xs →  runᶜ′ (⟦_⟧ᶜ {s} (regn-distribute n)) ((v ∷ x) ∷ cs) (zipWith _∷_ (repeat v) xs)
                                             ≈ₚ map (_⋎ replicate n v) (x ∷ ♯ xs)
regn-distribute-runᶜ′ s v cs x (x′ ∷ xs′) = lemma-⋎-repl v x ∷ₚ ♯ transₚ (regn-distribute-runᶜ′ s v ((v ∷ x) ∷ cs) x′ (♭ xs′)) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</regn-distribute-runᶜ′>

%<*regn-distribute-ω>
\AgdaTarget{regn-distribute-ω}
\begin{code}
regn-distribute-ω : ∀ {s n v} xs → ⟦ regn-distribute n ⟧ω (zipWith _∷_ (repeat v) xs) ≈ map (_⋎ replicate n v) xs
regn-distribute-ω {s} {n} {v} (x′ ∷ xs′) = ≈ₚ-to-≈ $ transₚ (regn-distribute-runᶜ′ s v [] x′ (♭ xs′)) (refl ∷ₚ ♯ reflₚ)
\end{code}
%</regn-distribute-ω>




%<*vc*>
\AgdaTarget{vc*}
\begin{code}
vc* : ∀ {ℓ} {α : Set ℓ} n → Vec α n → Vec α (n * 1)
vc* n = vecCoerce $ sym ((proj₂ *-id) n)
\end{code}
%</vc*>

%<*vc⋎>
\AgdaTarget{vc⋎}
\begin{code}
vc⋎ : ∀ {ℓ} {α : Set ℓ} n → Vec α (n +⋎ n) → Vec α (n * 2)
vc⋎ n = vecCoerce (+⋎-*2 n)
\end{code}
%</vc⋎>


-- TODO: for any serializable data
%<*zipWith-++-⇑>
\AgdaTarget{zipWith-++-⇑}
\begin{code}
zipWith-++-⇑ : ∀ {xs y} → zipWith _++_ xs (repeat [ y ]) ≈ map ⇓² (zipWith _,_ (map ⇑ xs) (repeat y))
zipWith-++-⇑ {x ∷ _} {y} = reduce x ∷ ♯ zipWith-++-⇑
  where  reduce : ∀ z → z ++ (y ∷ []) ≡ head z ∷ y ∷ []
         reduce (_ ∷ []) = refl
\end{code}
%</zipWith-++-⇑>


%<*⋎-replicate>
\AgdaTarget{⋎-replicate}
\begin{code}
⋎-replicate :  ∀ {n ℓ} {α : Set ℓ} {xs : Vec α (suc n)} {y}
               →  (vc⋎ (suc n)) (xs ⋎ replicate (suc n) y) ≡ (₁∘split1′ xs ++ [ y ]) ++ (vc⋎ n) (₂∘split1′ xs ⋎ replicate n y)
⋎-replicate {_} {xs = xs}          {_} with splitAt 1 xs
⋎-replicate {n} {xs = .(x₀ ∷ x₊)}  {y} | (x₀ ∷ []) , x₊ , refl = begin
  (vc⋎ (suc n)) ((x₀ ∷ x₊) ⋎ replicate (suc n) y)                 ≡⟨ vecCoerce-step (+⋎-*2 (suc n)) (cong suc (+⋎-*2 n)) ⟩
  x₀ ∷ vecCoerce (cong suc (+⋎-*2 n)) (y ∷ x₊ ⋎ replicate n y)    ≡⟨ cong (x₀ ∷_) (vecCoerce-step (cong suc (+⋎-*2 n)) (+⋎-*2 n)) ⟩
  x₀ ∷ y ∷ (vc⋎ n) (x₊ ⋎ replicate n y)                           ≡⟨⟩
  (₁∘split1′ (x₀ ∷ x₊) ++ [ y ]) ++ (vc⋎ n) (₂∘split1′ (x₀ ∷ x₊) ⋎ replicate n y)  ∎
\end{code}
%</⋎-replicate>


%<*map-⋎-replicate>
\AgdaTarget{map-⋎-replicate}
\begin{code}
map-⋎-replicate : ∀ {ℓ n} {α : Set ℓ} {xs} {y : α}
  →  map (λ x → (vc⋎ (suc n)) (x ⋎ replicate (suc n) y)) xs
     ≈ zipWith _++_  (zipWith _++_ (map ₁∘split1′ xs) (repeat [ y ])) (map (λ z → (vc⋎ n) (z ⋎ replicate n y)) (map ₂∘split1′ xs))
map-⋎-replicate {n = n} {xs = x ∷ _} = ⋎-replicate {n = n} {xs = x} ∷ ♯ map-⋎-replicate
\end{code}
%</map-⋎-replicate>


%<*vecCoerce-replicate>
\AgdaTarget{vecCoerce-replicate}
\begin{code}
vecCoerce-replicate : ∀ {n ℓ} {α : Set ℓ} {y : α} → (vc* (suc n)) (replicate (suc n) y) ≡ y ∷ (vc* n) (replicate n y)
vecCoerce-replicate {n} = vecCoerce-step (sym $ (proj₂ *-id) (suc n)) (sym $ (proj₂ *-id) n)
\end{code}
%</vecCoerce-replicate>


%<*repeat-replicate>
\AgdaTarget{repeat-replicate}
\begin{code}
repeat-replicate :  ∀ {ℓ n} {α : Set ℓ} {y : α}
                    →  zipWith _++_ (repeat [ y ]) (map (vc* n) $ repeat (replicate n y))
                       ≈ map (vc* (suc n)) (repeat (replicate (suc n) y))
repeat-replicate = sym vecCoerce-replicate ∷ ♯ repeat-replicate
\end{code}
%</repeat-replicate>


%<*lemmaRegN-never>
\AgdaTarget{lemmaRegN-never}
\begin{code}
postulate lemmaRegN-never :  ∀ {n} {xs : Stream (W n)}
                             →  ⟦ regn-regs n ⟧ω (map (λ x → (vc⋎ n) (x ⋎ replicate n 𝔽)) xs)
                                ≈ map (vc* n) (repeat (replicate n 𝔽))
\end{code}
lemmaRegN-never {zero}   {xs} = transₛ (runᶜ-const {xs = map (λ x → (vc⋎ zero) (x ⋎ replicate zero 𝔽)) xs} (replicate zero 𝔽)) (symₛ map-id)
lemmaRegN-never {suc n}  {xs} = beginₛ

  ⟦ regn-regs (suc n) ⟧ω (map (λ x → (vc⋎ (suc n)) (x ⋎ replicate (suc n) 𝔽)) xs)

    ≈⟨ ⟦⟧ω[–]-cong {c = regn-regs (suc n)} spec-B₃ (map-⋎-replicate {n} {xs = xs}) ⟩

  ⟦ reg ∥ regn-regs n ⟧ω (zipWith _++_
      (zipWith _++_ (map ₁∘split1′ xs) (repeat [ 𝔽 ]))
      (map (λ x → (vc⋎ n) (x ⋎ replicate n 𝔽)) (map ₂∘split1′ xs)))

    ≈⟨ proofPar {g = spec-B₃} {c = reg} {d = regn-regs n}
       {ins₁ = zipWith _++_ (map ₁∘split1′ xs) (repeat [ 𝔽 ])}
       {ins₂ = map (λ x → (vc⋎ n) (x ⋎ replicate n 𝔽)) (map ₂∘split1′ xs)} ⟩

  zipWith _++_
      (⟦ reg ⟧ω (zipWith _++_ (map ₁∘split1′ xs) (repeat [ 𝔽 ])))
      (⟦ regn-regs n ⟧ω (map (λ x → (vc⋎ n) (x ⋎ replicate n 𝔽)) (map ₂∘split1′ xs)))

    ≈⟨ zipWith-cong _++_
       (proofRegNeverLoadPlain {xs = map ₁∘split1′ xs})
       (lemmaRegN-never {n = n} {xs = map ₂∘split1′ xs}) ⟩

  zipWith _++_ (repeat [ 𝔽 ]) (map (vc* n) (repeat (replicate n 𝔽)))

    ≈⟨ repeat-replicate {n} ⟩

  map (vc* (suc n)) (repeat (replicate (suc n) 𝔽))
  ∎ₛ
%</lemmaRegN-never>


%<*map-id-Stream-Vec0>
\AgdaTarget{map-id-Stream-Vec0}
\begin{code}
map-id-Stream-Vec0 : ∀ {ℓ} {α : Set ℓ} {xs : Stream (Vec α 0)} → repeat [] ≈ map id xs
map-id-Stream-Vec0 {xs = [] ∷ _} = refl ∷ ♯ map-id-Stream-Vec0
\end{code}
%</map-id-Stream-Vec0>


%<*vecCoerce-p₁p₂-split1>
\AgdaTarget{vecCoerce-p₁p₂-split1}
\begin{code}
vecCoerce-p₁p₂-split1 : ∀ {n ℓ} {α : Set ℓ} {xs : Vec α (suc n)} → ₁∘split1′ xs ++ (vc* n) (₂∘split1′ xs) ≡ (vc* (suc n)) xs
vecCoerce-p₁p₂-split1 {xs = xs}  with splitAt 1 xs
vecCoerce-p₁p₂-split1 {n}        | _ ∷ [] , _ , refl = sym (vecCoerce-step (sym $ (proj₂ *-id) (suc n)) (sym $ (proj₂ *-id) n))
\end{code}
%</vecCoerce-p₁p₂-split1>


%<*map-vecCoerce-p₁p₂-split1>
\AgdaTarget{map-vecCoerce-p₁p₂-split1}
\begin{code}
map-vecCoerce-p₁p₂-split1 :  ∀ {ℓ n} {α : Set ℓ} {xs : Stream (Vec α (suc n))}
                             → zipWith _++_ (map ₁∘split1′ xs) (map (vc* n) (map ₂∘split1′ xs)) ≈ map (vc* (suc n)) xs
map-vecCoerce-p₁p₂-split1 {xs = x ∷ _} = vecCoerce-p₁p₂-split1 {xs = x} ∷ ♯ map-vecCoerce-p₁p₂-split1
\end{code}
%</map-vecCoerce-p₁p₂-split1>


%<*lemmaRegNAlwaysLoad>
\AgdaTarget{lemmaRegNAlwaysLoad}
\begin{code}
postulate lemmaRegNAlwaysLoad :  ∀ {n} {xs : Stream (W n)}
                                 → ⟦ regn-regs n ⟧ω (map (λ x → (vc⋎ n) (x ⋎ replicate n 𝕋)) xs) ≈ map (vc* n) xs
\end{code}
lemmaRegNAlwaysLoad {zero} {xs} = transₛ (runᶜ-const {xs = map (λ x → (vc⋎ zero) (x ⋎ replicate 𝕋)) xs} ?) map-id-Stream-Vec0
lemmaRegNAlwaysLoad {suc n} {xs} = beginₛ
  ⟦ regn-regs (suc n) ⟧ω (map (λ x → (vc⋎ (suc n)) (x ⋎ replicate {n = suc n} 𝕋)) xs)
    ≈⟨ ⟦⟧ω[–]-cong {c = regn-regs (suc n)} spec-B₃ (map-⋎-replicate {n} {xs = xs}) ⟩
  ⟦ reg ∥ regn-regs n ⟧ω (zipWith _++_
      (zipWith _++_ (map ₁∘split1′ xs) (repeat [ 𝕋 ]))
      (map (λ x → (vc⋎ n) (x ⋎ replicate {n = n} 𝕋)) (map ₂∘split1′ xs)))
    ≈⟨ proofPar {c = reg} {d = regn-regs n}
       {ins₁ = zipWith _++_ (map ₁∘split1′ xs) (repeat [ 𝕋 ])}
       {ins₂ = map (λ x → (vc⋎ n) (x ⋎ replicate {n = n} 𝕋)) (map ₂∘split1′ xs)} ⟩
  zipWith _++_
      (⟦ reg ⟧ω (zipWith _++_ (map ₁∘split1′ xs) (repeat [ 𝕋 ])))
      (⟦ regn-regs n ⟧ω (map (λ x → (vc⋎ n) (x ⋎ replicate {n = n} 𝕋)) (map ₂∘split1′ xs)))
    ≈⟨ zipWith-cong _++_
       (proofRegAlwaysLoadPlain {xs = map ₁∘split1′ xs})
       (lemmaRegNAlwaysLoad {n = n} {xs = map ₂∘split1′ xs}) ⟩
  zipWith _++_
      (map ₁∘split1′ xs)
      (map (vc* n) (map ₂∘split1′ xs))
    ≈⟨ map-vecCoerce-p₁p₂-split1 {n} ⟩
  map (vc* (suc n)) xs ∎ₛ
%<*lemmaRegNAlwaysLoad>


-- TODO: this (removing eq plugs) seems more like a general principle with simpler proof
%<*reg-remove-eq-plugs>
\AgdaTarget{reg-remove-eq-plugs}
\begin{code}
reg-remove-eq-plugs : ∀ {s n v} {xs ys : Stream (W n)}
  → ⟦ regn-regs n ⟧ω                                   (map (λ x → (vc⋎ n) (  x ⋎ replicate n v)) xs) ≈ map (vc* n) ys
  → ⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n ⟧ω  (map (λ x →            x ⋎ replicate n v)  xs) ≈ ys
reg-remove-eq-plugs {s} {n} {v} {xs} {ys} p = beginₛ
  ⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n ⟧ω (map (λ x → x ⋎ replicate n v) xs)
    ≈⟨ ⟦⟧ω[–]-⟫⇒∘ b₃ (regn-n+⋎n≡n*2 n ⟫ regn-regs n) (regn-n*1≡n n) {map (λ x → x ⋎ replicate n v) xs} ⟩
  ⟦ regn-n*1≡n n ⟧ω (⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟧ω (map (λ x → x ⋎ replicate n v) xs))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (regn-n*1≡n n) (⟦⟧ω[–]-⟫⇒∘ b₃ (regn-n+⋎n≡n*2 n) (regn-regs n) {map (λ x → x ⋎ replicate n v) xs}) ⟩
  ⟦ regn-n*1≡n n ⟧ω (⟦ regn-regs n ⟧ω (⟦ regn-n+⋎n≡n*2 n ⟧ω (map (λ x → x ⋎ replicate n v) xs)))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (regn-n*1≡n n) (⟦⟧ω[–]-cong b₃ (regn-regs n) (eq⤨⊑ωid {s} {g = b₃} {p = +⋎-*2 n} {x⁰⁺ = map (λ x → x ⋎ replicate n v) xs})) ⟩
  ⟦ regn-n*1≡n n ⟧ω (⟦ regn-regs n ⟧ω (map (vc⋎ n) (map (λ x → x ⋎ replicate n v) xs)))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (regn-n*1≡n n) (⟦⟧ω[–]-cong b₃ (regn-regs n) (symₛ (map-∘ {f = (λ x → x ⋎ replicate n v)} {vc⋎ n} {xs}))) ⟩
  ⟦ regn-n*1≡n n ⟧ω (⟦ regn-regs n ⟧ω (map (λ x → (vc⋎ n) (x ⋎ replicate n v)) xs))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (regn-n*1≡n n) p ⟩
  ⟦ regn-n*1≡n n ⟧ω (map (vc* n) ys)
    ≈⟨ eq⤨⊑ωid {s} {g = b₃} {p = (proj₂ *-id) n} {x⁰⁺ = map (vc* n) ys} ⟩
  map (vecCoerce $ (proj₂ *-id) n) (map (vc* n) ys)
    ≈⟨ symₛ map-∘ ⟩
  map (vecCoerce ((proj₂ *-id) n) ∘′ vc* n) ys
    ≈⟨ map-cong-fun (vecCoerce-rightInverse ((proj₂ *-id) n)) ⟩
  map id ys
    ≈⟨ map-id ⟩
  ys ∎ₛ
\end{code}
%</reg-remove-eq-plugs>


%<*proofRegN-never>
\AgdaTarget{proofRegN-never}
\begin{code}
proofRegN-never : ∀ {s n xs} → ⟦ regn n ⟧ω (zipWith _∷_ (repeat 𝔽) xs) ≈ repeat (replicate n 𝔽)
proofRegN-never {s} {n} {xs} = beginₛ
  ⟦ regn-distribute n ⟫ (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n) ⟧ω (zipWith _∷_ (repeat 𝔽) xs)
    ≈⟨ ⟦⟧ω[–]-⟫⇒∘ b₃ (regn-distribute n) (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n) {zipWith _∷_ (repeat 𝔽) xs} ⟩
  ⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n ⟧ω (⟦ regn-distribute n ⟧ω (zipWith _∷_ (repeat 𝔽) xs))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n) (regn-distribute-ω {s} xs) ⟩
  ⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n ⟧ω (map (λ x → x ⋎ replicate n 𝔽) xs)
    ≈⟨ reg-remove-eq-plugs {s} {xs = xs} {ys = repeat (replicate n 𝔽)} (lemmaRegN-never {xs = xs}) ⟩
  repeat (replicate n 𝔽) ∎ₛ
\end{code}
%</proofRegN-never>


%<*proofRegN-always>
\AgdaTarget{proofRegN-always}
\begin{code}
proofRegN-always : ∀ {s n xs} → ⟦ regn n ⟧ω (zipWith _∷_ (repeat 𝕋) xs) ≈ xs
proofRegN-always {s} {n} {xs} = beginₛ
  ⟦ regn-distribute n ⟫ (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n) ⟧ω (zipWith _∷_ (repeat 𝕋) xs)
    ≈⟨ ⟦⟧ω[–]-⟫⇒∘ b₃ (regn-distribute n) (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n) {zipWith _∷_ (repeat 𝕋) xs} ⟩
  ⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n ⟧ω (⟦ regn-distribute n ⟧ω (zipWith _∷_ (repeat 𝕋) xs))
    ≈⟨ ⟦⟧ω[–]-cong b₃ (regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n) (regn-distribute-ω {s} xs) ⟩
  ⟦ regn-n+⋎n≡n*2 n ⟫ regn-regs n ⟫ regn-n*1≡n n ⟧ω (map (λ x → x ⋎ replicate n 𝕋) xs)
    ≈⟨ reg-remove-eq-plugs {s} {xs = xs} {ys = xs} (lemmaRegNAlwaysLoad {xs = xs}) ⟩
  xs ∎ₛ
\end{code}
%</proofRegN-always>
