\begin{code}
module PiWare.Typed.Circuit.Notation where

open import Data.Nat.Base using (ℕ)
\end{code}


\begin{code}
variable
 α β γ δ : Set  -- input/output types for circuits (cannot be universe-polymoprhic)
 i j k l : ℕ  -- sizes of input/output circuit types
\end{code}
