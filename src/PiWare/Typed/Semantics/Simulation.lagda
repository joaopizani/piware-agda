\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Typed.Semantics.Simulation (A : Atomic) where

open import Function.Base using (_∘′_)
open import Codata.Musical.Stream using (Stream; map)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)

open import PiWare.Typed.Circuit A using (ℂ̂[_]; Mkℂ̂; σ; s) renaming (module WithGates to CircuitWithGates)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Semantics.Simulation A using (W⟶W; ⟦_⟧[_]; ⟦_⟧ω[_])

open import PiWare.Gates using (G)
open import PiWare.Typed.Circuit.Notation using (α; β; i; j)
\end{code}

\begin{code}
open import Data.Serializable using (_⇕_)
open _⇕_ ⦃ ... ⦄ using (⇓; ⇑)
\end{code}


%<*eval-typed>
\AgdaTarget{⟦\_⟧̂}
\begin{code}
⟦_⟧[_]̂ : (c : ℂ̂[ G ] {σ} α β {i} {j}) (g : TyGate G W⟶W) → (α → β)
⟦ Mkℂ̂ c ⟧[ g ]̂ = ⇑ ∘′ ⟦ c ⟧[ g ] ∘′ ⇓
\end{code}
%</eval-typed>


%<*eval-seq-typed>
\AgdaTarget{⟦\_⟧̂*}
\begin{code}
⟦_⟧ω[_]̂ : (c : ℂ̂[ G ] {s} α β {i} {j}) (g : TyGate G W⟶W) → (Stream α → Stream β)
⟦ Mkℂ̂ c ⟧ω[ g ]̂ = map ⇑ ∘′ ⟦ c ⟧ω[ g ] ∘′ map ⇓
\end{code}
%</eval-seq-typed>


%<*implements-combinational-typed>
\AgdaTarget{\_⊑̂\_}
\begin{code}
_⊑̂[_]_ : (c : ℂ̂[ G ] {σ} α β {i} {j}) (g : TyGate G W⟶W) → (α → β) → Set
ĉ ⊑̂[ g ] f = ∀ x → ⟦ ĉ ⟧[ g ]̂ x ≡ f x
\end{code}
%</implements-combinational-typed>



\begin{code}
module WithGates {G} (g : TyGate G W⟶W) where
 open CircuitWithGates G using (ℂ̂)
\end{code}

%<*eval-typed>
\AgdaTarget{⟦\_⟧̂}
\begin{code}
 ⟦_⟧̂ : ℂ̂ {σ} α β {i} {j} → (α → β)
 ⟦_⟧̂ = ⟦_⟧[ g ]̂
\end{code}
%</eval-typed>

%<*eval-seq-typed>
\AgdaTarget{⟦\_⟧ω̂}
\begin{code}
 ⟦_⟧ω̂ : ℂ̂ {s} α β {i} {j} → (Stream α → Stream β)
 ⟦_⟧ω̂ = ⟦_⟧ω[ g ]̂
\end{code}
%</eval-seq-typed>

%<*implements-combinational-typed>
\AgdaTarget{\_⊑̂\_}
\begin{code}
 _⊑̂_ : ℂ̂ {σ} α β {i} {j} → (α → β) → Set
 _⊑̂_ = _⊑̂[ g ]_
\end{code}
%</implements-combinational-typed>
