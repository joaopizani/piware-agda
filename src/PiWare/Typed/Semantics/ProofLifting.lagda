\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Typed.Semantics.ProofLifting (A : Atomic) where

open import Function.Base using (_∘_; id)
open import Relation.Binary.PropositionalEquality.Core using (_≗_)
open import Codata.Musical.Stream using (Stream; map; _≈_; map-cong)
open import Codata.Musical.Stream.Properties using (map-∘; map-cong-fun; map-id; module EqReasoningₛ; module Setoidₛ)
open Setoidₛ using () renaming (sym to symₛ)
open EqReasoningₛ using (begin_; step-≈-⟩; _∎)

open import PiWare.Circuit using (ℂ[_]; s)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Typed.Circuit using (Mkℂ̂)
open import PiWare.Semantics.Simulation A using (⟦_⟧ω[_]; W⟶W)
open import PiWare.Typed.Semantics.Simulation A using (⟦_⟧ω[_]̂)
open Atomic A using (Atom)
open import Data.Serializable using (_⇕_)
open _⇕_ using (⇓; ⇑)

open import PiWare.Gates using (G)
open import PiWare.Typed.Circuit.Notation using (α; β; i)
\end{code}

\begin{code}
-- TODO: make it consistent nomenclature
open import Data.Nat.Base using (ℕ)
private variable o : ℕ
\end{code}


%<*naturalityₛ>
\AgdaTarget{naturalityₛ}
\begin{code}
naturalityₛ : (∀ {α} → Stream α → Stream α) → Set₁
naturalityₛ g = ∀ {α β} {f : α → β} {s} → (g ∘ map f) s ≈ (map f ∘ g) s
\end{code}
%</naturalityₛ>


%<*⇑∘⇓≗id>
\AgdaTarget{⇑∘⇓≗id}
\begin{code}
⇑∘⇓≗id : (α ⇕ Atom) {i} → Set
⇑∘⇓≗id α̂ = (⇑ α̂) ∘ (⇓ α̂) ≗ id
\end{code}
%</⇑∘⇓≗id>


%<*liftProof>
\AgdaTarget{liftProof}
\begin{code}
liftProof :  ∀ {g : TyGate G W⟶W} {c : ℂ[ G ] {s} i o} {xs ys} ⦃ α̂ : α ⇕ Atom ⦄ ⦃ β̂ : β ⇕ Atom ⦄

             {h : ∀ {α} → Stream α → Stream α} (nh : naturalityₛ h) (inv : ⇑∘⇓≗id β̂ )

             → h (⟦                  c ⟧ω[ g ] (map (⇓ α̂ )  xs))  ≈ map (⇓ β̂ )  ys
             → h (⟦ Mkℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄  c ⟧ω[ g ]̂              xs)   ≈             ys

liftProof {g} {c} {xs} {ys} ⦃ α̂ ⦄ ⦃ β̂ ⦄ {h} nh inv p = begin
                  h (⟦ Mkℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄  c ⟧ω[ g ]̂              xs)    ≈⟨ nh ⟩
  map (⇑ β̂ )   (  h (⟦                  c ⟧ω[ g ] (map (⇓ α̂ )  xs)))  ≈⟨ map-cong (⇑ β̂ ) p ⟩
  map (⇑ β̂ )   (  map  (⇓ β̂ )       ys)                               ≈⟨ symₛ map-∘ ⟩
  map ((⇑ β̂ ) ∘        (⇓ β̂ ))      ys                                ≈⟨ map-cong-fun inv ⟩
  map id                            ys                                ≈⟨ map-id ⟩
                                    ys                                ∎
\end{code}
%</liftProof>
