\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Typed.Plugs (A : Atomic) where

open import Data.Unit.Base using (⊤)
open import Data.Nat.Base using (suc; _+_; _*_)
open import Data.Vec.Base using (Vec)
open import Data.Product.Base using (_×_)
open import Data.Nat.Properties using (+-*-semiring)
open import Algebra.Properties.Semiring.Exp (+-*-semiring) using (_^_)

open Atomic A using (Atom)
open import Data.Serializable using (_⇕_; ⇕×; ⇕Vec; ⇕⊤)
open import PiWare.Typed.Circuit A using (Ĉ[_]; Mkℂ̂)
open import PiWare.Plugs
  using  ( nil⤨; id⤨; id⤨₁; swap⤨; swap⤨₁; intertwine⤨; head⤨; vecHalf⤨; cons⤨; cons⤨₁
         ; nil⤨₀; vecHalfPow⤨; fst⤨; snd⤨; singleton⤨; forkVec⤨; fork×⤨; uncons⤨; uncons⤨₁ )

open import PiWare.Gates using (G)
open import PiWare.Typed.Circuit.Notation using (α; β; γ; δ; i; j; k; l)
\end{code}


%<*nil-plug-typed>
\AgdaTarget{nil⤨̂}
\begin{code}
nil⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] α ⊤
nil⤨̂ ⦃ α̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ ⇕⊤ ⦄ nil⤨
\end{code}
%</nil-plug-typed>

%<*nil-plug-typed-zero>
\AgdaTarget{nil⤨̂₀}
\begin{code}
nil⤨̂₀ : Ĉ[ G ] ⊤ ⊤
nil⤨̂₀ = Mkℂ̂ ⦃ ⇕⊤ ⦄ ⦃ ⇕⊤ ⦄ nil⤨₀
\end{code}
%</nil-plug-typed-zero>


%<*id-plug-typed>
\AgdaTarget{id⤨̂}
\begin{code}
id⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] α α
id⤨̂ ⦃ α̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ α̂ ⦄ id⤨
\end{code}
%</id-plug-typed>

%<*id-plug-typed-one>
\AgdaTarget{id⤨̂₁}
\begin{code}
id⤨̂₁ : ⦃ α̂ : (α ⇕ Atom) {1} ⦄ → Ĉ[ G ] α α
id⤨̂₁ ⦃ α̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ α̂ ⦄ id⤨₁
\end{code}
%</id-plug-typed-one>


%<*swap-plug-typed>
\AgdaTarget{swap⤨̂}
\begin{code}
swap⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ → Ĉ[ G ] (α × β) (β × α)
swap⤨̂ {i = i} ⦃ α̂ ⦄ ⦃ β̂ ⦄ = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦄ ⦃ ⇕× ⦃ β̂ ⦄ ⦃ α̂ ⦄ ⦄ (swap⤨ {a = i})
\end{code}
%</swap-plug-typed>

%<*swap-plug-typed-one>
\AgdaTarget{swap⤨̂₁}
\begin{code}
swap⤨̂₁ : ⦃ α̂ : (α ⇕ Atom) {1} ⦄ ⦃ β̂ : (β ⇕ Atom) {1} ⦄ → Ĉ[ G ] (α × β) (β × α)
swap⤨̂₁ ⦃ α̂ ⦄ ⦃ β̂ ⦄ = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦄ ⦃ ⇕× ⦃ β̂ ⦄ ⦃ α̂ ⦄ ⦄ swap⤨₁
\end{code}
%</swap-plug-typed-one>


%<*intertwine-plug-typed>
\AgdaTarget{intertwine⤨̂}
\begin{code}
intertwine⤨̂ :  ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ ⦃ γ̂ : (γ ⇕ Atom) {k} ⦄ ⦃ δ̂ : (δ ⇕ Atom) {l} ⦄
                → Ĉ[ G ] ((α × β) × (γ × δ)) ((α × γ) × (β × δ))
intertwine⤨̂ {i = i} ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦃ γ̂ ⦄ ⦃ δ̂ ⦄ = Mkℂ̂  ⦃ ⇕× ⦃ ⇕× ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦄ ⦃ ⇕× ⦃ γ̂ ⦄ ⦃ δ̂ ⦄ ⦄ ⦄
                                                   ⦃ ⇕× ⦃ ⇕× ⦃ α̂ ⦄ ⦃ γ̂ ⦄ ⦄ ⦃ ⇕× ⦃ β̂ ⦄ ⦃ δ̂ ⦄ ⦄ ⦄
                                                   (intertwine⤨ {a = i})
\end{code}
%</intertwine-plug-typed>


-- vector plugs
%<*head-plug-typed>
\AgdaTarget{head⤨̂}
\begin{code}
head⤨̂ : ∀ n ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] (Vec α (suc n)) α
head⤨̂ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ α̂ ⦄ (head⤨ n)
\end{code}
%</head-plug-typed>


%<*uncons-plug-typed>
\AgdaTarget{uncons⤨̂}
\begin{code}
uncons⤨̂ : ∀ n ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] (Vec α (suc n)) (α × Vec α n)
uncons⤨̂ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦄ (uncons⤨ n)
\end{code}
%</uncons-plug-typed>

%<*uncons-plug-typed-one>
\AgdaTarget{uncons⤨̂₁}
\begin{code}
uncons⤨̂₁ : ∀ n ⦃ α̂ : (α ⇕ Atom) {1} ⦄ → Ĉ[ G ] (Vec α (suc n)) (α × Vec α n)
uncons⤨̂₁ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦄ (uncons⤨₁ n)
\end{code}
%</uncons-plug-typed-one>


%<*cons-plug-typed>
\AgdaTarget{cons⤨̂}
\begin{code}
cons⤨̂ : ∀ n ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] (α × Vec α n) (Vec α (suc n))
cons⤨̂ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ (cons⤨ n)
\end{code}
%</cons-plug-typed>

%<*cons-plug-typed-one>
\AgdaTarget{cons⤨̂₁}
\begin{code}
cons⤨̂₁ : ∀ n ⦃ α̂ : (α ⇕ Atom) {1} ⦄ → Ĉ[ G ] (α × Vec α n) (Vec α (suc n))
cons⤨̂₁ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ (cons⤨₁ n)
\end{code}
%</cons-plug-typed-one>


%<*singleton-plug-typed>
\AgdaTarget{singleton⤨̂}
\begin{code}
singleton⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] α (Vec α 1)
singleton⤨̂ ⦃ α̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ singleton⤨
\end{code}
%</singleton-plug-typed>


%<*vecHalf-plug-typed>
\AgdaTarget{vecHalf⤨̂}
\begin{code}
vecHalf⤨̂ : ∀ n ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] (Vec α (2 * suc n)) (Vec α (suc n) × Vec α (suc n))
vecHalf⤨̂ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕× ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦄ (vecHalf⤨ n)
\end{code}
%</vecHalf-plug-typed>


%<*vecHalfPow-plug-typed>
\AgdaTarget{vecHalfPow⤨̂}
\begin{code}
vecHalfPow⤨̂ : ∀ n ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] (Vec α (2 ^ suc n)) (Vec α (2 ^ n) × Vec α (2 ^ n))
vecHalfPow⤨̂ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕× ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦄ (vecHalfPow⤨ n)
\end{code}
%</vecHalfPow-plug-typed>


%<*forkVec-plug-typed>
\AgdaTarget{forkVec⤨̂}
\begin{code}
forkVec⤨̂ : ∀ n ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] α (Vec α n)
forkVec⤨̂ n ⦃ α̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ (forkVec⤨ n)
\end{code}
%</forkVec-plug-typed>


%<*forkProduct-plug-typed>
\AgdaTarget{fork×⤨̂}
\begin{code}
fork×⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ → Ĉ[ G ] α (α × α)
fork×⤨̂ ⦃ α̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ α̂ ⦄ ⦄ fork×⤨
\end{code}
%</forkProduct-plug-typed>


-- pairs
%<*fst-plug-typed>
\AgdaTarget{fst⤨̂}
\begin{code}
fst⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ → Ĉ[ G ] (α × β) α
fst⤨̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦄ ⦃ α̂ ⦄ fst⤨
\end{code}
%</fst-plug-typed>


%<*snd-plug-typed>
\AgdaTarget{snd⤨̂}
\begin{code}
snd⤨̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ → Ĉ[ G ] (α × β) β
snd⤨̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦄ ⦃ β̂ ⦄ snd⤨
\end{code}
%</snd-plug-typed>
