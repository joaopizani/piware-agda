\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Typed.Circuit (A : Atomic) where

open import Data.Nat.Base using (ℕ; _+_)
open import Data.Product.Base using (_×_)

open import Data.Serializable using (_⇕_; ⇕×)
open import PiWare.Plugs.Core using (_⤪_)
open import PiWare.Gates using (Gates)
open Gates using (#in; #out)
open Atomic A using (Atom)

import PiWare.Circuit as Circuit
open Circuit using (ℂ[_]; IsComb; Gate; Plug; DelayLoop; _⟫_; _∥_)
open Circuit using (σ; ω) public

open import PiWare.Gates using (G)
open import PiWare.Circuit using (s) public
open import PiWare.Typed.Circuit.Notation using (α; β; δ; γ; i; j; k; l)
\end{code}


-- "High-level" circuit types, packing the synthesizable instances
%<*Circuit-typed>
\AgdaTarget{ℂ̂[\_]}
\begin{code}
record ℂ̂[_] (G : Gates) {s : IsComb} (α β : Set) {i j : ℕ} : Set where
  constructor Mkℂ̂
  field  ⦃ α̂ ⦄  : (α ⇕ Atom) {i}
         ⦃ β̂ ⦄  : (β ⇕ Atom) {j}
         base   : ℂ[ G ] {s} i j
\end{code}
%</Circuit-typed>

%<*Circuit-any-typed>
\AgdaTarget{Ĉ[\_]}
\begin{code}
Ĉ[_] : (G : Gates) (α β : Set) {i j : ℕ} → Set
Ĉ[ G ] α β {i} {j} = ∀ {s} → ℂ̂[ G ] {s} α β {i} {j}
\end{code}
%</Circuit-any-typed>


-- "Smart constructors"
%<*gateC>
\AgdaTarget{gateℂ̂}
\begin{code}
gateℂ̂ : ∀ g ⦃ α̂ : (α ⇕ Atom) {#in G g} ⦄ ⦃ β̂ : (β ⇕ Atom) {#out G g} ⦄ → Ĉ[ G ] α β
gateℂ̂ g ⦃ α̂ ⦄ ⦃ β̂ ⦄ = Mkℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ (Gate g)
\end{code}
%</gateC>


%<*plugC>
\AgdaTarget{plugℂ̂}
\begin{code}
plugℂ̂ : ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ → i ⤪ j → Ĉ[ G ] α β {i} {j}
plugℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ f = Mkℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ (Plug f)
\end{code}
%</plugC>


%<*delayC>
\AgdaTarget{delayℂ̂}
\begin{code}
delayℂ̂ :  ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ ⦃ δ̂ : (δ ⇕ Atom) {k} ⦄
           → ℂ̂[ G ] {σ} (α × δ) (β × δ) {i + k} {j + k} → ℂ̂[ G ] {ω} α β {i} {j}
delayℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ (Mkℂ̂ c) = Mkℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ (DelayLoop c)
\end{code}
%</delayC>


\begin{code}
infixl 8 _⟫̂_
\end{code}

%<*seq>
\AgdaTarget{\_⟫̂\_}
\begin{code}
_⟫̂_ :  ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ ⦃ γ̂ : (γ ⇕ Atom) {k} ⦄
        → ℂ̂[ G ] {s} α γ {i} {k} → ℂ̂[ G ] {s} γ β {k} {j} → ℂ̂[ G ] {s} α β {i} {j}
_⟫̂_ ⦃ α̂ ⦄ ⦃ β̂ ⦄ (Mkℂ̂ c) (Mkℂ̂ d) = Mkℂ̂ ⦃ α̂ ⦄ ⦃ β̂ ⦄ (c ⟫ d)
\end{code}
%</seq>


\begin{code}
infixr 9 _∥̂_
\end{code}

%<*par>
\AgdaTarget{\_∥̂\_}
\begin{code}
_∥̂_ :  ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ γ̂ : (γ ⇕ Atom) {k} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄ ⦃ δ̂ : (δ ⇕ Atom) {l} ⦄
        → ℂ̂[ G ] {s} α γ {i} {k} → ℂ̂[ G ] {s} β δ {j} {l} → ℂ̂[ G ] {s} (α × β) (γ × δ) {i + j} {k + l}
_∥̂_ ⦃ α̂ ⦄ ⦃ γ̂ ⦄ ⦃ β̂ ⦄ ⦃ δ̂ ⦄ (Mkℂ̂ c) (Mkℂ̂ d) = Mkℂ̂ ⦃ ⇕× ⦃ α̂ ⦄ ⦃ β̂ ⦄ ⦄ ⦃ ⇕× ⦃ γ̂ ⦄ ⦃ δ̂ ⦄ ⦄ (c ∥ d)
\end{code}
%</par>



\begin{code}
module WithGates (G : Gates) where
\end{code}

%<*Circuit-typed-with-gates>
\AgdaTarget{ℂ̂}
\begin{code}
 ℂ̂ : {s : IsComb} (α β : Set) {i j : ℕ} → Set
 ℂ̂ {s} α β {i} {j} = ℂ̂[ G ] {s} α β {i} {j}
\end{code}
%</Circuit-typed>

%<*Circuit-any-typed-with-gates>
\AgdaTarget{Ĉ}
\begin{code}
 Ĉ : (α β : Set) {i j : ℕ} → Set
 Ĉ α β {i} {j} = Ĉ[ G ] α β {i} {j}
\end{code}
%</Circuit-any-typed-with-gates>
