\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)

module PiWare.Typed.Patterns (A : Atomic) where

open import Data.Nat.Base using (_*_)
open import Data.Vec.Base using (Vec)

open import Data.Serializable using (_⇕_; ⇕Vec)
open import PiWare.Typed.Circuit A using (ℂ̂[_]; Mkℂ̂)
open import PiWare.Patterns A using (parsN; seqsN)
open Atomic A using (Atom)

open import PiWare.Gates using (G)
open import PiWare.Circuit using (s)
open import PiWare.Typed.Circuit.Notation using (α; β; γ; i; j)
\end{code}


%<*parsN-typed>
\AgdaTarget{parsN̂}
\begin{code}
parsN̂ :  ∀ k ⦃ α̂ : (α ⇕ Atom) {i} ⦄ ⦃ β̂ : (β ⇕ Atom) {j} ⦄
         → ℂ̂[ G ] {s} α β {i} {j} → ℂ̂[ G ] {s} (Vec α k) (Vec β k) {k * i} {k * j}
parsN̂ k ⦃ α̂ ⦄ ⦃ β̂ ⦄ (Mkℂ̂ c) = Mkℂ̂ ⦃ ⇕Vec ⦃ α̂ ⦄ ⦄ ⦃ ⇕Vec ⦃ β̂ ⦄ ⦄ (parsN k c)
\end{code}
%</parsN-typed>


%<*seqsN-typed>
\AgdaTarget{seqsN̂}
\begin{code}
seqsN̂ : ∀ k ⦃ γ̂ : (γ ⇕ Atom) {j} ⦄ → ℂ̂[ G ] {s} γ γ {j} {j} → ℂ̂[ G ] {s} γ γ {j} {j}
seqsN̂ k ⦃ γ̂ ⦄ (Mkℂ̂ c) = Mkℂ̂ ⦃ γ̂ ⦄ ⦃ γ̂ ⦄ (seqsN k c)
\end{code}
%</seqsN-typed>
