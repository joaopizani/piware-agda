\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Typed.Samples.Muxes where

open import Data.Bool.Base using (false; true; Bool; if_then_else_)
open import Data.Product.Base using (_×_; _,_; proj₂)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
open import Data.Bool.Properties using (∨-∧-commutativeSemiring)
open import Algebra.Bundles using (module CommutativeSemiring)
open CommutativeSemiring ∨-∧-commutativeSemiring using () renaming (+-identity to ∨-identity)

open import Data.Serializable using (_⇕_)
open import Data.Serializable.Bool using ()  -- only for instances
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Typed.Circuit Atomic-Bool using (Mkℂ̂) renaming (module WithGates to CircuitWithGates)
open import PiWare.Gates.BoolTrio using (B₃)
open CircuitWithGates B₃ using (Ĉ)
open import PiWare.Typed.Semantics.Simulation Atomic-Bool using () renaming (module WithGates to SimulationWithGates)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to b₃)
open SimulationWithGates b₃ using (⟦_⟧̂; _⊑̂_)
open import PiWare.Samples.Muxes using (mux; muxN)

open import PiWare.Typed.Circuit.Notation using (α; i)
\end{code}


-- s × (a × b) → c
%<*mux-typed>
\AgdaTarget{mux̂}
\begin{code}
mux̂ : Ĉ (Bool × (Bool × Bool)) Bool
mux̂ = Mkℂ̂ mux
\end{code}
%</mux-typed>


%<*muxN-typed>
\AgdaTarget{muxN}
\begin{code}
muxN̂ : ⦃ α̂ : (α ⇕ Bool) {i} ⦄ → Ĉ (Bool × (α × α)) α
muxN̂ ⦃ α̂ ⦄ = Mkℂ̂ (muxN _)
\end{code}
%</muxN-typed>



%<*test-mux1>
\AgdaTarget{test-mux₁}
\begin{code}
test-mux₁ : ⟦ mux̂ ⟧̂ (false , true , false) ≡ true
test-mux₁ = refl
\end{code}
%</test-mux1>

%<*test-mux2>
\AgdaTarget{test-mux₂}
\begin{code}
test-mux₂ : ⟦ mux̂ ⟧̂ (true , true , false) ≡ false
test-mux₂ = refl
\end{code}
%</test-mux2>


%<*ite-typed>
\AgdaTarget{itê}
\begin{code}
itê : (Bool × Bool × Bool) → Bool
itê (s , a , b) = if s then b else a
\end{code}
%</ite-typed>


%<*mux-impl-ite-table>
\AgdaTarget{mux⊑itê}
\begin{code}
mux⊑itê : mux̂ ⊑̂ itê
mux⊑itê (true   , _ , _) = refl
mux⊑itê (false  , a , _) = (proj₂ ∨-identity) a
\end{code}
