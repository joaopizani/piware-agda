\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Typed.Samples.BoolTrioComb where

open import Data.Unit.Base using (⊤)
open import Data.Nat.Base using (zero; suc)
open import Data.Bool.Base using (Bool; true; false; _xor_; _∧_)
open import Data.Vec.Base using (Vec; replicate)
open import Data.Product.Base using (_×_; _,_; proj₂; uncurry′)
open import Data.Bool.Properties using (∨-∧-commutativeSemiring)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
open import Algebra.Bundles using (module CommutativeSemiring)
open CommutativeSemiring ∨-∧-commutativeSemiring using () renaming (+-identity to ∨-identity)

open import Data.Serializable using ()  -- only for the instances
open import Data.Serializable.Bool using ()  -- only for the instances
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Typed.Circuit Atomic-Bool using (Mkℂ̂) renaming (module WithGates to CircuitWithGates)
open import PiWare.Gates.BoolTrio using (B₃)
open CircuitWithGates B₃ using (ℂ̂; Ĉ)
open import PiWare.Typed.Semantics.Simulation Atomic-Bool using () renaming (module WithGates to SimulationWithGates)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to spec-B₃)
open SimulationWithGates spec-B₃ using (⟦_⟧̂)
open import PiWare.Samples.BoolTrioComb using (⊥ℂ; ⊤ℂ; ¬ℂ; ∧ℂ; ∨ℂ; ⊼ℂ; ⊻ℂ; hadd; fadd; andN; ripple; proof-andN-alltrue)
\end{code}


%<*fundamentals-typed-type>
\begin{code}
⊥ℂ̂ ⊤ℂ̂  : Ĉ ⊤ Bool
∧ℂ̂ ∨ℂ̂  : Ĉ (Bool × Bool) Bool
¬ℂ̂      : Ĉ Bool Bool
\end{code}
%</fundamentals-typed-type>
%<*fundamentals-typed-def>
\AgdaTarget{⊥ℂ̂, ⊤ℂ̂, ¬ℂ̂, ∧ℂ̂, ∨ℂ̂}
\begin{code}
⊥ℂ̂ = Mkℂ̂ ⊥ℂ
⊤ℂ̂ = Mkℂ̂ ⊤ℂ
∧ℂ̂ = Mkℂ̂ ∧ℂ
∨ℂ̂ = Mkℂ̂ ∨ℂ
¬ℂ̂ = Mkℂ̂ ¬ℂ
\end{code}
%</fundamentals-typed-def>


%<*nand-typed>
\AgdaTarget{¬∧ℂ̂}
\begin{code}
⊼ℂ̂ : Ĉ (Bool × Bool) Bool
⊼ℂ̂ = Mkℂ̂ ⊼ℂ
\end{code}
%</nand-typed>

%<*xor-typed>
\AgdaTarget{⊻ℂ̂}
\begin{code}
⊻ℂ̂ : Ĉ (Bool × Bool) Bool
⊻ℂ̂ = Mkℂ̂ ⊻ℂ
\end{code}
%</xor-typed>


a × b → c × s
%<*hadd-typed>
\AgdaTarget{hadd̂}
\begin{code}
hadd̂ : Ĉ (Bool × Bool) (Bool × Bool)
hadd̂ = Mkℂ̂ hadd
\end{code}
%</hadd-typed>


(a × b) × cin → co × s
%<*fadd-typed>
\AgdaTarget{fadd̂}
\begin{code}
fadd̂ : Ĉ ((Bool × Bool) × Bool) (Bool × Bool)
fadd̂ = Mkℂ̂ fadd
\end{code}
%</fadd-typed>


%<*andN-typed>
\AgdaTarget{andN̂}
\begin{code}
andN̂ : ∀ n → ℂ̂ (Vec Bool n) Bool
andN̂ n = Mkℂ̂ (andN n)
\end{code}
%</andN-typed>


-- TODO: | Vn | = (n * 1) vs. ripple (sized) input has n
%<*ripple-typed>
\AgdaTarget{ripplê}
\begin{code}
\end{code}
ripplê : ∀ n → ℂ̂ (Bool × Vec Bool n × Vec Bool n) (Vec Bool n × Bool)
ripplê n = Mkℂ̂ ⦃ ⇕× {1} ⦃ β̂ = ⇕× ⦃ ⇕Vec {n} ⦄ ⦃ ⇕Vec {n} ⦄ ⦄ ⦄ ⦃ ⇕× ⦃ ⇕Vec {n} ⦄ ⦄ (ripple n)
%</ripple-typed>



%<*xor-spec-table>
\AgdaTarget{⊻ℂ̂-spec-table}
\begin{code}
⊻ℂ̂-spec-table : (Bool × Bool) → Bool
⊻ℂ̂-spec-table (false  , false  ) = false
⊻ℂ̂-spec-table (false  , true   ) = true
⊻ℂ̂-spec-table (true   , false  ) = true
⊻ℂ̂-spec-table (true   , true   ) = false
\end{code}
%</xor-spec-table>


%<*xor-proof-table>
\AgdaTarget{⊻ℂ̂-proof-table}
\begin{code}
⊻ℂ̂-proof-table : ∀ a b → ⟦ ⊻ℂ̂ ⟧̂ (a , b) ≡ ⊻ℂ̂-spec-table (a , b)
⊻ℂ̂-proof-table false  false  = refl
⊻ℂ̂-proof-table false  true   = refl
⊻ℂ̂-proof-table true   false  = refl
⊻ℂ̂-proof-table true   true   = refl
\end{code}
%</xor-proof-table>


%<*xor-spec-subfunc>
\AgdaTarget{⊻ℂ̂-spec-subfunc}
\begin{code}
⊻ℂ̂-spec-subfunc : (Bool × Bool) → Bool
⊻ℂ̂-spec-subfunc = uncurry′ _xor_
\end{code}
%</xor-spec-subfunc>


%<*xor-proof-subfunc>
\AgdaTarget{⊻ℂ̂-proof-subfunc}
\begin{code}
⊻ℂ̂-proof-subfunc : ∀ a b → ⟦ ⊻ℂ̂ ⟧̂ (a , b) ≡ ⊻ℂ̂-spec-subfunc (a , b)
⊻ℂ̂-proof-subfunc true   b = refl
⊻ℂ̂-proof-subfunc false  b = (proj₂ ∨-identity) b
\end{code}
%</xor-proof-subfunc>


%<*haddSpec>
\AgdaTarget{haddSpeĉ}
\begin{code}
haddSpeĉ : Bool → Bool → (Bool × Bool)
haddSpeĉ a b = (a ∧ b) , (a xor b)
\end{code}
%</haddSpec>

-- TODO
%<*proofHaddBool>
\AgdaTarget{proofHaddBool̂}
\begin{code}
\end{code}
proofHaddBool̂ : ∀ a b → ⟦ hadd̂ ⟧̂ (a , b) ≡ haddSpeĉ a b
proofHaddBool̂ a b = cong (_,_ (a ∧ b)) (⊻ℂ̂-proof-subfunc a b)
%</proofHaddBool>


%<*faddSpec>
\AgdaTarget{faddSpeĉ}
\begin{code}
faddSpeĉ : Bool → Bool → Bool → (Bool × Bool)
faddSpeĉ false  false  false  = false  , false
faddSpeĉ false  false  true   = false  , true
faddSpeĉ false  true   false  = false  , true
faddSpeĉ false  true   true   = true   , false
faddSpeĉ true   false  false  = false  , true
faddSpeĉ true   false  true   = true   , false
faddSpeĉ true   true   false  = true   , false
faddSpeĉ true   true   true   = true   , true
\end{code}
%</faddSpec>


-- TODO
%<*proofFaddBool>
\AgdaTarget{proofFaddBool̂}
proofFaddBool̂ : ∀ a b c → ⟦ fadd̂ ⟧̂ ((a , b) , c) ≡ faddSpeĉ a b c
proofFaddBool̂ true   true   true   = refl
proofFaddBool̂ true   true   false  = refl
proofFaddBool̂ true   false  true   = refl
proofFaddBool̂ true   false  false  = refl
proofFaddBool̂ false  true   true   = refl
proofFaddBool̂ false  true   false  = refl
proofFaddBool̂ false  false  true   = refl
proofFaddBool̂ false  false  false  = refl
%</proofFaddBool>


%<*proof-andN-alltrue-typed>
\AgdaTarget{proof-andN-alltruê}
\begin{code}
proof-andN-alltruê : ∀ n → ⟦ andN̂ n ⟧̂ (replicate n true) ≡ true
proof-andN-alltruê zero    = refl
proof-andN-alltruê (suc n) rewrite proof-andN-alltrue n = ⊥ where postulate ⊥ : _
\end{code}
%</proof-andN-alltrue-typed>
