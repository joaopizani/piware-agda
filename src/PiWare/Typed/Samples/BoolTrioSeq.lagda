\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module PiWare.Typed.Samples.BoolTrioSeq where

open import Codata.Musical.Notation using (♯_)
open import Function.Base using (_$_)
open import Data.Unit.Base using (⊤; tt)
open import Data.Bool.Base using (Bool; not; false; true)
open import Data.Product.Base using (_,_; _×_)
open import Codata.Musical.Stream using (Stream; _∷_; head; tail; take; repeat; iterate; zipWith; _≈_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)

open import Data.Serializable using ()  -- only for instances
open import Data.Serializable.Bool using ()  -- only for instances
open import PiWare.Atomic.Bool using (Atomic-Bool)
open import PiWare.Typed.Circuit Atomic-Bool using (Mkℂ̂) renaming (module WithGates to CircuitWithGates)
open import PiWare.Gates.BoolTrio using (B₃)
open CircuitWithGates B₃ using (ℂ̂)
open import PiWare.Typed.Semantics.Simulation Atomic-Bool using () renaming (module WithGates to SimulationWithGates)
open import PiWare.Semantics.Simulation.BoolTrio using () renaming (spec to b₃)
open SimulationWithGates b₃ using (⟦_⟧ω̂)
open import PiWare.Samples.BoolTrioSeq using (shift; toggle; reg)
\end{code}


%<*shift-typed>
\AgdaTarget{shift̂}
\begin{code}
shift̂ : ℂ̂ Bool Bool
shift̂ = Mkℂ̂ shift
\end{code}
%</shift-typed>


%<*toggle-typed>
\AgdaTarget{togglê}
\begin{code}
togglê : ℂ̂ ⊤ Bool
togglê = Mkℂ̂ toggle
\end{code}
%</toggle-typed>


-- input × load → out
%<*reg-typed>
\AgdaTarget{reĝ}
\begin{code}
reĝ : ℂ̂ (Bool × Bool) Bool
reĝ = Mkℂ̂ reg
\end{code}
%</reg-typed>



\begin{code}
zip : ∀ {ℓ} {α β : Set ℓ} → Stream α → Stream β → Stream (α × β)
zip = zipWith _,_
\end{code}


%<*toggle7>
\AgdaTarget{toggle7̂}
\begin{code}
toggle7̂ = take 7 $ ⟦ togglê ⟧ω̂ (repeat tt)
\end{code}
%</toggle7>


%<*shift7>
\AgdaTarget{shift7̂}
\begin{code}
shift7̂ = take 7 $ ⟦ shift̂ ⟧ω̂ (iterate not false)
\end{code}
%</shift7>


%<*rhold>
\AgdaTarget{rhold̂}
\begin{code}
rhold̂ = take 7 (⟦ reĝ ⟧ω̂ $ zip (true ∷ ♯ (true ∷ ♯ repeat false)) (true ∷ ♯ repeat false))
\end{code}
%</rhold>


%<*rload>
\AgdaTarget{rload̂}
\begin{code}
rload̂ = take 7 (⟦ reĝ ⟧ω̂ $ zip (true ∷ ♯ (true ∷ ♯ repeat false)) (false ∷ ♯ (true ∷ ♯ repeat false)))
\end{code}
%</rload>


%<*proofShiftHead>
\begin{code}
proofShiftHead̂ : ∀ {x y zs} → head (⟦ shift̂ ⟧ω̂ (x ∷ ♯ (y ∷ ♯ zs)) ) ≡ false
proofShiftHead̂ = refl
\end{code}
%</proofShiftHead>


%<*proofShiftTail>
\begin{code}
postulate proofShiftTail̂ : ∀ {ins} → tail (⟦ shift̂ ⟧ω̂ ins) ≈ ins
\end{code}
proofShiftTail̂ {true ∷ xs} = {!!}
proofShiftTail̂ {false ∷ xs} = {!!}
%</proofShiftTail>
