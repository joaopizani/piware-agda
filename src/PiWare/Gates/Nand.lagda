\begin{code}
module PiWare.Gates.Nand where

open import Data.Nat.Base using (ℕ)

open import PiWare.Gates using (Gates)
\end{code}


%<*GateNand>
\AgdaTarget{Gate\#Nand}
\begin{code}
data Gate#Nand : Set where ⊼ℂ' : Gate#Nand
\end{code}
%</GateNand>


%<*ins-outs>
\AgdaTarget{\#inNand, \#outNand}
\begin{code}
#inNand #outNand : Gate#Nand → ℕ
#inNand   ⊼ℂ' = 2
#outNand  ⊼ℂ' = 1
\end{code}
%</ins-outs>


%<*Nand>
\AgdaTarget{Nand}
\begin{code}
Nand : Gates
Nand = record  { Gate#  = Gate#Nand
               ; #in    = #inNand
               ; #out   = #outNand }
\end{code}
%</Nand>
