\begin{code}
module PiWare.Gates.BoolTrio where

open import Data.Nat.Base using (ℕ)

open import PiWare.Gates using (Gates)
\end{code}


%<*GateB3>
\AgdaTarget{Gate\#B₃, ⊥ℂ', ⊤ℂ', ¬ℂ', ∧ℂ', ∨ℂ'}
\begin{code}
data Gate#B₃ : Set where
  ⊥ℂ' ⊤ℂ' ¬ℂ' ∧ℂ' ∨ℂ' : Gate#B₃
\end{code}
%</GateB3>


%<*ins-outs>
\AgdaTarget{\#inB₃, \#outB₃}
\begin{code}
#inB₃ #outB₃ : Gate#B₃ → ℕ
#outB₃ _ = 1

#inB₃ ⊥ℂ' = 0
#inB₃ ⊤ℂ' = 0
#inB₃ ¬ℂ' = 1
#inB₃ ∧ℂ' = 2
#inB₃ ∨ℂ' = 2
\end{code}
%</ins-outs>


%<*B3>
\AgdaTarget{B₃}
\begin{code}
B₃ : Gates
B₃ = record  { Gate#  = Gate#B₃
             ; #in    = #inB₃
             ; #out   = #outB₃ }
\end{code}
%</B3>
