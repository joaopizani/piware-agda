\begin{code}
module PiWare.Circuit where

open import Data.Nat.Base using (ℕ; _+_)

open import PiWare.Plugs.Core using (_⤪_)
open import PiWare.Gates using (Gates)
open Gates using (#in; #out; Gate#)

open import PiWare.Circuit.Notation using (i; o; m; l; i₁; o₁; i₂; o₂)
\end{code}


%<*IsComb>
\AgdaTarget{IsComb, σ, ω}
\begin{code}
data IsComb : Set where σ ω : IsComb
\end{code}
%</IsComb>
\begin{code}
variable
 s : IsComb
\end{code}


%<*Circuit-predecl>
\begin{code}
data ℂ[_] (G : Gates) : {s : IsComb} → ℕ → ℕ → Set
\end{code}
%</Circuit-predecl>

%<*Circuit-any>
\AgdaTarget{C[\_]}
\begin{code}
C[_] : (G : Gates) → ℕ → ℕ → Set
C[ G ] i o = ∀ {s} → ℂ[ G ] {s} i o
\end{code}
%</Circuit-any>

\begin{code}
infixl 4 _⟫_
infixr 5 _∥_
\end{code}

%<*Circuit>
\AgdaTarget{ℂ[\_], Gate, Plug, \_⟫\_, \_∥\_, DelayLoop}
\begin{code}
data ℂ[_] G where
  Gate  : (g# : Gate# G)  → C[ G ] (#in G g#) (#out G g#)
  Plug  : i ⤪ o           → C[ G ] i o

  _⟫_  : ℂ[ G ] {s} i m    → ℂ[ G ] {s} m o    → ℂ[ G ] {s} i o
  _∥_  : ℂ[ G ] {s} i₁ o₁  → ℂ[ G ] {s} i₂ o₂  → ℂ[ G ] {s} (i₁ + i₂) (o₁ + o₂)

  DelayLoop : ℂ[ G ] {σ} (i + l) (o + l) → ℂ[ G ] {ω} i o
\end{code}
%</Circuit>



\begin{code}
module WithGates (G : Gates) where
\end{code}

%<*Circuit-with-gates>
\begin{code}
 ℂ : {s : IsComb} → ℕ → ℕ → Set
 ℂ {s} = ℂ[ G ] {s}
\end{code}
%</Circuit-with-gates>

%<*Circuit-any-with-gates>
\AgdaTarget{C}
\begin{code}
 C : ℕ → ℕ → Set
 C = C[ G ]
\end{code}
%</Circuit-any-with-gates>
