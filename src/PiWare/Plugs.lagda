\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module PiWare.Plugs where

open import Data.Nat.Base using (ℕ; suc; _+_; _*_)
open import Data.Vec using (allFin)
open import Data.Nat.Properties using (+-*-commutativeSemiring)
open import Algebra.Properties.CommutativeSemiring.Exp +-*-commutativeSemiring using (_^_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)

open import Data.Vec.Extra using (VecMorphism)
open import Effect.Functor using (Morphism)
open Morphism using (op)

open import PiWare.Circuit using (C[_]; ℂ[_]; Plug)
open import PiWare.Plugs.Core
  using  ( nil⤪; nil⤪₀; id⤪; id⤪₁; eq⤪; swap⤪; swap⤪₁; intertwine⤪; head⤪; vecHalf⤪; cons⤪₁
         ; vecHalfPow⤪; fst⤪; snd⤪; singleton⤪; forkVec⤪; fork×⤪; uncons⤪; uncons⤪₁; cons⤪)

open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o; m)
\end{code}

\begin{code}
private
 variable w a b c d  : ℕ
\end{code}


%<*nil-plug>
\AgdaTarget{nil⤨}
\begin{code}
nil⤨ : C[ G ] i 0
nil⤨ = Plug nil⤪
\end{code}
%</nil-plug>

%<*nil-plug-zero>
\AgdaTarget{nil⤨₀}
\begin{code}
nil⤨₀ : C[ G ] 0 0
nil⤨₀ = Plug nil⤪₀
\end{code}
%</nil-plug-zero>


%<*id-plug>
\AgdaTarget{id⤨}
\begin{code}
id⤨ : C[ G ] m m
id⤨ = Plug id⤪
\end{code}
%</id-plug>

%<*id-plug-one>
\AgdaTarget{id⤨₁}
\begin{code}
id⤨₁ : C[ G ] 1 1
id⤨₁ = Plug id⤪₁
\end{code}
%</id-plug-one>


%<*eq-plug>
\AgdaTarget{eq⤨}
\begin{code}
eq⤨ : (p : i ≡ o) → C[ G ] i o
eq⤨ p = Plug (eq⤪ p)
\end{code}
%</eq-plug>


%<*swap-plug>
\AgdaTarget{swap⤨}
\begin{code}
swap⤨ : C[ G ] (a + b) (b + a)
swap⤨ {a} {b} = Plug (swap⤪ {a} {b})
\end{code}
%</swap-plug>

%<*swap-plug-one>
\AgdaTarget{swap⤨₁}
\begin{code}
swap⤨₁ : C[ G ] (1 + 1) (1 + 1)
swap⤨₁ = Plug swap⤪₁
\end{code}
%</swap-plug-one>


%<*intertwine-plug>
\AgdaTarget{intertwine⤨}
\begin{code}
intertwine⤨ : C[ G ] ((a + b) + (c + d)) ((a + c) + (b + d))
intertwine⤨ {a} {b} {c} {d} = Plug (intertwine⤪ {a} {b} {c} {d})
\end{code}
%</intertwine-plug>


%<*head-plug>
\AgdaTarget{head⤨}
\begin{code}
head⤨ : ∀ n → C[ G ] (suc n * w) w
head⤨ n = Plug (head⤪ n)
\end{code}
%</head-plug>


%<*uncons-plug>
\AgdaTarget{uncons⤨}
\begin{code}
uncons⤨ : ∀ n → C[ G ] (suc n * i) (i + n * i)
uncons⤨ n = Plug (uncons⤪ n)
\end{code}
%</uncons-plug>

%<*uncons-plug-one>
\AgdaTarget{uncons⤨₁}
\begin{code}
uncons⤨₁ : ∀ n → C[ G ] (suc n * 1) (1 + n * 1)
uncons⤨₁ n = Plug (uncons⤪₁ n)
\end{code}
%</uncons-plug-one>


%<*cons-plug>
\AgdaTarget{cons⤨}
\begin{code}
cons⤨ : ∀ n → C[ G ] (i + n * i) (suc n * i)
cons⤨ n = Plug (cons⤪ n)
\end{code}
%</cons-plug>

%<*cons-plug-one>
\AgdaTarget{cons⤨₁}
\begin{code}
cons⤨₁ : ∀ n → C[ G ] (1 + n * 1) (suc n * 1)
cons⤨₁ n = Plug (cons⤪₁ n)
\end{code}
%</cons-plug-one>


%<*singleton-plug>
\AgdaTarget{singleton⤨}
\begin{code}
singleton⤨ : C[ G ] w (1 * w)
singleton⤨ = Plug singleton⤪
\end{code}
%</singleton-plug>


%<*vecHalf-plug>
\AgdaTarget{vecHalf⤨}
\begin{code}
vecHalf⤨ : ∀ n → C[ G ] ((2 * (suc n)) * w) ((suc n) * w + (suc n) * w)
vecHalf⤨ n = Plug (vecHalf⤪ n)
\end{code}
%</vecHalf-plug>


%<*vecHalfPow-plug>
\AgdaTarget{vecHalfPow⤨}
\begin{code}
vecHalfPow⤨ : ∀ n → C[ G ] ((2 ^ (suc n)) * w) ((2 ^ n) * w + (2 ^ n) * w)
vecHalfPow⤨ n = Plug (vecHalfPow⤪ n)
\end{code}
%</vecHalfPow-plug>


%<*forkVec-plug>
\AgdaTarget{forkVec⤨}
\begin{code}
forkVec⤨ : ∀ n {k} → C[ G ] k (n * k)
forkVec⤨ n = Plug (forkVec⤪ n)
\end{code}
%</forkVec-plug>


%<*forkProd-plug>
\AgdaTarget{fork×⤨}
\begin{code}
fork×⤨ : C[ G ] w (w + w)
fork×⤨ = Plug fork×⤪
\end{code}
%</forkProd-plug>


%<*fst-plug>
\AgdaTarget{fst⤨}
\begin{code}
fst⤨ : C[ G ] (a + b) a
fst⤨ = Plug fst⤪
\end{code}
%</fst-plug>


%<*snd-plug>
\AgdaTarget{snd⤨}
\begin{code}
snd⤨ : C[ G ] (a + b) b
snd⤨ = Plug snd⤪
\end{code}
%</snd-plug>


%<*plug-Vec-eta>
\AgdaTarget{plug-Vecη}
\begin{code}
plug-Vecη : VecMorphism i o → C[ G ] i o
plug-Vecη {i} η = Plug (op η (allFin i))
\end{code}
%</plug-Vec-eta>
