\begin{code}
module PiWare.Circuit.Notation where

open import Data.Nat.Base using (ℕ)
\end{code}


\begin{code}
variable
 -- circuit input/output sizes
 i   i₁   i₂   i₃   o   o₁   o₂   o₃   m   m₁   m₂   m₃   l   : ℕ
 i′  i₁′  i₂′  i₃′  o′  o₁′  o₂′  o₃′  m′  m₁′  m₂′  m₃′  l′  : ℕ
\end{code}
