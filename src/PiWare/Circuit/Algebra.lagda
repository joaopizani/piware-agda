\begin{code}
module PiWare.Circuit.Algebra where

open import Data.Nat.Base using (ℕ; _+_)

open import PiWare.Plugs.Core using (_⤪_)
open import PiWare.Circuit using (ℂ[_]; σ; Gate; Plug; DelayLoop; _⟫_; _∥_)
open import PiWare.Gates using (Gates)
open Gates using (#in; #out)

open import PiWare.Circuit.Notation using (i; o; l)
open import PiWare.Circuit using (s)
\end{code}


\begin{code}
TyGate : (G : Gates) → (ℕ → ℕ → Set) → Set
\end{code}
%<*combinator-type-gate>
\AgdaTarget{TyGate}
\begin{code}
TyGate G F = ∀ g# → F (#in G g#) (#out G g#)
\end{code}
%</combinator-type-gate>


\begin{code}
TyPlug Ty⟫ Ty∥ : (ℕ → ℕ → Set) → Set
\end{code}
%<*combinator-types-parameterized>
\AgdaTarget{TyGate, TyPlug, Ty⟫, Ty∥}
\begin{code}
TyPlug     F = ∀ {i o} → i ⤪ o  → F i o
Ty⟫        F = ∀ {i m o}        → F i m    → F m o    → F i o
Ty∥        F = ∀ {i₁ o₁ i₂ o₂}  → F i₁ o₁  → F i₂ o₂  → F (i₁ + i₂) (o₁ + o₂)
\end{code}
%</combinator-types-parameterized>



\begin{code}
module _ {G : Gates} where
\end{code}


\begin{code}
 module _ (F : ℕ → ℕ → Set) where
\end{code}
%<*Circuit-combinational-algebra-type>
\AgdaTarget{ℂσA}
\begin{code}
  record ℂσA : Set where
    field  GateA  : TyGate  G  F
           PlugA  : TyPlug     F
           _⟫A_   : Ty⟫        F
           _∥A_   : Ty∥        F
\end{code}
%</Circuit-combinational-algebra-type>

\begin{code}
  module _ (a : ℂσA) where
   open ℂσA a
\end{code}
%<*Circuit-combinational-cata>
\AgdaTarget{cataℂσ}
\begin{code}
   cataℂσ : ℂ[ G ] {σ} i o → F i o
   cataℂσ (Gate g)   = GateA g
   cataℂσ (Plug f)   = PlugA f
   cataℂσ (c₁ ⟫ c₂)  = cataℂσ c₁ ⟫A cataℂσ c₂
   cataℂσ (c₁ ∥ c₂)  = cataℂσ c₁ ∥A cataℂσ c₂
\end{code}
%</Circuit-combinational-cata>


\begin{code}
 module _ (Fσ : ℕ → ℕ → Set) (F : ℕ → ℕ → Set) where
\end{code}
%<*Circuit-algebra-type>
\AgdaTarget{ℂ★}
\begin{code}
  record ℂA : Set where
    field  GateA  : TyGate  G  F
           PlugA  : TyPlug     F
           _⟫A_   : Ty⟫        F
           _∥A_   : Ty∥        F
 
           DelayLoopA : Fσ (i + l) (o + l) → F i o
\end{code}
%</Circuit-algebra-type>

\begin{code}
  module _ (aσ : ℂσA Fσ) (a : ℂA) where
   open ℂA a
\end{code}

%<*Circuit-cata>
\AgdaTarget{cataℂ}
\begin{code}
   cataℂ : ℂ[ G ] {s} i o → F i o
   cataℂ (Gate g)       = GateA g
   cataℂ (Plug f)       = PlugA f
   cataℂ (DelayLoop c)  = DelayLoopA (cataℂσ Fσ aσ c)
   cataℂ (c₁ ⟫ c₂)      = cataℂ c₁ ⟫A cataℂ c₂
   cataℂ (c₁ ∥ c₂)      = cataℂ c₁ ∥A cataℂ c₂
\end{code}
%</Circuit-cata>
