\begin{code}
module PiWare.Gates where

open import Data.Nat.Base using (ℕ)
\end{code}


%<*Gates>
\AgdaTarget{Gates, \#in, \#out, Gate\#}
\begin{code}
record Gates : Set₁ where
  field  Gate#     : Set
         #in #out  : Gate# → ℕ
\end{code}
%</Gates>


\begin{code}
variable
 G : Gates
\end{code}
