\begin{code}
{-# OPTIONS --hidden-argument-puns #-}
\end{code}
\begin{code}
module PiWare.Plugs.Core where

open import Function.Base using (id; _$_; _⟨_⟩_)
open import Data.Product.Base using (_,_)
open import Data.Fin.Base using (Fin; _↑ˡ_; _↑ʳ_)
open import Data.Vec.Base using (Vec; []; map; _++_; lookup; tabulate; splitAt; replicate; concat)
open import Data.Nat.Base using (ℕ; zero; suc; _+_; _*_)
open import Data.Nat.Properties using (+-identityʳ; +-assoc; +-comm; *-assoc; *-comm; *-distribʳ-+; +-*-commutativeSemiring)
open import Algebra.Properties.CommutativeSemiring.Exp +-*-commutativeSemiring using (_^_)
open import Data.Nat.Solver using (module +-*-Solver)
open +-*-Solver using (solve; _:=_; con; _:+_; _:*_)

open import Relation.Binary.PropositionalEquality using (cong; cong₂; sym; refl; trans; _≡_; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-∣; _∎)

open import Notation.JP.Base using (m; n; n′)
\end{code}

\begin{code}
private
 variable
  i w a b c d : ℕ
\end{code}


\begin{code}
infix 8 _⤪_
\end{code}

%<*Plug-type>
\AgdaTarget{\_⤪\_}
\begin{code}
_⤪_ : ℕ → ℕ → Set
i ⤪ o = Vec (Fin i) o
\end{code}

%</Plug-type>


\begin{code}
infixr 7 _|⤪_
\end{code}

%<*Plug-par>
\AgdaTarget{\_|⤪\_}
\begin{code}
_|⤪_ : a ⤪ b → c ⤪ d → (a + c) ⤪ (b + d)
_|⤪_ {a} {c} f g = map (_↑ˡ c) f ++ map (a ↑ʳ_) g
\end{code}
%</Plug-par>

\begin{code}
infixr 6 _⟫⤪_
\end{code}

%<*Plug-seq>
\AgdaTarget{\_⟫⤪\_}
\begin{code}
_⟫⤪_ : a ⤪ b → b ⤪ c → a ⤪ c
f ⟫⤪ g = map (lookup f) g
\end{code}
%<*Plug-seq>


%<*nil-fin>
\AgdaTarget{nil⤪}
\begin{code}
nil⤪ : n ⤪ 0
nil⤪ = []
\end{code}
%</nil-fin>

%<*nil-fin-zero>
\AgdaTarget{nil⤪₀}
\begin{code}
nil⤪₀ : 0 ⤪ 0
nil⤪₀ = nil⤪
\end{code}
%</nil-fin-zero>


%<*id-fin>
\AgdaTarget{id⤪}
\begin{code}
id⤪ : n ⤪ n
id⤪ = tabulate id
\end{code}
%</id-fin>

%<*id-fin-one>
\AgdaTarget{id⤪₁}
\begin{code}
id⤪₁ : 1 ⤪ 1
id⤪₁ = id⤪
\end{code}
%</id-fin-one>


%<*eq-fin>
\AgdaTarget{eq⤪}
\begin{code}
eq⤪ : (p : n ≡ n′) → n ⤪ n′
eq⤪ refl = id⤪
\end{code}
%</eq-fin>


%<*swap-fin>
\AgdaTarget{swap⤪}
\begin{code}
swap⤪ : (n + m) ⤪ (m + n)
swap⤪ {n} with splitAt n (tabulate id)
swap⤪ {n} | vₙ , vₘ , _ = vₘ ++ vₙ
\end{code}
%</swap-fin>

%<*swap-fin-one>
\AgdaTarget{swap⤪₁}
\begin{code}
swap⤪₁ : (1 + 1) ⤪ (1 + 1)
swap⤪₁ = swap⤪ {1} 
\end{code}
%</swap-fin-one>


%<*intertwine-fin>
\AgdaTarget{intertwine⤪}
\begin{code}
intertwine⤪ : ((a + b) + (c + d)) ⤪ ((a + c) + (b + d))
intertwine⤪ {a} {b} {c} {d} =      eq⤪ (+-assoc a b (c + d) ⟨ trans ⟩ cong (a +_) (sym (+-assoc b c d)))
                                ⟫⤪ id⤪ {a}  |⤪  swap⤪ {b} {c}  |⤪  id⤪
                                ⟫⤪ eq⤪ (cong (a +_) (+-assoc c b d) ⟨ trans ⟩ sym (+-assoc a c (b + d)))
\end{code}
%</intertwine-fin>


%<*head-fin>
\AgdaTarget{head⤪}
\begin{code}
head⤪ : ∀ n → (suc n * w) ⤪ w
head⤪ {w} n = tabulate $ _↑ˡ (n * w)
\end{code}
%</head-fin>


%<*uncons-fin>
\AgdaTarget{uncons⤪}
\begin{code}
uncons⤪ : ∀ n → (suc n * i) ⤪ (i + n * i)
uncons⤪ _n = id⤪
\end{code}
%</uncons-fin>

%<*uncons-fin-one>
\AgdaTarget{uncons⤪₁}
\begin{code}
uncons⤪₁ : ∀ n → (suc n * 1) ⤪ (1 + n * 1)
uncons⤪₁ = uncons⤪
\end{code}
%</uncons-fin-one>


%<*cons-fin>
\AgdaTarget{cons⤪}
\begin{code}
cons⤪ : ∀ n → (i + n * i) ⤪ (suc n * i)
cons⤪ _n = id⤪
\end{code}
%</cons-fin>

%<*cons-fin-one>
\AgdaTarget{cons⤪₁}
\begin{code}
cons⤪₁ : ∀ n → (1 + n * 1) ⤪ (suc n * 1)
cons⤪₁ = cons⤪
\end{code}
%</cons-fin-one>


%<*singleton-fin>
\AgdaTarget{singleton⤪}
\begin{code}
singleton⤪ : w ⤪ (1 * w)
singleton⤪ {w} = eq⤪ (sym (+-identityʳ w))
\end{code}
%</singleton-fin>


\begin{code}
twiceSuc : ∀ n w → w + (n + suc n) * w ≡ w + n * w + (w + n * w)
twiceSuc = solve 2 eq refl where
  eq = λ n w → w :+ (n :+ (con 1 :+ n)) :* w := w :+ n :* w :+ (w :+ n :* w)
\end{code}

%<*vecHalf-fin>
\AgdaTarget{vecHalf⤪}
\begin{code}
vecHalf⤪ : ∀ n → ((2 * suc n) * w) ⤪ (suc n * w + suc n * w)
vecHalf⤪ {w} n rewrite +-identityʳ n | twiceSuc n w = id⤪
\end{code}
%</vecHalf-fin>


\begin{code}
vecHalfPowEq : ∀ n w → (2 ^ suc n * w) ≡ (2 ^ n * w  +  2 ^ n * w)
vecHalfPowEq zero w rewrite +-identityʳ w = refl
vecHalfPowEq (suc n) w = begin
  2 ^ suc (suc n) * w                 ≡⟨⟩
  2 * 2 ^ suc n * w                   ≡⟨ *-assoc 2 (2 ^ suc n) w ⟩
  2 * (2 ^ suc n * w)                 ≡⟨ cong (λ x → 2 * x) $ vecHalfPowEq n w ⟩
  2 * (2 ^ n * w  +  2 ^ n * w)       ≡⟨ *-comm 2 (2 ^ n * w + 2 ^ n * w) ⟩
  (2 ^ n * w + 2 ^ n * w) * 2         ≡⟨ *-distribʳ-+ 2 (2 ^ n * w) (2 ^ n * w) ⟩
  2 ^ n * w * 2   +  2 ^ n * w * 2    ≡⟨ (let p = *-comm (2 ^ n * w) 2  in  cong₂ _+_ p p) ⟩
  2 * (2 ^ n * w) +  2 * (2 ^ n * w)  ≡⟨ (let p = sym (*-assoc 2 (2 ^ n) w)  in  cong₂ _+_ p p) ⟩
  2 * 2 ^ n * w   +  2 * 2 ^ n * w    ≡⟨⟩
  2 ^ suc n * w   +  2 ^ suc n * w    ∎
\end{code}

%<*vecHalfPow-fin>
\AgdaTarget{vecHalfPow⤪}
\begin{code}
vecHalfPow⤪ : ∀ n → (2 ^ suc n * w) ⤪ (2 ^ n * w  +  2 ^ n * w)
vecHalfPow⤪ {w} n = eq⤪ (vecHalfPowEq n w)
\end{code}
%</vecHalfPow-fin>


%<*forkVec-fin>
\AgdaTarget{forkVec⤪}
\begin{code}
forkVec⤪ : ∀ n {k} → k ⤪ (n * k)
forkVec⤪ n = concat $ replicate n (tabulate id)
\end{code}
%</forkVec-fin>


%<*forkProd-fin>
\AgdaTarget{fork×⤪}
\begin{code}
fork×⤪ : w ⤪ (w + w)
fork×⤪ = tabulate id ++ tabulate id
\end{code}
%</forkProd-fin>


%<*fst-fin>
\AgdaTarget{fst⤪}
\begin{code}
fst⤪ : (m + n) ⤪ m
fst⤪ {n} = tabulate (_↑ˡ n)
\end{code}
%</fst-fin>


%<*snd-fin>
\AgdaTarget{snd⤪}
\begin{code}
snd⤪ : (m + n) ⤪ n
snd⤪ {m} = tabulate (m ↑ʳ_)
\end{code}
%</snd-fin>
