\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
open import PiWare.Atomic using (Atomic)
module PiWare.Patterns (A : Atomic) where
\end{code}

\begin{code}
open import Function.Base using (id; const; _∘′_; _∘_)
open import Data.Nat.Base using (ℕ; zero; suc; _+_; _*_)
open import Data.Product.Base using (proj₁; _,_)
open import Data.Vec.Base using (Vec; []; _∷_; _++_; replicate; foldr; sum; group)
open import Data.Nat.Properties using (+-identityʳ)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; sym; subst₂; cong)

open import Data.IVec using (Vec₂; []₂; _∷₂_; replicate₂)
open import PiWare.Circuit using (ℂ[_]; σ; _⟫_; _∥_)
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Plugs using (id⤨; nil⤨)
open import PiWare.Patterns.Id using (adaptEqI)
open import PiWare.Semantics.Simulation A using (W; W⟶W; ⟦_⟧[_])
open import PiWare.Semantics.Simulation.Compliance A using (_⫉[_]_)

open import Notation.JP.Base using (n; k)
open import PiWare.Gates using (G)
open import PiWare.Circuit.Notation using (i; o; m)
open import PiWare.Circuit using (s)
\end{code}


%<*pars>
\AgdaTarget{pars}
\begin{code}
pars : {is os : Vec ℕ n} (cs : Vec₂ (ℂ[ G ] {s}) is os) → ℂ[ G ] {s} (sum is) (sum os)
pars {is = []}     []₂         = nil⤨
pars {is = _ ∷ _}  (c ∷₂ cs′)  = c ∥ pars cs′
\end{code}
%</pars>


%<*sum-replicate>
\AgdaTarget{sum-replicate}
\begin{code}
*-sum-replicate : ∀ n k → sum (replicate n k) ≡ n * k
*-sum-replicate zero      _ = refl
*-sum-replicate (suc n′)  k = cong (k +_) (*-sum-replicate n′ k)
\end{code}
%</sum-replicate>

%<*parsN>
\AgdaTarget{parsN}
\begin{code}
parsN : ∀ n → ℂ[ G ] {s} i o → ℂ[ G ] {s} (n * i) (n * o)
parsN {G} {_} {i} {o} n c = subst₂  ℂ[ G ]
                                    (*-sum-replicate n i)
                                    (*-sum-replicate n o)
                                    (pars (replicate₂ n c))
\end{code}
%</parsN>


-- TODO: Here we force all ℂs to have the same size, a better version would be with type-aligned sequences
%<*seqs>
\AgdaTarget{seqs}
\begin{code}
seqs : Vec (ℂ[ G ] {s} m m) n → ℂ[ G ] {s} m m
seqs {G} {s} {m} = foldr (const (ℂ[ G ] {s} m m)) _⟫_ id⤨
\end{code}
%</seqs>


%<*seqsN>
\AgdaTarget{seqsN}
\begin{code}
seqsN : ∀ n → ℂ[ G ] {s} m m → ℂ[ G ] {s} m m
seqsN n = seqs ∘′ replicate n
\end{code}
%</seqsN>


-- TODO the pattern from which a typical ripple-carry adder is built.
%<*row>
\AgdaTarget{row}
\begin{code}
row : ∀ {a b c} → ℂ[ G ] {s} (a + b) (c + a) → ℂ[ G ] {s} (a + (k * b)) ((k * c) + a)
row {k = zero}    {a} _   = adaptEqI (sym (+-identityʳ a)) id⤨
row {k = suc k′}  {a} _f  = ⊥ where postulate ⊥ : _
\end{code}
%</row>


%<*foldr-Circ>
\begin{code}
linear-reductor : ∀ n → ℂ[ G ] {σ} (k + k) k → ℂ[ G ] {σ} zero k → ℂ[ G ] {σ} (n * k) k
linear-reductor zero      _ e  = e
linear-reductor (suc n′)  c e  = id⤨ ∥ (linear-reductor n′) c e ⟫ c
\end{code}
%</foldr-Circ>

\begin{code}
postulate
 linear-reductor-spec :  {g : TyGate G W⟶W} (c : ℂ[ G ] {σ} (k + k) k) (e : ℂ[ G ] {σ} zero k)
                         →  linear-reductor n c e  ⫉[ g ]
                            foldr (const (W k)) (λ x y → ⟦ c ⟧[ g ] (x ++ y)) (⟦ e ⟧[ g ] []) ∘ (proj₁ ∘ group n k)
\end{code}
