\begin{code}
module PiWare.Atomic where

open import Data.Fin.Base using (Fin)
open import Data.Vec.Base using (Vec)
open import Relation.Binary.Definitions using (DecidableEquality)
open import Relation.Binary.Bundles using (DecSetoid)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)
open import Relation.Binary.PropositionalEquality.Properties using (decSetoid)

open import Data.FiniteInhabited.Base using (FiniteInhabited)
\end{code}


%<*Atomic-base>
\begin{AgdaMultiCode}
\AgdaTarget{Atomic}
\begin{code}
record Atomic : Set₁ where
\end{code}
\begin{code}[hide]
  constructor MkAtomic
\end{code}
\AgdaTarget{Atom, enum, \_≟A\_}
\begin{code}
  field  Atom : Set
         enum : FiniteInhabited Atom
         _≟A_ : DecidableEquality Atom
\end{code}
\begin{code}[hide]
  open FiniteInhabited enum public
\end{code}
\begin{code}
  W = Vec Atom
\end{code}
\end{AgdaMultiCode}
%</Atomic-base>
%<*Atomic-extra>
\begin{code}
  |Atom|  = #α
  Atom#   = Fin #α

  decSetoidA : DecSetoid _ _
  decSetoidA = decSetoid _≟A_
\end{code}
%</Atomic-extra>


%<*Atomic-default-decEq>
\AgdaTarget{MkAtomic′}
\begin{code}
MkAtomic′ : ∀ Atom (enum : FiniteInhabited Atom) → Atomic
MkAtomic′ Atom enum =  let  open FiniteInhabited enum using (_≟ⁿ_)
                       in   MkAtomic Atom enum _≟ⁿ_
\end{code}
%</Atomic-default-decEq>


\begin{code}
open Atomic ⦃ … ⦄
\end{code}
