\begin{code}
module Function.Extra where

open import Level using (_⊔_)
open import Data.Product.Base using (_×_; _,_)

open import Notation.JP.Base using (ℓ; ℓ₁; ℓ₂)
\end{code}

\begin{code}
private variable I I₁ I₂ : Set ℓ
\end{code}


%<*fun>
\AgdaTarget{fun}
\begin{code}
fun : Set ℓ₁ → Set ℓ₂ → Set (ℓ₁ ⊔ ℓ₂)
fun α β = α → β
\end{code}
%</fun>


%<*fun1>
\AgdaTarget{fun₁}
\begin{code}
fun₁ : (C : I → Set ℓ₂) → I → I → Set ℓ₂
fun₁ C i j = C i → C j
\end{code}
%</fun1>


%<*fun2>
\AgdaTarget{fun₂}
\begin{code}
fun₂ : (C : I₁ → I₂ → Set ℓ₂) (i : I₁ × I₁) (j : I₂ × I₂) → Set ℓ₂
fun₂ C (i₁ , i₂) (j₁ , j₂) = C i₁ j₁ → C i₂ j₂
\end{code}
%</fun2>
