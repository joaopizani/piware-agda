\begin{code}
module Notation.JP.Base where

open import Level using (Level)
open import Data.Nat.Base using (ℕ)
\end{code}


%<*levels>
\AgdaTarget{ℓ,ℓ₁,ℓ₂,ℓ₃}
\begin{code}
variable
 ℓ ℓ₁ ℓ₂ ℓ₃ : Level
\end{code}
%</levels>


%<*sets>
\AgdaTarget{α,β}
\begin{code}
variable
 α β γ : Set ℓ
\end{code}
%</sets>


%<*sizes-naturals>
\AgdaTarget{m,n}
\begin{code}
variable
 m m′ n n′ k k′ : ℕ
\end{code}
%</sizes-naturals>
